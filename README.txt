DocBuild - 基于Doxygen的工程文档快速生成系统
2012

请事先安装Doxygen!

文件目录说明
============

doc		- 输出目录，生成的文档保存在该目录下
example		- 示例目录，存放工程的使用示例程序源码
media		- 媒体目录，存放生成文档需要的媒体文件（图像、视频、音频）
proj		- 工程目录，存放实际的工程目录

Doxyfile	- Doxygen工程文件
build.bat	- 生成文档
clean.bat	- 清空文档
rebuild.bat	- 重新生成文档

用法
=====

1.将工程复制到proj目录下
2.用Doxywizard打开Doxyfile文件，修改各项参数，如工程名、工程简介、工程版本、工程Logo等
3.修改proj目录下的mainpage.h文件，该文件中定义文档首页内容以及定义文档使用的“示例”索引
4.如果有示例程序，将示例程序源码放在example目录下
5.将文档所需媒体文件按照分类分别放在media目录下的image（图像）、audio（音频）、video（视频）子目录中
6.运行build.bat生成文档，生成的文档在doc目录下，其中chm文档保存为doc目录下的doc.chm

Doxyfile配置
============

【Expert->Project】
OUTPUT_LANGUAGE=Chinese
SEPARATE_MEMBER_PAGES=NO
TAB_SIZE=4
OPTIMIZE_OUTPUT_FOR_C=YES
【Expert->Build】
EXTRACT_STATIC=YES
CASE_SENSE_NAMES=YES
HIDE_SCOPE_NAMES=YES
SHOW_USED_FILES=NO
SORT_MEMBER_DOCS=NO
【Expert->Input】
EXAMPLE_PATH
IMAGE_PATH
INPUT_ENCODING=GB2312
RECURSIVE=YES
【Expert->Source Browser】
SOURCE_BROWSER=YES
VERBATIM_HEADERS=NO
【Expert->HTML】
GENERATE_HTMLHELP=YES
CHM_FILE
HHC_LOCATION
CHM_INDEX_ENCODING=GB2312

DocBuild语法
===========

为了兼容较老的C编译器，源码中的注释请全部使用/**/风格

【首页】
/**
 * @mainpage xxxxx
 */

【段落】
/** 
 * @section xxxxx
 */

【子页】
/**
 * @subpage xxxxx xxxxxxx
 */

/**
 * @page xxxx xxxxxxx
 */

【示例】
/**
 * @example xxxxx
 */

【文件】
/**
 * @file xxx
 * @brief xxxxxx
 */

【模块】
/**
 * @defgroup xxxxxx xxxx
 * @{
 */

/** @} */

【子模块】
/**
 * @defgroup xxxxx xxxxxx
 * @ingroup xxxxx
 * @{
 */

/** @} */

【结构体】
/** 
 * @struct xxxx
 * @brief xxxx
 */

【枚举】
/** 
 * @enum xxxx
 * @brief xxxx
 */

【共用体】
/** 
 * @union xxxx
 * @brief xxxx
 */

【变量】
/** 
 * @var xxxx
 * @brief xxxxx
 */

【重定义】
/** 
 * @typedef xxxx
 * @brief xxxxxx
 */

【宏定义】
/** 
 * @def xxxx
 * @brief xxxxx
 */

【名空间】
/** 
 * @namespace xxxx
 * @brief xxxxx
 */

【函数】
[无返回值]
/**
 * @fn xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
 * @brief xxxxxx
 * @param[in,out] xxxxxx
 */
[有返回值]
/**
 * @fn xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
 * @brief xxxxxxxx
 * @param[in,out] xxxxxxx
 * @return xxxx
 * @retval xxxxx xxxxx
 */
[完整]
/**
 * @fn xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
 * @brief xxxxxxxx
 * @param[in,out] xxxxx
 * @return xxxx
 * @retval xxxx xxxx
 * @note xxxxxxxxxxxxxxxxxxx
 * @par 示例：
 * @code
xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
 * @endcode
 */

【注释】
/**  */




