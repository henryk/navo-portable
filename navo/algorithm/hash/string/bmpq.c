/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/bmpq.c
 *
 * @brief Implements for BMPQ hash.
 *
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 *
 */

#include "bmpq.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

NV_IMPL void bmpq_build(uint32_t* tbl)
{
	uint32_t i, j, k, seed = 0x00100001UL;

	for (i = 0; i < 0x100; ++i) {
		for (j = i, k = 0; k < 5; ++k, j += 0x100) {
			uint32_t t1, t2;

			seed = (seed * 125 + 3) % 0x002AAAABUL;
			t1 = (seed & 0xFFFFUL) << 16;
			seed = (seed * 125 + 3) % 0x002AAAABUL;
			t2 = (seed & 0xFFFFUL) << 16;

			tbl[j] = t1 | t2;
		}
	}
}

NV_IMPL void __bmpq_update(bmpq_ctx* ctx, uint32_t* tbl,
                            uint8_t* buf, size_t cnt)
{
	uint32_t seed1 = ctx->seed1;
	uint32_t seed2 = ctx->seed2;
	uint32_t type = ctx->type << 8;
	uint32_t ch;

	while (cnt--) {
		ch = (uint32_t)(*buf++);
		seed1 = tbl[type + ch] ^ (seed1 + seed2);
		seed2 += (seed2 << 5) + seed1 + ch + 3;
	}

	ctx->seed1 = seed1;
	ctx->seed2 = seed2;
}

NV_IMPL uint32_t bmpq_hash_nbstr(const char* str,
								uint32_t* tbl, uint32_t type)
{
	uint32_t seed1 = BMPQ_HASH_SEED1;
	uint32_t seed2 = BMPQ_HASH_SEED2;
	uint32_t ch;

	type <<= 8;
	while ((ch = (uint32_t)(*str++))) {
		seed1 = tbl[type + ch] ^ (seed1 + seed2);
		seed2 += (seed2 << 5) + seed1 + ch + 3;
	}

	return __cpu_to_le32(seed1);
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */
