/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/sdbm.c
 * 
 * @brief Implements for SDBM hash.
 * 
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 * 
 */

#include "sdbm.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

NV_IMPL void __sdbm_update(sdbm_ctx* ctx, uint8_t* buf, size_t cnt)
{
	uint32_t hash = ctx->hash;

	while (cnt--) {
		hash = (hash << 6) + (hash << 16) + (*buf++) - hash;
	}

	ctx->hash = hash;
}

NV_IMPL uint32_t sdbm_hash_nbstr(const char* str)
{
	uint32_t hash = 0;
	uint8_t ch;

	while ((ch = *str++)) {
		hash = (hash << 6) + (hash << 16) + ch - hash;
	}

	return __cpu_to_le32(hash);
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

