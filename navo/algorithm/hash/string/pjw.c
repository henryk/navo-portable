/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/pjw.c
 * 
 * @brief Implements for PJW hash.
 * 
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 * 
 */

#include "pjw.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

NV_IMPL void __pjw_update(pjw_ctx* ctx, uint8_t* buf, size_t cnt)
{
	uint32_t seed;
	uint32_t hash = ctx->hash;

	while (cnt--) {
		hash = (hash << 4) + *buf++;
		if ((seed = (hash & 0xF0000000UL))) {
			hash = ((hash ^ (seed >> 24)) & 0x0FFFFFFFUL);
		}
	}

	ctx->hash = hash;
}

NV_IMPL uint32_t pjw_hash_nbstr(const char* str)
{
	uint32_t seed;
	uint32_t hash = 0;
	uint8_t ch;

	while ((ch = *str++)) {
		hash = (hash << 4) + ch;
		if ((seed = (hash & 0xF0000000UL))) {
			hash = ((hash ^ (seed >> 24)) & 0x0FFFFFFFUL);
		}
	}

	return __cpu_to_le32(hash);
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */
