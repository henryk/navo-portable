/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/rs.h
 * 
 * @brief RS string hash
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_RS__
#define __NV_RS__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup RS
 * @ingroup 字符串哈希
 * @{
 */

/**
 * @struct rs_ctx
 * @brief RS哈希上下文
 */
typedef struct {
	/** @brief 哈希值 */
	uint32_t hash;
	/** @brief 种子1 */
	uint32_t seed1;
	/** @brief 种子2 */
	uint32_t seed2;
}rs_ctx;

#define RS_HASH_SEED1 0x0005C6B7UL
#define RS_HASH_SEED2 0x0000FBC9UL

/**
 * @def RS_CTX(name)
 * @brief 定义名为name的RS上下文
 */
#define RS_CTX(name) rs_ctx name = {0, RS_HASH_SEED1, RS_HASH_SEED2}

/**
 * @fn static inline void rs_init(rs_ctx* ctx);
 * @brief 初始化RS上下文
 * @param[out] ctx 上下文
 */
static inline void rs_init(rs_ctx* ctx)
{
	ctx->hash = 0;
	ctx->seed1 = RS_HASH_SEED1;
	ctx->seed2 = RS_HASH_SEED2;
}

NV_API void __rs_update(rs_ctx* ctx, uint8_t* buf, size_t cnt);

/**
 * @def rs_update(ctx, buf, cnt)
 * @brief 计算哈希值
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小
 * @par 示例:
 * @code
 	rs_ctx ctx;

 	// 初始化上下文
 	rs_init(&ctx);

 	// 计算校验码
 	rs_update(&ctx, str, strlen(str));
 	
 	// 返回结果
 	return rs_final(&ctx);
 * @endcode
 * @see rs_init, rs_final
 */
#define rs_update(ctx, buf, cnt) __rs_update(ctx, (uint8_t*)(buf), cnt)

static inline uint32_t __rs_final(rs_ctx* ctx, uint32_t hash)
{
	ctx->hash = 0;
	return __cpu_to_le32(hash);
}

/**
 * @fn static inline uint32_t rs_final(rs_ctx* ctx);
 * @brief 获取RS校验值
 * @param[in,out] ctx 上下文
 * @return 返回小端字节序32位校验码
 * @see rs_init, rs_update
 */
static inline uint32_t rs_final(rs_ctx* ctx)
{
	return __rs_final(ctx, ctx->hash);
}

/**
 * @fn uint32_t rs_hash_nbstr(const char* str);
 * @brief 直接哈希字符串
 * @param[in] str 多字节字符串
 * @return uint32_t 返回小端字节序32位哈希值
 */
NV_API uint32_t rs_hash_nbstr(const char* str);

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_RS__ */

