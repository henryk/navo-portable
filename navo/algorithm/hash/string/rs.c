/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/rs.c
 * 
 * @brief Implements for RS hash.
 * 
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 * 
 */

#include "rs.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

NV_IMPL void __rs_update(rs_ctx* ctx, uint8_t* buf, size_t cnt)
{
	uint32_t hash = ctx->hash;
	uint32_t seed1 = ctx->seed1;
	uint32_t seed2 = ctx->seed2;

	while (cnt--) {
		hash = (hash * seed1 + *buf++);
		seed1 *= seed2;
	}

	ctx->hash = hash;
	ctx->seed1 = seed1;
	ctx->seed2 = seed2;
}

NV_IMPL uint32_t rs_hash_nbstr(const char* str)
{
	uint32_t hash = 0;
	uint32_t seed1 = RS_HASH_SEED1;
	uint32_t seed2 = RS_HASH_SEED2;
	uint8_t ch;

	while ((ch = *str++)) {
		hash = (hash * seed1 + ch);
		seed1 *= seed2;
	}

	return __cpu_to_le32(hash);
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */
