/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/bkdr.h
 *
 * @brief BKDR string hash
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_BKDR__
#define __NV_BKDR__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup BKDR
 * @ingroup 字符串哈希
 * @{
 */

/**
 * @struct bkdr_ctx
 * @brief BKDR哈希上下文
 */
typedef struct {
	/** @brief 哈希值 */
	uint32_t hash;
	/** @brief 种子 */
	uint32_t seed;
}bkdr_ctx;

/**
 * @def BKDR_CTX(name)
 * @brief 定义名为name的BKDR上下文
 */
#define BKDR_CTX(name) bkdr_ctx name = {0, 131}

/**
 * @fn static inline void bkdr_init(bkdr_ctx* ctx, uint32_t seed);
 * @brief 初始化BKDR上下文
 * @param[out] ctx 上下文
 * @param[in] seed 设置哈希种子，如31、131、1313、13131、131313等
 					若设为0，表示保持原来的种子
 */
static inline void bkdr_init(bkdr_ctx* ctx, uint32_t seed)
{
	ctx->hash = 0;
	if (seed)
		ctx->seed = seed;
}

NV_API void __bkdr_update(bkdr_ctx* ctx, uint8_t* buf, size_t cnt);

/**
 * @def bkdr_update(ctx, buf, cnt)
 * @brief 计算哈希值
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小
 * @par 示例:
 * @code
 	bkdr_ctx ctx;

 	// 初始化上下文
 	bkdr_init(&ctx, 31);

 	// 计算校验码
 	bkdr_update(&ctx, str, strlen(str));

 	// 返回结果
 	return bkdr_final(&ctx);
 * @endcode
 * @see bkdr_init, bkdr_final
 */
#define bkdr_update(ctx, buf, cnt) __bkdr_update(ctx, (uint8_t*)(buf), cnt)

static inline uint32_t __bkdr_final(bkdr_ctx* ctx, uint32_t hash)
{
	ctx->hash = 0;
	return __cpu_to_le32(hash);
}

/**
 * @fn static inline uint32_t bkdr_final(bkdr_ctx* ctx);
 * @brief 获取BKDR校验值
 * @param[in,out] ctx 上下文
 * @return 返回小端字节序32位校验码
 * @see bkdr_init, bkdr_update
 */
static inline uint32_t bkdr_final(bkdr_ctx* ctx)
{
	return __bkdr_final(ctx, ctx->hash);
}

/**
 * @fn uint32_t bkdr_hash_nbstr(const char* str, const uint32_t seed);
 * @brief 直接哈希字符串
 * @param[in] str 多字节字符串
 * @param[in] seed 设置哈希种子，如31、131、1313、13131、131313等
 * @return uint32_t 返回小端字节序32位哈希值
 */
NV_API uint32_t bkdr_hash_nbstr(const char* str, const uint32_t seed);

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_BKDR__ */

