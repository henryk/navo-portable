/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/fnv.h
 * 
 * @brief FNV string hash
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_FNV__
#define __NV_FNV__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup FNV
 * @ingroup 字符串哈希
 * @{
 */

/**
 * @struct fnv_ctx
 * @brief FNV哈希上下文
 */
typedef struct {
	/** @brief 哈希值 */
	uint32_t hash;
}fnv_ctx;

/**
 * @def FNV_CTX(name)
 * @brief 定义名为name的FNV上下文
 */
#define FNV_CTX(name) fnv_ctx name = {0}

/**
 * @fn static inline void fnv_init(fnv_ctx* ctx);
 * @brief 初始化FNV上下文
 * @param[out] ctx 上下文
 */
static inline void fnv_init(fnv_ctx* ctx)
{
	ctx->hash = 0;
}

NV_API void __fnv_update(fnv_ctx* ctx, uint8_t* buf, size_t cnt);

/**
 * @def fnv_update(ctx, buf, cnt)
 * @brief 计算哈希值
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小
 * @par 示例:
 * @code
 	fnv_ctx ctx;

 	// 初始化上下文
 	fnv_init(&ctx);

 	// 计算校验码
 	fnv_update(&ctx, str, strlen(str));
 	
 	// 返回结果
 	return fnv_final(&ctx);
 * @endcode
 * @see fnv_init, fnv_final
 */
#define fnv_update(ctx, buf, cnt) __fnv_update(ctx, (uint8_t*)(buf), cnt)

static inline uint32_t __fnv_final(fnv_ctx* ctx, uint32_t hash)
{
	ctx->hash = 0;
	return __cpu_to_le32(hash);
}

/**
 * @fn static inline uint32_t fnv_final(fnv_ctx* ctx);
 * @brief 获取FNV校验值
 * @param[in,out] ctx 上下文
 * @return 返回小端字节序32位校验码
 * @see fnv_init, fnv_update
 */
static inline uint32_t fnv_final(fnv_ctx* ctx)
{
	return __fnv_final(ctx, ctx->hash);
}

/**
 * @fn uint32_t fnv_hash_nbstr(const char* str);
 * @brief 直接哈希字符串
 * @param[in] str 多字节字符串
 * @return uint32_t 返回小端字节序32位哈希值
 */
NV_API uint32_t fnv_hash_nbstr(const char* str);

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_FNV__ */

