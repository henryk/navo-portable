/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/djb.h
 * 
 * @brief DJB string hash
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_DJB__
#define __NV_DJB__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup DJB
 * @ingroup 字符串哈希
 * @{
 */

/**
 * @struct djb_ctx
 * @brief DJB哈希上下文
 */
typedef struct {
	/** @brief 哈希值 */
	uint32_t hash;
}djb_ctx;

/**
 * @def DJB_CTX(name)
 * @brief 定义名为name的DJB上下文
 */
#define DJB_CTX(name) djb_ctx name = {0x1505}

/**
 * @fn static inline void djb_init(djb_ctx* ctx);
 * @brief 初始化DJB上下文
 * @param[out] ctx 上下文
 */
static inline void djb_init(djb_ctx* ctx)
{
	ctx->hash = 0x1505;
}

NV_API void __djb_update(djb_ctx* ctx, uint8_t* buf, size_t cnt);

/**
 * @def djb_update(ctx, buf, cnt)
 * @brief 计算哈希值
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小
 * @par 示例:
 * @code
 	djb_ctx ctx;

 	// 初始化上下文
 	djb_init(&ctx);

 	// 计算校验码
 	djb_update(&ctx, str, strlen(str));
 	
 	// 返回结果
 	return djb_final(&ctx);
 * @endcode
 * @see djb_init, djb_final
 */
#define djb_update(ctx, buf, cnt) __djb_update(ctx, (uint8_t*)(buf), cnt)

static inline uint32_t __djb_final(djb_ctx* ctx, uint32_t hash)
{
	ctx->hash = 0x1505;
	return __cpu_to_le32(hash);
}

/**
 * @fn static inline uint32_t djb_final(djb_ctx* ctx);
 * @brief 获取DJB校验值
 * @param[in,out] ctx 上下文
 * @return 返回小端字节序32位校验码
 * @see djb_init, djb_update
 */
static inline uint32_t djb_final(djb_ctx* ctx)
{
	return __djb_final(ctx, ctx->hash);
}

/**
 * @fn uint32_t djb_hash_nbstr(const char* str);
 * @brief 直接哈希字符串
 * @param[in] str 多字节字符串
 * @return uint32_t 返回小端字节序32位哈希值
 */
NV_API uint32_t djb_hash_nbstr(const char* str);

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_DJB__ */

