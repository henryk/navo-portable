/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/js.h
 * 
 * @brief JS string hash
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_JS__
#define __NV_JS__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup JS
 * @ingroup 字符串哈希
 * @{
 */

/**
 * @struct js_ctx
 * @brief JS哈希上下文
 */
typedef struct {
	/** @brief 哈希值 */
	uint32_t hash;
}js_ctx;

/**
 * @def JS_CTX(name)
 * @brief 定义名为name的JS上下文
 */
#define JS_CTX(name) js_ctx name = {0x4E67C6A7UL}

/**
 * @fn static inline void js_init(js_ctx* ctx);
 * @brief 初始化JS上下文
 * @param[out] ctx 上下文
 */
static inline void js_init(js_ctx* ctx)
{
	ctx->hash = 0x4E67C6A7UL;
}

NV_API void __js_update(js_ctx* ctx, uint8_t* buf, size_t cnt);

/**
 * @def js_update(ctx, buf, cnt)
 * @brief 计算哈希值
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小
 * @par 示例:
 * @code
 	js_ctx ctx;

 	// 初始化上下文
 	js_init(&ctx);

 	// 计算校验码
 	js_update(&ctx, str, strlen(str));
 	
 	// 返回结果
 	return js_final(&ctx);
 * @endcode
 * @see js_init, js_final
 */
#define js_update(ctx, buf, cnt) __js_update(ctx, (uint8_t*)(buf), cnt)

static inline uint32_t __js_final(js_ctx* ctx, uint32_t hash)
{
	ctx->hash = 0x4E67C6A7UL;
	return __cpu_to_le32(hash);
}

/**
 * @fn static inline uint32_t js_final(js_ctx* ctx);
 * @brief 获取JS校验值
 * @param[in,out] ctx 上下文
 * @return 返回小端字节序32位校验码
 * @see js_init, js_update
 */
static inline uint32_t js_final(js_ctx* ctx)
{
	return __js_final(ctx, ctx->hash);
}

/**
 * @fn uint32_t js_hash_nbstr(const char* str);
 * @brief 直接哈希字符串
 * @param[in] str 多字节字符串
 * @return uint32_t 返回小端字节序32位哈希值
 */
NV_API uint32_t js_hash_nbstr(const char* str);

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_JS__ */

