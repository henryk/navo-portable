/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/sdbm.h
 * 
 * @brief SDBM string hash
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_SDBM__
#define __NV_SDBM__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup SDBM
 * @ingroup 字符串哈希
 * @{
 */

/**
 * @struct sdbm_ctx
 * @brief SDBM哈希上下文
 */
typedef struct {
	/** @brief 哈希值 */
	uint32_t hash;
}sdbm_ctx;

/**
 * @def SDBM_CTX(name)
 * @brief 定义名为name的SDBM上下文
 */
#define SDBM_CTX(name) sdbm_ctx name = {0}

/**
 * @fn static inline void sdbm_init(sdbm_ctx* ctx);
 * @brief 初始化SDBM上下文
 * @param[out] ctx 上下文
 */
static inline void sdbm_init(sdbm_ctx* ctx)
{
	ctx->hash = 0;
}

NV_API void __sdbm_update(sdbm_ctx* ctx, uint8_t* buf, size_t cnt);

/**
 * @def sdbm_update(ctx, buf, cnt)
 * @brief 计算哈希值
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小
 * @par 示例:
 * @code
 	sdbm_ctx ctx;

 	// 初始化上下文
 	sdbm_init(&ctx);

 	// 计算校验码
 	sdbm_update(&ctx, str, strlen(str));
 	
 	// 返回结果
 	return sdbm_final(&ctx);
 * @endcode
 * @see sdbm_init, sdbm_final
 */
#define sdbm_update(ctx, buf, cnt) __sdbm_update(ctx, (uint8_t*)(buf), cnt)

static inline uint32_t __sdbm_final(sdbm_ctx* ctx, uint32_t hash)
{
	ctx->hash = 0;
	return __cpu_to_le32(hash);
}

/**
 * @fn static inline uint32_t sdbm_final(sdbm_ctx* ctx);
 * @brief 获取SDBM校验值
 * @param[in,out] ctx 上下文
 * @return 返回小端字节序32位校验码
 * @see sdbm_init, sdbm_update
 */
static inline uint32_t sdbm_final(sdbm_ctx* ctx)
{
	return __sdbm_final(ctx, ctx->hash);
}

/**
 * @fn uint32_t sdbm_hash_nbstr(const char* str);
 * @brief 直接哈希字符串
 * @param[in] str 多字节字符串
 * @return uint32_t 返回小端字节序32位哈希值
 */
NV_API uint32_t sdbm_hash_nbstr(const char* str);

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_SDBM__ */

