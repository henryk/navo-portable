/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/ap.h
 *
 * @brief AP string hash
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_AP__
#define __NV_AP__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup AP
 * @ingroup 字符串哈希
 * @{
 */

/**
 * @struct ap_ctx
 * @brief AP哈希上下文
 */
typedef struct {
	/** @brief 哈希值 */
	uint32_t hash;
}ap_ctx;

/**
 * @def AP_CTX(name)
 * @brief 定义名为name的AP上下文
 */
#define AP_CTX(name) ap_ctx name = {0xAAAAAAAAUL}

/**
 * @fn static inline void ap_init(ap_ctx* ctx);
 * @brief 初始化AP上下文
 * @param[out] ctx 上下文
 */
static inline void ap_init(ap_ctx* ctx)
{
	ctx->hash = 0xAAAAAAAAUL;
}

NV_API void __ap_update(ap_ctx* ctx, uint8_t* buf, size_t cnt);

/**
 * @def ap_update(ctx, buf, cnt)
 * @brief 计算哈希值
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小
 * @par 示例:
 * @code
 	ap_ctx ctx;

 	// 初始化上下文
 	ap_init(&ctx);

 	// 计算校验码
 	ap_update(&ctx, str, strlen(str));

 	// 返回结果
 	return ap_final(&ctx);
 * @endcode
 * @see ap_init, ap_final
 */
#define ap_update(ctx, buf, cnt) __ap_update(ctx, (uint8_t*)(buf), cnt)

static inline uint32_t __ap_final(ap_ctx* ctx, uint32_t hash)
{
	ctx->hash = 0xAAAAAAAAUL;
	return __cpu_to_le32(hash);
}

/**
 * @fn static inline uint32_t ap_final(ap_ctx* ctx);
 * @brief 获取AP校验值
 * @param[in,out] ctx 上下文
 * @return 返回小端字节序32位校验码
 * @see ap_init, ap_update
 */
static inline uint32_t ap_final(ap_ctx* ctx)
{
	return __ap_final(ctx, ctx->hash);
}

/**
 * @fn uint32_t ap_hash_nbstr(const char* str);
 * @brief 直接哈希字符串
 * @param[in] str 多字节字符串
 * @return uint32_t 返回小端字节序32位哈希值
 */
NV_API uint32_t ap_hash_nbstr(const char* str);

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_AP__ */

