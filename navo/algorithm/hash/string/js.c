/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/js.c
 * 
 * @brief Implements for JS hash.
 * 
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 * 
 */

#include "js.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

NV_IMPL void __js_update(js_ctx* ctx, uint8_t* buf, size_t cnt)
{
	uint32_t hash = ctx->hash;

	while (cnt--) {
		hash ^= ((hash << 5) + (hash >> 2) + *buf++);
	}

	ctx->hash = hash;
}

NV_IMPL uint32_t js_hash_nbstr(const char* str)
{
	uint32_t hash = 0x4E67C6A7UL;
	uint8_t ch;

	while ((ch = *str++)) {
		hash ^= ((hash << 5) + (hash >> 2) + ch);
	}

	return __cpu_to_le32(hash);
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */
