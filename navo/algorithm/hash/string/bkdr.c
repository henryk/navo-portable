/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/bkdr.c
 * 
 * @brief Implements for BKDR hash.
 * 
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 * 
 */

#include "bkdr.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

NV_IMPL void __bkdr_update(bkdr_ctx* ctx, uint8_t* buf, size_t cnt)
{
	uint32_t hash = ctx->hash;
	uint32_t seed = ctx->seed;

	while (cnt--) {
		hash = (hash * seed) + *buf++;
	}

	ctx->hash = hash;
}

NV_IMPL uint32_t bkdr_hash_nbstr(const char* str, const uint32_t seed)
{
	uint32_t hash = 0;
	uint8_t ch;

	while ((ch = *str++)) {
		hash = (hash * seed) + ch;
	}

	return __cpu_to_le32(hash);
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */
