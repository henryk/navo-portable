/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/bp.c
 * 
 * @brief Implements for BP hash.
 * 
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 * 
 */

#include "bp.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

NV_IMPL void __bp_update(bp_ctx* ctx, uint8_t* buf, size_t cnt)
{
	uint32_t hash = ctx->hash;

	while (cnt--) {
		hash = (hash << 7) ^ *buf++;
	}

	ctx->hash = hash;
}

NV_IMPL uint32_t bp_hash_nbstr(const char* str)
{
	uint32_t hash = 0;
	uint8_t ch;

	while ((ch = *str++)) {
		hash = (hash << 7) ^ ch;
	}

	return __cpu_to_le32(hash);
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */
