/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/bmpq.h
 * 
 * @brief BMPQ string hash
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_BMPQ__
#define __NV_BMPQ__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup BMPQ
 * @ingroup 字符串哈希
 * @{
 */

/**
 * @struct bmpq_ctx
 * @brief BMPQ哈希上下文
 */
typedef struct {
	/** @brief 类型 */
	uint32_t type;
	/** @brief 种子1 */
	uint32_t seed1;
	/** @brief 种子2 */
	uint32_t seed2;
}bmpq_ctx;

#define BMPQ_HASH_SEED1 0x7FED7FEDUL
#define BMPQ_HASH_SEED2 0xEEEEEEEEUL

/**
 * @def BMPQ_SIZE
 * @brief BMPQ哈希构造表长度 uint32_t
 */
#define BMPQ_SIZE 0x500

/**
 * @def BMPQ_CTX(name)
 * @brief 定义名为name的BMPQ上下文
 */
#define BMPQ_CTX(name) bmpq_ctx name = {0, BMPQ_HASH_SEED1, BMPQ_HASH_SEED2}

/**
 * @fn void bmpq_build(uint32_t* tbl);
 * @brief 建立BMPQ构造表
 * @param[out] tbl 码表缓存
 * @par 示例:
 * @code
 	bmpq_ctx ctx;
 	
 	// 分配构造表缓存
 	uint32_t* tbl = (uint32_t*)malloc(sizeof(uint32_t) * BMPQ_SIZE);
 	if (!tbl)
 		return 0;

	// 建立构造表
 	bmpq_build(tbl);

 	// 初始化上下文
 	bmpq_init(&ctx);

 	while (buf_size >= BLOCK_SIZE) {
 		// 计算校验码
 		bmpq_update(&ctx, tbl, buf, BLOCK_SIZE);
 		buf += BLOCK_SIZE;
 		buf_size -= BLOCK_SIZE;
 	}
 	if (buf_size) {
 		// 计算校验码
 		bmpq_update(&ctx, tbl, buf, buf_size);
 	}
 	
 	// 返回结果
 	return bmpq_final(&ctx);
 * @endcode
 * @see bmpq_init
 */
NV_API void bmpq_build(uint32_t* tbl);

/**
 * @fn static inline void bmpq_init(bmpq_ctx* ctx, uint32_t type);
 * @brief 初始化BMPQ上下文
 * @param[out] ctx 上下文
 * @Param[in] type BMPQ类型，通常取0、1、2、3等
 */
static inline void bmpq_init(bmpq_ctx* ctx, uint32_t type)
{
	ctx->type = type;
	ctx->seed1 = BMPQ_HASH_SEED1;
	ctx->seed2 = BMPQ_HASH_SEED2;
}

NV_API void __bmpq_update(bmpq_ctx* ctx, uint32_t* tbl, 
								uint8_t* buf, size_t cnt);

/**
 * @def bmpq_update(ctx, buf, cnt)
 * @brief 计算哈希值
 * @param[in,out] ctx 上下文
 * @param[in] tbl BMPQ构造表
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小
 * @par 示例:
 * @code
 	bmpq_ctx ctx;

 	// 分配构造表缓存
 	uint32_t* tbl = (uint32_t*)malloc(sizeof(uint32_t) * BMPQ_SIZE);
 	if (!tbl)
 		return 0;

	// 建立构造表
 	bmpq_build(tbl);

 	// 初始化上下文
 	bmpq_init(&ctx);

 	// 计算校验码
 	bmpq_update(&ctx, tbl, str, strlen(str));
 	
 	// 返回结果
 	return bmpq_final(&ctx);
 * @endcode
 * @see bmpq_init, bmpq_final
 */
#define bmpq_update(ctx, tbl, buf, cnt) \
		__bmpq_update(ctx, tbl, (uint8_t*)(buf), cnt)

static inline uint32_t __bmpq_final(bmpq_ctx* ctx, uint32_t hash)
{
	ctx->seed1 = BMPQ_HASH_SEED1;
	ctx->seed2 = BMPQ_HASH_SEED2;
	return __cpu_to_le32(hash);
}

/**
 * @fn static inline uint32_t bmpq_final(bmpq_ctx* ctx);
 * @brief 获取BMPQ校验值
 * @param[in,out] ctx 上下文
 * @return 返回小端字节序32位校验码
 * @see bmpq_init, bmpq_update
 */
static inline uint32_t bmpq_final(bmpq_ctx* ctx)
{
	return __bmpq_final(ctx, ctx->seed1);
}

/**
 * @fn uint32_t bmpq_hash_nbstr(const char* str, 
						uint32_t* tbl, uint32_t type);
 * @brief 直接哈希字符串
 * @param[in] str 多字节字符串
 * @param[in] tbl BMPQ构造表
 * @param[in] type BMPQ类型
 * @return uint32_t 返回小端字节序32位哈希值
 */
NV_API uint32_t bmpq_hash_nbstr(const char* str, 
								uint32_t* tbl, uint32_t type);

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_BMPQ__ */

