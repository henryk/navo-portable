/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/elf.c
 * 
 * @brief Implements for ELF hash.
 * 
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 * 
 */

#include "elf.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

NV_IMPL void __elf_update(elf_ctx* ctx, uint8_t* buf, size_t cnt)
{
	uint32_t seed;
	uint32_t hash = ctx->hash;

	while (cnt--) {
		hash = (hash << 4) + *buf++;
		if ((seed = (hash & 0xF0000000UL))) {
			hash ^= (seed >> 24);
		}
		hash &= ~seed;
	}

	ctx->hash = hash;
}

NV_IMPL uint32_t elf_hash_nbstr(const char* str)
{
	uint32_t seed;
	uint32_t hash = 0;
	uint8_t ch;

	while ((ch = *str++)) {
		hash = (hash << 4) + ch;
		if ((seed = (hash & 0xF0000000UL))) {
			hash ^= (seed >> 24);
		}
		hash &= ~seed;
	}

	return __cpu_to_le32(hash);
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */
