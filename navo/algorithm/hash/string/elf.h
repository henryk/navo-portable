/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/elf.h
 * 
 * @brief ELF string hash
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_ELF__
#define __NV_ELF__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup ELF
 * @ingroup 字符串哈希
 * @{
 */

/**
 * @struct elf_ctx
 * @brief ELF哈希上下文
 */
typedef struct {
	/** @brief 哈希值 */
	uint32_t hash;
}elf_ctx;

/**
 * @def ELF_CTX(name)
 * @brief 定义名为name的ELF上下文
 */
#define ELF_CTX(name) elf_ctx name = {0}

/**
 * @fn static inline void elf_init(elf_ctx* ctx);
 * @brief 初始化ELF上下文
 * @param[out] ctx 上下文
 */
static inline void elf_init(elf_ctx* ctx)
{
	ctx->hash = 0;
}

NV_API void __elf_update(elf_ctx* ctx, uint8_t* buf, size_t cnt);

/**
 * @def elf_update(ctx, buf, cnt)
 * @brief 计算哈希值
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小
 * @par 示例:
 * @code
 	elf_ctx ctx;

 	// 初始化上下文
 	elf_init(&ctx);

 	// 计算校验码
 	elf_update(&ctx, str, strlen(str));
 	
 	// 返回结果
 	return elf_final(&ctx);
 * @endcode
 * @see elf_init, elf_final
 */
#define elf_update(ctx, buf, cnt) __elf_update(ctx, (uint8_t*)(buf), cnt)

static inline uint32_t __elf_final(elf_ctx* ctx, uint32_t hash)
{
	ctx->hash = 0;
	return __cpu_to_le32(hash);
}

/**
 * @fn static inline uint32_t elf_final(elf_ctx* ctx);
 * @brief 获取ELF校验值
 * @param[in,out] ctx 上下文
 * @return 返回小端字节序32位校验码
 * @see elf_init, elf_update
 */
static inline uint32_t elf_final(elf_ctx* ctx)
{
	return __elf_final(ctx, ctx->hash);
}

/**
 * @fn uint32_t elf_hash_nbstr(const char* str);
 * @brief 直接哈希字符串
 * @param[in] str 多字节字符串
 * @return uint32_t 返回小端字节序32位哈希值
 */
NV_API uint32_t elf_hash_nbstr(const char* str);

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_ELF__ */

