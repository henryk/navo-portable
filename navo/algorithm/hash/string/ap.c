/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/ap.c
 *
 * @brief Implements for AP hash.
 *
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 *
 */

#include "ap.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

NV_IMPL void __ap_update(ap_ctx* ctx, uint8_t* buf, size_t cnt)
{
	uint32_t hash = ctx->hash;
	size_t i;

	for (i = 0; i < cnt;) {
		if (i++ & 1)
			hash ^= (~((hash << 11) ^ (hash >> 5) ^ *buf++));
		else
			hash ^= ((hash << 7) ^ (hash >> 3) ^ *buf++);
	}

	ctx->hash = hash;
}

NV_IMPL uint32_t ap_hash_nbstr(const char* str)
{
	uint32_t hash = 0xAAAAAAAAUL;
	uint8_t ch;
	size_t i;

	for (i = 0; (ch = *str++);) {
		if (i++ & 1)
			hash ^= (~((hash << 11) ^ (hash >> 5) ^ ch));
		else
			hash ^= ((hash << 7) ^ (hash >> 3) ^ ch);
	}

	return __cpu_to_le32(hash);
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */
