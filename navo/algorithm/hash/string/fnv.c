/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/fnv.c
 *
 * @brief Implements for FNV hash.
 *
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 *
 */

#include "fnv.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

NV_IMPL void __fnv_update(fnv_ctx* ctx, uint8_t* buf, size_t cnt)
{
	uint32_t hash = ctx->hash;

	while (cnt--) {
		hash = (hash * 0x811C9DC5UL) ^ *buf++;
	}

	ctx->hash = hash;
}

NV_IMPL uint32_t fnv_hash_nbstr(const char* str)
{
	uint32_t hash = 0;
	uint8_t ch;

	while ((ch = *str++)) {
		hash = (hash * 0x811C9DC5UL) ^ ch;
	}

	return __cpu_to_le32(hash);
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */
