/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/pjw.h
 * 
 * @brief PJW string hash
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_PJW__
#define __NV_PJW__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup PJW
 * @ingroup 字符串哈希
 * @{
 */

/**
 * @struct pjw_ctx
 * @brief PJW哈希上下文
 */
typedef struct {
	/** @brief 哈希值 */
	uint32_t hash;
}pjw_ctx;

/**
 * @def PJW_CTX(name)
 * @brief 定义名为name的PJW上下文
 */
#define PJW_CTX(name) pjw_ctx name = {0}

/**
 * @fn static inline void pjw_init(pjw_ctx* ctx);
 * @brief 初始化PJW上下文
 * @param[out] ctx 上下文
 */
static inline void pjw_init(pjw_ctx* ctx)
{
	ctx->hash = 0;
}

NV_API void __pjw_update(pjw_ctx* ctx, uint8_t* buf, size_t cnt);

/**
 * @def pjw_update(ctx, buf, cnt)
 * @brief 计算哈希值
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小
 * @par 示例:
 * @code
 	pjw_ctx ctx;

 	// 初始化上下文
 	pjw_init(&ctx);

 	// 计算校验码
 	pjw_update(&ctx, str, strlen(str));
 	
 	// 返回结果
 	return pjw_final(&ctx);
 * @endcode
 * @see pjw_init, pjw_final
 */
#define pjw_update(ctx, buf, cnt) __pjw_update(ctx, (uint8_t*)(buf), cnt)

static inline uint32_t __pjw_final(pjw_ctx* ctx, uint32_t hash)
{
	ctx->hash = 0;
	return __cpu_to_le32(hash);
}

/**
 * @fn static inline uint32_t pjw_final(pjw_ctx* ctx);
 * @brief 获取PJW校验值
 * @param[in,out] ctx 上下文
 * @return 返回小端字节序32位校验码
 * @see pjw_init, pjw_update
 */
static inline uint32_t pjw_final(pjw_ctx* ctx)
{
	return __pjw_final(ctx, ctx->hash);
}

/**
 * @fn uint32_t pjw_hash_nbstr(const char* str);
 * @brief 直接哈希字符串
 * @param[in] str 多字节字符串
 * @return uint32_t 返回小端字节序32位哈希值
 */
NV_API uint32_t pjw_hash_nbstr(const char* str);

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_PJW__ */

