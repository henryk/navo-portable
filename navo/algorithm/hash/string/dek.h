/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/string/dek.h
 * 
 * @brief DEK string hash
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_DEK__
#define __NV_DEK__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup DEK
 * @ingroup 字符串哈希
 * @{
 */

/**
 * @struct dek_ctx
 * @brief DEK哈希上下文
 */
typedef struct {
	/** @brief 哈希值 */
	uint32_t hash;
}dek_ctx;

/**
 * @def DEK_CTX(name)
 * @brief 定义名为name的DEK上下文
 */
#define DEK_CTX(name) dek_ctx name = {0x4E67C6A7UL}

/**
 * @fn static inline void dek_init(dek_ctx* ctx);
 * @brief 初始化DEK上下文
 * @param[out] ctx 上下文
 */
static inline void dek_init(dek_ctx* ctx)
{
	ctx->hash = 0x4E67C6A7UL;
}

NV_API void __dek_update(dek_ctx* ctx, uint8_t* buf, size_t cnt);

/**
 * @def dek_update(ctx, buf, cnt)
 * @brief 计算哈希值
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小
 * @par 示例:
 * @code
 	dek_ctx ctx;

 	// 初始化上下文
 	dek_init(&ctx);

 	// 计算校验码
 	dek_update(&ctx, str, strlen(str));
 	
 	// 返回结果
 	return dek_final(&ctx);
 * @endcode
 * @see dek_init, dek_final
 */
#define dek_update(ctx, buf, cnt) __dek_update(ctx, (uint8_t*)(buf), cnt)

static inline uint32_t __dek_final(dek_ctx* ctx, uint32_t hash)
{
	ctx->hash = 0x4E67C6A7UL;
	return __cpu_to_le32(hash);
}

/**
 * @fn static inline uint32_t dek_final(dek_ctx* ctx);
 * @brief 获取DEK校验值
 * @param[in,out] ctx 上下文
 * @return 返回小端字节序32位校验码
 * @see dek_init, dek_update
 */
static inline uint32_t dek_final(dek_ctx* ctx)
{
	return __dek_final(ctx, ctx->hash);
}

/**
 * @fn uint32_t dek_hash_nbstr(const char* str);
 * @brief 直接哈希字符串
 * @param[in] str 多字节字符串
 * @return uint32_t 返回小端字节序32位哈希值
 */
NV_API uint32_t dek_hash_nbstr(const char* str);

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_DEK__ */


