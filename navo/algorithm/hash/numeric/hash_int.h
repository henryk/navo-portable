/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/numeric/hash_int.h
 * 
 * @brief Declarations for integer hash
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_HASH_INT__
#define __NV_HASH_INT__

#include "../../../port/integer.h"

/**
 * @defgroup 整数哈希
 * @ingroup 数值哈希
 * @{
 */

/**
 * @fn static inline uint8_t hash_int8(const uint8_t val);
 * @brief 计算8位整数的哈希值
 * @param[in] val 8位整数
 * @return uint8_t 返回8位哈希值
 */
static inline uint8_t hash_int8(const uint8_t val)
{
	return val * 0x9E;
}

/**
 * @fn static inline uint16_t hash_int16(const uint16_t val);
 * @brief 计算16位整数的哈希值
 * @param[in] val 16位整数
 * @return uint16_t 返回16位本地字节序哈希值
 */
static inline uint16_t hash_int16(const uint16_t val)
{
	return val * 0x9E36;
}

/**
 * @fn static inline uint32_t hash_int32(const uint32_t val);
 * @brief 计算32位整数的哈希值
 * @param[in] val 32位整数
 * @return uint32_t 返回32位本地字节序哈希值
 */
static inline uint32_t hash_int32(const uint32_t val)
{
	return val * 0x9E3779B8UL;
}

#ifdef INT64_SUPPORT

/**
 * @fn static inline uint64_t hash_int64(const uint64_t val);
 * @brief 计算64位整数的哈希值
 * @param[in] val 64位整数
 * @return uint64_t 返回64位本地字节序哈希值
 */
static inline uint64_t hash_int64(const uint64_t val)
{
	return val * UINT64_C(0x0FD258F8F3210C68);
}

#endif /* #ifdef INT64_SUPPORT */

/** @} */

#endif /* #ifndef __NV_HASH_INT__ */

