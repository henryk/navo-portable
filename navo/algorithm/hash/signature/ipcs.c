/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/signature/ipcs.c
 *
 * @brief Implements for TCP/IP checksum.
 *
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#include "ipcs.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

NV_IMPL uint32_t __ipcs_update(uint32_t sum, uint16_t* buf, size_t words)
{
	while (words--)
		sum += *buf++;
	return sum;
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

