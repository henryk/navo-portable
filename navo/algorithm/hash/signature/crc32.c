/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/signature/crc32.c
 *
 * @brief Implements for 32-bit CRC.
 *
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#include "crc32.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

NV_IMPL void crc32_build(uint32_t* buf, uint32_t poly)
{
	uint32_t i, j, v;

	poly = __le32_to_cpu(poly);
	for (j = 0; j < CRC32_TBL_SIZE; buf[j++] = v) {
		v = j;
		for (i = 0; i++ < 8;) {
			if (v & 1) {
				v >>= 1;
				v ^= poly;
			} else
				v >>= 1;
		}
	}
}

NV_IMPL void __crc32_process(crc32_ctx* ctx, uint8_t* buf, size_t cnt)
{
	int i;
	uint32_t poly = ctx->poly;
	uint32_t crc = ~ctx->crc;

	while (cnt--) {
		crc ^= *buf++;
		for (i = 0; i++ < 8;) {
			if (crc & 1) {
				crc >>= 1;
				crc ^= poly;
			} else
				crc >>= 1;
		}
	}

	ctx->crc = ~crc;
}

NV_IMPL void __crc32_update(crc32_ctx* ctx, const uint32_t* tbl,
									uint8_t* buf, size_t cnt)
{
	uint32_t crc = ~ctx->crc;

	while (cnt--) {
		crc = (crc >> 8) ^ tbl[(crc ^ *buf++) & 0xFF];
	}

	ctx->crc = ~crc;
}

NV_IMPL uint32_t crc32_hash_nbstr(const char* str, uint32_t* tbl)
{
	uint32_t hash = ~0;
	uint8_t ch;

	while ((ch = *str++)) {
		hash = (hash >> 8) ^ tbl[(hash ^ ch) & 0xFF];
	}

	return __cpu_to_le32(~hash);
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

