/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/signature/md4.c
 * 
 * @brief Implements for MD4.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#include "md4.h"
#include "../../../port/swab.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
#include <cstring>
extern "C" {
#else
#include <string.h>
#endif /* #ifdef __cplusplus */

#define F(x, y, z) (((x) & (y)) | ((~(x)) & (z)))
#define G(x, y, z) (((x) & (y)) | ((x) & (z)) | ((y) & (z)))
#define H(x, y, z) ((x) ^ (y) ^ (z))

#define R0(w, x, y, z, k, s) \
	((w) = __rol32_c(((w) + F(x, y, z) + (k)), (s)))

#define R1(w, x, y, z, k, s) \
	((w) = __rol32_c(((w) + G(x, y, z) + (k) + 0x5A827999UL), (s)))

#define R2(w, x, y, z, k, s) \
	((w) = __rol32_c(((w) + H(x, y, z) + (k) + 0x6ED9EBA1UL), (s)))

static void md4_transform(md4_ctx* ctx)
{
	uint32_t a = ctx->digest[0];
	uint32_t b = ctx->digest[1];
	uint32_t c = ctx->digest[2];
	uint32_t d = ctx->digest[3];
	uint32_t* in = (uint32_t*)(ctx->in);

	R0(a, b, c, d, in[0],  3);
	R0(d, a, b, c, in[1],  7);
	R0(c, d, a, b, in[2], 11);
	R0(b, c, d, a, in[3], 19);
	R0(a, b, c, d, in[4],  3);
	R0(d, a, b, c, in[5],  7);
	R0(c, d, a, b, in[6], 11);
	R0(b, c, d, a, in[7], 19);
	R0(a, b, c, d, in[8],  3);
	R0(d, a, b, c, in[9],  7);
	R0(c, d, a, b, in[10], 11);
	R0(b, c, d, a, in[11], 19);
	R0(a, b, c, d, in[12],  3);
	R0(d, a, b, c, in[13],  7);
	R0(c, d, a, b, in[14], 11);
	R0(b, c, d, a, in[15], 19);
	
	R1(a, b, c, d,in[ 0], 3);
	R1(d, a, b, c, in[4], 5);
	R1(c, d, a, b, in[8], 9);
	R1(b, c, d, a, in[12], 13);
	R1(a, b, c, d, in[1], 3);
	R1(d, a, b, c, in[5], 5);
	R1(c, d, a, b, in[9], 9);
	R1(b, c, d, a, in[13], 13);
	R1(a, b, c, d, in[2], 3);
	R1(d, a, b, c, in[6], 5);
	R1(c, d, a, b, in[10], 9);
	R1(b, c, d, a, in[14], 13);
	R1(a, b, c, d, in[3], 3);
	R1(d, a, b, c, in[7], 5);
	R1(c, d, a, b, in[11], 9);
	R1(b, c, d, a, in[15], 13);
	
	R2(a, b, c, d,in[ 0], 3);
	R2(d, a, b, c, in[8], 9);
	R2(c, d, a, b, in[4], 11);
	R2(b, c, d, a, in[12], 15);
	R2(a, b, c, d, in[2], 3);
	R2(d, a, b, c, in[10], 9);
	R2(c, d, a, b, in[6], 11);
	R2(b, c, d, a, in[14], 15);
	R2(a, b, c, d, in[1], 3);
	R2(d, a, b, c, in[9], 9);
	R2(c, d, a, b, in[5], 11);
	R2(b, c, d, a, in[13], 15);
	R2(a, b, c, d, in[3], 3);
	R2(d, a, b, c, in[11], 9);
	R2(c, d, a, b, in[7], 11);
	R2(b, c, d, a, in[15], 15);

	ctx->digest[0] += a;
	ctx->digest[1] += b;
	ctx->digest[2] += c;
	ctx->digest[3] += d;

	/* 为了安全考虑，擦除内存 */
	a = 0;b = 0;c = 0;d = 0;
	in = NULL;
}

NV_IMPL void __md4_update(md4_ctx* ctx, uint8_t* buf, size_t len)
{
	uint32_t offset = ((ctx->cnt[0] >> 3) & 0x3F);
	uint32_t space = sizeof(ctx->in) - offset;

	/* 更新比特计数 */
	if ((ctx->cnt[0] += (len << 3)) < (len << 3))
		++ctx->cnt[1];
	ctx->cnt[1] += (len >> 29);

	if (len < space)
		memcpy(ctx->in + offset, buf, len);
	else {
		memcpy(ctx->in + offset, buf, space);
		__le32_to_cpu_array(ctx->in, 16);
		md4_transform(ctx);
		buf += space;
		len -= space;

		while (len >= sizeof(ctx->in)) {
			memcpy(ctx->in, buf, sizeof(ctx->in));
			__le32_to_cpu_array(ctx->in, 16);
			md4_transform(ctx);
			buf += sizeof(ctx->in);
			len -= sizeof(ctx->in);
		}
		if (len)
			memcpy(ctx->in, buf, len);
	}

	/* 为了安全考虑，擦除内存 */
	offset = 0; space = 0;
}

NV_IMPL void md4_final(md4_ctx* ctx, void* digest)
{
	uint8_t padding[64] = {0x80, 0};
	uint32_t offset = (ctx->cnt[0] >> 3) & 0x3F;
	int padlen;
	
	padlen = 56 > offset? 56 - offset: 120 - offset;

	((uint32_t*)ctx->in)[14] = ctx->cnt[0];
	((uint32_t*)ctx->in)[15] = ctx->cnt[1];

	md4_update(ctx, padding, padlen);
	__le32_to_cpu_array(ctx->in, 14);
	md4_transform(ctx);
	__cpu_to_le32_array(ctx->digest, 4);

	memcpy(digest, ctx->digest, sizeof(ctx->digest));

	/* 为了安全考虑，擦除内存 */
	memset(ctx, 0, sizeof(*ctx));
	memset(padding, 0, sizeof(padding));
	offset = 0; padlen = 0;
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

