/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/signature/sha512.h
 * 
 * @brief Definitions and declarations for SHA-512.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_SHA512__
#define __NV_SHA512__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"

/**
 * @defgroup SHA-512_384
 * @ingroup 签名哈希
 * @{
 */

#ifdef INT64_SUPPORT

#ifdef __cplusplus
#include <cstring>
extern "C" {
#else
#include <string.h>
#endif	/* #ifdef __cplusplus */

/**
 * @struct sha512_ctx
 * @brief SHA-512上下文
 * @struct sha384_ctx
 * @brief SHA-384上下文
 */
typedef struct {
	/** @brief 计数 */
	uint64_t cnt[2];
	/** @brief 摘要 */
	uint64_t digest[8];
	/** @biref 缓存 */
	uint8_t in[128];
}sha512_ctx, sha384_ctx;

#define SHA512_HASH_H0 UINT64_C(0x6A09E667F3BCC908)
#define SHA512_HASH_H1 UINT64_C(0xBB67AE8584CAA73B)
#define SHA512_HASH_H2 UINT64_C(0x3C6EF372FE94F82B)
#define SHA512_HASH_H3 UINT64_C(0xA54FF53A5F1D36F1)
#define SHA512_HASH_H4 UINT64_C(0x510E527FADE682D1)
#define SHA512_HASH_H5 UINT64_C(0x9B05688C2B3E6C1F)
#define SHA512_HASH_H6 UINT64_C(0x1F83D9ABFB41BD6B)
#define SHA512_HASH_H7 UINT64_C(0x5BE0CD19137E2179)

#define SHA384_HASH_H0 UINT64_C(0xcbbb9d5dc1059ed8)
#define SHA384_HASH_H1 UINT64_C(0x629a292a367cd507)
#define SHA384_HASH_H2 UINT64_C(0x9159015a3070dd17)
#define SHA384_HASH_H3 UINT64_C(0x152fecd8f70e5939)
#define SHA384_HASH_H4 UINT64_C(0x67332667ffc00b31)
#define SHA384_HASH_H5 UINT64_C(0x8eb44a8768581511)
#define SHA384_HASH_H6 UINT64_C(0xdb0c2e0d64f98fa7)
#define SHA384_HASH_H7 UINT64_C(0x47b5481dbefa4fa4)

#define SHA512_SIZE 64
#define SHA384_SIZE 48

/**
 * @def SHA512_CTX(name)
 * @brief 定义名为name的SHA-512上下文
 */
#define SHA512_CTX(name) \
	sha512_ctx name = {{0, 0}, {			\
		SHA512_HASH_H0, SHA512_HASH_H1,		\
		SHA512_HASH_H2, SHA512_HASH_H3,		\
		SHA512_HASH_H4, SHA512_HASH_H5,		\
		SHA512_HASH_H6, SHA512_HASH_H7}, {0}}

/**
 * @def SHA384_CTX(name)
 * @brief 定义名为name的SHA-384上下文
 */
#define SHA384_CTX(name) \
	sha512_ctx name = {{0, 0}, {			\
		SHA384_HASH_H0, SHA384_HASH_H1,		\
		SHA384_HASH_H2, SHA384_HASH_H3,		\
		SHA384_HASH_H4, SHA384_HASH_H5,		\
		SHA384_HASH_H6, SHA384_HASH_H7}, {0}}

static inline void __sha512_init(uint64_t* digest, uint64_t* cnt)
{
	digest[0] = SHA512_HASH_H0;
	digest[1] = SHA512_HASH_H1;
	digest[2] = SHA512_HASH_H2;
	digest[3] = SHA512_HASH_H3;
	digest[4] = SHA512_HASH_H4;
	digest[5] = SHA512_HASH_H5;
	digest[6] = SHA512_HASH_H6;
	digest[7] = SHA512_HASH_H7;
	cnt[0] = 0;
	cnt[1] = 0;
}

/**
 * @fn static inline void sha512_init(sha512_ctx* ctx);
 * @brief 初始化SHA512上下文
 * @param[out] ctx 上下文
 */
static inline void sha512_init(sha512_ctx* ctx)
{
	__sha512_init(ctx->digest, ctx->cnt);
}

NV_API void __sha512_update(sha512_ctx* ctx, uint8_t* buf, size_t cnt);

/**
 * @def sha512_update(ctx, buf, cnt)
 * @brief 计算哈希值
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小
 * @par 示例:
 * @code
 	sha512_ctx ctx;
 	unsigned char digest[SHA512_SIZE];

 	// 初始化上下文
 	sha512_init(&ctx);

 	while (buf_size >= BLOCK_SIZE) {
 		// 计算校验码
 		sha512_update(&ctx, buf, BLOCK_SIZE);
 		buf += BLOCK_SIZE;
 		buf_size -= BLOCK_SIZE;
 	}
 	if (buf_size) {
 		// 计算校验码
 		sha512_update(&ctx, buf, buf_size);
 	}
 	
 	// 返回结果
 	sha512_final(&ctx, digest);
 * @endcode
 * @see sha512_init, sha512_final
 */
#define sha512_update(ctx, buf, cnt) __sha512_update(ctx, (uint8_t*)(buf), cnt)

/**
 * @fn void sha512_final(sha512_ctx* ctx, void* digest);
 * @brief 获取SHA512校验值
 * @param[in,out] ctx 上下文
 * @param[out] digest 输出大端字节序512位校验码
 * @see sha512_init, sha512_update
 */
NV_API void sha512_final(sha512_ctx* ctx, void* digest);

static inline void __sha384_init(uint64_t* digest, uint64_t* cnt)
{
	digest[0] = SHA384_HASH_H0;
	digest[1] = SHA384_HASH_H1;
	digest[2] = SHA384_HASH_H2;
	digest[3] = SHA384_HASH_H3;
	digest[4] = SHA384_HASH_H4;
	digest[5] = SHA384_HASH_H5;
	digest[6] = SHA384_HASH_H6;
	digest[7] = SHA384_HASH_H7;
	cnt[0] = 0;
	cnt[1] = 0;
}

/**
 * @fn static inline void sha384_init(sha384_ctx* ctx);
 * @brief 初始化SHA-384上下文
 * @param[out] ctx 上下文
 */
static inline void sha384_init(sha384_ctx* ctx)
{
	__sha384_init(ctx->digest, ctx->cnt);
}

/**
 * @def sha384_update(ctx, buf, cnt)
 * @brief 计算哈希值
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小
 * @par 示例:
 * @code
 	sha384_ctx ctx;
 	unsigned char digest[SHA384_SIZE];

 	// 初始化上下文
 	sha384_init(&ctx);

 	while (buf_size >= BLOCK_SIZE) {
 		// 计算校验码
 		sha384_update(&ctx, buf, BLOCK_SIZE);
 		buf += BLOCK_SIZE;
 		buf_size -= BLOCK_SIZE;
 	}
 	if (buf_size) {
 		// 计算校验码
 		sha384_update(&ctx, buf, buf_size);
 	}
 	
 	// 返回结果
 	sha384_final(&ctx, digest);
 * @endcode
 * @see sha384_init, sha384_final
 */
#define sha384_update(ctx, buf, cnt) __sha512_update(ctx, (uint8_t*)(buf), cnt)

/**
 * @fn static inline void sha384_final(sha384_ctx* ctx, void* digest);
 * @brief 获取SHA-384校验值
 * @param[in,out] ctx 上下文
 * @param[out] digest 输出大端字节序384位校验码
 * @see sha384_init, sha384_update
 */
static inline void sha384_final(sha384_ctx* ctx, void* digest)
{
	uint8_t t[64];

	sha512_final(ctx, t);
	memcpy(digest, t, 48);
	memset(t, 0, sizeof(t));
}

#ifdef __cplusplus
}
#endif	/* #ifdef __cplusplus */

#else
#error "Need int64 support"
#endif /* #ifdef INT64_SUPPORT */

/** @} */

#endif /* #ifndef __NV_SHA512__ */

