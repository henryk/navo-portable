/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/signature/crc16.c
 *
 * @brief Implements for 16-bit CRC.
 *
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#include "crc16.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

NV_IMPL void crc16_build(uint16_t* buf, uint16_t poly)
{
	uint16_t i, j, v;

	poly = __le16_to_cpu(poly);
	for (j = 0; j < CRC16_TBL_SIZE; buf[j++] = v) {
		v = j << 8;
		for (i = 0; i++ < 8;) {
			if (v & 0x8000) {
				v <<= 1;
				v ^= poly;
			} else
				v <<= 1;
		}
	}
}

NV_IMPL void __crc16_process(crc16_ctx* ctx, uint8_t* buf, size_t cnt)
{
	int i;
	uint16_t poly = ctx->poly;
	uint16_t crc = ctx->crc;

	while (cnt--) {
		crc ^= (*buf++ << 8);
		for (i = 0; i++ < 8;) {
			if (crc & 0x8000) {
				crc <<= 1;
				crc ^= poly;
			} else
				crc <<= 1;
		}
	}

	ctx->crc = crc;
}

NV_IMPL void __crc16_update(crc16_ctx* ctx, const uint16_t* tbl,
									uint8_t* buf, size_t cnt)
{
	uint16_t crc = ctx->crc;

	while (cnt--) {
		crc = (crc << 8) ^ tbl[(crc >> 8) ^ *buf++];
	}

	ctx->crc = crc;
}

NV_IMPL uint16_t crc16_hash_nbstr(const char* str, uint16_t* tbl)
{
	uint16_t hash = 0;
	uint8_t ch;

	while ((ch = *str++)) {
		hash = (hash << 8) ^ tbl[(hash >> 8) ^ ch];
	}

	return __cpu_to_le16(hash);
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

