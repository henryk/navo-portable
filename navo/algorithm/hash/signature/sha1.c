/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/signature/sha1.c
 *
 * @brief Implements for SHA-1.
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#include "sha1.h"
#include "../../../port/swab.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
#include <cstring>
extern "C" {
#else
#include <string.h>
#endif /* #ifdef __cplusplus */

#define F1(x, y, z) ((z) ^ ((x) & ((y) ^ (z))))
#define F2(x, y, z) ((x) ^ (y) ^ (z))
#define F3(x, y, z) (((x) & (y)) | ((z) & ((x) | (y))))

#define K1 0x5A827999UL	/* 循环  0-19: sqrt(2) * 2^30 */
#define K2 0x6ED9EBA1UL	/* 循环 20-39: sqrt(3) * 2^30 */
#define K3 0x8F1BBCDCUL	/* 循环 40-59: sqrt(5) * 2^30 */
#define K4 0xCA62C1D6UL	/* 循环 60-79: sqrt(10) * 2^30 */

#define IN1(i) \
	(in[i] = (__rol32_c(in[i], 24) & 0xFF00FF00UL) | \
	(__rol32_c(in[i], 8) & 0x00FF00FFUL))

#define IN2(i) \
	(in[i & 0xF] = __rol32_c((in[i & 0xF] ^ in[(i + 2) & 0xF] ^ \
	in[(i + 8) & 0xF] ^ in[(i + 13) & 0xF]), 1))

#define R0(v, w, x, y, z, i) \
	((z) += (F1(w, x, y) + IN1(i) + K1 + __rol32_c((v), 5)), \
	(w) = __rol32_c((w), 30))

#define R1(v, w, x, y, z, i) \
	((z) += (F1(w, x, y) + IN2(i) + K1 + __rol32_c((v), 5)), \
	(w) = __rol32_c((w), 30))

#define R2(v, w, x, y, z, i) \
	((z) += (F2(w, x, y) + IN2(i) + K2 + __rol32_c((v), 5)), \
	(w) = __rol32_c((w), 30))

#define R3(v, w, x, y, z, i) \
	((z) += (F3(w, x, y) + IN2(i) + K3 + __rol32_c((v), 5)), \
	(w) = __rol32_c((w), 30))

#define R4(v, w, x, y, z, i) \
	((z) += (F2(w, x, y) + IN2(i) + K4 + __rol32_c((v), 5)), \
	(w) = __rol32_c((w), 30))

static void sha1_transform(sha1_ctx* ctx)
{
	uint32_t a = ctx->digest[0];
	uint32_t b = ctx->digest[1];
	uint32_t c = ctx->digest[2];
	uint32_t d = ctx->digest[3];
	uint32_t e = ctx->digest[4];
	uint32_t* in = (uint32_t*)(ctx->in);

	R0(a,b,c,d,e, 0); R0(e,a,b,c,d, 1);
	R0(d,e,a,b,c, 2); R0(c,d,e,a,b, 3);
	R0(b,c,d,e,a, 4); R0(a,b,c,d,e, 5);
	R0(e,a,b,c,d, 6); R0(d,e,a,b,c, 7);
	R0(c,d,e,a,b, 8); R0(b,c,d,e,a, 9);
	R0(a,b,c,d,e,10); R0(e,a,b,c,d,11);
	R0(d,e,a,b,c,12); R0(c,d,e,a,b,13);
	R0(b,c,d,e,a,14); R0(a,b,c,d,e,15);
	R1(e,a,b,c,d,16); R1(d,e,a,b,c,17);
	R1(c,d,e,a,b,18); R1(b,c,d,e,a,19);
	R2(a,b,c,d,e,20); R2(e,a,b,c,d,21);
	R2(d,e,a,b,c,22); R2(c,d,e,a,b,23);
	R2(b,c,d,e,a,24); R2(a,b,c,d,e,25);
	R2(e,a,b,c,d,26); R2(d,e,a,b,c,27);
	R2(c,d,e,a,b,28); R2(b,c,d,e,a,29);
	R2(a,b,c,d,e,30); R2(e,a,b,c,d,31);
	R2(d,e,a,b,c,32); R2(c,d,e,a,b,33);
	R2(b,c,d,e,a,34); R2(a,b,c,d,e,35);
	R2(e,a,b,c,d,36); R2(d,e,a,b,c,37);
	R2(c,d,e,a,b,38); R2(b,c,d,e,a,39);
	R3(a,b,c,d,e,40); R3(e,a,b,c,d,41);
	R3(d,e,a,b,c,42); R3(c,d,e,a,b,43);
	R3(b,c,d,e,a,44); R3(a,b,c,d,e,45);
	R3(e,a,b,c,d,46); R3(d,e,a,b,c,47);
	R3(c,d,e,a,b,48); R3(b,c,d,e,a,49);
	R3(a,b,c,d,e,50); R3(e,a,b,c,d,51);
	R3(d,e,a,b,c,52); R3(c,d,e,a,b,53);
	R3(b,c,d,e,a,54); R3(a,b,c,d,e,55);
	R3(e,a,b,c,d,56); R3(d,e,a,b,c,57);
	R3(c,d,e,a,b,58); R3(b,c,d,e,a,59);
	R4(a,b,c,d,e,60); R4(e,a,b,c,d,61);
	R4(d,e,a,b,c,62); R4(c,d,e,a,b,63);
	R4(b,c,d,e,a,64); R4(a,b,c,d,e,65);
	R4(e,a,b,c,d,66); R4(d,e,a,b,c,67);
	R4(c,d,e,a,b,68); R4(b,c,d,e,a,69);
	R4(a,b,c,d,e,70); R4(e,a,b,c,d,71);
	R4(d,e,a,b,c,72); R4(c,d,e,a,b,73);
	R4(b,c,d,e,a,74); R4(a,b,c,d,e,75);
	R4(e,a,b,c,d,76); R4(d,e,a,b,c,77);
	R4(c,d,e,a,b,78); R4(b,c,d,e,a,79);

	ctx->digest[0] += a;
	ctx->digest[1] += b;
	ctx->digest[2] += c;
	ctx->digest[3] += d;
	ctx->digest[4] += e;

	/* 为了安全考虑，擦除内存 */
	a = 0; b = 0; c = 0; d = 0; e = 0;
	in = NULL;
}

NV_IMPL void __sha1_update(sha1_ctx* ctx, uint8_t* buf, size_t len)
{
	uint32_t offset = ((ctx->cnt[0] >> 3) & 0x3F);
	uint32_t space = sizeof(ctx->in) - offset;

	/* 更新比特计数 */
	if ((ctx->cnt[0] += (len << 3)) < (len << 3))
		++ctx->cnt[1];
	ctx->cnt[1] += (len >> 29);

	if (len < space)
		memcpy(ctx->in + offset, buf, len);
	else {
		memcpy(ctx->in + offset, buf, space);
		__le32_to_cpu_array(ctx->in, 16);
		sha1_transform(ctx);
		buf += space;
		len -= space;

		while (len >= sizeof(ctx->in)) {
			memcpy(ctx->in, buf, sizeof(ctx->in));
			__le32_to_cpu_array(ctx->in, 16);
			sha1_transform(ctx);
			buf += sizeof(ctx->in);
			len -= sizeof(ctx->in);
		}
		if (len)
			memcpy(ctx->in, buf, len);
	}

	/* 为了安全考虑，擦除内存 */
	offset = 0; space = 0;
}

NV_IMPL void sha1_final(sha1_ctx* ctx, void* digest)
{
	uint32_t* buf = ctx->digest;
	uint8_t padding[64] = {0x80, 0};
	uint32_t offset = (ctx->cnt[0] >> 3) & 0x3F;
	uint32_t bits[2];
	int padlen, i;

	padlen = 56 > offset? 56 - offset: 120 - offset;

	bits[1] = __cpu_to_be32(ctx->cnt[0]);
	bits[0] = __cpu_to_be32(ctx->cnt[1]);

	__sha1_update(ctx, padding, padlen);
	sha1_update(ctx, bits, 8);

	for (i = 0; i < 5; ++i)
		((uint32_t*)digest)[i] = __cpu_to_be32(buf[i]);

	/* 为了安全考虑，擦除内存 */
	memset(ctx, 0, sizeof(*ctx));
	memset(padding, 0, sizeof(padding));
	bits[0] = 0; bits[1] = 0;
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

