/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/signature/sha512.c
 *
 * @brief Implements for SHA-512.
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#include "sha512.h"

#ifdef INT64_SUPPORT

#include "../../../port/swab.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

#define Ch(x, y, z)		((z) ^ ((x) & ((y) ^ (z))))
#define Maj(x, y, z)	(((x) & (y)) | ((z) & ((x) | (y))))

#define E0(x) (__ror64_c(x,28) ^ __ror64_c(x,34) ^ __ror64_c(x,39))
#define E1(x) (__ror64_c(x,14) ^ __ror64_c(x,18) ^ __ror64_c(x,41))
#define S0(x) (__ror64_c(x, 1) ^ __ror64_c(x, 8) ^ (x >> 7))
#define S1(x) (__ror64_c(x,19) ^ __ror64_c(x,61) ^ (x >> 6))

#define ROUND(a, b, c, d, e, f, g, h, t1, t2, I) \
	(t1) = (h) + E1(e) + Ch(e, f, g) + K[(i) + (I)] + w[(i & 15) + I];	\
	(t2) = E0(a) + Maj(a, b, c); (d) += (t1); (h) = (t1) + (t2);

static const uint64_t K[80] = {
	UINT64_C(0x428a2f98d728ae22), UINT64_C(0x7137449123ef65cd),
	UINT64_C(0xb5c0fbcfec4d3b2f), UINT64_C(0xe9b5dba58189dbbc),
	UINT64_C(0x3956c25bf348b538), UINT64_C(0x59f111f1b605d019),
	UINT64_C(0x923f82a4af194f9b), UINT64_C(0xab1c5ed5da6d8118),
	UINT64_C(0xd807aa98a3030242), UINT64_C(0x12835b0145706fbe),
	UINT64_C(0x243185be4ee4b28c), UINT64_C(0x550c7dc3d5ffb4e2),
	UINT64_C(0x72be5d74f27b896f), UINT64_C(0x80deb1fe3b1696b1),
	UINT64_C(0x9bdc06a725c71235), UINT64_C(0xc19bf174cf692694),
	UINT64_C(0xe49b69c19ef14ad2), UINT64_C(0xefbe4786384f25e3),
	UINT64_C(0x0fc19dc68b8cd5b5), UINT64_C(0x240ca1cc77ac9c65),
	UINT64_C(0x2de92c6f592b0275), UINT64_C(0x4a7484aa6ea6e483),
	UINT64_C(0x5cb0a9dcbd41fbd4), UINT64_C(0x76f988da831153b5),
	UINT64_C(0x983e5152ee66dfab), UINT64_C(0xa831c66d2db43210),
	UINT64_C(0xb00327c898fb213f), UINT64_C(0xbf597fc7beef0ee4),
	UINT64_C(0xc6e00bf33da88fc2), UINT64_C(0xd5a79147930aa725),
	UINT64_C(0x06ca6351e003826f), UINT64_C(0x142929670a0e6e70),
	UINT64_C(0x27b70a8546d22ffc), UINT64_C(0x2e1b21385c26c926),
	UINT64_C(0x4d2c6dfc5ac42aed), UINT64_C(0x53380d139d95b3df),
	UINT64_C(0x650a73548baf63de), UINT64_C(0x766a0abb3c77b2a8),
	UINT64_C(0x81c2c92e47edaee6), UINT64_C(0x92722c851482353b),
	UINT64_C(0xa2bfe8a14cf10364), UINT64_C(0xa81a664bbc423001),
	UINT64_C(0xc24b8b70d0f89791), UINT64_C(0xc76c51a30654be30),
	UINT64_C(0xd192e819d6ef5218), UINT64_C(0xd69906245565a910),
	UINT64_C(0xf40e35855771202a), UINT64_C(0x106aa07032bbd1b8),
	UINT64_C(0x19a4c116b8d2d0c8), UINT64_C(0x1e376c085141ab53),
	UINT64_C(0x2748774cdf8eeb99), UINT64_C(0x34b0bcb5e19b48a8),
	UINT64_C(0x391c0cb3c5c95a63), UINT64_C(0x4ed8aa4ae3418acb),
	UINT64_C(0x5b9cca4f7763e373), UINT64_C(0x682e6ff3d6b2b8a3),
	UINT64_C(0x748f82ee5defb2fc), UINT64_C(0x78a5636f43172f60),
	UINT64_C(0x84c87814a1f0ab72), UINT64_C(0x8cc702081a6439ec),
	UINT64_C(0x90befffa23631e28), UINT64_C(0xa4506cebde82bde9),
	UINT64_C(0xbef9a3f7b2c67915), UINT64_C(0xc67178f2e372532b),
	UINT64_C(0xca273eceea26619c), UINT64_C(0xd186b8c721c0c207),
	UINT64_C(0xeada7dd6cde0eb1e), UINT64_C(0xf57d4f7fee6ed178),
	UINT64_C(0x06f067aa72176fba), UINT64_C(0x0a637dc5a2c898a6),
	UINT64_C(0x113f9804bef90dae), UINT64_C(0x1b710b35131c471b),
	UINT64_C(0x28db77f523047d84), UINT64_C(0x32caab7b40c72493),
	UINT64_C(0x3c9ebe0a15c9bebc), UINT64_C(0x431d67c49c100d4c),
	UINT64_C(0x4cc5d4becb3e42b6), UINT64_C(0x597f299cfc657e2a),
	UINT64_C(0x5fcb6fab3ad6faec), UINT64_C(0x6c44198c4a475817)
};

static inline void __sha512_load_op(uint64_t* w, int i, uint64_t* in)
{
	w[i] = __be64_to_cpu(in[i]);
}

static inline void __sha512_blend_op(uint64_t* w, int i)
{
	w[i & 15] += S1(w[(i - 2) & 15]) + w[(i - 7) & 15] + S0(w[(i - 15) & 15]);
}

#define __SHA512_LOAD_OP() while (j < 16) __sha512_load_op(w, i + (j++), in)
#define __SHA512_BLEND_OP() while (j < 16) __sha512_blend_op(w, i + (j++))

static void __sha512_transform(uint64_t* digest, uint64_t* in)
{
	uint64_t a = digest[0];
	uint64_t b = digest[1];
	uint64_t c = digest[2];
	uint64_t d = digest[3];
	uint64_t e = digest[4];
	uint64_t f = digest[5];
	uint64_t g = digest[6];
	uint64_t h = digest[7];
	uint64_t t1, t2;

	do {
		uint64_t w[16];
		int i;
		for (i = 0; i < 80; i += 8) {
			if (!(i & 8)) {
				int j = 0;

				if (i < 16)
					__SHA512_LOAD_OP();
				else
					__SHA512_BLEND_OP();
			}

			ROUND(a, b, c, d, e, f, g, h, t1, t2, 0);
			ROUND(h, a, b, c, d, e, f, g, t1, t2, 1);
			ROUND(g, h, a, b, c, d, e, f, t1, t2, 2);
			ROUND(f, g, h, a, b, c, d, e, t1, t2, 3);
			ROUND(e, f, g, h, a, b, c, d, t1, t2, 4);
			ROUND(d, e, f, g, h, a, b, c, t1, t2, 5);
			ROUND(c, d, e, f, g, h, a, b, t1, t2, 6);
			ROUND(b, c, d, e, f, g, h, a, t1, t2, 7);
		}
	} while (0);

	digest[0] += a; digest[1] += b;
	digest[2] += c; digest[3] += d;
	digest[4] += e; digest[5] += f;
	digest[6] += g; digest[7] += h;

	/* 为了安全考虑，擦除内存 */
	a = 0; b = 0; c = 0; d = 0;
	e = 0; f = 0; g = 0; h = 0;
	t1 = 0; t2 = 0;
}

static inline void sha512_transform(sha512_ctx* ctx)
{
	__sha512_transform(ctx->digest, ctx->in);
}

NV_IMPL void __sha512_update(sha512_ctx* ctx, uint8_t* buf, size_t len)
{
	uint32_t offset = (uint32_t)(ctx->cnt[0] & 0x7F);
	uint32_t space = 128 - offset;

	/* 更新比特计数 */
	if ((ctx->cnt[0] += len) < len)
		++ctx->cnt[1];

	if (len < space)
		memcpy(ctx->in + offset, buf, len);
	else {
		memcpy(ctx->in + offset, buf, space);
		sha512_transform(ctx);
		buf += space;
		len -= space;

		while (len >= sizeof(ctx->in)) {
			memcpy(ctx->in, buf, sizeof(ctx->in));
			sha512_transform(ctx);
			buf += sizeof(ctx->in);
			len -= sizeof(ctx->in);
		}
		if (len)
			memcpy(ctx->in, buf, len);
	}

	/* 为了安全考虑，擦除内存 */
	offset = 0; space = 0;
}

NV_IMPL void sha512_final(sha512_ctx* ctx, void* digest)
{
	uint64_t* buf = ctx->digest;
	uint8_t padding[128] = {0x80, 0};
	uint32_t offset = (uint32_t)(ctx->cnt[0] & 0x7F);
	uint64_t bits[2];
	int padlen, i;

	padlen = (112 > offset)? 112 - offset: 240 - offset;

	bits[1] = __cpu_to_be64(ctx->cnt[0] << 3);
	bits[0] = __cpu_to_be64(ctx->cnt[1] << 3 | ctx->cnt[0] >> 61);

	__sha512_update(ctx, padding, padlen);
	sha512_update(ctx, bits, sizeof(bits));
	
	for (i = 0; i < 8; ++i)
		((uint64_t*)digest)[i] = __cpu_to_be64(buf[i]);

	/* 为了安全考虑，擦除内存 */
	memset(ctx, 0, sizeof(sha512_ctx));
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifdef INT64_SUPPORT */

