/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/signature/md4.h
 * 
 * @brief Definitions and declarations for MD4.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_MD4__
#define __NV_MD4__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"

#ifdef __cplusplus
extern "C" {
#endif	/* #ifdef __cplusplus */

/**
 * @defgroup MD4
 * @ingroup 签名哈希
 * @{
 */

/**
 * @struct md4_ctx
 * @brief MD4上下文
 */
typedef struct {
	/** @brief 计数 */
	uint32_t cnt[2];
	/** @brief 摘要 */
	uint32_t digest[4];
	/** @biref 缓存 */
	uint8_t in[64];
}md4_ctx;

#define MD4_SIZE 16

/**
 * @def MD4_CTX(name)
 * @brief 定义名为name的MD4上下文
 */
#define MD4_CTX(name) \
	md4_ctx name = {{0, 0}, {				\
		0x67452301UL, 0xEFCDAB89UL, 		\
		0x98BADCFEUL, 0x10325476UL}, {0}}

static inline void __md4_init(uint32_t* digest, uint32_t* cnt)
{
	digest[0] = 0x67452301UL;
	digest[1] = 0xEFCDAB89UL;
	digest[2] = 0x98BADCFEUL;
	digest[3] = 0x10325476UL;
	cnt[0] = 0;
	cnt[1] = 0;
}

/**
 * @fn static inline void md4_init(md4_ctx* ctx);
 * @brief 初始化MD4上下文
 * @param[out] ctx 上下文
 */
static inline void md4_init(md4_ctx* ctx)
{
	__md4_init(ctx->digest, ctx->cnt);
}

NV_API void __md4_update(md4_ctx* ctx, uint8_t* buf, size_t cnt);

/**
 * @def md4_update(ctx, buf, cnt)
 * @brief 计算哈希值
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小
 * @par 示例:
 * @code
 	md4_ctx ctx;
 	unsigned char digest[MD4_SIZE];

 	// 初始化上下文
 	md4_init(&ctx);

 	while (buf_size >= BLOCK_SIZE) {
 		// 计算校验码
 		md4_update(&ctx, buf, BLOCK_SIZE);
 		buf += BLOCK_SIZE;
 		buf_size -= BLOCK_SIZE;
 	}
 	if (buf_size) {
 		// 计算校验码
 		md4_update(&ctx, buf, buf_size);
 	}
 	
 	// 返回结果
 	md4_final(&ctx, digest);
 * @endcode
 * @see md4_init, md4_final
 */
#define md4_update(ctx, buf, cnt) __md4_update(ctx, (uint8_t*)(buf), cnt)

/**
 * @fn void md4_final(md4_ctx* ctx, void* digest);
 * @brief 获取MD4校验值
 * @param[in,out] ctx 上下文
 * @param[out] digest 输出小端字节序128位校验码
 * @see md4_init, md4_update
 */
NV_API void md4_final(md4_ctx* ctx, void* digest);

/** @} */

#ifdef __cplusplus
}
#endif	/* #ifdef __cplusplus */

#endif /* #ifndef __NV_MD4__ */

