/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/signature/crc32.h
 *
 * @brief Definitions and declarations for 32 bit CRC.
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_CRC32__
#define __NV_CRC32__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
extern "C" {
#endif	/* #ifdef __cplusplus */

/**
 * @defgroup CRC-32
 * @ingroup 签名哈希
 * @{
 */

/**
 * @struct crc32_ctx
 * @brief CRC-32上下文
 */
typedef struct {
	/** @brief 多项式 */
	uint32_t poly;
	/** @biref 校验码 */
	uint32_t crc;
}crc32_ctx;

#define CRC32_SIZE 4

#define CRC32_POS_IEEE802_3 	__cpu_to_le32_c(INT32_C(0x04C11DB7))
#define CRC32_NEG_IEEE802_3 	__cpu_to_le32_c(INT32_C(0xEDB88320))
#define CRC32_POS_CASTAGNOLI 	__cpu_to_le32_c(INT32_C(0x1EDC6F41))
#define CRC32_NEG_CASTAGNOLI 	__cpu_to_le32_c(INT32_C(0x82F63B78))

/**
 * @def CRC32_CTX(name)
 * @brief 定义名为name的CRC32上下文
 */
#define CRC32_CTX(name) crc32_ctx name = {CRC32_NEG_IEEE802_3, 0}

/**
 * @fn void crc32_build(uint32_t* buf, uint32_t poly);
 * @brief 建立CRC码表
 * @param[out] buf 码表缓存
 * @param[in] poly 多项式，必须是小端字节序的32位整数
 * @par 示例:
 * @code
 	crc32_ctx crc;

 	// 分配码表缓存
 	uint32_t* crc_tbl = (uint32_t*)malloc(sizeof(uint32_t) * CRC32_TBL_SIZE);
 	if (!crc_tbl)
 		return 0;

	// 建立码表
 	crc32_build(crc_tbl, CRC32_NEG_IEEE802_3);

 	// 初始化上下文
 	crc32_init(&crc, 0);

 	while (buf_size >= BLOCK_SIZE) {
 		// 计算校验码
 		crc32_update(&crc, crc_tbl, buf, BLOCK_SIZE);
 		buf += BLOCK_SIZE;
 		buf_size -= BLOCK_SIZE;
 	}
 	if (buf_size) {
 		// 计算校验码
 		crc32_update(&crc, crc_tbl, buf, buf_size);
 	}

 	// 返回结果
 	return crc32_final(&crc);
 * @endcode
 * @see crc32_init
 */
NV_API void crc32_build(uint32_t* buf, uint32_t poly);

/**
 * @def CRC32_SIZE
 * @brief 32位CRC码表长度
 */
#define CRC32_TBL_SIZE 	0x100

/**
 * @fn static inline void crc32_init(crc32_ctx* ctx, const uint32_t poly);
 * @brief 初始化CRC上下文
 * @param[out] ctx 上下文
 * @param[in] poly 多项式，必须是小端字节序的32位整数，若为0，表示使用原poly值
 * @see crc32_build
 */
static inline void crc32_init(crc32_ctx* ctx, uint32_t poly)
{
	if (poly)
		ctx->poly = __le32_to_cpu(poly);
	ctx->crc = 0;
}

NV_API void __crc32_process(crc32_ctx* ctx, uint8_t* buf, size_t cnt);

/**
 * @def crc32_process(ctx, buf, cnt)
 * @brief 用位移的方式计算CRC
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小(按字节)
 * @note 用法同 crc32_update
 * @see crc32_init, crc32_update, crc32_final
 */
#define crc32_process(ctx, buf, cnt) \
	__crc32_process(ctx, (uint8_t*)(buf), cnt)

NV_API void __crc32_update(crc32_ctx* ctx, const uint32_t* tbl,
								uint8_t* buf, size_t cnt);

/**
 * @def crc32_update(ctx, tbl, buf, cnt)
 * @brief 用查表的方式计算CRC
 * @param[in,out] ctx 上下文
 * @param[in] tbl CRC码表
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小(按字节)
 * @note 用法同 crc32_process
 * @see crc32_init, crc32_process, crc32_final
 */
#define crc32_update(ctx, tbl, buf, cnt) \
	__crc32_update(ctx, tbl, (uint8_t*)(buf), cnt)

static inline uint32_t __crc32_final(crc32_ctx* ctx, uint32_t crc)
{
	ctx->crc = 0;
	return __cpu_to_le32(crc);
}

/**
 * @fn static inline uint32_t crc32_final(crc32_ctx* ctx);
 * @brief 获取CRC校验值
 * @param[in,out] ctx 上下文
 * @return uint32_t 返回小端字节序的校验值
 * @see crc32_init, crc32_process, crc32_update
 */
static inline uint32_t crc32_final(crc32_ctx* ctx)
{
	return __crc32_final(ctx, ctx->crc);
}

/**
 * @fn uint32_t crc32_hash_nbstr(const char* str, uint32_t* tbl);
 * @brief 直接哈希字符串
 * @param[in] str 多字节字符串
 * @param[in] tbl 码表
 * @return uint32_t 返回小端字节序的哈希值
 */
NV_API uint32_t crc32_hash_nbstr(const char* str, uint32_t* tbl);

/** @} */

#ifdef __cplusplus
}
#endif	/* #ifdef __cplusplus */

#endif /* #ifndef __NV_CRC32__ */

