/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/signature/tiger.h
 *
 * @brief Definitions and declarations for Tiger hash algorithm.
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_TIGER__
#define __NV_TIGER__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"

/**
 * @defgroup Tiger
 * @ingroup 签名哈希
 * @{
 */

#ifdef INT64_SUPPORT
#define TIGER_SUPPORT

#ifdef __cplusplus
#include <cstring>
extern "C" {
#else
#include <string.h>
#endif	/* #ifdef __cplusplus */

/**
 * @struct tiger_ctx
 * @brief Tiger上下文
 */
typedef struct {
	/** @brief A,B,C */
	uint64_t a, b, c;
	/** @brief 计数 */
	uint32_t cnt;
	/** @biref 缓存 */
	uint32_t blks;
	/** @brief 摘要 */
	uint8_t digest[64];
}tiger_ctx;

#define TIGER128_SIZE 16
#define TIGER160_SIZE 20
#define TIGER192_SIZE 24

/**
 * @def TIGER_CTX(name)
 * @brief 定义名为name的Tiger上下文
 */
#define TIGER_CTX(name) \
	tiger_ctx name = {					\
		UINT64_C(0x0123456789ABCDEF), 	\
		UINT64_C(0xFEDCBA9876543210), 	\
		UINT64_C(0xF096A5B4C3B2E187), 	\
		0, 0, {0}}

/**
 * @fn static inline void tiger_init(tiger_ctx* ctx);
 * @brief 初始化Tiger上下文
 * @param[out] ctx 上下文
 */
static inline void tiger_init(tiger_ctx* ctx)
{
	ctx->a = UINT64_C(0x0123456789ABCDEF);
	ctx->b = UINT64_C(0xFEDCBA9876543210);
	ctx->c = UINT64_C(0xF096A5B4C3B2E187);
	ctx->cnt = 0;
	ctx->blks = 0;
}

NV_API void __tiger_update(tiger_ctx* ctx, uint8_t* buf, size_t len);
NV_API void __tiger_final(tiger_ctx* ctx, uint8_t* hash, uint64_t* digest);

/**
 * @fn static inline void tiger_update(tiger_ctx* ctx, void* buf, size_t cnt);
 * @brief 计算哈希值
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小
 * @par 示例:
 * @code
 	tiger_ctx ctx;
 	unsigned char digest[TIGER192_SIZE];

 	// 初始化上下文
 	tiger_init(&ctx);

 	while (buf_size >= BLOCK_SIZE) {
 		// 计算校验码
 		tiger_update(&ctx, buf, BLOCK_SIZE);
 		buf += BLOCK_SIZE;
 		buf_size -= BLOCK_SIZE;
 	}
 	if (buf_size) {
 		// 计算校验码
 		tiger_update(&ctx, buf, buf_size);
 	}

 	// 返回192位结果
 	tiger_final(&ctx, digest);
 * @endcode
 * @see tiger_init, tiger_final, tiger128_final, tiger160_final
 */
static inline void tiger_update(tiger_ctx* ctx, void* buf, size_t cnt)
{
	__tiger_update(ctx, (uint8_t*)(buf), cnt);
}

/**
 * @fn static inline void tiger_final(tiger_ctx* ctx, void* digest);
 * @brief 获取Tiger 192位校验值
 * @param[in,out] ctx 上下文
 * @param[out] digest 输出大端字节序192位校验码
 * @see tiger_init, tiger_update
 */
static inline void tiger_final(tiger_ctx* ctx, void* digest)
{
	__tiger_final(ctx, ctx->digest, (uint64_t*)digest);
}

#define tiger192_final tiger_final

/**
 * @fn void tiger128_final(tiger_ctx* ctx, void* digest);
 * @brief 获取Tiger 128位校验值
 * @param[in,out] ctx 上下文
 * @param[out] digest 输出大端字节序128位校验码
 * @see tiger_init, tiger_update
 */
static inline void tiger128_final(tiger_ctx* ctx, void* digest)
{
	uint8_t buf[TIGER192_SIZE];

	tiger_final(ctx, buf);
	memcpy(digest, buf, TIGER128_SIZE);
	memset(buf, 0, sizeof(buf));
}

/**
 * @fn void tiger160_final(tiger_ctx* ctx, void* digest);
 * @brief 获取Tiger 160位校验值
 * @param[in,out] ctx 上下文
 * @param[out] digest 输出大端字节序160位校验码
 * @see tiger_init, tiger_update
 */
static inline void tiger160_final(tiger_ctx* ctx, void* digest)
{
	uint8_t buf[TIGER192_SIZE];

	tiger_final(ctx, buf);
	memcpy(digest, buf, TIGER160_SIZE);
	memset(buf, 0, sizeof(buf));
}

#ifdef __cplusplus
}
#endif	/* #ifdef __cplusplus */

#else
#error "Need int64 support"
#endif /* #ifdef INT64_SUPPORT */

/** @} */

#endif /* #ifndef __NV_TIGER__ */

