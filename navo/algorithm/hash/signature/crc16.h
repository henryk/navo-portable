/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/signature/crc16.h
 *
 * @brief Definitions and declarations for 16 bit CRC.
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_CRC16__
#define __NV_CRC16__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
extern "C" {
#endif	/* #ifdef __cplusplus */

/**
 * @defgroup CRC-16
 * @ingroup 签名哈希
 * @{
 */

/**
 * @struct crc16_ctx
 * @brief CRC-16上下文
 */
typedef struct {
	/** @brief 多项式 */
	uint16_t poly;
	/** @biref 校验码 */
	uint16_t crc;
}crc16_ctx;

#define CRC16_SIZE 2

#define CRC16_POS_CCITT 	__cpu_to_le16_c(INT16_C(0x1021))
#define CRC16_NEG_CCITT 	__cpu_to_le16_c(INT16_C(0x8428))
#define CRC16_POS_IBM 		__cpu_to_le16_c(INT16_C(0x8005))
#define CRC16_NEG_IBM 		__cpu_to_le16_c(INT16_C(0xA001))

/**
 * @def CRC16_CTX(name)
 * @brief 定义名为name的CRC16上下文
 */
#define CRC16_CTX(name) crc16_ctx name = {CRC16_POS_CCITT, 0}

/**
 * @fn void crc16_build(uint16_t* buf, const uint16_t poly);
 * @brief 建立CRC码表
 * @param[out] buf 码表缓存
 * @param[in] poly 多项式，必须是小端字节序的16位整数
 * @par 示例:
 * @code
 	crc16_ctx crc;

 	// 分配码表缓存
 	uint16_t* crc_tbl = (uint16_t*)malloc(sizeof(uint16_t) * CRC16_TBL_SIZE);
 	if (!crc_tbl)
 		return 0;

	// 建立码表
 	crc16_build(crc_tbl, CRC16_POS_IBM);

 	// 初始化上下文
 	crc16_init(&crc, 0);

 	while (buf_size >= BLOCK_SIZE) {
 		// 计算校验码
 		crc16_update(&crc, crc_tbl, buf, BLOCK_SIZE);
 		buf += BLOCK_SIZE;
 		buf_size -= BLOCK_SIZE
 	}
 	if (buf_size) {
 		// 计算校验码
 		crc16_update(&crc, crc_tbl, buf, buf_size);
 	}

 	// 返回结果
 	return crc16_final(&crc);
 * @endcode
 * @see crc16_init
 */
NV_API void crc16_build(uint16_t* buf, uint16_t poly);

/**
 * @def CRC16_SIZE
 * @brief 16位CRC码表长度
 */
#define CRC16_TBL_SIZE 	0x100

/**
 * @fn static inline void crc16_init(crc16_ctx* ctx, uint16_t poly);
 * @brief 初始化CRC上下文
 * @param[out] ctx 上下文
 * @param[in] poly 多项式，必须是小端字节序的16位整数，若为0，表示使用原poly值
 * @see crc16_build
 */
static inline void crc16_init(crc16_ctx* ctx, uint16_t poly)
{
	if (poly)
		ctx->poly = __le16_to_cpu(poly);
	ctx->crc = 0;
}

NV_API void __crc16_process(crc16_ctx* ctx, uint8_t* buf, size_t cnt);

/**
 * @def crc16_process(ctx, buf, cnt)
 * @brief 用位移的方式计算CRC
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小(按字节)
 * @note 用法同 crc16_update
 * @see crc16_init, crc16_update, crc16_final
 */
#define crc16_process(ctx, buf, cnt) \
	__crc16_process(ctx, (uint8_t*)(buf), cnt)

NV_API void __crc16_update(crc16_ctx* ctx, const uint16_t* tbl,
									uint8_t* buf, size_t cnt);

/**
 * @def crc16_update(ctx, tbl, buf, cnt)
 * @brief 用查表的方式计算CRC
 * @param[in,out] ctx 上下文
 * @param[in] tbl CRC码表
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小(按字节)
 * @note 用法同 crc16_process
 * @see crc16_init, crc16_process, crc16_final
 */
#define crc16_update(ctx, tbl, buf, cnt) \
	__crc16_update(ctx, tbl, (uint8_t*)(buf), cnt)

static inline uint16_t __crc16_final(crc16_ctx* ctx, uint16_t crc)
{
	ctx->crc = 0;
	return __cpu_to_le16(crc);
}

/**
 * @fn static inline uint16_t crc16_final(crc16_ctx* ctx);
 * @brief 获取CRC校验值
 * @param[in,out] ctx 上下文
 * @return uint16_t 返回小端字节序的校验值
 * @see crc16_init, crc16_process, crc16_update
 */
static inline uint16_t crc16_final(crc16_ctx* ctx)
{
	return __crc16_final(ctx, ctx->crc);
}

/**
 * @fn uint16_t crc16_hash_nbstr(const char* str, uint16_t* tbl);
 * @brief 直接哈希字符串
 * @param[in] str 多字节字符串
 * @param[in] tbl 码表
 * @return uint16_t 返回小端字节序的哈希值
 */
NV_API uint16_t crc16_hash_nbstr(const char* str, uint16_t* tbl);

/** @} */

#ifdef __cplusplus
}
#endif	/* #ifdef __cplusplus */

#endif /* #ifndef __NV_CRC16__ */

