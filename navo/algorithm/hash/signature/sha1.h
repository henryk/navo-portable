/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/signature/sha1.h
 * 
 * @brief Definitions and declarations for SHA-1.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_SHA1__
#define __NV_SHA1__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"

#ifdef __cplusplus
extern "C" {
#endif	/* #ifdef __cplusplus */

/**
 * @defgroup SHA-1
 * @ingroup 签名哈希
 * @{
 */

/**
 * @struct sha1_ctx
 * @brief SHA1上下文
 */
typedef struct {
	/** @brief 计数 */
	uint32_t cnt[2];
	/** @brief 摘要 */
	uint32_t digest[5];
	/** @biref 缓存 */
	uint8_t in[64];
}sha1_ctx;

#define SHA1_HASH_H0 0x67452301UL
#define SHA1_HASH_H1 0xEFCDAB89UL
#define SHA1_HASH_H2 0x98BADCFEUL
#define SHA1_HASH_H3 0x10325476UL
#define SHA1_HASH_H4 0xC3D2E1F0UL

#define SHA1_SIZE 20

/**
 * @def SHA1_CTX(name)
 * @brief 定义名为name的SHA1上下文
 */
#define SHA1_CTX(name) \
	sha1_ctx name = {{0, 0}, {						\
		SHA1_HASH_H0, SHA1_HASH_H1, SHA1_HASH_H2, 	\
		SHA1_HASH_H3, SHA1_HASH_H4}, {0}}

static inline void __sha1_init(uint32_t* digest, uint32_t* cnt)
{
	digest[0] = SHA1_HASH_H0;
	digest[1] = SHA1_HASH_H1;
	digest[2] = SHA1_HASH_H2;
	digest[3] = SHA1_HASH_H3;
	digest[4] = SHA1_HASH_H4;
	cnt[0] = 0;
	cnt[1] = 0;
}

/**
 * @fn static inline void sha1_init(sha1_ctx* ctx);
 * @brief 初始化SHA1上下文
 * @param[out] ctx 上下文
 */
static inline void sha1_init(sha1_ctx* ctx)
{
	__sha1_init(ctx->digest, ctx->cnt);
}

NV_API void __sha1_update(sha1_ctx* ctx, uint8_t* buf, size_t cnt);

/**
 * @def sha1_update(ctx, buf, cnt)
 * @brief 计算哈希值
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小
 * @par 示例:
 * @code
 	sha1_ctx ctx;
 	unsigned char digest[SHA1_SIZE];

 	// 初始化上下文
 	sha1_init(&ctx);

 	while (buf_size >= BLOCK_SIZE) {
 		// 计算校验码
 		sha1_update(&ctx, buf, BLOCK_SIZE);
 		buf += BLOCK_SIZE;
 		buf_size -= BLOCK_SIZE;
 	}
 	if (buf_size) {
 		// 计算校验码
 		sha1_update(&ctx, buf, buf_size);
 	}
 	
 	// 返回结果
 	sha1_final(&ctx, digest);
 * @endcode
 * @see sha1_init, sha1_final
 */
#define sha1_update(ctx, buf, cnt) __sha1_update(ctx, (uint8_t*)(buf), cnt)

/**
 * @fn void sha1_final(sha1_ctx* ctx, void* digest);
 * @brief 获取SHA1校验值
 * @param[in,out] ctx 上下文
 * @param[out] digest 输出小端字节序160位校验码
 * @see sha1_init, sha1_update
 */
NV_API void sha1_final(sha1_ctx* ctx, void* digest);

/** @} */

#ifdef __cplusplus
}
#endif	/* #ifdef __cplusplus */

#endif /* #ifndef __NV_SHA1__ */

