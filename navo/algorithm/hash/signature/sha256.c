/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/signature/sha256.c
 *
 * @brief Implements for SHA-256.
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#include "sha256.h"
#include "../../../port/swab.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

static const uint32_t K[64] = {
	0x428a2f98UL, 0x71374491UL, 0xb5c0fbcfUL, 0xe9b5dba5UL,
	0x3956c25bUL, 0x59f111f1UL, 0x923f82a4UL, 0xab1c5ed5UL,
	0xd807aa98UL, 0x12835b01UL, 0x243185beUL, 0x550c7dc3UL,
	0x72be5d74UL, 0x80deb1feUL, 0x9bdc06a7UL, 0xc19bf174UL,
	0xe49b69c1UL, 0xefbe4786UL, 0x0fc19dc6UL, 0x240ca1ccUL,
	0x2de92c6fUL, 0x4a7484aaUL, 0x5cb0a9dcUL, 0x76f988daUL,
	0x983e5152UL, 0xa831c66dUL, 0xb00327c8UL, 0xbf597fc7UL,
	0xc6e00bf3UL, 0xd5a79147UL, 0x06ca6351UL, 0x14292967UL,
	0x27b70a85UL, 0x2e1b2138UL, 0x4d2c6dfcUL, 0x53380d13UL,
	0x650a7354UL, 0x766a0abbUL, 0x81c2c92eUL, 0x92722c85UL,
	0xa2bfe8a1UL, 0xa81a664bUL, 0xc24b8b70UL, 0xc76c51a3UL,
	0xd192e819UL, 0xd6990624UL, 0xf40e3585UL, 0x106aa070UL,
	0x19a4c116UL, 0x1e376c08UL, 0x2748774cUL, 0x34b0bcb5UL,
	0x391c0cb3UL, 0x4ed8aa4aUL, 0x5b9cca4fUL, 0x682e6ff3UL,
	0x748f82eeUL, 0x78a5636fUL, 0x84c87814UL, 0x8cc70208UL,
	0x90befffaUL, 0xa4506cebUL, 0xbef9a3f7UL, 0xc67178f2UL
};

#define Ch(x, y, z)		((z) ^ ((x) & ((y) ^ (z))))
#define Maj(x, y, z)	(((x) & (y)) | ((z) & ((x) | (y))))

#define E0(x) (__ror32_c(x, 2) ^ __ror32_c(x, 13) ^ __ror32_c(x, 22))
#define E1(x) (__ror32_c(x, 6) ^ __ror32_c(x, 11) ^ __ror32_c(x, 25))
#define S0(x) (__ror32_c(x, 7) ^ __ror32_c(x, 18) ^ (x >>  3))
#define S1(x) (__ror32_c(x,17) ^ __ror32_c(x, 19) ^ (x >> 10))

#define ROUND(a, b, c, d, e, f, g, h, t1, t2, I) \
	(t1) = (h) + E1(e) + Ch(e, f, g) + K[i + I] + w[i + I];		\
	(t2) = E0(a) + Maj(a, b, c); (d) += (t1); (h) = (t1) + (t2);

static inline void __sha256_load_op(uint32_t* w, int i, uint32_t* in)
{
	w[i] = __be32_to_cpu(in[i]);
}

static inline void __sha256_blend_op(uint32_t* w, int i)
{
	w[i] = S1(w[i - 2]) + w[i - 7] + S0(w[i - 15]) + w[i - 16];
}

static void __sha256_transform(uint32_t* digest, uint32_t* in)
{
	uint32_t a = digest[0];
	uint32_t b = digest[1];
	uint32_t c = digest[2];
	uint32_t d = digest[3];
	uint32_t e = digest[4];
	uint32_t f = digest[5];
	uint32_t g = digest[6];
	uint32_t h = digest[7];
	uint32_t t1, t2;
	uint32_t w[64];
	int i;

	for (i = 0; i < 16; ++i)
		__sha256_load_op(w, i, in);

	for (i = 16; i < 64; ++i)
		__sha256_blend_op(w, i);

	for (i = 0; i < 64; i += 8) {
		ROUND(a, b, c, d, e, f, g, h, t1, t2, 0);
		ROUND(h, a, b, c, d, e, f, g, t1, t2, 1);
		ROUND(g, h, a, b, c, d, e, f, t1, t2, 2);
		ROUND(f, g, h, a, b, c, d, e, t1, t2, 3);
		ROUND(e, f, g, h, a, b, c, d, t1, t2, 4);
		ROUND(d, e, f, g, h, a, b, c, t1, t2, 5);
		ROUND(c, d, e, f, g, h, a, b, t1, t2, 6);
		ROUND(b, c, d, e, f, g, h, a, t1, t2, 7);
	}

	digest[0] += a; digest[1] += b;
	digest[2] += c; digest[3] += d;
	digest[4] += e; digest[5] += f;
	digest[6] += g; digest[7] += h;

	/* 为了安全考虑，擦除内存 */
	memset(w, 0, sizeof(w));
	a = 0; b = 0; c = 0; d = 0;
	e = 0; f = 0; g = 0; h = 0;
	t1 = 0; t2 = 0;
}

static inline void sha256_transform(sha256_ctx* ctx)
{
	__sha256_transform(ctx->digest, (uint32_t*)ctx->in);
}

NV_IMPL void __sha256_update(sha256_ctx* ctx, uint8_t* buf, size_t len)
{
	uint32_t offset = ctx->cnt[0] & 0x3F;
	uint32_t space = sizeof(ctx->in) - offset;

	/* 更新比特计数 */
	if ((ctx->cnt[0] += len) < len)
		++ctx->cnt[1];

	if (len < space)
		memcpy(ctx->in + offset, buf, len);
	else {
		memcpy(ctx->in + offset, buf, space);
		sha256_transform(ctx);
		buf += space;
		len -= space;

		while (len >= sizeof(ctx->in)) {
			memcpy(ctx->in, buf, sizeof(ctx->in));
			sha256_transform(ctx);
			buf += sizeof(ctx->in);
			len -= sizeof(ctx->in);
		}
		if (len)
			memcpy(ctx->in, buf, len);
	}

	/* 为了安全考虑，擦除内存 */
	offset = 0; space = 0;
}

NV_IMPL void sha256_final(sha256_ctx* ctx, void* digest)
{
	uint32_t* buf = ctx->digest;
	uint32_t* cnt = ctx->cnt;
	uint8_t padding[64] = {0x80, 0};
	uint32_t offset = ctx->cnt[0] & 0x3F;
	uint32_t bits[2];
	int padlen, i;

	padlen = (56 > offset)? 56 - offset: 120 - offset;

	bits[0] = __cpu_to_be32((cnt[1] << 3) | (cnt[0] >> 29));
	bits[1] = __cpu_to_be32(cnt[0] << 3);

	__sha256_update(ctx, padding, padlen);
	__sha256_update(ctx, (uint8_t*)bits, sizeof(bits));

	for (i = 0; i < 8; ++i)
		((uint32_t*)digest)[i] = __cpu_to_be32(buf[i]);

	/* 为了安全考虑，擦除内存 */
	memset(ctx, 0, sizeof(*ctx));
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */
