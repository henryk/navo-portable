/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/signature/sha256.h
 *
 * @brief Definitions and declarations for SHA-256.
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_SHA256__
#define __NV_SHA256__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"

#ifdef __cplusplus
#include <cstring>
extern "C" {
#else
#include <string.h>
#endif	/* #ifdef __cplusplus */

/**
 * @defgroup SHA-256_224
 * @ingroup 签名哈希
 * @{
 */

/**
 * @struct sha256_ctx
 * @brief SHA-256上下文
 * @struct sha224_ctx
 * @brief SHA-224上下文
 */
typedef struct {
	/** @brief 计数 */
	uint32_t cnt[2];
	/** @brief 摘要 */
	uint32_t digest[8];
	/** @biref 缓存 */
	uint8_t in[64];
}sha256_ctx, sha224_ctx;

#define SHA256_HASH_H0 0x6A09E667UL
#define SHA256_HASH_H1 0xBB67AE85UL
#define SHA256_HASH_H2 0x3C6EF372UL
#define SHA256_HASH_H3 0xA54FF53AUL
#define SHA256_HASH_H4 0x510E527FUL
#define SHA256_HASH_H5 0x9B05688CUL
#define SHA256_HASH_H6 0x1F83D9ABUL
#define SHA256_HASH_H7 0x5BE0CD19UL

#define SHA224_HASH_H0 0x6A09E667UL
#define SHA224_HASH_H1 0xBB67AE85UL
#define SHA224_HASH_H2 0x3C6EF372UL
#define SHA224_HASH_H3 0xA54FF53AUL
#define SHA224_HASH_H4 0x510E527FUL
#define SHA224_HASH_H5 0x9B05688CUL
#define SHA224_HASH_H6 0x1F83D9ABUL
#define SHA224_HASH_H7 0x5BE0CD19UL

#define SHA256_SIZE 32
#define SHA224_SIZE 28

/**
 * @def SHA256_CTX(name)
 * @brief 定义名为name的SHA-256上下文
 */
#define SHA256_CTX(name) \
	sha256_ctx name = {{0, 0}, {											\
		SHA256_HASH_H0, SHA256_HASH_H1, SHA256_HASH_H2, SHA256_HASH_H3,		\
		SHA256_HASH_H4, SHA256_HASH_H5, SHA256_HASH_H6, SHA256_HASH_H7}, {0}}

/**
 * @def SHA224_CTX(name)
 * @brief 定义名为name的SHA-224上下文
 */
#define SHA224_CTX(name) \
	sha256_ctx name = {{0, 0}, {											\
		SHA224_HASH_H0, SHA224_HASH_H1, SHA224_HASH_H2, SHA224_HASH_H3,		\
		SHA224_HASH_H4, SHA224_HASH_H5, SHA224_HASH_H6, SHA224_HASH_H7}, {0}}

static inline void __sha256_init(uint32_t* digest, uint32_t* cnt)
{
	digest[0] = SHA256_HASH_H0;
	digest[1] = SHA256_HASH_H1;
	digest[2] = SHA256_HASH_H2;
	digest[3] = SHA256_HASH_H3;
	digest[4] = SHA256_HASH_H4;
	digest[5] = SHA256_HASH_H5;
	digest[6] = SHA256_HASH_H6;
	digest[7] = SHA256_HASH_H7;
	cnt[0] = 0;
	cnt[1] = 0;
}

/**
 * @fn static inline void sha256_init(sha256_ctx* ctx);
 * @brief 初始化SHA256上下文
 * @param[out] ctx 上下文
 */
static inline void sha256_init(sha256_ctx* ctx)
{
	__sha256_init(ctx->digest, ctx->cnt);
}

NV_API void __sha256_update(sha256_ctx* ctx, uint8_t* buf, size_t cnt);

/**
 * @def sha256_update(ctx, buf, cnt)
 * @brief 计算哈希值
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小
 * @par 示例:
 * @code
 	sha256_ctx ctx;
 	unsigned char digest[SHA256_SIZE];

 	// 初始化上下文
 	sha256_init(&ctx);

 	while (buf_size >= BLOCK_SIZE) {
 		// 计算校验码
 		sha256_update(&ctx, buf, BLOCK_SIZE);
 		buf += BLOCK_SIZE;
 		buf_size -= BLOCK_SIZE;
 	}
 	if (buf_size) {
 		// 计算校验码
 		sha256_update(&ctx, buf, buf_size);
 	}

 	// 返回结果
 	sha256_final(&ctx, digest);
 * @endcode
 * @see sha256_init, sha256_final
 */
#define sha256_update(ctx, buf, cnt) __sha256_update(ctx, (uint8_t*)(buf), cnt)

/**
 * @fn void sha256_final(sha256_ctx* ctx, void* digest);
 * @brief 获取SHA256校验值
 * @param[in,out] ctx 上下文
 * @param[out] digest 输出大端字节序256位校验码
 * @see sha256_init, sha256_update
 */
NV_API void sha256_final(sha256_ctx* ctx, void* digest);

static inline void __sha224_init(uint32_t* digest, uint32_t* cnt)
{
	digest[0] = SHA224_HASH_H0;
	digest[1] = SHA224_HASH_H1;
	digest[2] = SHA224_HASH_H2;
	digest[3] = SHA224_HASH_H3;
	digest[4] = SHA224_HASH_H4;
	digest[5] = SHA224_HASH_H5;
	digest[6] = SHA224_HASH_H6;
	digest[7] = SHA224_HASH_H7;
	cnt[0] = 0;
	cnt[1] = 0;
}

/**
 * @fn static inline void sha224_init(sha224_ctx* ctx);
 * @brief 初始化SHA224上下文
 * @param[out] ctx 上下文
 */
static inline void sha224_init(sha224_ctx* ctx)
{
	__sha224_init(ctx->digest, ctx->cnt);
}

NV_API void __sha224_update(sha224_ctx* ctx, uint8_t* buf, size_t cnt);

/**
 * @def sha224_update(ctx, buf, cnt)
 * @brief 计算哈希值
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小
 * @par 示例:
 * @code
 	sha224_ctx ctx;
 	unsigned char digest[SHA224_SIZE];

 	// 初始化上下文
 	sha224_init(&ctx);

 	while (buf_size >= BLOCK_SIZE) {
 		// 计算校验码
 		sha224_update(&ctx, buf, BLOCK_SIZE);
 		buf += BLOCK_SIZE;
 		buf_size -= BLOCK_SIZE;
 	}
 	if (buf_size) {
 		// 计算校验码
 		sha224_update(&ctx, buf, buf_size);
 	}

 	// 返回结果
 	sha224_final(&ctx, digest);
 * @endcode
 * @see sha224_init, sha224_final
 */
#define sha224_update(ctx, buf, cnt) __sha256_update(ctx, (uint8_t*)(buf), cnt)

/**
 * @fn static inline void sha224_final(sha224_ctx* ctx, void* digest);
 * @brief 获取SHA224校验值
 * @param[in,out] ctx 上下文
 * @param[out] digest 输出大端字节序224位校验码
 * @see sha224_init, sha224_update
 */
static inline void sha224_final(sha224_ctx* ctx, void* digest)
{
	uint8_t t[32];

	sha256_final(ctx, t);
	memcpy(digest, t, 28);
	memset(t, 0, 32);
}

/** @} */

#ifdef __cplusplus
}
#endif	/* #ifdef __cplusplus */

#endif /* #ifndef __NV_SHA256__ */
