/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/signature/md5.c
 * 
 * @brief Implements for MD5.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#include "md5.h"
#include "../../../port/swab.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
#include <cstring>
extern "C" {
#else
#include <string.h>
#endif /* #ifdef __cplusplus */

#define F1(x, y, z) ((z) ^ ((x) & ((y) ^ (z))))
#define F2(x, y, z) F1(z, x, y)
#define F3(x, y, z) ((x) ^ (y) ^ (z))
#define F4(x, y, z) ((y) ^ ((x) | ~(z)))

#define STEP(fn, w, x, y, z, in, c, s) \
	((w) += (fn(x, y, z) + (in) + (c)), (w) = __rol32_c(w, s) + (x))

static void md5_transform(md5_ctx* ctx)
{
	uint32_t a = ctx->digest[0];
	uint32_t b = ctx->digest[1];
	uint32_t c = ctx->digest[2];
	uint32_t d = ctx->digest[3];
	uint32_t* in = (uint32_t*)(ctx->in);

	STEP(F1, a, b, c, d, in[0],  0xd76aa478,  7);
	STEP(F1, d, a, b, c, in[1],  0xe8c7b756, 12);
	STEP(F1, c, d, a, b, in[2],  0x242070db, 17);
	STEP(F1, b, c, d, a, in[3],  0xc1bdceee, 22);
	STEP(F1, a, b, c, d, in[4],  0xf57c0faf,  7);
	STEP(F1, d, a, b, c, in[5],  0x4787c62a, 12);
	STEP(F1, c, d, a, b, in[6],  0xa8304613, 17);
	STEP(F1, b, c, d, a, in[7],  0xfd469501, 22);
	STEP(F1, a, b, c, d, in[8],  0x698098d8,  7);
	STEP(F1, d, a, b, c, in[9],  0x8b44f7af, 12);
	STEP(F1, c, d, a, b, in[10], 0xffff5bb1, 17);
	STEP(F1, b, c, d, a, in[11], 0x895cd7be, 22);
	STEP(F1, a, b, c, d, in[12], 0x6b901122,  7);
	STEP(F1, d, a, b, c, in[13], 0xfd987193, 12);
	STEP(F1, c, d, a, b, in[14], 0xa679438e, 17);
	STEP(F1, b, c, d, a, in[15], 0x49b40821, 22);
	
	STEP(F2, a, b, c, d, in[1],  0xf61e2562,  5);
	STEP(F2, d, a, b, c, in[6],  0xc040b340,  9);
	STEP(F2, c, d, a, b, in[11], 0x265e5a51, 14);
	STEP(F2, b, c, d, a, in[0],  0xe9b6c7aa, 20);
	STEP(F2, a, b, c, d, in[5],  0xd62f105d,  5);
	STEP(F2, d, a, b, c, in[10], 0x02441453,  9);
	STEP(F2, c, d, a, b, in[15], 0xd8a1e681, 14);
	STEP(F2, b, c, d, a, in[4],  0xe7d3fbc8, 20);
	STEP(F2, a, b, c, d, in[9],  0x21e1cde6,  5);
	STEP(F2, d, a, b, c, in[14], 0xc33707d6,  9);
	STEP(F2, c, d, a, b, in[3],  0xf4d50d87, 14);
	STEP(F2, b, c, d, a, in[8],  0x455a14ed, 20);
	STEP(F2, a, b, c, d, in[13], 0xa9e3e905,  5);
	STEP(F2, d, a, b, c, in[2],  0xfcefa3f8,  9);
	STEP(F2, c, d, a, b, in[7],  0x676f02d9, 14);
	STEP(F2, b, c, d, a, in[12], 0x8d2a4c8a, 20);
	
	STEP(F3, a, b, c, d, in[5],  0xfffa3942,  4);
	STEP(F3, d, a, b, c, in[8],  0x8771f681, 11);
	STEP(F3, c, d, a, b, in[11], 0x6d9d6122, 16);
	STEP(F3, b, c, d, a, in[14], 0xfde5380c, 23);
	STEP(F3, a, b, c, d, in[1],  0xa4beea44,  4);
	STEP(F3, d, a, b, c, in[4],  0x4bdecfa9, 11);
	STEP(F3, c, d, a, b, in[7],  0xf6bb4b60, 16);
	STEP(F3, b, c, d, a, in[10], 0xbebfbc70, 23);
	STEP(F3, a, b, c, d, in[13], 0x289b7ec6,  4);
	STEP(F3, d, a, b, c, in[0],  0xeaa127fa, 11);
	STEP(F3, c, d, a, b, in[3],  0xd4ef3085, 16);
	STEP(F3, b, c, d, a, in[6],  0x04881d05, 23);
	STEP(F3, a, b, c, d, in[9],  0xd9d4d039,  4);
	STEP(F3, d, a, b, c, in[12], 0xe6db99e5, 11);
	STEP(F3, c, d, a, b, in[15], 0x1fa27cf8, 16);
	STEP(F3, b, c, d, a, in[2],  0xc4ac5665, 23);
	
	STEP(F4, a, b, c, d, in[0],  0xf4292244,  6);
	STEP(F4, d, a, b, c, in[7],  0x432aff97, 10);
	STEP(F4, c, d, a, b, in[14], 0xab9423a7, 15);
	STEP(F4, b, c, d, a, in[5],  0xfc93a039, 21);
	STEP(F4, a, b, c, d, in[12], 0x655b59c3,  6);
	STEP(F4, d, a, b, c, in[3],  0x8f0ccc92, 10);
	STEP(F4, c, d, a, b, in[10], 0xffeff47d, 15);
	STEP(F4, b, c, d, a, in[1],  0x85845dd1, 21);
	STEP(F4, a, b, c, d, in[8],  0x6fa87e4f,  6);
	STEP(F4, d, a, b, c, in[15], 0xfe2ce6e0, 10);
	STEP(F4, c, d, a, b, in[6],  0xa3014314, 15);
	STEP(F4, b, c, d, a, in[13], 0x4e0811a1, 21);
	STEP(F4, a, b, c, d, in[4],  0xf7537e82,  6);
	STEP(F4, d, a, b, c, in[11], 0xbd3af235, 10);
	STEP(F4, c, d, a, b, in[2],  0x2ad7d2bb, 15);
	STEP(F4, b, c, d, a, in[9],  0xeb86d391, 21);

	ctx->digest[0] += a;
	ctx->digest[1] += b;
	ctx->digest[2] += c;
	ctx->digest[3] += d;

	/* 为了安全考虑，擦除内存 */
	a = 0;b = 0;c = 0;d = 0;
	in = NULL;
}

NV_IMPL void __md5_update(md5_ctx* ctx, uint8_t* buf, size_t len)
{
	uint32_t offset = ((ctx->cnt[0] >> 3) & 0x3F);
	uint32_t space = sizeof(ctx->in) - offset;

	/* 更新比特计数 */
	if ((ctx->cnt[0] += (len << 3)) < (len << 3))
		++ctx->cnt[1];
	ctx->cnt[1] += (len >> 29);

	if (len < space)
		memcpy(ctx->in + offset, buf, len);
	else {
		memcpy(ctx->in + offset, buf, space);
		__le32_to_cpu_array(ctx->in, 16);
		md5_transform(ctx);
		buf += space;
		len -= space;

		while (len >= sizeof(ctx->in)) {
			memcpy(ctx->in, buf, sizeof(ctx->in));
			__le32_to_cpu_array(ctx->in, 16);
			md5_transform(ctx);
			buf += sizeof(ctx->in);
			len -= sizeof(ctx->in);
		}
		if (len)
			memcpy(ctx->in, buf, len);
	}

	/* 为了安全考虑，擦除内存 */
	offset = 0; space = 0;
}

NV_IMPL void md5_final(md5_ctx* ctx, void* digest)
{
	uint8_t padding[64] = {0x80, 0};
	uint32_t offset = (ctx->cnt[0] >> 3) & 0x3F;
	int padlen;
	
	if (56 > offset)
		padlen = 56 - offset;
	else
		padlen = 120 - offset;

	((uint32_t*)ctx->in)[14] = ctx->cnt[0];
	((uint32_t*)ctx->in)[15] = ctx->cnt[1];

	md5_update(ctx, padding, padlen);
	__le32_to_cpu_array(ctx->in, 14);
	md5_transform(ctx);
	__cpu_to_le32_array(ctx->digest, 4);

	memcpy(digest, ctx->digest, sizeof(ctx->digest));

	/* 为了安全考虑，擦除内存 */
	memset(ctx, 0, sizeof(*ctx));
	memset(padding, 0, sizeof(padding));
	offset = 0; padlen = 0;
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

