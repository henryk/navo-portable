/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/signature/ipcs.h
 *
 * @brief Definitions and declarations for TCP/IP checksum.
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_IPCS__
#define __NV_IPCS__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
extern "C" {
#endif	/* #ifdef __cplusplus */

/**
 * @defgroup TCP/IP-checksum
 * @ingroup 签名哈希
 * @{
 */

/**
 * @struct ipcs_ctx
 * @brief TCP/IP checksum上下文
 */
typedef struct {
	/** @biref 校验码 */
	uint32_t sum;
}ipcs_ctx;

#define IPCS_SIZE 2

/**
 * @def IPCS_CTX(name)
 * @brief 定义名为name的TCP/IP checksum上下文
 */
#define IPCS_CTX(name) ipcs_ctx name = {0}

/**
 * @fn static inline void ipcs_init(ipcs_ctx* ctx);
 * @brief 初始化TCP/IP checksum上下文
 * @param[out] ctx 上下文
 * @see ipcs_update, ipcs_final
 */
static inline void ipcs_init(ipcs_ctx* ctx)
{
	ctx->sum = 0;
}

NV_API uint32_t __ipcs_update(uint32_t sum, uint16_t* buf, size_t words);

/**
 * @fn static inline ipcs_update(ipcs_ctx* ctx, void* buf, size_t cnt);
 * @brief 计算TCP/IP校验码
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小(按字节)
 * @see ipcs_init, ipcs_final
 */
static inline ipcs_update(ipcs_ctx* ctx, void* buf, size_t cnt)
{
	ctx->sum = __ipcs_update(ctx->sum, (uint16_t*)buf, cnt >> 1);
}

static inline uint32_t __ipcs_final(ipcs_ctx* ctx, uint32_t sum)
{
	ctx->sum = 0;
	
	sum = (sum >> 16) + (sum & 0xFFFF);
	sum += (sum >> 16);
	
	return ~sum;
}

/**
 * @fn static inline uint16_t ipcs_final(ipcs_ctx* ctx);
 * @brief 获取CRC校验值
 * @param[in,out] ctx 上下文
 * @return uint16_t 返回网络字节序的校验值
 * @see ipcs_init, ipcs_update
 */
static inline uint16_t ipcs_final(ipcs_ctx* ctx)
{
	return (uint16_t)__ipcs_final(ctx, ctx->sum);
}

/** @} */

#ifdef __cplusplus
}
#endif	/* #ifdef __cplusplus */

#endif /* #ifndef __NV_IPCS__ */

