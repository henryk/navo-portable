/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/hash/signature/md5.h
 * 
 * @brief Definitions and declarations for MD5.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_MD5__
#define __NV_MD5__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"

#ifdef __cplusplus
extern "C" {
#endif	/* #ifdef __cplusplus */

/**
 * @defgroup MD5
 * @ingroup 签名哈希
 * @{
 */

/**
 * @struct md5_ctx
 * @brief MD5上下文
 */
typedef struct {
	/** @brief 计数 */
	uint32_t cnt[2];
	/** @brief 摘要 */
	uint32_t digest[4];
	/** @biref 缓存 */
	uint8_t in[64];
}md5_ctx;

#define MD5_SIZE 16

/**
 * @def MD5_CTX(name)
 * @brief 定义名为name的MD5上下文
 */
#define MD5_CTX(name) \
	md5_ctx name = {{0, 0}, {				\
		0x67452301UL, 0xEFCDAB89UL, 		\
		0x98BADCFEUL, 0x10325476UL}, {0}}

static inline void __md5_init(uint32_t* digest, uint32_t* cnt)
{
	digest[0] = 0x67452301UL;
	digest[1] = 0xEFCDAB89UL;
	digest[2] = 0x98BADCFEUL;
	digest[3] = 0x10325476UL;
	cnt[0] = 0;
	cnt[1] = 0;
}

/**
 * @fn static inline void md5_init(md5_ctx* ctx);
 * @brief 初始化MD5上下文
 * @param[out] ctx 上下文
 */
static inline void md5_init(md5_ctx* ctx)
{
	__md5_init(ctx->digest, ctx->cnt);
}

NV_API void __md5_update(md5_ctx* ctx, uint8_t* buf, size_t cnt);

/**
 * @def md5_update(ctx, buf, cnt)
 * @brief 计算哈希值
 * @param[in,out] ctx 上下文
 * @param[in] buf 输入缓存
 * @param[in] cnt 缓存大小
 * @par 示例:
 * @code
 	md5_ctx ctx;
 	unsigned char digest[MD5_SIZE];

 	// 初始化上下文
 	md5_init(&ctx);

 	while (buf_size >= BLOCK_SIZE) {
 		// 计算校验码
 		md5_update(&ctx, buf, BLOCK_SIZE);
 		buf += BLOCK_SIZE;
 		buf_size -= BLOCK_SIZE;
 	}
 	if (buf_size) {
 		// 计算校验码
 		md5_update(&ctx, buf, buf_size);
 	}
 	
 	// 返回结果
 	md5_final(&ctx, digest);
 * @endcode
 * @see md5_init, md5_final
 */
#define md5_update(ctx, buf, cnt) __md5_update(ctx, (uint8_t*)(buf), cnt)

/**
 * @fn void md5_final(md5_ctx* ctx, void* digest);
 * @brief 获取MD5校验值
 * @param[in,out] ctx 上下文
 * @param[out] digest 输出小端字节序128位校验码
 * @see md5_init, md5_update
 */
NV_API void md5_final(md5_ctx* ctx, void* digest);

/** @} */

#ifdef __cplusplus
}
#endif	/* #ifdef __cplusplus */

#endif /* #ifndef __NV_MD5__ */

