/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/crypto/symmetric/cast6.c
 *
 * @brief Cast6.
 *
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#include "cast6.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
#include <cstring>
extern "C" {
#else
#include <string.h>
#endif /* #ifdef __cplusplus */

static const uint32_t TM[24][8] = {
 {	0x5a827999UL, 0xc95c653aUL, 0x383650dbUL, 0xa7103c7cUL,
	0x15ea281dUL, 0x84c413beUL, 0xf39dff5fUL, 0x6277eb00UL },
 {	0xd151d6a1UL, 0x402bc242UL, 0xaf05ade3UL, 0x1ddf9984UL, 
 	0x8cb98525UL, 0xfb9370c6UL, 0x6a6d5c67UL, 0xd9474808UL },
 {	0x482133a9UL, 0xb6fb1f4aUL, 0x25d50aebUL, 0x94aef68cUL, 
 	0x0388e22dUL, 0x7262cdceUL, 0xe13cb96fUL, 0x5016a510UL },
 {	0xbef090b1UL, 0x2dca7c52UL, 0x9ca467f3UL, 0x0b7e5394UL, 
 	0x7a583f35UL, 0xe9322ad6UL, 0x580c1677UL, 0xc6e60218UL },
 {	0x35bfedb9UL, 0xa499d95aUL, 0x1373c4fbUL, 0x824db09cUL, 
 	0xf1279c3dUL, 0x600187deUL, 0xcedb737fUL, 0x3db55f20UL },
 {	0xac8f4ac1UL, 0x1b693662UL, 0x8a432203UL, 0xf91d0da4UL, 
 	0x67f6f945UL, 0xd6d0e4e6UL, 0x45aad087UL, 0xb484bc28UL },
 {	0x235ea7c9UL, 0x9238936aUL, 0x01127f0bUL, 0x6fec6aacUL, 
 	0xdec6564dUL, 0x4da041eeUL, 0xbc7a2d8fUL, 0x2b541930UL },
 {	0x9a2e04d1UL, 0x0907f072UL, 0x77e1dc13UL, 0xe6bbc7b4UL, 
 	0x5595b355UL, 0xc46f9ef6UL, 0x33498a97UL, 0xa2237638UL },
 {	0x10fd61d9UL, 0x7fd74d7aUL, 0xeeb1391bUL, 0x5d8b24bcUL, 
 	0xcc65105dUL, 0x3b3efbfeUL, 0xaa18e79fUL, 0x18f2d340UL },
 {	0x87ccbee1UL, 0xf6a6aa82UL, 0x65809623UL, 0xd45a81c4UL, 
 	0x43346d65UL, 0xb20e5906UL, 0x20e844a7UL, 0x8fc23048UL },
 {	0xfe9c1be9UL, 0x6d76078aUL, 0xdc4ff32bUL, 0x4b29deccUL, 
 	0xba03ca6dUL, 0x28ddb60eUL, 0x97b7a1afUL, 0x06918d50UL },
 {	0x756b78f1UL, 0xe4456492UL, 0x531f5033UL, 0xc1f93bd4UL, 
 	0x30d32775UL, 0x9fad1316UL, 0x0e86feb7UL, 0x7d60ea58UL },
 {	0xec3ad5f9UL, 0x5b14c19aUL, 0xc9eead3bUL, 0x38c898dcUL, 
 	0xa7a2847dUL, 0x167c701eUL, 0x85565bbfUL, 0xf4304760UL },
 {	0x630a3301UL, 0xd1e41ea2UL, 0x40be0a43UL, 0xaf97f5e4UL, 
 	0x1e71e185UL, 0x8d4bcd26UL, 0xfc25b8c7UL, 0x6affa468UL },
 {	0xd9d99009UL, 0x48b37baaUL, 0xb78d674bUL, 0x266752ecUL, 
 	0x95413e8dUL, 0x041b2a2eUL, 0x72f515cfUL, 0xe1cf0170UL },
 {	0x50a8ed11UL, 0xbf82d8b2UL, 0x2e5cc453UL, 0x9d36aff4UL, 
 	0x0c109b95UL, 0x7aea8736UL, 0xe9c472d7UL, 0x589e5e78UL },
 {	0xc7784a19UL, 0x365235baUL, 0xa52c215bUL, 0x14060cfcUL, 
 	0x82dff89dUL, 0xf1b9e43eUL, 0x6093cfdfUL, 0xcf6dbb80UL },
 {	0x3e47a721UL, 0xad2192c2UL, 0x1bfb7e63UL, 0x8ad56a04UL, 
 	0xf9af55a5UL, 0x68894146UL, 0xd7632ce7UL, 0x463d1888UL },
 {	0xb5170429UL, 0x23f0efcaUL, 0x92cadb6bUL, 0x01a4c70cUL, 
 	0x707eb2adUL, 0xdf589e4eUL, 0x4e3289efUL, 0xbd0c7590UL },
 {	0x2be66131UL, 0x9ac04cd2UL, 0x099a3873UL, 0x78742414UL, 
 	0xe74e0fb5UL, 0x5627fb56UL, 0xc501e6f7UL, 0x33dbd298UL },
 {	0xa2b5be39UL, 0x118fa9daUL, 0x8069957bUL, 0xef43811cUL, 
 	0x5e1d6cbdUL, 0xccf7585eUL, 0x3bd143ffUL, 0xaaab2fa0UL },
 {	0x19851b41UL, 0x885f06e2UL, 0xf738f283UL, 0x6612de24UL, 
 	0xd4ecc9c5UL, 0x43c6b566UL, 0xb2a0a107UL, 0x217a8ca8UL },
 {	0x90547849UL, 0xff2e63eaUL, 0x6e084f8bUL, 0xdce23b2cUL, 
 	0x4bbc26cdUL, 0xba96126eUL, 0x296ffe0fUL, 0x9849e9b0UL },
 {	0x0723d551UL, 0x75fdc0f2UL, 0xe4d7ac93UL, 0x53b19834UL, 
 	0xc28b83d5UL, 0x31656f76UL, 0xa03f5b17UL, 0x0f1946b8UL }
 };

static const uint8_t TR[4][8] = {
 { 0x13, 0x04, 0x15, 0x06, 0x17, 0x08, 0x19, 0x0a },
 { 0x1b, 0x0c, 0x1d, 0x0e, 0x1f, 0x10, 0x01, 0x12 },
 { 0x03, 0x14, 0x05, 0x16, 0x07, 0x18, 0x09, 0x1a },
 { 0x0b, 0x1c, 0x0d, 0x1e, 0x0f, 0x00, 0x11, 0x02 }
};

extern const uint32_t __cast_s1[256];
extern const uint32_t __cast_s2[256];
extern const uint32_t __cast_s3[256];
extern const uint32_t __cast_s4[256];

#define S1 __cast_s1
#define S2 __cast_s2
#define S3 __cast_s3
#define S4 __cast_s4

#define FN(i, r) ((i) = __rol32((i), (r)), \
	((S1[(i) >> 24] ^ S2[((i) >> 16) & 0xFF]) - \
	S3[((i) >> 8) & 0xFF] + S4[(i) & 0xFF]))

#define F1(i, d, m, r) ((i) = ((m) + (d)), FN(i, r))
#define F2(i, d, m, r) ((i) = ((m) ^ (d)), FN(i, r))
#define F3(i, d, m, r) ((i) = ((m) - (d)), FN(i, r))

#define ROUND1(i, l, r, t, c) \
	(t) = (l); (l) = (r); (r) = (t) ^ F1(i, r, km[c], kr[c])
#define ROUND2(i, l, r, t, c) \
	(t) = (l); (l) = (r); (r) = (t) ^ F2(i, r, km[c], kr[c])
#define ROUND3(i, l, r, t, c) \
	(t) = (l); (l) = (r); (r) = (t) ^ F3(i, r, km[c], kr[c])

#define XI(i) ((x[(i) >> 2] >> ((3 - ((i) & 3)) << 3)) & 0xFF)
#define ZI(i) ((z[(i) >> 2] >> ((3 - ((i) & 3)) << 3)) & 0xFF)

static inline void W(uint32_t* key, size_t i)
{
	uint32_t I;

	key[6] ^= F1(I, key[7], TR[i & 3][0], TM[i][0]);
	key[5] ^= F2(I, key[6], TR[i & 3][1], TM[i][1]);
	key[4] ^= F3(I, key[5], TR[i & 3][2], TM[i][2]);
	key[3] ^= F1(I, key[4], TR[i & 3][3], TM[i][3]);
	key[2] ^= F2(I, key[3], TR[i & 3][4], TM[i][4]);
	key[1] ^= F3(I, key[2], TR[i & 3][5], TM[i][5]);
	key[0] ^= F1(I, key[1], TR[i & 3][6], TM[i][6]);
	key[7] ^= F2(I, key[0], TR[i & 3][7], TM[i][7]);
}

static inline void Q(uint32_t* blk, uint32_t* km, uint8_t* kr)
{
	uint32_t I;

	blk[2] ^= F1(I, blk[3], km[0], kr[0]);
	blk[1] ^= F2(I, blk[2], km[1], kr[1]);
	blk[0] ^= F3(I, blk[1], km[2], kr[2]);
	blk[3] ^= F1(I, blk[0], km[3], kr[3]);
}

static inline void QBAR(uint32_t* blk, uint32_t* km, uint8_t* kr)
{
	uint32_t I;

	blk[3] ^= F1(I, blk[0], km[3], kr[3]);
	blk[0] ^= F3(I, blk[1], km[2], kr[2]);
	blk[1] ^= F2(I, blk[2], km[1], kr[1]);
	blk[2] ^= F1(I, blk[3], km[0], kr[0]);
}

NV_IMPL int __cast6_set_key(uint32_t (*km)[4], uint8_t (*kr)[4],
								uint8_t* src_key, size_t cnt)
{
	uint32_t k[8];
	uint32_t _k[8] = {0, 0, 0, 0, 0, 0, 0, 0};
	int i;

	if (cnt & 3)
		return 0;

	memcpy(_k, src_key, cnt);

	k[0] = __be32_to_cpu(_k[0]);
	k[1] = __be32_to_cpu(_k[1]);
	k[2] = __be32_to_cpu(_k[2]);
	k[3] = __be32_to_cpu(_k[3]);
	k[4] = __be32_to_cpu(_k[4]);
	k[5] = __be32_to_cpu(_k[5]);
	k[6] = __be32_to_cpu(_k[6]);
	k[7] = __be32_to_cpu(_k[7]);

	for (i = 0; i < 12; ++i) {
		W(k, (i << 1));
		W(k, (i << 1) + 1);

		kr[i][0] = k[0] & 0x1F;
		kr[i][1] = k[2] & 0x1F;
		kr[i][2] = k[4] & 0x1F;
		kr[i][3] = k[6] & 0x1F;

		kr[i][0] = k[7];
		kr[i][1] = k[5];
		kr[i][2] = k[3];
		kr[i][3] = k[1];
	}

	return 1;
}

NV_IMPL void __cast6_encrypt(uint32_t (*km)[4], uint8_t (*kr)[4],
							uint32_t* dst, uint32_t* src)
{
	uint32_t blk[4];
	uint32_t* pm;
	uint8_t* pr;

	blk[0] = __be32_to_cpu(src[0]);
	blk[1] = __be32_to_cpu(src[1]);
	blk[2] = __be32_to_cpu(src[2]);
	blk[3] = __be32_to_cpu(src[3]);

	pm = km[0]; pr = kr[0]; Q(blk, pm, pr);
	pm = km[1]; pr = kr[1]; Q(blk, pm, pr);
	pm = km[2]; pr = kr[2]; Q(blk, pm, pr);
	pm = km[3]; pr = kr[3]; Q(blk, pm, pr);
	pm = km[4]; pr = kr[4]; Q(blk, pm, pr);
	pm = km[5]; pr = kr[5]; Q(blk, pm, pr);

	pm = km[ 6]; pr = kr[ 6]; QBAR(blk, pm, pr);
	pm = km[ 7]; pr = kr[ 7]; QBAR(blk, pm, pr);
	pm = km[ 8]; pr = kr[ 8]; QBAR(blk, pm, pr);
	pm = km[ 9]; pr = kr[ 9]; QBAR(blk, pm, pr);
	pm = km[10]; pr = kr[10]; QBAR(blk, pm, pr);
	pm = km[11]; pr = kr[11]; QBAR(blk, pm, pr);

	dst[0] = __cpu_to_be32(blk[0]);
	dst[1] = __cpu_to_be32(blk[1]);
	dst[2] = __cpu_to_be32(blk[2]);
	dst[3] = __cpu_to_be32(blk[3]);
}

NV_IMPL void __cast6_decrypt(uint32_t (*km)[4], uint8_t (*kr)[4],
							uint32_t* dst, uint32_t* src)
{
	uint32_t blk[4];
	uint32_t* pm;
	uint8_t* pr;

	blk[0] = __be32_to_cpu(src[0]);
	blk[1] = __be32_to_cpu(src[1]);
	blk[2] = __be32_to_cpu(src[2]);
	blk[3] = __be32_to_cpu(src[3]);

	pm = km[11]; pr = kr[11]; Q(blk, pm, pr);
	pm = km[10]; pr = kr[10]; Q(blk, pm, pr);
	pm = km[ 9]; pr = kr[ 9]; Q(blk, pm, pr);
	pm = km[ 8]; pr = kr[ 8]; Q(blk, pm, pr);
	pm = km[ 7]; pr = kr[ 7]; Q(blk, pm, pr);
	pm = km[ 6]; pr = kr[ 6]; Q(blk, pm, pr);

	pm = km[5]; pr = kr[5]; QBAR(blk, pm, pr);
	pm = km[4]; pr = kr[4]; QBAR(blk, pm, pr);
	pm = km[3]; pr = kr[3]; QBAR(blk, pm, pr);
	pm = km[2]; pr = kr[2]; QBAR(blk, pm, pr);
	pm = km[1]; pr = kr[1]; QBAR(blk, pm, pr);
	pm = km[0]; pr = kr[0]; QBAR(blk, pm, pr);

	dst[0] = __cpu_to_be32(blk[0]);
	dst[1] = __cpu_to_be32(blk[1]);
	dst[2] = __cpu_to_be32(blk[2]);
	dst[3] = __cpu_to_be32(blk[3]);
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

