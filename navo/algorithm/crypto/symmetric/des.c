/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/crypto/symmetric/des.c
 *
 * @brief DES.
 *
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#include "des.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

static const uint32_t SB1[64] = {
	0x01010400UL, 0x00000000UL, 0x00010000UL, 0x01010404UL,
	0x01010004UL, 0x00010404UL, 0x00000004UL, 0x00010000UL,
	0x00000400UL, 0x01010400UL, 0x01010404UL, 0x00000400UL,
	0x01000404UL, 0x01010004UL, 0x01000000UL, 0x00000004UL,
	0x00000404UL, 0x01000400UL, 0x01000400UL, 0x00010400UL,
	0x00010400UL, 0x01010000UL, 0x01010000UL, 0x01000404UL,
	0x00010004UL, 0x01000004UL, 0x01000004UL, 0x00010004UL,
	0x00000000UL, 0x00000404UL, 0x00010404UL, 0x01000000UL,
	0x00010000UL, 0x01010404UL, 0x00000004UL, 0x01010000UL,
	0x01010400UL, 0x01000000UL, 0x01000000UL, 0x00000400UL,
	0x01010004UL, 0x00010000UL, 0x00010400UL, 0x01000004UL,
	0x00000400UL, 0x00000004UL, 0x01000404UL, 0x00010404UL,
	0x01010404UL, 0x00010004UL, 0x01010000UL, 0x01000404UL,
	0x01000004UL, 0x00000404UL, 0x00010404UL, 0x01010400UL,
	0x00000404UL, 0x01000400UL, 0x01000400UL, 0x00000000UL,
	0x00010004UL, 0x00010400UL, 0x00000000UL, 0x01010004UL
};

static const uint32_t SB2[64] = {
	0x80108020UL, 0x80008000UL, 0x00008000UL, 0x00108020UL,
	0x00100000UL, 0x00000020UL, 0x80100020UL, 0x80008020UL,
	0x80000020UL, 0x80108020UL, 0x80108000UL, 0x80000000UL,
	0x80008000UL, 0x00100000UL, 0x00000020UL, 0x80100020UL,
	0x00108000UL, 0x00100020UL, 0x80008020UL, 0x00000000UL,
	0x80000000UL, 0x00008000UL, 0x00108020UL, 0x80100000UL,
	0x00100020UL, 0x80000020UL, 0x00000000UL, 0x00108000UL,
	0x00008020UL, 0x80108000UL, 0x80100000UL, 0x00008020UL,
	0x00000000UL, 0x00108020UL, 0x80100020UL, 0x00100000UL,
	0x80008020UL, 0x80100000UL, 0x80108000UL, 0x00008000UL,
	0x80100000UL, 0x80008000UL, 0x00000020UL, 0x80108020UL,
	0x00108020UL, 0x00000020UL, 0x00008000UL, 0x80000000UL,
	0x00008020UL, 0x80108000UL, 0x00100000UL, 0x80000020UL,
	0x00100020UL, 0x80008020UL, 0x80000020UL, 0x00100020UL,
	0x00108000UL, 0x00000000UL, 0x80008000UL, 0x00008020UL,
	0x80000000UL, 0x80100020UL, 0x80108020UL, 0x00108000UL
};

static const uint32_t SB3[64] = {
	0x00000208UL, 0x08020200UL, 0x00000000UL, 0x08020008UL,
	0x08000200UL, 0x00000000UL, 0x00020208UL, 0x08000200UL,
	0x00020008UL, 0x08000008UL, 0x08000008UL, 0x00020000UL,
	0x08020208UL, 0x00020008UL, 0x08020000UL, 0x00000208UL,
	0x08000000UL, 0x00000008UL, 0x08020200UL, 0x00000200UL,
	0x00020200UL, 0x08020000UL, 0x08020008UL, 0x00020208UL,
	0x08000208UL, 0x00020200UL, 0x00020000UL, 0x08000208UL,
	0x00000008UL, 0x08020208UL, 0x00000200UL, 0x08000000UL,
	0x08020200UL, 0x08000000UL, 0x00020008UL, 0x00000208UL,
	0x00020000UL, 0x08020200UL, 0x08000200UL, 0x00000000UL,
	0x00000200UL, 0x00020008UL, 0x08020208UL, 0x08000200UL,
	0x08000008UL, 0x00000200UL, 0x00000000UL, 0x08020008UL,
	0x08000208UL, 0x00020000UL, 0x08000000UL, 0x08020208UL,
	0x00000008UL, 0x00020208UL, 0x00020200UL, 0x08000008UL,
	0x08020000UL, 0x08000208UL, 0x00000208UL, 0x08020000UL,
	0x00020208UL, 0x00000008UL, 0x08020008UL, 0x00020200UL
};

static const uint32_t SB4[64] = {
	0x00802001UL, 0x00002081UL, 0x00002081UL, 0x00000080UL,
	0x00802080UL, 0x00800081UL, 0x00800001UL, 0x00002001UL,
	0x00000000UL, 0x00802000UL, 0x00802000UL, 0x00802081UL,
	0x00000081UL, 0x00000000UL, 0x00800080UL, 0x00800001UL,
	0x00000001UL, 0x00002000UL, 0x00800000UL, 0x00802001UL,
	0x00000080UL, 0x00800000UL, 0x00002001UL, 0x00002080UL,
	0x00800081UL, 0x00000001UL, 0x00002080UL, 0x00800080UL,
	0x00002000UL, 0x00802080UL, 0x00802081UL, 0x00000081UL,
	0x00800080UL, 0x00800001UL, 0x00802000UL, 0x00802081UL,
	0x00000081UL, 0x00000000UL, 0x00000000UL, 0x00802000UL,
	0x00002080UL, 0x00800080UL, 0x00800081UL, 0x00000001UL,
	0x00802001UL, 0x00002081UL, 0x00002081UL, 0x00000080UL,
	0x00802081UL, 0x00000081UL, 0x00000001UL, 0x00002000UL,
	0x00800001UL, 0x00002001UL, 0x00802080UL, 0x00800081UL,
	0x00002001UL, 0x00002080UL, 0x00800000UL, 0x00802001UL,
	0x00000080UL, 0x00800000UL, 0x00002000UL, 0x00802080UL
};

static const uint32_t SB5[64] = {
	0x00000100UL, 0x02080100UL, 0x02080000UL, 0x42000100UL,
	0x00080000UL, 0x00000100UL, 0x40000000UL, 0x02080000UL,
	0x40080100UL, 0x00080000UL, 0x02000100UL, 0x40080100UL,
	0x42000100UL, 0x42080000UL, 0x00080100UL, 0x40000000UL,
	0x02000000UL, 0x40080000UL, 0x40080000UL, 0x00000000UL,
	0x40000100UL, 0x42080100UL, 0x42080100UL, 0x02000100UL,
	0x42080000UL, 0x40000100UL, 0x00000000UL, 0x42000000UL,
	0x02080100UL, 0x02000000UL, 0x42000000UL, 0x00080100UL,
	0x00080000UL, 0x42000100UL, 0x00000100UL, 0x02000000UL,
	0x40000000UL, 0x02080000UL, 0x42000100UL, 0x40080100UL,
	0x02000100UL, 0x40000000UL, 0x42080000UL, 0x02080100UL,
	0x40080100UL, 0x00000100UL, 0x02000000UL, 0x42080000UL,
	0x42080100UL, 0x00080100UL, 0x42000000UL, 0x42080100UL,
	0x02080000UL, 0x00000000UL, 0x40080000UL, 0x42000000UL,
	0x00080100UL, 0x02000100UL, 0x40000100UL, 0x00080000UL,
	0x00000000UL, 0x40080000UL, 0x02080100UL, 0x40000100UL
};

static const uint32_t SB6[64] = {
	0x20000010UL, 0x20400000UL, 0x00004000UL, 0x20404010UL,
	0x20400000UL, 0x00000010UL, 0x20404010UL, 0x00400000UL,
	0x20004000UL, 0x00404010UL, 0x00400000UL, 0x20000010UL,
	0x00400010UL, 0x20004000UL, 0x20000000UL, 0x00004010UL,
	0x00000000UL, 0x00400010UL, 0x20004010UL, 0x00004000UL,
	0x00404000UL, 0x20004010UL, 0x00000010UL, 0x20400010UL,
	0x20400010UL, 0x00000000UL, 0x00404010UL, 0x20404000UL,
	0x00004010UL, 0x00404000UL, 0x20404000UL, 0x20000000UL,
	0x20004000UL, 0x00000010UL, 0x20400010UL, 0x00404000UL,
	0x20404010UL, 0x00400000UL, 0x00004010UL, 0x20000010UL,
	0x00400000UL, 0x20004000UL, 0x20000000UL, 0x00004010UL,
	0x20000010UL, 0x20404010UL, 0x00404000UL, 0x20400000UL,
	0x00404010UL, 0x20404000UL, 0x00000000UL, 0x20400010UL,
	0x00000010UL, 0x00004000UL, 0x20400000UL, 0x00404010UL,
	0x00004000UL, 0x00400010UL, 0x20004010UL, 0x00000000UL,
	0x20404000UL, 0x20000000UL, 0x00400010UL, 0x20004010UL
};

static const uint32_t SB7[64] = {
	0x00200000UL, 0x04200002UL, 0x04000802UL, 0x00000000UL,
	0x00000800UL, 0x04000802UL, 0x00200802UL, 0x04200800UL,
	0x04200802UL, 0x00200000UL, 0x00000000UL, 0x04000002UL,
	0x00000002UL, 0x04000000UL, 0x04200002UL, 0x00000802UL,
	0x04000800UL, 0x00200802UL, 0x00200002UL, 0x04000800UL,
	0x04000002UL, 0x04200000UL, 0x04200800UL, 0x00200002UL,
	0x04200000UL, 0x00000800UL, 0x00000802UL, 0x04200802UL,
	0x00200800UL, 0x00000002UL, 0x04000000UL, 0x00200800UL,
	0x04000000UL, 0x00200800UL, 0x00200000UL, 0x04000802UL,
	0x04000802UL, 0x04200002UL, 0x04200002UL, 0x00000002UL,
	0x00200002UL, 0x04000000UL, 0x04000800UL, 0x00200000UL,
	0x04200800UL, 0x00000802UL, 0x00200802UL, 0x04200800UL,
	0x00000802UL, 0x04000002UL, 0x04200802UL, 0x04200000UL,
	0x00200800UL, 0x00000000UL, 0x00000002UL, 0x04200802UL,
	0x00000000UL, 0x00200802UL, 0x04200000UL, 0x00000800UL,
	0x04000002UL, 0x04000800UL, 0x00000800UL, 0x00200002UL
};

static const uint32_t SB8[64] = {
	0x10001040UL, 0x00001000UL, 0x00040000UL, 0x10041040UL,
	0x10000000UL, 0x10001040UL, 0x00000040UL, 0x10000000UL,
	0x00040040UL, 0x10040000UL, 0x10041040UL, 0x00041000UL,
	0x10041000UL, 0x00041040UL, 0x00001000UL, 0x00000040UL,
	0x10040000UL, 0x10000040UL, 0x10001000UL, 0x00001040UL,
	0x00041000UL, 0x00040040UL, 0x10040040UL, 0x10041000UL,
	0x00001040UL, 0x00000000UL, 0x00000000UL, 0x10040040UL,
	0x10000040UL, 0x10001000UL, 0x00041040UL, 0x00040000UL,
	0x00041040UL, 0x00040000UL, 0x10041000UL, 0x00001000UL,
	0x00000040UL, 0x10040040UL, 0x00001000UL, 0x00041040UL,
	0x10001000UL, 0x00000040UL, 0x10000040UL, 0x10040000UL,
	0x10040040UL, 0x10000000UL, 0x00040000UL, 0x10001040UL,
	0x00000000UL, 0x10041040UL, 0x00040040UL, 0x10000040UL,
	0x10040000UL, 0x10001000UL, 0x10001040UL, 0x00000000UL,
	0x10041040UL, 0x00041000UL, 0x00041000UL, 0x00001040UL,
	0x00001040UL, 0x00040040UL, 0x10000000UL, 0x10041000UL
};

/* PC1: left and right halves bit-swap */
static const uint32_t LHS[16] = {
	0x00000000UL, 0x00000001UL, 0x00000100UL, 0x00000101UL,
	0x00010000UL, 0x00010001UL, 0x00010100UL, 0x00010101UL,
	0x01000000UL, 0x01000001UL, 0x01000100UL, 0x01000101UL,
	0x01010000UL, 0x01010001UL, 0x01010100UL, 0x01010101UL
};

static const uint32_t RHS[16] = {
	0x00000000UL, 0x01000000UL, 0x00010000UL, 0x01010000UL,
	0x00000100UL, 0x01000100UL, 0x00010100UL, 0x01010100UL,
	0x00000001UL, 0x01000001UL, 0x00010001UL, 0x01010001UL,
	0x00000101UL, 0x01000101UL, 0x00010101UL, 0x01010101UL
};

/* ��ʼ�û� */
#define IP(x, y) do {											\
    t = ((x >>  4) ^ y) & 0x0F0F0F0FUL; y ^= t; x ^= (t <<  4);	\
    t = ((x >> 16) ^ y) & 0x0000FFFFUL; y ^= t; x ^= (t << 16);	\
    t = ((y >>  2) ^ x) & 0x33333333UL; x ^= t; y ^= (t <<  2);	\
    t = ((y >>  8) ^ x) & 0x00FF00FFUL; x ^= t; y ^= (t <<  8);	\
    y = ((y << 1) | (y >> 31)) & 0xFFFFFFFFUL;					\
    t = (x ^ y) & 0xAAAAAAAAUL; y ^= t; x ^= t;					\
    x = ((x << 1) | (x >> 31)) & 0xFFFFFFFFUL;					\
} while(0)

/* �����û� */
#define FP(x,y) do {											\
    x = ((x << 31) | (x >> 1)) & 0xFFFFFFFFUL;					\
    t = (x ^ y) & 0xAAAAAAAAUL; x ^= t; y ^= t;					\
    y = ((y << 31) | (y >> 1)) & 0xFFFFFFFFUL;					\
    t = ((y >>  8) ^ x) & 0x00FF00FFUL; x ^= t; y ^= (t <<  8);	\
    t = ((y >>  2) ^ x) & 0x33333333UL; x ^= t; y ^= (t <<  2);	\
    t = ((x >> 16) ^ y) & 0x0000FFFFUL; y ^= t; x ^= (t << 16);	\
    t = ((x >>  4) ^ y) & 0x0F0F0F0FUL; y ^= t; x ^= (t <<  4);	\
} while(0)

/* DESѭ�� */
#define ROUND(x,y) do {						\
	    t = *sk++ ^ x;						\
	    y ^= SB8[(t      ) & 0x3F] ^		\
	         SB6[(t >>  8) & 0x3F] ^		\
	         SB4[(t >> 16) & 0x3F] ^		\
	         SB2[(t >> 24) & 0x3F];			\
	    t = *sk++ ^ ((x << 28) | (x >> 4));	\
	    y ^= SB7[(t      ) & 0x3F] ^		\
	         SB5[(t >>  8) & 0x3F] ^		\
	         SB3[(t >> 16) & 0x3F] ^		\
	         SB1[(t >> 24) & 0x3F];			\
	} while(0)

static void __des_key_schedule(uint32_t* sk, uint8_t* key)
{
	uint32_t x, y, t;
	int i;

	x = __be32_to_cpu_p(key    );
	y = __be32_to_cpu_p(key + 4);

	/* �û�ѡ��1 */
	t = ((y >>  4) ^ x) & 0x0F0F0F0FUL; x ^= t; y ^= (t <<  4);
	t = ((y) ^ x) & 0x10101010UL; x ^= t; y ^= (t);

	x =   (LHS[(x) & 0xF] << 3) | (LHS[(x >> 8) & 0xF] << 2)
		| (LHS[(x >> 16) & 0xF] << 1) | (LHS[(x >> 24) & 0xF]     )
		| (LHS[(x >>  5) & 0xF] << 7) | (LHS[(x >> 13) & 0xF] << 6)
		| (LHS[(x >> 21) & 0xF] << 5) | (LHS[(x >> 29) & 0xF] << 4);

	y =   (RHS[(y >>  1) & 0xF] << 3) | (RHS[(y >>  9) & 0xF] << 2)
		| (RHS[(y >> 17) & 0xF] << 1) | (RHS[(y >> 25) & 0xF]     )
		| (RHS[(y >>  4) & 0xF] << 7) | (RHS[(y >> 12) & 0xF] << 6)
		| (RHS[(y >> 20) & 0xF] << 5) | (RHS[(y >> 28) & 0xF] << 4);

	x &= 0x0FFFFFFFUL;
	y &= 0x0FFFFFFFUL;

	/* ��������Կ */
	for(i = 0; i < 16; i++) {
		if(i < 2 || i == 8 || i == 15) {
			x = ((x <<  1) | (x >> 27)) & 0x0FFFFFFF;
			y = ((y <<  1) | (y >> 27)) & 0x0FFFFFFF;
		} else {
			x = ((x <<  2) | (x >> 26)) & 0x0FFFFFFF;
			y = ((y <<  2) | (y >> 26)) & 0x0FFFFFFF;
		}
		*sk++ =   ((x <<  4) & 0x24000000UL) | ((x << 28) & 0x10000000UL)
				| ((x << 14) & 0x08000000UL) | ((x << 18) & 0x02080000UL)
				| ((x <<  6) & 0x01000000UL) | ((x <<  9) & 0x00200000UL)
				| ((x >>  1) & 0x00100000UL) | ((x << 10) & 0x00040000UL)
				| ((x <<  2) & 0x00020000UL) | ((x >> 10) & 0x00010000UL)
				| ((y >> 13) & 0x00002000UL) | ((y >>  4) & 0x00001000UL)
				| ((y <<  6) & 0x00000800UL) | ((y >>  1) & 0x00000400UL)
				| ((y >> 14) & 0x00000200UL) | ((y      ) & 0x00000100UL)
				| ((y >>  5) & 0x00000020UL) | ((y >> 10) & 0x00000010UL)
				| ((y >>  3) & 0x00000008UL) | ((y >> 18) & 0x00000004UL)
				| ((y >> 26) & 0x00000002UL) | ((y >> 24) & 0x00000001UL);

		*sk++ =   ((x << 15) & 0x20000000UL) | ((x << 17) & 0x10000000UL)
				| ((x << 10) & 0x08000000UL) | ((x << 22) & 0x04000000UL)
				| ((x >>  2) & 0x02000000UL) | ((x <<  1) & 0x01000000UL)
				| ((x << 16) & 0x00200000UL) | ((x << 11) & 0x00100000UL)
				| ((x <<  3) & 0x00080000UL) | ((x >>  6) & 0x00040000UL)
				| ((x << 15) & 0x00020000UL) | ((x >>  4) & 0x00010000UL)
				| ((y >>  2) & 0x00002000UL) | ((y <<  8) & 0x00001000UL)
				| ((y >> 14) & 0x00000808UL) | ((y >>  9) & 0x00000400UL)
				| ((y      ) & 0x00000200UL) | ((y <<  7) & 0x00000100UL)
				| ((y >>  7) & 0x00000020UL) | ((y >>  3) & 0x00000011UL)
				| ((y <<  2) & 0x00000004UL) | ((y >> 21) & 0x00000002UL);
	}

	/* Ϊ�˰�ȫ���ǣ������ڴ� */
	x = 0; y = 0; t = 0;
}

NV_IMPL void des_set_key(des_key* key, void* src_key)
{
	/* ���ɼ�����Կ */
	__des_key_schedule(key->esk, (uint8_t*)src_key);

	/* ���ɽ�����Կ */
	do {
		int i;
		for (i = 0; i < 32; i += 2) {
			key->dsk[i    ] = key->esk[30 - i];
			key->dsk[i + 1] = key->esk[31 - i];
		}
	} while (0);
}

NV_IMPL void __des_crypt(uint32_t* sk, uint8_t* dst, uint8_t* src)
{
	uint32_t x, y, t;

	x = __be32_to_cpu_p(src    );
	y = __be32_to_cpu_p(src + 4);

	IP(x, y);

	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);

	FP(y, x);

	*((uint32_t*)(dst    )) = __cpu_to_be32(y);
	*((uint32_t*)(dst + 4)) = __cpu_to_be32(x);

	/* Ϊ�˰�ȫ���ǣ������ڴ� */
	x = 0; y = 0; t = 0;
}

NV_IMPL void tdes_set_double_key(tdes_key* key, void* k1, void* k2)
{
	__des_key_schedule(key->esk     , (uint8_t*)k1);
	__des_key_schedule(key->dsk + 32, (uint8_t*)k2);

	do {
		int i;
		for(i = 0; i < 32; i += 2) {
			key->dsk[i     ] = key->esk[30 - i];
			key->dsk[i +  1] = key->esk[31 - i];
			key->esk[i + 32] = key->dsk[62 - i];
			key->esk[i + 33] = key->dsk[63 - i];
			key->esk[i + 64] = key->esk[i     ];
			key->esk[i + 65] = key->esk[i +  1];
			key->dsk[i + 64] = key->dsk[i     ];
			key->dsk[i + 65] = key->dsk[i +  1];
		}
	} while (0);
}

NV_IMPL void tdes_set_triple_key(tdes_key* key, void* k1, void* k2, void* k3)
{
	__des_key_schedule(key->esk     , (uint8_t*)k1);
	__des_key_schedule(key->dsk + 32, (uint8_t*)k2);
	__des_key_schedule(key->esk + 64, (uint8_t*)k3);

	do {
		int i;
		for(i = 0; i < 32; i += 2) {
			key->dsk[i     ] = key->esk[94 - i];
			key->dsk[i +  1] = key->esk[95 - i];
			key->esk[i + 32] = key->dsk[62 - i];
			key->esk[i + 33] = key->dsk[63 - i];
			key->dsk[i + 64] = key->esk[30 - i];
			key->dsk[i + 65] = key->esk[31 - i];
		}
	} while (0);
}

NV_IMPL void __tdes_crypt(uint32_t* sk, uint8_t* dst, uint8_t* src)
{
	uint32_t x, y, t;

	x = __be32_to_cpu_p(src    );
	y = __be32_to_cpu_p(src + 4);

	IP(x, y);

	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);

	ROUND(x, y); ROUND(y, x);
	ROUND(x, y); ROUND(y, x);
	ROUND(x, y); ROUND(y, x);
	ROUND(x, y); ROUND(y, x);
	ROUND(x, y); ROUND(y, x);
	ROUND(x, y); ROUND(y, x);
	ROUND(x, y); ROUND(y, x);
	ROUND(x, y); ROUND(y, x);

	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);
	ROUND(y, x); ROUND(x, y);

	FP(y, x);

	*((uint32_t*)(dst    )) = __cpu_to_be32(y);
	*((uint32_t*)(dst + 4)) = __cpu_to_be32(x);

	/* Ϊ�˰�ȫ���ǣ������ڴ� */
	x = 0; y = 0; t = 0;
}


#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

