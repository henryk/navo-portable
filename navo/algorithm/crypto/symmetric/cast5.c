/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/crypto/symmetric/cast5.c
 *
 * @brief Cast5.
 *
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#include "cast5.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
#include <cstring>
extern "C" {
#else
#include <string.h>
#endif /* #ifdef __cplusplus */

static const uint32_t S5[256] = {
	0x7ec90c04UL, 0x2c6e74b9UL, 0x9b0e66dfUL, 0xa6337911UL, 
	0xb86a7fffUL, 0x1dd358f5UL, 0x44dd9d44UL, 0x1731167fUL,
	0x08fbf1faUL, 0xe7f511ccUL, 0xd2051b00UL, 0x735aba00UL, 
	0x2ab722d8UL, 0x386381cbUL, 0xacf6243aUL, 0x69befd7aUL,
	0xe6a2e77fUL, 0xf0c720cdUL, 0xc4494816UL, 0xccf5c180UL, 
	0x38851640UL, 0x15b0a848UL, 0xe68b18cbUL, 0x4caadeffUL,
	0x5f480a01UL, 0x0412b2aaUL, 0x259814fcUL, 0x41d0efe2UL, 
	0x4e40b48dUL, 0x248eb6fbUL, 0x8dba1cfeUL, 0x41a99b02UL,
	0x1a550a04UL, 0xba8f65cbUL, 0x7251f4e7UL, 0x95a51725UL, 
	0xc106ecd7UL, 0x97a5980aUL, 0xc539b9aaUL, 0x4d79fe6aUL,
	0xf2f3f763UL, 0x68af8040UL, 0xed0c9e56UL, 0x11b4958bUL, 
	0xe1eb5a88UL, 0x8709e6b0UL, 0xd7e07156UL, 0x4e29fea7UL,
	0x6366e52dUL, 0x02d1c000UL, 0xc4ac8e05UL, 0x9377f571UL, 
	0x0c05372aUL, 0x578535f2UL, 0x2261be02UL, 0xd642a0c9UL,
	0xdf13a280UL, 0x74b55bd2UL, 0x682199c0UL, 0xd421e5ecUL, 
	0x53fb3ce8UL, 0xc8adedb3UL, 0x28a87fc9UL, 0x3d959981UL,
	0x5c1ff900UL, 0xfe38d399UL, 0x0c4eff0bUL, 0x062407eaUL, 
	0xaa2f4fb1UL, 0x4fb96976UL, 0x90c79505UL, 0xb0a8a774UL,
	0xef55a1ffUL, 0xe59ca2c2UL, 0xa6b62d27UL, 0xe66a4263UL, 
	0xdf65001fUL, 0x0ec50966UL, 0xdfdd55bcUL, 0x29de0655UL,
	0x911e739aUL, 0x17af8975UL, 0x32c7911cUL, 0x89f89468UL, 
	0x0d01e980UL, 0x524755f4UL, 0x03b63cc9UL, 0x0cc844b2UL,
	0xbcf3f0aaUL, 0x87ac36e9UL, 0xe53a7426UL, 0x01b3d82bUL, 
	0x1a9e7449UL, 0x64ee2d7eUL, 0xcddbb1daUL, 0x01c94910UL,
	0xb868bf80UL, 0x0d26f3fdUL, 0x9342ede7UL, 0x04a5c284UL, 
	0x636737b6UL, 0x50f5b616UL, 0xf24766e3UL, 0x8eca36c1UL,
	0x136e05dbUL, 0xfef18391UL, 0xfb887a37UL, 0xd6e7f7d4UL, 
	0xc7fb7dc9UL, 0x3063fcdfUL, 0xb6f589deUL, 0xec2941daUL,
	0x26e46695UL, 0xb7566419UL, 0xf654efc5UL, 0xd08d58b7UL, 
	0x48925401UL, 0xc1bacb7fUL, 0xe5ff550fUL, 0xb6083049UL,
	0x5bb5d0e8UL, 0x87d72e5aUL, 0xab6a6ee1UL, 0x223a66ceUL, 
	0xc62bf3cdUL, 0x9e0885f9UL, 0x68cb3e47UL, 0x086c010fUL,
	0xa21de820UL, 0xd18b69deUL, 0xf3f65777UL, 0xfa02c3f6UL, 
	0x407edac3UL, 0xcbb3d550UL, 0x1793084dUL, 0xb0d70ebaUL,
	0x0ab378d5UL, 0xd951fb0cUL, 0xded7da56UL, 0x4124bbe4UL, 
	0x94ca0b56UL, 0x0f5755d1UL, 0xe0e1e56eUL, 0x6184b5beUL,
	0x580a249fUL, 0x94f74bc0UL, 0xe327888eUL, 0x9f7b5561UL, 
	0xc3dc0280UL, 0x05687715UL, 0x646c6bd7UL, 0x44904db3UL,
	0x66b4f0a3UL, 0xc0f1648aUL, 0x697ed5afUL, 0x49e92ff6UL, 
	0x309e374fUL, 0x2cb6356aUL, 0x85808573UL, 0x4991f840UL,
	0x76f0ae02UL, 0x083be84dUL, 0x28421c9aUL, 0x44489406UL, 
	0x736e4cb8UL, 0xc1092910UL, 0x8bc95fc6UL, 0x7d869cf4UL,
	0x134f616fUL, 0x2e77118dUL, 0xb31b2be1UL, 0xaa90b472UL, 
	0x3ca5d717UL, 0x7d161bbaUL, 0x9cad9010UL, 0xaf462ba2UL,
	0x9fe459d2UL, 0x45d34559UL, 0xd9f2da13UL, 0xdbc65487UL, 
	0xf3e4f94eUL, 0x176d486fUL, 0x097c13eaUL, 0x631da5c7UL,
	0x445f7382UL, 0x175683f4UL, 0xcdc66a97UL, 0x70be0288UL, 
	0xb3cdcf72UL, 0x6e5dd2f3UL, 0x20936079UL, 0x459b80a5UL,
	0xbe60e2dbUL, 0xa9c23101UL, 0xeba5315cUL, 0x224e42f2UL, 
	0x1c5c1572UL, 0xf6721b2cUL, 0x1ad2fff3UL, 0x8c25404eUL,
	0x324ed72fUL, 0x4067b7fdUL, 0x0523138eUL, 0x5ca3bc78UL, 
	0xdc0fd66eUL, 0x75922283UL, 0x784d6b17UL, 0x58ebb16eUL,
	0x44094f85UL, 0x3f481d87UL, 0xfcfeae7bUL, 0x77b5ff76UL, 
	0x8c2302bfUL, 0xaaf47556UL, 0x5f46b02aUL, 0x2b092801UL,
	0x3d38f5f7UL, 0x0ca81f36UL, 0x52af4a8aUL, 0x66d5e7c0UL, 
	0xdf3b0874UL, 0x95055110UL, 0x1b5ad7a8UL, 0xf61ed5adUL,
	0x6cf6e479UL, 0x20758184UL, 0xd0cefa65UL, 0x88f7be58UL, 
	0x4a046826UL, 0x0ff6f8f3UL, 0xa09c7f70UL, 0x5346aba0UL,
	0x5ce96c28UL, 0xe176eda3UL, 0x6bac307fUL, 0x376829d2UL, 
	0x85360fa9UL, 0x17e3fe2aUL, 0x24b79767UL, 0xf5a96b20UL,
	0xd6cd2595UL, 0x68ff1ebfUL, 0x7555442cUL, 0xf19f06beUL, 
	0xf9e0659aUL, 0xeeb9491dUL, 0x34010718UL, 0xbb30cab8UL,
	0xe822fe15UL, 0x88570983UL, 0x750e6249UL, 0xda627e55UL, 
	0x5e76ffa8UL, 0xb1534546UL, 0x6d47de08UL, 0xefe9e7d4UL
};

static const uint32_t S6[256] = {
	0xf6fa8f9dUL, 0x2cac6ce1UL, 0x4ca34867UL, 0xe2337f7cUL, 
	0x95db08e7UL, 0x016843b4UL, 0xeced5cbcUL, 0x325553acUL,
	0xbf9f0960UL, 0xdfa1e2edUL, 0x83f0579dUL, 0x63ed86b9UL, 
	0x1ab6a6b8UL, 0xde5ebe39UL, 0xf38ff732UL, 0x8989b138UL,
	0x33f14961UL, 0xc01937bdUL, 0xf506c6daUL, 0xe4625e7eUL, 
	0xa308ea99UL, 0x4e23e33cUL, 0x79cbd7ccUL, 0x48a14367UL,
	0xa3149619UL, 0xfec94bd5UL, 0xa114174aUL, 0xeaa01866UL, 
	0xa084db2dUL, 0x09a8486fUL, 0xa888614aUL, 0x2900af98UL,
	0x01665991UL, 0xe1992863UL, 0xc8f30c60UL, 0x2e78ef3cUL, 
	0xd0d51932UL, 0xcf0fec14UL, 0xf7ca07d2UL, 0xd0a82072UL,
	0xfd41197eUL, 0x9305a6b0UL, 0xe86be3daUL, 0x74bed3cdUL, 
	0x372da53cUL, 0x4c7f4448UL, 0xdab5d440UL, 0x6dba0ec3UL,
	0x083919a7UL, 0x9fbaeed9UL, 0x49dbcfb0UL, 0x4e670c53UL, 
	0x5c3d9c01UL, 0x64bdb941UL, 0x2c0e636aUL, 0xba7dd9cdUL,
	0xea6f7388UL, 0xe70bc762UL, 0x35f29adbUL, 0x5c4cdd8dUL, 
	0xf0d48d8cUL, 0xb88153e2UL, 0x08a19866UL, 0x1ae2eac8UL,
	0x284caf89UL, 0xaa928223UL, 0x9334be53UL, 0x3b3a21bfUL, 
	0x16434be3UL, 0x9aea3906UL, 0xefe8c36eUL, 0xf890cdd9UL,
	0x80226daeUL, 0xc340a4a3UL, 0xdf7e9c09UL, 0xa694a807UL, 
	0x5b7c5eccUL, 0x221db3a6UL, 0x9a69a02fUL, 0x68818a54UL,
	0xceb2296fUL, 0x53c0843aUL, 0xfe893655UL, 0x25bfe68aUL, 
	0xb4628abcUL, 0xcf222ebfUL, 0x25ac6f48UL, 0xa9a99387UL,
	0x53bddb65UL, 0xe76ffbe7UL, 0xe967fd78UL, 0x0ba93563UL, 
	0x8e342bc1UL, 0xe8a11be9UL, 0x4980740dUL, 0xc8087dfcUL,
	0x8de4bf99UL, 0xa11101a0UL, 0x7fd37975UL, 0xda5a26c0UL, 
	0xe81f994fUL, 0x9528cd89UL, 0xfd339fedUL, 0xb87834bfUL,
	0x5f04456dUL, 0x22258698UL, 0xc9c4c83bUL, 0x2dc156beUL, 
	0x4f628daaUL, 0x57f55ec5UL, 0xe2220abeUL, 0xd2916ebfUL,
	0x4ec75b95UL, 0x24f2c3c0UL, 0x42d15d99UL, 0xcd0d7fa0UL, 
	0x7b6e27ffUL, 0xa8dc8af0UL, 0x7345c106UL, 0xf41e232fUL,
	0x35162386UL, 0xe6ea8926UL, 0x3333b094UL, 0x157ec6f2UL, 
	0x372b74afUL, 0x692573e4UL, 0xe9a9d848UL, 0xf3160289UL,
	0x3a62ef1dUL, 0xa787e238UL, 0xf3a5f676UL, 0x74364853UL, 
	0x20951063UL, 0x4576698dUL, 0xb6fad407UL, 0x592af950UL,
	0x36f73523UL, 0x4cfb6e87UL, 0x7da4cec0UL, 0x6c152daaUL, 
	0xcb0396a8UL, 0xc50dfe5dUL, 0xfcd707abUL, 0x0921c42fUL,
	0x89dff0bbUL, 0x5fe2be78UL, 0x448f4f33UL, 0x754613c9UL, 
	0x2b05d08dUL, 0x48b9d585UL, 0xdc049441UL, 0xc8098f9bUL,
	0x7dede786UL, 0xc39a3373UL, 0x42410005UL, 0x6a091751UL, 
	0x0ef3c8a6UL, 0x890072d6UL, 0x28207682UL, 0xa9a9f7beUL,
	0xbf32679dUL, 0xd45b5b75UL, 0xb353fd00UL, 0xcbb0e358UL, 
	0x830f220aUL, 0x1f8fb214UL, 0xd372cf08UL, 0xcc3c4a13UL,
	0x8cf63166UL, 0x061c87beUL, 0x88c98f88UL, 0x6062e397UL, 
	0x47cf8e7aUL, 0xb6c85283UL, 0x3cc2acfbUL, 0x3fc06976UL,
	0x4e8f0252UL, 0x64d8314dUL, 0xda3870e3UL, 0x1e665459UL, 
	0xc10908f0UL, 0x513021a5UL, 0x6c5b68b7UL, 0x822f8aa0UL,
	0x3007cd3eUL, 0x74719eefUL, 0xdc872681UL, 0x073340d4UL, 
	0x7e432fd9UL, 0x0c5ec241UL, 0x8809286cUL, 0xf592d891UL,
	0x08a930f6UL, 0x957ef305UL, 0xb7fbffbdUL, 0xc266e96fUL, 
	0x6fe4ac98UL, 0xb173ecc0UL, 0xbc60b42aUL, 0x953498daUL,
	0xfba1ae12UL, 0x2d4bd736UL, 0x0f25faabUL, 0xa4f3fcebUL, 
	0xe2969123UL, 0x257f0c3dUL, 0x9348af49UL, 0x361400bcUL,
	0xe8816f4aUL, 0x3814f200UL, 0xa3f94043UL, 0x9c7a54c2UL, 
	0xbc704f57UL, 0xda41e7f9UL, 0xc25ad33aUL, 0x54f4a084UL,
	0xb17f5505UL, 0x59357cbeUL, 0xedbd15c8UL, 0x7f97c5abUL, 
	0xba5ac7b5UL, 0xb6f6deafUL, 0x3a479c3aUL, 0x5302da25UL,
	0x653d7e6aUL, 0x54268d49UL, 0x51a477eaUL, 0x5017d55bUL, 
	0xd7d25d88UL, 0x44136c76UL, 0x0404a8c8UL, 0xb8e5a121UL,
	0xb81a928aUL, 0x60ed5869UL, 0x97c55b96UL, 0xeaec991bUL, 
	0x29935913UL, 0x01fdb7f1UL, 0x088e8dfaUL, 0x9ab6f6f5UL,
	0x3b4cbf9fUL, 0x4a5de3abUL, 0xe6051d35UL, 0xa0e1d855UL, 
	0xd36b4cf1UL, 0xf544edebUL, 0xb0e93524UL, 0xbebb8fbdUL,
	0xa2d762cfUL, 0x49c92f54UL, 0x38b5f331UL, 0x7128a454UL, 
	0x48392905UL, 0xa65b1db8UL, 0x851c97bdUL, 0xd675cf2fUL
};
static const uint32_t S7[256] = {
	0x85e04019UL, 0x332bf567UL, 0x662dbfffUL, 0xcfc65693UL, 
	0x2a8d7f6fUL, 0xab9bc912UL, 0xde6008a1UL, 0x2028da1fUL,
	0x0227bce7UL, 0x4d642916UL, 0x18fac300UL, 0x50f18b82UL, 
	0x2cb2cb11UL, 0xb232e75cUL, 0x4b3695f2UL, 0xb28707deUL,
	0xa05fbcf6UL, 0xcd4181e9UL, 0xe150210cUL, 0xe24ef1bdUL, 
	0xb168c381UL, 0xfde4e789UL, 0x5c79b0d8UL, 0x1e8bfd43UL,
	0x4d495001UL, 0x38be4341UL, 0x913cee1dUL, 0x92a79c3fUL, 
	0x089766beUL, 0xbaeeadf4UL, 0x1286becfUL, 0xb6eacb19UL,
	0x2660c200UL, 0x7565bde4UL, 0x64241f7aUL, 0x8248dca9UL, 
	0xc3b3ad66UL, 0x28136086UL, 0x0bd8dfa8UL, 0x356d1cf2UL,
	0x107789beUL, 0xb3b2e9ceUL, 0x0502aa8fUL, 0x0bc0351eUL, 
	0x166bf52aUL, 0xeb12ff82UL, 0xe3486911UL, 0xd34d7516UL,
	0x4e7b3affUL, 0x5f43671bUL, 0x9cf6e037UL, 0x4981ac83UL, 
	0x334266ceUL, 0x8c9341b7UL, 0xd0d854c0UL, 0xcb3a6c88UL,
	0x47bc2829UL, 0x4725ba37UL, 0xa66ad22bUL, 0x7ad61f1eUL, 
	0x0c5cbafaUL, 0x4437f107UL, 0xb6e79962UL, 0x42d2d816UL,
	0x0a961288UL, 0xe1a5c06eUL, 0x13749e67UL, 0x72fc081aUL, 
	0xb1d139f7UL, 0xf9583745UL, 0xcf19df58UL, 0xbec3f756UL,
	0xc06eba30UL, 0x07211b24UL, 0x45c28829UL, 0xc95e317fUL, 
	0xbc8ec511UL, 0x38bc46e9UL, 0xc6e6fa14UL, 0xbae8584aUL,
	0xad4ebc46UL, 0x468f508bUL, 0x7829435fUL, 0xf124183bUL, 
	0x821dba9fUL, 0xaff60ff4UL, 0xea2c4e6dUL, 0x16e39264UL,
	0x92544a8bUL, 0x009b4fc3UL, 0xaba68cedUL, 0x9ac96f78UL, 
	0x06a5b79aUL, 0xb2856e6eUL, 0x1aec3ca9UL, 0xbe838688UL,
	0x0e0804e9UL, 0x55f1be56UL, 0xe7e5363bUL, 0xb3a1f25dUL, 
	0xf7debb85UL, 0x61fe033cUL, 0x16746233UL, 0x3c034c28UL,
	0xda6d0c74UL, 0x79aac56cUL, 0x3ce4e1adUL, 0x51f0c802UL, 
	0x98f8f35aUL, 0x1626a49fUL, 0xeed82b29UL, 0x1d382fe3UL,
	0x0c4fb99aUL, 0xbb325778UL, 0x3ec6d97bUL, 0x6e77a6a9UL, 
	0xcb658b5cUL, 0xd45230c7UL, 0x2bd1408bUL, 0x60c03eb7UL,
	0xb9068d78UL, 0xa33754f4UL, 0xf430c87dUL, 0xc8a71302UL, 
	0xb96d8c32UL, 0xebd4e7beUL, 0xbe8b9d2dUL, 0x7979fb06UL,
	0xe7225308UL, 0x8b75cf77UL, 0x11ef8da4UL, 0xe083c858UL, 
	0x8d6b786fUL, 0x5a6317a6UL, 0xfa5cf7a0UL, 0x5dda0033UL,
	0xf28ebfb0UL, 0xf5b9c310UL, 0xa0eac280UL, 0x08b9767aUL, 
	0xa3d9d2b0UL, 0x79d34217UL, 0x021a718dUL, 0x9ac6336aUL,
	0x2711fd60UL, 0x438050e3UL, 0x069908a8UL, 0x3d7fedc4UL, 
	0x826d2befUL, 0x4eeb8476UL, 0x488dcf25UL, 0x36c9d566UL,
	0x28e74e41UL, 0xc2610acaUL, 0x3d49a9cfUL, 0xbae3b9dfUL, 
	0xb65f8de6UL, 0x92aeaf64UL, 0x3ac7d5e6UL, 0x9ea80509UL,
	0xf22b017dUL, 0xa4173f70UL, 0xdd1e16c3UL, 0x15e0d7f9UL, 
	0x50b1b887UL, 0x2b9f4fd5UL, 0x625aba82UL, 0x6a017962UL,
	0x2ec01b9cUL, 0x15488aa9UL, 0xd716e740UL, 0x40055a2cUL, 
	0x93d29a22UL, 0xe32dbf9aUL, 0x058745b9UL, 0x3453dc1eUL,
	0xd699296eUL, 0x496cff6fUL, 0x1c9f4986UL, 0xdfe2ed07UL, 
	0xb87242d1UL, 0x19de7eaeUL, 0x053e561aUL, 0x15ad6f8cUL,
	0x66626c1cUL, 0x7154c24cUL, 0xea082b2aUL, 0x93eb2939UL, 
	0x17dcb0f0UL, 0x58d4f2aeUL, 0x9ea294fbUL, 0x52cf564cUL,
	0x9883fe66UL, 0x2ec40581UL, 0x763953c3UL, 0x01d6692eUL, 
	0xd3a0c108UL, 0xa1e7160eUL, 0xe4f2dfa6UL, 0x693ed285UL,
	0x74904698UL, 0x4c2b0eddUL, 0x4f757656UL, 0x5d393378UL, 
	0xa132234fUL, 0x3d321c5dUL, 0xc3f5e194UL, 0x4b269301UL,
	0xc79f022fUL, 0x3c997e7eUL, 0x5e4f9504UL, 0x3ffafbbdUL, 
	0x76f7ad0eUL, 0x296693f4UL, 0x3d1fce6fUL, 0xc61e45beUL,
	0xd3b5ab34UL, 0xf72bf9b7UL, 0x1b0434c0UL, 0x4e72b567UL, 
	0x5592a33dUL, 0xb5229301UL, 0xcfd2a87fUL, 0x60aeb767UL,
	0x1814386bUL, 0x30bcc33dUL, 0x38a0c07dUL, 0xfd1606f2UL, 
	0xc363519bUL, 0x589dd390UL, 0x5479f8e6UL, 0x1cb8d647UL,
	0x97fd61a9UL, 0xea7759f4UL, 0x2d57539dUL, 0x569a58cfUL, 
	0xe84e63adUL, 0x462e1b78UL, 0x6580f87eUL, 0xf3817914UL,
	0x91da55f4UL, 0x40a230f3UL, 0xd1988f35UL, 0xb6e318d2UL, 
	0x3ffa50bcUL, 0x3d40f021UL, 0xc3c0bdaeUL, 0x4958c24cUL,
	0x518f36b2UL, 0x84b1d370UL, 0x0fedce83UL, 0x878ddadaUL, 
	0xf2a279c7UL, 0x94e01be8UL, 0x90716f4bUL, 0x954b8aa3UL
};

static const uint32_t S8[256] = {
	0xe216300dUL, 0xbbddfffcUL, 0xa7ebdabdUL, 0x35648095UL, 
	0x7789f8b7UL, 0xe6c1121bUL, 0x0e241600UL, 0x052ce8b5UL,
	0x11a9cfb0UL, 0xe5952f11UL, 0xece7990aUL, 0x9386d174UL, 
	0x2a42931cUL, 0x76e38111UL, 0xb12def3aUL, 0x37ddddfcUL,
	0xde9adeb1UL, 0x0a0cc32cUL, 0xbe197029UL, 0x84a00940UL, 
	0xbb243a0fUL, 0xb4d137cfUL, 0xb44e79f0UL, 0x049eedfdUL,
	0x0b15a15dUL, 0x480d3168UL, 0x8bbbde5aUL, 0x669ded42UL, 
	0xc7ece831UL, 0x3f8f95e7UL, 0x72df191bUL, 0x7580330dUL,
	0x94074251UL, 0x5c7dcdfaUL, 0xabbe6d63UL, 0xaa402164UL, 
	0xb301d40aUL, 0x02e7d1caUL, 0x53571daeUL, 0x7a3182a2UL,
	0x12a8ddecUL, 0xfdaa335dUL, 0x176f43e8UL, 0x71fb46d4UL, 
	0x38129022UL, 0xce949ad4UL, 0xb84769adUL, 0x965bd862UL,
	0x82f3d055UL, 0x66fb9767UL, 0x15b80b4eUL, 0x1d5b47a0UL, 
	0x4cfde06fUL, 0xc28ec4b8UL, 0x57e8726eUL, 0x647a78fcUL,
	0x99865d44UL, 0x608bd593UL, 0x6c200e03UL, 0x39dc5ff6UL, 
	0x5d0b00a3UL, 0xae63aff2UL, 0x7e8bd632UL, 0x70108c0cUL,
	0xbbd35049UL, 0x2998df04UL, 0x980cf42aUL, 0x9b6df491UL, 
	0x9e7edd53UL, 0x06918548UL, 0x58cb7e07UL, 0x3b74ef2eUL,
	0x522fffb1UL, 0xd24708ccUL, 0x1c7e27cdUL, 0xa4eb215bUL, 
	0x3cf1d2e2UL, 0x19b47a38UL, 0x424f7618UL, 0x35856039UL,
	0x9d17dee7UL, 0x27eb35e6UL, 0xc9aff67bUL, 0x36baf5b8UL, 
	0x09c467cdUL, 0xc18910b1UL, 0xe11dbf7bUL, 0x06cd1af8UL,
	0x7170c608UL, 0x2d5e3354UL, 0xd4de495aUL, 0x64c6d006UL, 
	0xbcc0c62cUL, 0x3dd00db3UL, 0x708f8f34UL, 0x77d51b42UL,
	0x264f620fUL, 0x24b8d2bfUL, 0x15c1b79eUL, 0x46a52564UL, 
	0xf8d7e54eUL, 0x3e378160UL, 0x7895cda5UL, 0x859c15a5UL,
	0xe6459788UL, 0xc37bc75fUL, 0xdb07ba0cUL, 0x0676a3abUL, 
	0x7f229b1eUL, 0x31842e7bUL, 0x24259fd7UL, 0xf8bef472UL,
	0x835ffcb8UL, 0x6df4c1f2UL, 0x96f5b195UL, 0xfd0af0fcUL, 
	0xb0fe134cUL, 0xe2506d3dUL, 0x4f9b12eaUL, 0xf215f225UL,
	0xa223736fUL, 0x9fb4c428UL, 0x25d04979UL, 0x34c713f8UL, 
	0xc4618187UL, 0xea7a6e98UL, 0x7cd16efcUL, 0x1436876cUL,
	0xf1544107UL, 0xbedeee14UL, 0x56e9af27UL, 0xa04aa441UL, 
	0x3cf7c899UL, 0x92ecbae6UL, 0xdd67016dUL, 0x151682ebUL,
	0xa842eedfUL, 0xfdba60b4UL, 0xf1907b75UL, 0x20e3030fUL, 
	0x24d8c29eUL, 0xe139673bUL, 0xefa63fb8UL, 0x71873054UL,
	0xb6f2cf3bUL, 0x9f326442UL, 0xcb15a4ccUL, 0xb01a4504UL, 
	0xf1e47d8dUL, 0x844a1be5UL, 0xbae7dfdcUL, 0x42cbda70UL,
	0xcd7dae0aUL, 0x57e85b7aUL, 0xd53f5af6UL, 0x20cf4d8cUL, 
	0xcea4d428UL, 0x79d130a4UL, 0x3486ebfbUL, 0x33d3cddcUL,
	0x77853b53UL, 0x37effcb5UL, 0xc5068778UL, 0xe580b3e6UL, 
	0x4e68b8f4UL, 0xc5c8b37eUL, 0x0d809ea2UL, 0x398feb7cUL,
	0x132a4f94UL, 0x43b7950eUL, 0x2fee7d1cUL, 0x223613bdUL, 
	0xdd06caa2UL, 0x37df932bUL, 0xc4248289UL, 0xacf3ebc3UL,
	0x5715f6b7UL, 0xef3478ddUL, 0xf267616fUL, 0xc148cbe4UL, 
	0x9052815eUL, 0x5e410fabUL, 0xb48a2465UL, 0x2eda7fa4UL,
	0xe87b40e4UL, 0xe98ea084UL, 0x5889e9e1UL, 0xefd390fcUL, 
	0xdd07d35bUL, 0xdb485694UL, 0x38d7e5b2UL, 0x57720101UL,
	0x730edebcUL, 0x5b643113UL, 0x94917e4fUL, 0x503c2fbaUL, 
	0x646f1282UL, 0x7523d24aUL, 0xe0779695UL, 0xf9c17a8fUL,
	0x7a5b2121UL, 0xd187b896UL, 0x29263a4dUL, 0xba510cdfUL, 
	0x81f47c9fUL, 0xad1163edUL, 0xea7b5965UL, 0x1a00726eUL,
	0x11403092UL, 0x00da6d77UL, 0x4a0cdd61UL, 0xad1f4603UL, 
	0x605bdfb0UL, 0x9eedc364UL, 0x22ebe6a8UL, 0xcee7d28aUL,
	0xa0e736a0UL, 0x5564a6b9UL, 0x10853209UL, 0xc7eb8f37UL, 
	0x2de705caUL, 0x8951570fUL, 0xdf09822bUL, 0xbd691a6cUL,
	0xaa12e4f2UL, 0x87451c0fUL, 0xe0f6a27aUL, 0x3ada4819UL, 
	0x4cf1764fUL, 0x0d771c2bUL, 0x67cdb156UL, 0x350d8384UL,
	0x5938fa0fUL, 0x42399ef3UL, 0x36997b07UL, 0x0e84093dUL, 
	0x4aa93e61UL, 0x8360d87bUL, 0x1fa98b0cUL, 0x1149382cUL,
	0xe97625a5UL, 0x0614d1b7UL, 0x0e25244bUL, 0x0c768347UL, 
	0x589e8d82UL, 0x0d2059d1UL, 0xa466bb1eUL, 0xf8da0a82UL,
	0x04f19130UL, 0xba6e4ec0UL, 0x99265164UL, 0x1ee7230dUL, 
	0x50b2ad80UL, 0xeaee6801UL, 0x8db2a283UL, 0xea8bf59eUL
};

extern const uint32_t __cast_s1[256];
extern const uint32_t __cast_s2[256];
extern const uint32_t __cast_s3[256];
extern const uint32_t __cast_s4[256];

#define S1 __cast_s1
#define S2 __cast_s2
#define S3 __cast_s3
#define S4 __cast_s4

#define FN(i, r) ((i) = __rol32((i), (r)), \
	((S1[(i) >> 24] ^ S2[((i) >> 16) & 0xFF]) - \
	S3[((i) >> 8) & 0xFF] + S4[(i) & 0xFF]))

#define F1(i, d, m, r) ((i) = ((m) + (d)), FN(i, r))
#define F2(i, d, m, r) ((i) = ((m) ^ (d)), FN(i, r))
#define F3(i, d, m, r) ((i) = ((m) - (d)), FN(i, r))

#define ROUND1(i, l, r, t, c) \
	(t) = (l); (l) = (r); (r) = (t) ^ F1(i, r, km[c], kr[c])
#define ROUND2(i, l, r, t, c) \
	(t) = (l); (l) = (r); (r) = (t) ^ F2(i, r, km[c], kr[c])
#define ROUND3(i, l, r, t, c) \
	(t) = (l); (l) = (r); (r) = (t) ^ F3(i, r, km[c], kr[c])

#define XI(i) ((x[(i) >> 2] >> ((3 - ((i) & 3)) << 3)) & 0xFF)
#define ZI(i) ((z[(i) >> 2] >> ((3 - ((i) & 3)) << 3)) & 0xFF)

static void __cast5_key_schedule(uint32_t* x, uint32_t* z, uint32_t* k)
{
	z[0] = x[0] ^ S5[XI(13)] ^ S6[XI(15)] ^ S7[XI(12)] ^ S8[XI(14)] ^ S7[XI( 8)];
	z[1] = x[2] ^ S5[ZI( 0)] ^ S6[ZI( 2)] ^ S7[ZI( 1)] ^ S8[ZI( 3)] ^ S8[XI(10)];
	z[2] = x[3] ^ S5[ZI( 7)] ^ S6[ZI( 6)] ^ S7[ZI( 5)] ^ S8[ZI( 4)] ^ S5[XI( 9)];
	z[3] = x[1] ^ S5[ZI(10)] ^ S6[ZI( 9)] ^ S7[ZI(11)] ^ S8[ZI( 8)] ^ S6[XI(11)];

	k[0] = S5[ZI( 8)] ^ S6[ZI( 9)] ^ S7[ZI(7)] ^ S8[ZI(6)] ^ S5[ZI( 2)];
	k[1] = S5[ZI(10)] ^ S6[ZI(11)] ^ S7[ZI(5)] ^ S8[ZI(4)] ^ S6[ZI( 6)];
	k[2] = S5[ZI(12)] ^ S6[ZI(13)] ^ S7[ZI(3)] ^ S8[ZI(2)] ^ S7[ZI( 9)];
	k[3] = S5[ZI(14)] ^ S6[ZI(15)] ^ S7[ZI(1)] ^ S8[ZI(0)] ^ S8[ZI(12)];

	x[0] = z[2] ^ S5[ZI( 5)] ^ S6[ZI(7)] ^ S7[ZI( 4)] ^ S8[ZI(6)] ^ S7[ZI( 8)];
	x[1] = z[0] ^ S5[XI( 0)] ^ S6[XI(2)] ^ S7[XI( 1)] ^ S8[XI(3)] ^ S8[XI(10)];
	x[2] = z[1] ^ S5[XI( 7)] ^ S6[XI(6)] ^ S7[XI( 5)] ^ S8[XI(4)] ^ S5[XI( 9)];
	x[3] = z[3] ^ S5[XI(10)] ^ S6[XI(9)] ^ S7[XI(11)] ^ S8[XI(8)] ^ S6[XI(11)];

	k[4] = S5[XI(3)] ^ S6[XI(2)] ^ S7[XI(12)] ^ S8[XI(13)] ^ S5[XI( 8)];
	k[5] = S5[XI(1)] ^ S6[XI(0)] ^ S7[XI(14)] ^ S8[XI(15)] ^ S6[XI(13)];
	k[6] = S5[XI(7)] ^ S6[XI(6)] ^ S7[XI( 8)] ^ S8[XI( 9)] ^ S7[XI( 3)];
	k[7] = S5[XI(5)] ^ S6[XI(4)] ^ S7[XI(10)] ^ S8[XI(11)] ^ S8[XI( 7)];

	z[0] = x[0] ^ S5[XI(13)] ^ S6[XI(15)] ^ S7[XI(12)] ^ S8[XI(14)] ^ S7[XI( 8)];
	z[1] = x[2] ^ S5[ZI( 0)] ^ S6[ZI( 2)] ^ S7[ZI( 1)] ^ S8[ZI( 3)] ^ S8[XI(10)];
	z[2] = x[3] ^ S5[ZI( 7)] ^ S6[ZI( 6)] ^ S7[ZI( 5)] ^ S8[ZI( 4)] ^ S5[XI( 9)];
	z[3] = x[1] ^ S5[ZI(10)] ^ S6[ZI( 9)] ^ S7[ZI(11)] ^ S8[ZI( 8)] ^ S6[XI(11)];

	k[ 8] = S5[ZI(3)] ^ S6[ZI(2)] ^ S7[ZI(12)] ^ S8[ZI(13)] ^ S5[ZI( 9)];
	k[ 9] = S5[ZI(1)] ^ S6[ZI(0)] ^ S7[ZI(14)] ^ S8[ZI(15)] ^ S6[ZI(12)];
	k[10] = S5[ZI(7)] ^ S6[ZI(6)] ^ S7[ZI( 8)] ^ S8[ZI( 9)] ^ S7[ZI( 2)];
	k[11] = S5[ZI(5)] ^ S6[ZI(4)] ^ S7[ZI(10)] ^ S8[ZI(11)] ^ S8[ZI( 6)];

	x[0] = z[2] ^ S5[ZI( 5)] ^ S6[ZI(7)] ^ S7[ZI( 4)] ^ S8[ZI(6)] ^ S7[ZI(0)];
	x[1] = z[0] ^ S5[XI( 0)] ^ S6[XI(2)] ^ S7[XI( 1)] ^ S8[XI(3)] ^ S8[XI(2)];
	x[2] = z[1] ^ S5[XI( 7)] ^ S6[XI(6)] ^ S7[XI( 5)] ^ S8[XI(4)] ^ S5[XI(1)];
	x[3] = z[3] ^ S5[XI(10)] ^ S6[XI(9)] ^ S7[XI(11)] ^ S8[XI(8)] ^ S6[XI(3)];

	k[12] = S5[XI( 8)] ^ S6[XI( 9)] ^ S7[XI(7)] ^ S8[XI(6)] ^ S5[XI( 3)];
	k[13] = S5[XI(10)] ^ S6[XI(11)] ^ S7[XI(5)] ^ S8[XI(4)] ^ S6[XI( 7)];
	k[14] = S5[XI(12)] ^ S6[XI(13)] ^ S7[XI(3)] ^ S8[XI(2)] ^ S7[XI( 8)];
	k[15] = S5[XI(14)] ^ S6[XI(15)] ^ S7[XI(1)] ^ S8[XI(0)] ^ S8[XI(13)];
}

NV_IMPL void __cast5_set_key(cast5_key* key, uint32_t* km, uint8_t* kr, 
								uint8_t* src_key, size_t cnt)
{
	uint32_t x[4];
	uint32_t z[4];
	uint32_t k[16];
	uint32_t _k[4] = {0, 0, 0, 0};
	int i;
	
	key->rd = (cnt <= 10);

	memcpy(_k, src_key, cnt);

	x[0] = __be32_to_cpu(_k[0]);
	x[1] = __be32_to_cpu(_k[1]);
	x[2] = __be32_to_cpu(_k[2]);
	x[3] = __be32_to_cpu(_k[3]);

	__cast5_key_schedule(x, z, k);
	for (i = 0; i < 16; ++i)
		km[i] = k[i];
	
	__cast5_key_schedule(x, z, k);
	for (i = 0; i < 16; ++i)
		kr[i] = k[i] & 0x1F;
}

NV_IMPL void __cast5_encrypt(uint32_t* km, uint8_t* kr, int rd, 
							uint32_t* dst, uint32_t* src)
{
	uint32_t i, t;
	uint32_t l = __be32_to_cpu(src[0]);
	uint32_t r = __be32_to_cpu(src[1]);

	ROUND1(i, l, r, t,  0);
	ROUND2(i, l, r, t,  1);
	ROUND3(i, l, r, t,  2);
	ROUND1(i, l, r, t,  3);
	ROUND2(i, l, r, t,  4);
	ROUND3(i, l, r, t,  5);
	ROUND1(i, l, r, t,  6);
	ROUND2(i, l, r, t,  7);
	ROUND3(i, l, r, t,  8);
	ROUND1(i, l, r, t,  9);
	ROUND2(i, l, r, t, 10);
	ROUND3(i, l, r, t, 11);

	if (!rd) {
		ROUND1(i, l, r, t, 12);
		ROUND2(i, l, r, t, 13);
		ROUND3(i, l, r, t, 14);
		ROUND1(i, l, r, t, 15);
	}

	dst[0] = __cpu_to_be32(r);
	dst[1] = __cpu_to_be32(l);
}

NV_IMPL void __cast5_decrypt(uint32_t* km, uint8_t* kr, int rd, 
							uint32_t* dst, uint32_t* src)
{
	uint32_t i, t;
	uint32_t l = __be32_to_cpu(src[0]);
	uint32_t r = __be32_to_cpu(src[1]);

	if (!rd) {
		ROUND1(i, l, r, t, 15);
		ROUND3(i, l, r, t, 14);
		ROUND2(i, l, r, t, 13);
		ROUND1(i, l, r, t, 12);
	}

	ROUND3(i, l, r, t, 11);
	ROUND2(i, l, r, t, 10);
	ROUND1(i, l, r, t,  9);
	ROUND3(i, l, r, t,  8);
	ROUND2(i, l, r, t,  7);
	ROUND1(i, l, r, t,  6);
	ROUND3(i, l, r, t,  5);
	ROUND2(i, l, r, t,  4);
	ROUND1(i, l, r, t,  3);
	ROUND3(i, l, r, t,  2);
	ROUND2(i, l, r, t,  1);
	ROUND1(i, l, r, t,  0);

	dst[0] = __cpu_to_be32(r);
	dst[1] = __cpu_to_be32(l);
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

