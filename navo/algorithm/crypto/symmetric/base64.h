/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/crypto/symmetric/base64.h
 *
 * @brief Base64
 *
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_Base64__
#define __NV_Base64__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup Base64
 * @ingroup �ԳƼ���
 * @{
 */

/**
 * @struct base64_key
 * @brief Base64��Կ
 */
typedef struct {
	/** @brief ����Կ */
	uint8_t esk[64];
#define BASE64_KEY_MIN 42   /* '*' */
#define BASE64_KEY_MAX 122  /* 'z' */
	/** @brief ����Կ */
	uint8_t dsk[BASE64_KEY_MAX - BASE64_KEY_MIN];
}base64_key;

/**
 * @def BASE64_DEFAULT_KEY()
 * @brief Base64Ĭ����Կ
 * @see base64_set_key
 */
#define BASE64_DEFAULT_KEY()        \
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ"    \
    "abcdefghijklmnopqrstuvwxyz"    \
    "0123456789+/"

/**
 * @def BASE64_KEY(name)
 * @brief ������Ϊname��Base64��Կ
 */
#define BASE64_KEY(name) base64_key name = {BASE64_DEFAULT_KEY(), {0}}

/**
 * @fn void base64_set_key(base64_key* key, void* src_key);
 * @brief ����Base64��Կ
 * @param[out] key Base64��Կ
 * @param[in] src_key 64�ֽ�ԭʼ��Կ
 * @par ʾ��:
 * @code
    base64_key bkey;

    // ����Base64��Կ
    base64_set_key(&bkey, BASE64_DEFAULT_KEY());
 * @endcode
 */
NV_API void base64_set_key(base64_key* key, void* src_key);

NV_API void __base64_encrypt(uint8_t* esk, uint8_t* dst,
									uint8_t* src, size_t cnt);

/**
 * @fn void base64_encrypt(base64_key* key, void* dst, void* src);
 * @brief Base64����
 * @param[out] key Base64��Կ
 * @param[out] dst �����������
 * @param[in] src �������뻺��
 * @param[in] cnt ���ĳ���(�ֽ�)
 */
static inline void base64_encrypt(base64_key* key, void* dst,
										void* src, size_t cnt)
{
	__base64_encrypt(key->esk, (uint8_t*)dst, (uint8_t*)src, cnt);
}

NV_API void __base64_decrypt(uint8_t* dsk, uint8_t* dst,
									uint8_t* src, size_t cnt);

/**
 * @fn static inline void base64_decrypt(base64_key* key, void* dst,
										void* src, size_t cnt);
 * @brief Base64����
 * @param[out] key Base64��Կ
 * @param[out] dst �����������
 * @param[in] src �������뻺��
 * @param[in] cnt ���ĳ���(�ֽ�)
 */
static inline void base64_decrypt(base64_key* key, void* dst,
										void* src, size_t cnt)
{
	__base64_decrypt(key->dsk, (uint8_t*)dst, (uint8_t*)src, cnt);
}

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_Base64__ */

