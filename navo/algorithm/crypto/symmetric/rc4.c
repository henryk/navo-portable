/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/crypto/symmetric/rc4.c
 *
 * @brief RC4.
 *
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#include "rc4.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

#define FN(a, b) (((a) + (b)) & 0xFF)

NV_IMPL void __rc4_set_key(rc4_key* key, uint32_t* s, 
							uint8_t* src_key, size_t cnt)
{
	int i, j = 0, k = 0;
	
	key->x = 1;
	key->y = 0;

	for (i = 0; i < 256; ++i)
		s[i] = i;

	for (i = 0; i < 256; ++i) {
		uint32_t t = s[i];
		j = FN(j + t, src_key[k]);
		s[i] = s[j];
		s[j] = t;
		if (++k >= cnt)
			k = 0;
	}
}

NV_IMPL void __rc4_crypt(rc4_key* key, uint32_t* s, 
					uint8_t* out, uint8_t* in, size_t cnt)
{
	uint32_t ty, ta, tb;
	uint32_t x = key->x;
	uint32_t a = s[x];
	uint32_t y = FN(key->y, a);
	uint32_t b = s[y];

	while (1) {
		s[y] = a;
		a = FN(a, b);
		x = FN(x, 1);
		ta = s[x];
		ty = FN(y, ta);
		tb = s[ty];
		*out++ = *in++ ^ s[a];
		if (!(--cnt))
			break;
		y = ty;
		a = ta;
		b = tb;
	}

	key->x = x;
	key->y = y;
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

