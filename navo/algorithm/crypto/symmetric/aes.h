/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/crypto/symmetric/aes.h
 *
 * @brief AES
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_AES__
#define __NV_AES__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup AES
 * @ingroup 对称加密
 * @{
 */

/**
 * @struct aes_key
 * @brief AES密钥
 */
typedef struct {
	/** @brief 加密钥 */
	uint32_t esk[60];
	/** @brief 解密钥 */
	uint32_t dsk[60];
	/** @brief 密钥长度 */
	uint32_t cnt;
}aes_key;

#define AES_KEYSIZE_128		16
#define AES_KEYSIZE_192		24
#define AES_KEYSIZE_256		32

/**
 * @def AES_KEY(name)
 * @brief 定义名为name的AES密钥
 */
#define AES_KEY(name) aes_key name = {{0}, {0}， 0}

NV_API int __aes_set_key(aes_key* key, uint32_t* src_key, size_t cnt);

/**
 * @def aes_set_key(key, src_key, cnt)
 * @brief 设置AES密钥
 * @param[out] key AES密钥
 * @param[in] src_key 原始密钥
 * @param[in] cnt 原始密钥长度，密钥长度必须是16、24、32字节三种
 * @return int 返回操作结果
 * @retval 1 成功
 * @retval 0 失败
 */
#define aes_set_key(key, src_key, cnt) \
    __aes_set_key(key, (uint32_t*)(src_key), cnt)

NV_API void __aes_encrypt(uint32_t* esk, size_t cnt,
							uint32_t* dst, uint32_t* src);

/**
 * @fn static inline void aes_encrypt_block(aes_key* key, void* dst, void* src);
 * @brief AES加密一个区块(16字节)
 * @param[out] key AES密钥
 * @param[out] dst 密文输出缓存，小端字节序
 * @param[in] src 明文输入缓存
 */
static inline void aes_encrypt_block(aes_key* key, void* dst, void* src)
{
	__aes_encrypt(key->esk, key->cnt, (uint32_t*)dst, (uint32_t*)src);
}

NV_API void __aes_decrypt(uint32_t* dsk, size_t cnt,
							uint32_t* dst, uint32_t* src);

/**
 * @fn static inline void aes_decrypt_block(aes_key* key, void* dst, void* src);
 * @brief AES解密一个区块(16字节)
 * @param[out] key AES密钥
 * @param[out] dst 明文输出缓存
 * @param[in] src 密文输入缓存，小端字节序
 */
static inline void aes_decrypt_block(aes_key* key, void* dst, void* src)
{
	__aes_decrypt(key->dsk, key->cnt, (uint32_t*)dst, (uint32_t*)src);
}

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_AES__ */

