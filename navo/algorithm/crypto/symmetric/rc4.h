/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/crypto/symmetric/rc4.h
 *
 * @brief RC4
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_RC4__
#define __NV_RC4__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup RC4
 * @ingroup 对称加密
 * @{
 */

/**
 * @struct rc4_key
 * @brief RC4密钥
 */
typedef struct {
	/** @brief 置换盒 */
	uint32_t s[256];
	/** @brief x */
	uint32_t x;
	/** @brief y */
	uint32_t y;
}rc4_key;

/**
 * @def RC4_KEY(name)
 * @brief 定义名为name的RC4密钥
 */
#define RC4_KEY(name) rc4_key name = {{0}, 0, 0}

NV_API void __rc4_set_key(rc4_key* key, uint32_t* s,
							uint8_t* src_key, size_t cnt);

/**
 * @fn static inline void rc4_set_key(rc4_key* key, void* src_key, size_t cnt);
 * @brief 设置RC4密钥
 * @param[out] key RC4密钥
 * @param[in] src_key 原始密钥
 * @param[in] cnt 原始密钥长度
 */
static inline void rc4_set_key(rc4_key* key, void* src_key, size_t cnt)
{
	__rc4_set_key(key, key->s, (uint8_t*)src_key, cnt);
}

NV_API void __rc4_crypt(rc4_key* key, uint32_t* s,
					uint8_t* out, uint8_t* in, size_t cnt);

/**
 * @fn static inline void rc4_encrypt(rc4_key* key, void* dst,
								void* src, size_t cnt);
 * @brief RC4加密一个段流数据
 * @param[out] key RC4密钥
 * @param[out] dst 密文输出缓存
 * @param[in] src 明文输入缓存
 * @param[in] cnt 流长度
 * @note 在本地不能与加密公用一个rc4_key，解密与加密必须使用两个rc4_key
 * @see rc4_decrypt
 */
static inline void rc4_encrypt(rc4_key* key, void* dst,
								void* src, size_t cnt)
{
	__rc4_crypt(key, key->s, (uint8_t*)dst, (uint8_t*)src, cnt);
}

/**
 * @fn static inline void rc4_decrypt(rc4_key* key, void* dst,
								void* src, size_t cnt);
 * @brief RC4解密一个段流数据
 * @param[out] key RC4密钥
 * @param[out] dst 密文输出缓存
 * @param[in] src 明文输入缓存
 * @param[in] cnt 流长度
 * @note 在本地不能与加密公用一个rc4_key，解密与加密必须使用两个rc4_key
 * @see rc4_encrypt
 */
static inline void rc4_decrypt(rc4_key* key, void* dst,
								void* src, size_t cnt)
{
	__rc4_crypt(key, key->s, (uint8_t*)dst, (uint8_t*)src, cnt);
}

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_RC4__ */

