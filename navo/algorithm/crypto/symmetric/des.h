/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/crypto/symmetric/des.h
 * 
 * @brief DES
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_DES__
#define __NV_DES__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup DES
 * @ingroup 对称加密
 * @{
 */

/**
 * @struct des_key
 * @brief DES密钥
 */
typedef struct {
	/** @brief 加密钥 */
	uint32_t esk[32];
	/** @brief 解密钥 */
	uint32_t dsk[32];
}des_key;

/**
 * @def DES_KEY(name)
 * @brief 定义名为name的DES密钥
 */
#define DES_KEY(name) des_key name = {{0}, {0}}

/**
 * @fn void des_set_key(tdes_key* key, void* src_key);
 * @brief 设置DES密钥
 * @param[out] key DES密钥
 * @param[in] src_key 8字节原始密钥
 */
NV_API void des_set_key(des_key* key, void* src_key);

NV_API void __des_crypt(uint32_t* sk, uint8_t* dst, uint8_t* src);

/**
 * @fn void des_encrypt_block(des_key* key, void* dst, void* src);
 * @brief DES加密一个区块(8字节)
 * @param[out] key DES密钥
 * @param[out] dst 密文输出缓存，大端字节序
 * @param[in] src 明文输入缓存
 */
static inline void des_encrypt_block(des_key* key, void* dst, void* src)
{
	__des_crypt(key->esk, (uint8_t*)dst, (uint8_t*)src);
}

/**
 * @fn void des_decrypt_block(des_key* key, void* dst, void* src);
 * @brief DES解密一个区块(8字节)
 * @param[out] key DES密钥
 * @param[out] dst 明文输出缓存
 * @param[in] src 密文输入缓存，大端字节序
 */
static inline void des_decrypt_block(des_key* key, void* dst, void* src)
{
	__des_crypt(key->dsk, (uint8_t*)dst, (uint8_t*)src);
}

/**
 * @struct tdes_key
 * @brief 三重DES密钥
 */
typedef struct {
	/** @brief 加密钥 */
	uint32_t esk[96];
	/** @brief 解密钥 */
	uint32_t dsk[96];
}tdes_key;

/**
 * @def TDES_KEY(name)
 * @brief 定义名为name的三重DES密钥
 */
#define TDES_KEY(name) tdes_key name = {{0}, {0}}

/**
 * @fn void tdes_set_double_key(tdes_key* key, void* k1, void* k2);
 * @brief 设置双重DES密钥
 * @param[out] key 三重DES密钥
 * @param[in] k1 8字节原始密钥1
 * @param[in] k2 8字节原始密钥2
 */
NV_API void tdes_set_double_key(tdes_key* key, void* k1, void* k2);

/**
 * @fn void tdes_set_triple_key(tdes_key* key, void* k1, void* k2, void* k3);
 * @brief 设置三重DES密钥
 * @param[out] key 三重DES密钥
 * @param[in] k1 8字节原始密钥1
 * @param[in] k2 8字节原始密钥2
 * @param[in] k3 8字节原始密钥3
 */
NV_API void tdes_set_triple_key(tdes_key* key, void* k1, void* k2, void* k3);

NV_API void __tdes_crypt(uint32_t* sk, uint8_t* dst, uint8_t* src);

/**
 * @fn void tdes_encrypt_block(tdes_key* key, void* dst, void* src);
 * @brief 三重DES加密一个区块(8字节)
 * @param[out] key 三重DES密钥
 * @param[out] dst 密文输出缓存，大端字节序
 * @param[in] src 明文输入缓存
 */
static inline void tdes_encrypt_block(tdes_key* key, void* dst, void* src)
{
	__tdes_crypt(key->esk, (uint8_t*)dst, (uint8_t*)src);
}

/**
 * @fn void tdes_decrypt_block(tdes_key* key, void* dst, void* src);
 * @brief 三重DES解密一个区块(8字节)
 * @param[out] key 三重DES密钥
 * @param[out] dst 明文输出缓存
 * @param[in] src 密文输入缓存，大端字节序
 */
static inline void tdes_decrypt_block(tdes_key* key, void* dst, void* src)
{
	__tdes_crypt(key->dsk, (uint8_t*)dst, (uint8_t*)src);
}

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_DES__ */

