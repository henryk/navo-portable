/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/crypto/symmetric/base64.c
 *
 * @brief Implements for Base64.
 *
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#include "base64.h"

#ifdef __cplusplus
#include <cstring>
extern "C" {
#else
#include <string.h>
#endif /* #ifdef __cplusplus */

NV_IMPL void base64_set_key(base64_key* key, void* src_key)
{
    int i;
    uint8_t* esk = key->esk;
    uint8_t* dsk = key->dsk;

	memcpy(esk, src_key, sizeof(key->esk));

	for (i = 0; i < sizeof(key->esk); ++i) {
        dsk[esk[i] - BASE64_KEY_MIN] = (uint8_t)i;
	}
}

NV_IMPL void __base64_encrypt(uint8_t* esk, uint8_t* dst,
                            uint8_t* src, size_t cnt)
{
	while (cnt >= 3) {
        uint8_t j = ((src[0] << 4) | (src[1] >> 4)) & 0x3F;
		dst[0] = esk[src[0] >> 2];
		dst[1] = esk[j];
		dst[2] = esk[((src[1] << 2) | (src[2] >> 6)) & 0x3F];
		dst[3] = esk[src[2] & 0x3F];
		dst += 4; src += 3; cnt -= 3;
	}
	if (cnt) {
		dst[0] = esk[src[0] >> 2];
		if (cnt == 2) {
			dst[1] = esk[((src[0] << 4) | (src[1] >> 4)) & 0x3F];
			dst[2] = esk[(src[1] << 2) & 0x3F];
		} else {
			dst[1] = esk[(src[0] << 4) & 0x3F];
			dst[2] = '=';
		}
		dst[3] = '=';
	}
}

NV_IMPL void __base64_decrypt(uint8_t* dsk, uint8_t* dst,
                            uint8_t* src, size_t cnt)
{
    uint8_t I, J;
	while (cnt >= 4) {
        I = dsk[src[1] - BASE64_KEY_MIN];
        J = dsk[src[2] - BASE64_KEY_MIN];
		dst[0] = (I >> 4) | (dsk[src[0] - BASE64_KEY_MIN] << 2);
		dst[1] = (I << 4) | (J >> 2);
		dst[2] = (J << 6) | dsk[src[3] - BASE64_KEY_MIN];
		dst += 3;
		src += 4;
		cnt -= 4;
	}
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */


