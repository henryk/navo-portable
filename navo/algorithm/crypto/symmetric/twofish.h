/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/crypto/symmetric/twofish.h
 *
 * @brief Twofish
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_TWOFISH__
#define __NV_TWOFISH__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup Twofish
 * @ingroup 对称加密
 * @{
 */

/**
 * @struct twofish_key
 * @brief Twofish密钥
 */
typedef struct {
	/** @brief 置换盒 */
	uint32_t s[4][256];
	/** @brief 加密钥 */
	uint32_t esk[60];
	/** @brief 循环密钥 */
	uint32_t lsk[60];
}twofish_key;

#define TWOFISH_KEYSIZE_128		16
#define TWOFISH_KEYSIZE_192		24
#define TWOFISH_KEYSIZE_256		32

/**
 * @def TWOFISH_KEY(name)
 * @brief 定义名为name的TWOFISH密钥
 */
#define TWOFISH_KEY(name) twofish_key name = {{0}, {0}， 0}

NV_API int __twofish_set_key(twofish_key* key,
							uint8_t* src_key, size_t cnt);

/**
 * @def twofish_set_key(key, src_key, cnt)
 * @brief 设置TWOFISH密钥
 * @param[out] key TWOFISH密钥
 * @param[in] src_key 原始密钥
 * @param[in] cnt 原始密钥长度，密钥的长度必须是8的倍数
 * @return int 返回操作结果
 * @retval 1 成功
 * @retval 0 失败
 */
#define twofish_set_key(key, src_key, cnt) \
    __twofish_set_key(key, (uint8_t*)(src_key), cnt)

NV_API void __twofish_encrypt(uint32_t* esk, uint32_t* lsk,
				uint32_t (*S)[256], uint32_t* dst, uint32_t* src);

/**
 * @fn static inline void twofish_encrypt_block(twofish_key* key,
 											void* dst, void* src);
 * @brief TWOFISH加密一个区块(16字节)
 * @param[out] key TWOFISH密钥
 * @param[out] dst 密文输出缓存，小端字节序
 * @param[in] src 明文输入缓存
 */
static inline void twofish_encrypt_block(twofish_key* key,
                                        void* dst, void* src)
{
	__twofish_encrypt(key->esk, key->lsk, key->s,
				(uint32_t*)dst, (uint32_t*)src);
}

NV_API void __twofish_decrypt(uint32_t* esk, uint32_t* lsk,
				uint32_t (*S)[256], uint32_t* dst, uint32_t* src);

/**
 * @fn static inline void twofish_decrypt_block(twofish_key* key,
 											void* dst, void* src);
 * @brief TWOFISH解密一个区块(16字节)
 * @param[out] key TWOFISH密钥
 * @param[out] dst 明文输出缓存
 * @param[in] src 密文输入缓存，小端字节序
 */
static inline void twofish_decrypt_block(twofish_key* key,
                                        void* dst, void* src)
{
	__twofish_decrypt(key->esk, key->lsk, key->s,
				(uint32_t*)dst, (uint32_t*)src);
}

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_TWOFISH__ */

