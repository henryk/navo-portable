/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/crypto/symmetric/cast6.h
 *
 * @brief Cast6
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_CAST6__
#define __NV_CAST6__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup Cast6
 * @ingroup 对称加密
 * @{
 */

/**
 * @struct cast6_key
 * @brief Cast6密钥
 */
typedef struct {
	/** @brief  */
	uint32_t km[12][4];
	/** @brief  */
	uint8_t kr[12][4];
}cast6_key;

#define CAST6_MIN_KEYSIZE 	16
#define CAST6_MAX_KEYSIZE 	32

/**
 * @def CAST6_KEY(name)
 * @brief 定义名为name的CAST6密钥
 */
#define CAST6_KEY(name) cast6_key name = {{0}, {0}， 0}

NV_API int __cast6_set_key(uint32_t (*km)[4], uint8_t (*kr)[4],
							uint8_t* src_key, size_t cnt);

/**
 * @fn static inline void cast6_set_key(cast6_key* key,
 								void* src_key, size_t cnt);
 * @brief 设置Cast6密钥
 * @param[out] key Cast6密钥
 * @param[in] src_key 原始密钥
 * @param[in] cnt 原始密钥长度，密钥长度必须是在16、32之间的4的倍数
 * @return int 返回操作结果
 * @retval 1 成功
 * @retval 0 失败，密钥长度不匹配
 */
static inline int cast6_set_key(cast6_key* key, void* src_key, size_t cnt)
{
	return __cast6_set_key(key->km, key->kr, (uint8_t*)src_key, cnt);
}

NV_API void __cast6_encrypt(uint32_t (*km)[4], uint8_t (*kr)[4],
							uint32_t* dst, uint32_t* src);

/**
 * @fn static inline void cast6_encrypt_block(cast6_key* key, void* dst, void* src);
 * @brief Cast6加密一个区块(16字节)
 * @param[out] key Cast6密钥
 * @param[out] dst 密文输出缓存，大端字节序
 * @param[in] src 明文输入缓存
 * @see cast6_decrypt_block
 */
static inline void cast6_encrypt_block(cast6_key* key, void* dst, void* src)
{
	__cast6_encrypt(key->km, key->kr, (uint32_t*)dst, (uint32_t*)src);
}

NV_API void __cast6_decrypt(uint32_t (*km)[4], uint8_t (*kr)[4],
							uint32_t* dst, uint32_t* src);

/**
 * @fn static inline void cast6_decrypt_block(cast6_key* key, void* dst, void* src);
 * @brief Cast6解密一个区块(16字节)
 * @param[out] key Cast6密钥
 * @param[out] dst 明文输出缓存
 * @param[in] src 密文输入缓存，大端字节序
 * @see cast6_encrypt_block
 */
static inline void cast6_decrypt_block(cast6_key* key, void* dst, void* src)
{
	__cast6_decrypt(key->km, key->kr, (uint32_t*)dst, (uint32_t*)src);
}

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_CAST6__ */

