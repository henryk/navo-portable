/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/crypto/symmetric/cast5.h
 *
 * @brief Cast5
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_CAST5__
#define __NV_CAST5__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup Cast5
 * @ingroup 对称加密
 * @{
 */

/**
 * @struct cast5_key
 * @brief Cast5密钥
 */
typedef struct {
	/** @brief  */
	uint32_t km[16];
	/** @brief  */
	uint8_t kr[16];
	/** @brief  */
	uint32_t rd;
}cast5_key;

#define CAST5_MIN_KEYSIZE 	5
#define CAST5_MAX_KEYSIZE 	16

/**
 * @def CAST5_KEY(name)
 * @brief 定义名为name的CAST5密钥
 */
#define CAST5_KEY(name) cast5_key name = {{0}, {0}， 0}

NV_API void __cast5_set_key(cast5_key* key, uint32_t* km, uint8_t* kr, 
							uint8_t* src_key, size_t cnt);

/**
 * @fn static inline void cast5_set_key(cast5_key* key, 
 								void* src_key, size_t cnt);
 * @brief 设置Cast5密钥
 * @param[out] key Cast5密钥
 * @param[in] src_key 原始密钥
 * @param[in] cnt 原始密钥长度，密钥长度必须是在5、16之间
 */
static inline void cast5_set_key(cast5_key* key, void* src_key, size_t cnt)
{
	__cast5_set_key(key, key->km, key->kr, (uint8_t*)src_key, cnt);
}

NV_API void __cast5_encrypt(uint32_t* km, uint8_t* kr, int rd, 
							uint32_t* dst, uint32_t* src);

/**
 * @fn static inline void cast5_encrypt_block(cast5_key* key, void* dst, void* src);
 * @brief Cast5加密一个区块(8字节)
 * @param[out] key Cast5密钥
 * @param[out] dst 密文输出缓存，大端字节序
 * @param[in] src 明文输入缓存
 */
static inline void cast5_encrypt_block(cast5_key* key, void* dst, void* src)
{
	__cast5_encrypt(key->km, key->kr, key->rd, (uint32_t*)dst, (uint32_t*)src);
}

NV_API void __cast5_decrypt(uint32_t* km, uint8_t* kr, int rd, 
							uint32_t* dst, uint32_t* src);

/**
 * @fn static inline void cast5_decrypt_block(cast5_key* key, void* dst, void* src);
 * @brief Cast5解密一个区块(8字节)
 * @param[out] key Cast5密钥
 * @param[out] dst 明文输出缓存
 * @param[in] src 密文输入缓存，大端字节序
 */
static inline void cast5_decrypt_block(cast5_key* key, void* dst, void* src)
{
	__cast5_decrypt(key->km, key->kr, key->rd, (uint32_t*)dst, (uint32_t*)src);
}

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_CAST5__ */

