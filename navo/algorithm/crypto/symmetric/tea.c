/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/crypto/symmetric/tea.c
 *
 * @brief TEA.
 *
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#include "tea.h"
#include "../../../port/endian.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

NV_IMPL void tea_set_key(tea_key* key, void* src_key)
{
	uint32_t* pkey = (uint32_t*)src_key;
	key->sk[0] = __le32_to_cpu(pkey[0]);
	key->sk[1] = __le32_to_cpu(pkey[1]);
	key->sk[2] = __le32_to_cpu(pkey[2]);
	key->sk[3] = __le32_to_cpu(pkey[3]);
}

NV_IMPL void __tea_encrypt(uint32_t* sk, uint32_t* dst, uint32_t* src)
{
	uint32_t n = 32;
	uint32_t sum = 0;

	uint32_t y = __le32_to_cpu(src[0]);
	uint32_t z = __le32_to_cpu(src[1]);

	uint32_t k0 = sk[0];
	uint32_t k1 = sk[1];
	uint32_t k2 = sk[2];
	uint32_t k3 = sk[3];

	while (n--) {
		sum += 0x9E3779B9UL;
		y += ((z << 4) + k0) ^ (z + sum) ^ ((z >> 5) + k1);
		z += ((y << 4) + k2) ^ (y + sum) ^ ((y >> 5) + k3);
	}

	dst[0] = __cpu_to_le32(y);
	dst[1] = __cpu_to_le32(z);
}

NV_IMPL void __tea_decrypt(uint32_t* sk, uint32_t* dst, uint32_t* src)
{
	int n = 32;
	uint32_t sum = 0xC6EF3720UL;

	uint32_t y = __le32_to_cpu(src[0]);
	uint32_t z = __le32_to_cpu(src[1]);

	uint32_t k0 = sk[0];
	uint32_t k1 = sk[1];
	uint32_t k2 = sk[2];
	uint32_t k3 = sk[3];

	while (n--) {
		z -= ((y << 4) + k2) ^ (y + sum) ^ ((y >> 5) + k3);
		y -= ((z << 4) + k0) ^ (z + sum) ^ ((z >> 5) + k1);
		sum -= 0x9E3779B9UL;
	}

	dst[0] = __cpu_to_le32(y);
	dst[1] = __cpu_to_le32(z);
}

NV_IMPL void __xtea_encrypt(uint32_t* sk, uint32_t* dst, uint32_t* src)
{
	uint32_t sum = 0;

	uint32_t y = __le32_to_cpu(src[0]);
	uint32_t z = __le32_to_cpu(src[1]);

	while (sum != 0xC6EF3720UL) {
		y += ((((z << 4) ^ (z >> 5)) + z) ^ (sum + sk[sum & 3]));
		sum += 0x9E3779B9UL;
		z += ((((y << 4) ^ (y >> 5)) + y) ^ (sum + sk[(sum >> 11) & 3]));
	}

	dst[0] = __cpu_to_le32(y);
	dst[1] = __cpu_to_le32(z);
}

NV_IMPL void __xtea_decrypt(uint32_t* sk, uint32_t* dst, uint32_t* src)
{
	uint32_t sum = 0xC6EF3720UL;

	uint32_t y = __le32_to_cpu(src[0]);
	uint32_t z = __le32_to_cpu(src[1]);

	while (sum) {
		z -= ((((y << 4) ^ (y >> 5)) + y) ^ (sum + sk[(sum >> 11) & 3]));
		sum -= 0x9E3779B9UL;
		y -= ((((z << 4) ^ (z >> 5)) + z) ^ (sum + sk[sum & 3]));
	}

	dst[0] = __cpu_to_le32(y);
	dst[1] = __cpu_to_le32(z);
}


#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */


