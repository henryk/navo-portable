/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/crypto/symmetric/blowfish.h
 *
 * @brief Blowfish
 *
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_BLOWFISH__
#define __NV_BLOWFISH__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup Blowfish
 * @ingroup �ԳƼ���
 * @{
 */

/**
 * @struct blowfish_key
 * @brief Blowfish��Կ
 */
typedef struct {
	/** @brief ����Կ */
	uint32_t esk[18];
	/** @brief �û��� */
	uint32_t s[1024];
}blowfish_key;

/**
 * @def BLOWFISH_KEY(name)
 * @brief ������Ϊname��Blowfish��Կ
 */
#define BLOWFISH_KEY(name) blowfish_key name = {{0}, {0}}

NV_API void __blowfish_set_key(blowfish_key* key,
								uint8_t* src_key, size_t cnt);

/**
 * @def blowfish_set_key(key, src_key, cnt)
 * @brief ����Blowfish��Կ
 * @param[out] key Blowfish��Կ
 * @param[in] src_key ԭʼ��Կ
 * @param[in] cnt ԭʼ��Կ����
 */
#define blowfish_set_key(key, src_key, cnt) \
    __blowfish_set_key(key, (uint8_t*)(src_key), cnt)

NV_API void __blowfish_encrypt(uint32_t* esk, uint32_t* s_box,
								uint32_t* dst, uint32_t* src);

/**
 * @fn static inline void blowfish_encrypt_block(blowfish_key* key,
 											void* dst, void* src);
 * @brief Blowfish����һ������(8�ֽ�)
 * @param[out] key Blowfish��Կ
 * @param[out] dst ����������棬����ֽ���
 * @param[in] src �������뻺��
 */
static inline void blowfish_encrypt_block(blowfish_key* key,
											void* dst, void* src)
{
	__blowfish_encrypt(key->esk, key->s, (uint32_t*)dst, (uint32_t*)src);
}

NV_API void __blowfish_decrypt(uint32_t* dsk, uint32_t* s_box,
								uint32_t* dst, uint32_t* src);

/**
 * @fn static inline void blowfish_decrypt_block(blowfish_key* key,
 											void* dst, void* src);
 * @brief Blowfish����һ������(8�ֽ�)
 * @param[out] key Blowfish��Կ
 * @param[out] dst �����������
 * @param[in] src �������뻺�棬����ֽ���
 */
static inline void blowfish_decrypt_block(blowfish_key* key,
											void* dst, void* src)
{
	__blowfish_decrypt(key->esk, key->s, (uint32_t*)dst, (uint32_t*)src);
}

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_BLOWFISH__ */

