/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/crypto/symmetric/tea.h
 * 
 * @brief TEA
 * 
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_TEA__
#define __NV_TEA__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup TEA
 * @ingroup �ԳƼ���
 * @{
 */

/**
 * @struct tea_key
 * @brief TEA��Կ
 */
typedef struct {
	/** @brief ��Կ */
	uint32_t sk[4];
}tea_key, xtea_key;

/**
 * @def TEA_KEY(name)
 * @brief ������Ϊname��TEA��Կ
 */
#define TEA_KEY(name) tea_key name = {{0}}

/**
 * @def XTEA_KEY(name)
 * @brief ������Ϊname��XTEA��Կ
 */
#define XTEA_KEY(name) xtea_key name = {{0}}

/**
 * @fn void tea_set_key(xtea_key* key, void* src_key);
 * @brief ����TEA��Կ
 * @param[out] key TEA��Կ
 * @param[in] src_key 16�ֽ�ԭʼ��Կ
 */
NV_API void tea_set_key(tea_key* key, void* src_key);

NV_API void __tea_encrypt(uint32_t* sk, uint32_t* dst, uint32_t* src);

/**
 * @fn void tea_encrypt_block(tea_key* key, void* dst, void* src);
 * @brief TEA����һ������(8�ֽ�)
 * @param[out] key TEA��Կ
 * @param[out] dst ����������棬С���ֽ���
 * @param[in] src �������뻺��
 */
static inline void tea_encrypt_block(tea_key* key, void* dst, void* src)
{
	__tea_encrypt(key->sk, (uint32_t*)dst, (uint32_t*)src);
}

NV_API void __tea_decrypt(uint32_t* sk, uint32_t* dst, uint32_t* src);

/**
 * @fn void tea_decrypt_block(tea_key* key, void* dst, void* src);
 * @brief TEA����һ������(8�ֽ�)
 * @param[out] key TEA��Կ
 * @param[out] dst �����������
 * @param[in] src �������뻺�棬С���ֽ���
 */
static inline void tea_decrypt_block(tea_key* key, void* dst, void* src)
{
	__tea_decrypt(key->sk, (uint32_t*)dst, (uint32_t*)src);
}

/**
 * @def xtea_set_key(key, src_key)
 * @brief ����TEA��Կ
 * @param[out] key XTEA��Կ
 * @param[in] src_key 16�ֽ�ԭʼ��Կ
 */
#define xtea_set_key(key, src_key) tea_set_key(key, src_key)

NV_API void __xtea_encrypt(uint32_t* sk, uint32_t* dst, uint32_t* src);

/**
 * @fn void xtea_encrypt_block(xtea_key* key, void* dst, void* src);
 * @brief XTEA����һ������(8�ֽ�)
 * @param[out] key XTEA��Կ
 * @param[out] dst �����������
 * @param[in] src �������뻺��
 */
static inline void xtea_encrypt_block(xtea_key* key, void* dst, void* src)
{
	__xtea_encrypt(key->sk, (uint32_t*)dst, (uint32_t*)src);
}

NV_API void __xtea_decrypt(uint32_t* sk, uint32_t* dst, uint32_t* src);

/**
 * @fn void xtea_decrypt_block(xtea_key* key, void* dst, void* src);
 * @brief XTEA����һ������(8�ֽ�)
 * @param[out] key XTEA��Կ
 * @param[out] dst �����������
 * @param[in] src �������뻺��
 */
static inline void xtea_decrypt_block(xtea_key* key, void* dst, void* src)
{
	__xtea_decrypt(key->sk, (uint32_t*)dst, (uint32_t*)src);
}

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_TEA__ */

