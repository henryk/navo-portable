/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/nonlinear/binary/rblnk.h
 * 
 * @brief Red-black linked tree.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_RBLNK__
#define __NV_RBLNK__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"

#ifdef __cplusplus
extern "C" {
#endif	/* #ifdef __cplusplus */

/**
 * @defgroup 红黑树
 * @ingroup 二叉树
 * @{
 */

#if defined(_MSC_VER) || defined(__CYGWIN__)
#pragma pack(push, 2)
#endif /* #if defined(_MSC_VER) || defined(__CYGWIN__) */

/**
 * @struct __rblnk_node
 * @brief 红黑树节点
 * @typedef rblnk_node
 * @brief 节点
 * @typedef rblnk_iter
 * @brief 迭代器
 */
typedef struct intrin_align(2) __rblnk_node{
	/** @brief 父节点和颜色值 */
	uintptr_t parent_color;
	/** @brief 左子节点 */
	struct __rblnk_node* left;
	/** @brief 右子节点 */
	struct __rblnk_node* right;
}rblnk_node, *rblnk_iter;

#if defined(_MSC_VER) || defined(__CYGWIN__)
#pragma pack(pop)
#endif /* #if defined(_MSC_VER) || defined(__CYGWIN__) */

#define RBLNK_RED		0 /* 红色 */
#define RBLNK_BLACK		1 /* 黑色 */
#define RBLNK_MASK		1 /* 颜色掩码 */

/**
 * @struct rblnk_root
 * @brief 树根
 */
typedef struct {
	rblnk_node* node;
}rblnk_root;

/**
 * @struct rblnk_info
 * @brief 树信息表
 */
typedef struct {
	/** @brief 节点数 */
	size_t count;
	/** @brief 深度 */
	size_t depth;
	/** @brief 叶子数 */
	size_t leafs;
}rblnk_info;

/**
 * @def RBLNK_ROOT(name)
 * @brief 定义一个名为name的树根，并对其初始化
 * @param name 树根名称
 * @def RBLNK_NODE(name)
 * @brief 定义一个名为name的节点，并对其初始化
 * @param name 节点名称
 * @def RBLNK_INFO(name)
 * @brief 定义一个名为name的树信息表，并对其初始化
 * @param name 名称
 */
#define RBLNK_ROOT(name) rblnk_root name = {NULL}
#define RBLNK_NODE(name) rblnk_node name = {0, NULL, NULL}
#define RBLNK_INFO(name) rblnk_info name = {0, 0, 0}

/**
 * @typedef rblnk_pr1
 * @brief 单参数回调函数指针
 * @typedef rblnk_pr2
 * @brief 双参数回调函数指针
 */
typedef int (*rblnk_pr1)(rblnk_node*);
typedef int (*rblnk_pr2)(rblnk_node*, rblnk_node*);

/**
 * @fn size_t rblnk_count(rblnk_root* root);
 * @brief 获取树的节点总数
 * @param[in] root 树根
 * @return size_t 返回节点数
 * @see rblnk_get_info
 */
NV_API size_t rblnk_count(rblnk_root* root);

/**
 * @fn size_t rblnk_distance(rblnk_iter first, rblnk_iter last);
 * @brief 计算两个节点的迭代距离
 * @param[in] first 首节点
 * @param[in] last 尾节点
 * @note 以索引为例，2到4的距离是4 (4-2+1=3)
 * @return size_t 返回距离
 */
NV_API size_t rblnk_distance(rblnk_iter first, rblnk_iter last);

/**
 * @fn size_t rblnk_index_of(rblnk_root* root, rblnk_iter itr);
 * @brief 计算迭代器的正向索引
 * @param[in] root 树根
 * @param[in] itr 迭代器
 * @return size_t 返回索引
 * @see rblnk_reverse_index_of
 */
NV_API size_t rblnk_index_of(rblnk_root* root, rblnk_iter itr);

/**
 * @fn size_t rblnk_reverse_index_of(rblnk_root* root, rblnk_iter itr);
 * @brief 计算迭代器的逆向索引
 * @param[in] root 树根
 * @param[in] itr 迭代器
 * @return size_t 返回索引
 * @see rblnk_index_of
 */
NV_API size_t rblnk_reverse_index_of(rblnk_root* root, rblnk_iter itr);

/**
 * @fn size_t rblnk_depth(rblnk_root* root);
 * @brief 计算树的深度
 * @param[in] root 树根
 * @return size_t 返回深度
 * @see rblnk_get_info
 */
NV_API size_t rblnk_depth(rblnk_root* root);

/**
 * @fn size_t rblnk_leafs(rblnk_root* root);
 * @brief 计算树的叶子数
 * @param[in] root 树根
 * @return size_t 返回叶子数
 * @see rblnk_get_info
 */
NV_API size_t rblnk_leafs(rblnk_root* root);

/**
 * @fn void rblnk_get_info(rblnk_root* root, rblnk_info* info);
 * @brief 获取树的信息，包括节点数、深度、叶子数
 * @param[in] root 树根
 * @param[out] info 保存信息的结构体
 * @see rblnk_count, rblnk_depth, rblnk_leafs
 */
NV_API void rblnk_get_info(rblnk_root* root, rblnk_info* info);

/**
 * @fn void rblnk_coloring(rblnk_root* root, rblnk_node* node, 
								rblnk_node* parent);
 * @brief 插入节点后，对树进行着色调整
 * @param[in,out] root 树根
 * @param[in,out] node 目标节点
 * @param[in,out] parent node的父节点
 * @note 常用于节点插入，在rblnk_link之后调用
 * @par 示例:
 * @code
 	int insert_node(rblnk_root* root, DATA* newly)
 	{
	 	DATA* pdat;
 		rblnk_iter cur;
 		rblnk_iter* p = &root->node;
 		
 		// 初始化新节点
 		rblnk_node_init(&newly->node);
 		
 		while (*p) {
 			cur = *p;
 			pdat = container_of(cur, DATA, node);
 			if (pdat->data > newly->data)
 				p = &cur->left;
 			else if (pdat->data < newly->data)
 				p = &cur->right;
 			else {
 				// 有相同值的节点，无法插入
 				return 0;
 			}
 		}
 		
 		// 链接新节点到树中
 		rblnk_link(&newly->node, cur, p);
 		
 		// 着色调整
 		rblnk_coloring(root, &newly->node, cur);

 		return 1;
 	}
 * @endcode
 * @see rblnk_insert, rblnk_link
 */
NV_API void rblnk_coloring(rblnk_root* root, rblnk_node* node, 
							rblnk_node* parent);

/**
 * @fn void rblnk_erase(rblnk_root* root, rblnk_node* entry);
 * @brief 移除某个节点
 * @param[in,out] root 树根
 * @param[in,out] entry 待移除节点
 */
NV_API void rblnk_erase(rblnk_root* root, rblnk_node* entry);

/**
 * @fn void rblnk_erases(rblnk_root* root, rblnk_iter begin, rblnk_iter end, 
							rblnk_pr1 erase_handler);
 * @brief 移除区域内的节点
 * @param[in,out] root 树根

 * @param[in,out] begin 起始位置
 * @param[in,out] end 终止位置
 * @param[in] erase_handler 移除响应函数，通常用于监视和释放节点，可以传递NULL,
 							表示不为删除的节点调用响应来处理
 * @note 函数会为每一个被移除的节点调用erase_handler来处理节点
 * @see rblnk_clear
 */
NV_API void rblnk_erases(rblnk_root* root, rblnk_iter begin, 
						rblnk_iter end, rblnk_pr1 erase_handler);

/**
 * @fn static inline void rblnk_init(rblnk_root* root);
 * @brief 初始化树根
 * @param[out] root 目标树根
 * @par 示例:
 * @code
	rblnk_root r;
	// 初始化
	rblnk_init(&r);
 * @endcode
 */
static inline void rblnk_init(rblnk_root* root)
{
	root->node = NULL;
}

/**
 * @fn static inline int rblnk_empty(rblnk_root* root);
 * @brief 判断树是否为空
 * @param[in] root 树根
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	if (!rblnk_empty(&r)) {
 		......
 	}
 * @endcode
 */
static inline int rblnk_empty(rblnk_root* root)
{
	return !root->node;
}

static inline int __rblnk_singular(rblnk_node* root)
{
	return root && !root->left && !root->right;
}

/**
 * @fn static inline int rblnk_singular(rblnk_root* root);
 * @brief 判断树是否只有单个节点
 * @param[in] root 树根
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	if (rblnk_singular(&r)) {
 		......
 	}
 * @endcode
 */
static inline int rblnk_singular(rblnk_root* root)
{
	return __rblnk_singular(root->node);
}

/**
 * @fn static inline int rblnk_serialable(rblnk_root* root);
 * @brief 判断树是否可以序列化
 * @param[in] root 树根
 * @note 即判断是否至少有一个节点
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	// 判断是否可以序列化
 	if (rblnk_serialable(&r)) {
 		slseq_head seq;
 		// 分配序列缓存
 		DATA** buf = (DATA**)malloc(sizeof(DATA*) * avl_cnt);
 		// 序列化
 		rblnk_serialize(&r, buf, DATA, node);
 		SLSEQ_INIT_SERIAL(&seq, buf, avl_cnt);
 		......
 		// do something
 		......
 		// 反序列化
 		rblnk_deserialize(&r, buf, avl_cnt, DATA, node);
 		// 释放缓存
 		free(buf);
 	}
 * @endcode
 * @see rblnk_serialize, rblnk_deserialize
 */
static inline int rblnk_serialable(rblnk_root* root)
{
	return !!root->node;
}

/**
 * @fn static inline rblnk_iter rblnk_left(rblnk_node* node);
 * @brief 得到左子节点
 * @param[in] node 当前节点
 * @return rblnk_iter 返回左子节点
 * @see rblnk_right
 */
static inline rblnk_iter rblnk_left(rblnk_node* node)
{
	return node->left;
}

/**
 * @fn static inline rblnk_iter rblnk_right(rblnk_node* node);
 * @brief 得到右子节点
 * @param[in] node 当前节点
 * @return rblnk_iter 返回右子节点
 * @see rblnk_left
 */
static inline rblnk_iter rblnk_right(rblnk_node* node)
{
	return node->right;
}

/**
 * @fn static inline rblnk_iter rblnk_parent(rblnk_node* node);
 * @brief 得到父节点
 * @param[in] node 当前节点
 * @return rblnk_iter 返回父节点
 */
static inline rblnk_iter rblnk_parent(rblnk_node* node)
{
	return (rblnk_iter)(node->parent_color & (~RBLNK_MASK));
}

/**
 * @fn static inline uintptr_t rblnk_color(rblnk_node* node);
 * @brief 得到节点的颜色值
 * @param[in] node 当前节点
 * @return uintptr_t 返回颜色值
 * @retval RBLNK_RED 红色
 * @retval RBLNK_BLACK 黑色
 */
static inline uintptr_t rblnk_color(rblnk_node* node)
{
	return (node->parent_color & RBLNK_MASK);
}

/**
 * @fn static inline int rblnk_red(rblnk_node* node);
 * @brief 判断节点是否是红色的
 * @param[in] node 当前节点
 * @return int 返回结果
 * @retval 1 节点是红色的
 * @retval 0 节点是黑色的
 */
static inline int rblnk_red(rblnk_node* node)
{
	return rblnk_color(node) == RBLNK_RED;
}

/**
 * @fn static inline int rblnk_black(rblnk_node* node);
 * @brief 判断节点是否是黑色的
 * @param[in] node 当前节点
 * @return int 返回结果
 * @retval 1 节点是黑色的
 * @retval 0 节点是红色的
 */
static inline int rblnk_black(rblnk_node* node)
{
	return rblnk_color(node) == RBLNK_BLACK;
}

/**
 * @fn static inline int rblnk_is_root(rblnk_iter itr);
 * @brief 判断当前节点是否是根节点
 * @param[in] itr 当前节点
 * @return int 返回结果
 * @retval 1 是根节点
 * @retval 0 不是根节点
 */
static inline int rblnk_is_root(rblnk_iter itr)
{
	return !rblnk_parent(itr);
}

/**
 * @fn static inline int rblnk_is_leaf(rblnk_iter itr);
 * @brief 判断当前节点是否是叶子节点
 * @param[in] itr 当前节点
 * @return int 返回结果
 * @retval 1 是叶子节点
 * @retval 0 不是叶子节点
 */
static inline int rblnk_is_leaf(rblnk_iter itr)
{
	return !itr->left && !itr->right;
}

/**
 * @fn static inline void rblnk_link(rblnk_iter newly, rblnk_iter parent, 
								rblnk_iter* plink);
 * @brief 链接两个节点
 * @param[in,out] newly 新节点
 * @param[in,out] parent 父节点
 * @param[in,out] plink 父节点对应孩子指针。若newly加入parent的左子，则plink指向
 						parent的left成员，若newly加入parent的右子，则plink指向
 						parent的right成员
 * @note 链接newly和parent节点
 */
static inline void rblnk_link(rblnk_iter newly, rblnk_iter parent, 
								rblnk_iter* plink)
{
	/* 简单地将newly节点链接到parent节点下 */
	*plink = newly;
	newly->left = NULL;
	newly->right = NULL;
	/* 新节点默认是红色的 */
	newly->parent_color = (uintptr_t)parent;
}

/**
 * @fn static inline void rblnk_set_red(rblnk_node* node);
 * @brief 设置当前节点的颜色为红色
 * @param[in,out] node 当前节点
 */
static inline void rblnk_set_red(rblnk_node* node)
{
	node->parent_color &= (~RBLNK_MASK);
}

/**
 * @fn static inline void rblnk_set_black(rblnk_node* node);
 * @brief 设置当前节点的颜色为黑色
 * @param[in,out] node 当前节点
 */
static inline void rblnk_set_black(rblnk_node* node)
{
	node->parent_color = RBLNK_BLACK | 
		(node->parent_color & (~RBLNK_MASK));
}

/**
 * @fn static inline void rblnk_set_color(rblnk_node* node, uintptr_t color);
 * @brief 设置当前节点的颜色
 * @param[in,out] node 当前节点
 * @param[in] color 新颜色，有RBLNK_RED(红色)和RBLNK_BLACK(黑色)两种
 */
static inline void rblnk_set_color(rblnk_node* node, uintptr_t color)
{
	node->parent_color = color | 
		(node->parent_color & (~RBLNK_MASK));
}

/**
 * @fn static inline void rblnk_set_parent(rblnk_node* node, 
 										rblnk_node* parent);
 * @brief 设置当前节点的父节点
 * @param[in,out] node 当前节点
 * @param[in] parent 新的父节点
 */
static inline void rblnk_set_parent(rblnk_node* node, rblnk_node* parent)
{
	node->parent_color = (uintptr_t)(parent) | 
		(node->parent_color & RBLNK_MASK);
}

/**
 * @fn static inline void rblnk_set_parent_if(rblnk_node* node, 
									rblnk_node* parent);
 * @brief 仅在node不为空时，设置node的父节点
 * @param[in,out] node 当前节点
 * @param[in] parent 新的父节点
 */
static inline void rblnk_set_parent_if(rblnk_node* node, rblnk_node* parent)
{
	if (node)
		rblnk_set_parent(node, parent);
}

/**
 * @fn static inline void rblnk_set_parent_color(rblnk_node* node, 
							rblnk_node* parent, uintptr_t color);
 * @brief 同时设置节点的父节点和颜色值
 * @param[out] node 当前节点
 * @param[in] parent 新的父节点
 * @param[in] height 新的高度值，有RBLNK_RED(红色)和RBLNK_BLACK(黑色)两种
 */
static inline void rblnk_set_parent_color(rblnk_node* node, 
						rblnk_node* parent, uintptr_t color)
{
	node->parent_color = (uintptr_t)(parent) | color;
}

/**
 * @fn static inline void rblnk_node_init(rblnk_node* node);
 * @brief 初始化节点
 * @param[out] node 指定节点
 */
static inline void rblnk_node_init(rblnk_node* node)
{
	node->parent_color = 0;
	node->left = NULL;
	node->right = NULL;
}

/**
 * @fn static inline void rblnk_iter_replace(rblnk_root* root, 
							rblnk_iter victim,  rblnk_iter newly);
 * @brief 用新节点替换旧节点
 * @param[in,out] root 树根
 * @param[in,out] victim 受害目标节点
 * @param[out] newly 新节点
 */
static inline void rblnk_iter_replace(rblnk_root* root, rblnk_iter victim, 
									  rblnk_iter newly)
{
	rblnk_iter parent = rblnk_parent(victim);
	
	/* 将victim父节点的孩子设为newly */
	if (parent) {
		if (victim == parent->left)
			parent->left = newly;
		else
			parent->right = newly;
	} else
		root->node = newly;
	
	/* 重新确定victim孩子节点的父节点为newly */
	rblnk_set_parent_if(victim->left, newly);
	rblnk_set_parent_if(victim->right, newly);
	
	/* 替换节点成员 */
	*newly = *victim;
}

/**
 * @fn static inline void rblnk_replace(rblnk_root* victim, rblnk_root* newly);
 * @brief 用新的树根替换旧树根
 * @param[in,out] victim 受害目标树根
 * @param[out] newly 新树根
 */
static inline void rblnk_replace(rblnk_root* victim, rblnk_root* newly)
{
	newly->node = victim->node;
}

/**
 * @fn static inline void rblnk_swap(rblnk_root* a, rblnk_root* b);
 * @brief 交换两个树
 * @param[in,out] a 第一个树
 * @param[in,out] b 第二个树
 */
static inline void rblnk_swap(rblnk_root* a, rblnk_root* b)
{
	rblnk_iter t = a->node; a->node = b->node; b->node = t;
}

/**
 * @fn int rblnk_exist(rblnk_root* root, rblnk_iter itr);
 * @brief 判断一个节点是否存在于树中
 * @param[in] root 树根
 * @param[in] itr 指定节点
 * @return int 返回结果
 * @retval 1 存在
 * @retval 0 不存在
 */
NV_API int rblnk_exist(rblnk_root* root, rblnk_iter itr);

/**
 * @fn static inline rblnk_iter rblnk_at(rblnk_root* root, size_t index);
 * @brief 得到正向索引对应的节点的迭代器
 * @param[in] root 树根
 * @param[in] index 指定正向索引
 * @return rblnk_iter 返回迭代器
 * @see rblnk_reverse_at
 */
static inline rblnk_iter rblnk_at(rblnk_root* root, size_t index)
{
	return rblnk_advance(rblnk_front(root), index);
}

/**
 * @fn static inline rblnk_iter rblnk_reverse_at(rblnk_root* root, 
 												size_t index);
 * @brief 得到逆向索引对应的节点的迭代器
 * @param[in] root 树根
 * @param[in] index 指定逆向索引
 * @return rblnk_iter 返回迭代器
 * @see rblnk_at
 */
static inline rblnk_iter rblnk_reverse_at(rblnk_root* root, size_t index)
{
	return rblnk_advance(rblnk_back(root), -(int)(index));
}

/**
 * @fn rblnk_iter rblnk_front(rblnk_root* root);
 * @brief 得到树的最左端节点
 * @param[in] root 树根
 * @return rblnk_iter 返回迭代器
 * @see rblnk_back
 */
NV_API rblnk_iter rblnk_front(rblnk_root* root);

/**
 * @fn rblnk_iter rblnk_back(rblnk_root* root);
 * @brief 得到树的最右端节点
 * @param[in] root 树根
 * @return rblnk_iter 返回迭代器
 * @see rblnk_front
 */
NV_API rblnk_iter rblnk_back(rblnk_root* root);

/**
 * @fn static inline rblnk_iter rblnk_begin(rblnk_root* root);
 * @brief 得到指向正向起始位置的迭代器
 * @param[in] root 树根
 * @return rblnk_iter 返回迭代器
 * @see rblnk_end, rblnk_end_of, rblnk_rbegin, rblnk_rend
 */
static inline rblnk_iter rblnk_begin(rblnk_root* root)
{
	return rblnk_front(root);
}

/**
 * @fn static inline rblnk_iter rblnk_end();
 * @brief 得到指向正向终止位置的迭代器
 * @return rblnk_iter 返回迭代器，永远返回NULL
 * @see rblnk_begin, rblnk_end_of, rblnk_rbegin, rblnk_rend
 */
static inline rblnk_iter rblnk_end()
{
	return NULL;
}

/**
 * @fn static inline rblnk_iter rblnk_end_of(rblnk_iter cur);
 * @brief 得到当前迭代器的正向终止位置
 * @param[in] itr 迭代器
 * @return rblnk_iter 返回迭代器
 * @see rblnk_begin, rblnk_end
 */
static inline rblnk_iter rblnk_end_of(rblnk_iter cur)
{
	return rblnk_next(cur);
}

/**
 * @fn static inline rblnk_iter rblnk_rbegin(rblnk_root* root);
 * @brief 得到指向逆向起始位置的迭代器
 * @param[in] root 树根
 * @return rblnk_iter 返回迭代器
 * @see rblnk_rend, rblnk_rend_of, rblnk_begin, rblnk_end
 */
static inline rblnk_iter rblnk_rbegin(rblnk_root* root)
{
	return rblnk_back(root);
}

/**
 * @fn static inline rblnk_iter rblnk_rend();
 * @brief 得到指向逆向终止位置的迭代器
 * @return rblnk_iter 返回迭代器，永远返回NULL
 * @see rblnk_rbegin, rblnk_rend_of, rblnk_begin, rblnk_end
 */
static inline rblnk_iter rblnk_rend()
{
	return NULL;
}

/**
 * @fn static inline rblnk_iter rblnk_rend_of(rblnk_iter cur);
 * @brief 得到当前迭代器的逆向终止位置
 * @param[in] itr 迭代器
 * @return rblnk_iter 返回迭代器
 * @see rblnk_rbegin, rblnk_rend
 */
static inline rblnk_iter rblnk_rend_of(rblnk_iter cur)
{
	return rblnk_prev(cur);
}

/**
 * @fn rblnk_iter rblnk_next(rblnk_iter cur);
 * @brief 得到当前节点的中序后继节点
 * @param[in] cur 当前节点
 * @return rblnk_iter 返回后继节点，若无后继，则返回NULL
 * @see rblnk_prev
 */
NV_API rblnk_iter rblnk_next(rblnk_iter cur);

/**
 * @fn rblnk_iter rblnk_prev(rblnk_iter cur);
 * @brief 得到当前节点的中序前驱节点
 * @param[in] cur 当前节点
 * @return rblnk_iter 返回前驱节点，若无前驱，则返回NULL
 * @see rblnk_next
 */
NV_API rblnk_iter rblnk_prev(rblnk_iter cur);

/**
 * @fn rblnk_iter rblnk_advance(rblnk_iter cur, int dist);
 * @brief 给迭代器增加一段距离或减小一段距离
 * @param[in] cur 迭代器
 * @param[in] dist 增加的距离。正数时，表示增加；负数时，表示减小
 * @return rblnk_iter 返回新迭代器
 */
NV_API rblnk_iter rblnk_advance(rblnk_iter cur, int dist);

/**
 * @fn static inline void rblnk_clear(rblnk_root* root, 
 								rblnk_pr1 erase_handler);
 * @brief 清空树
 * @param[in,out] root 树根
 * @param[in] erase_handler 移除响应函数，通常用于监视和释放节点，可以传递NULL,
 							表示不为删除的节点调用响应来处理
 * @note 函数会为每一个被移除的节点调用erase_handler来处理节点
 * @see rblnk_erases
 */
static inline void rblnk_clear(rblnk_root* root, rblnk_pr1 erase_handler)
{
	if (!rblnk_empty(root))
		rblnk_erases(root, rblnk_front(root), NULL, erase_handler);
}

static inline rblnk_iter __rblnk_inc_later(rblnk_iter cur, rblnk_iter* p)
{
	*p = rblnk_next(cur);
	return cur;
}

static inline rblnk_iter __rblnk_dec_later(rblnk_iter cur, rblnk_iter* p)
{
	*p = rblnk_prev(cur);
	return cur;
}

/**
 * @def rblnk_inc(itr)
 * @brief 迭代器递增
 * @def rblnk_dec(itr)
 * @brief 迭代器递减
 * @def rblnk_inc_later(itr)
 * @brief 迭代器后自增
 * @def rblnk_dec_later(itr)
 * @brief 迭代器后自减
 */
#define rblnk_inc(itr)			((itr) = rblnk_next(itr))
#define rblnk_dec(itr)			((itr) = rblnk_prev(itr))
#define rblnk_inc_later(itr)	__rblnk_inc_later(itr, &(itr))
#define rblnk_dec_later(itr)	__rblnk_dec_later(itr, &(itr))

/**
 * @def rblnk_foreach(_begin, _end, fn, type, member)
 * @brief 正向遍历_begin到_end的节点，并为每一个节点调用回调函数或宏
 * @param[in] _begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define rblnk_foreach(_begin, _end, fn, type, member) do {\
		rblnk_iter _q_e = (_end);							\
		rblnk_iter _q_f = (_begin);							\
		while (_q_f != _q_e) {								\
			type * _q_t = container_of(_q_f, type, member);	\
			rblnk_inc(_q_f);								\
			fn(_q_t);										\
		}													\
	} while (0)

/**
 * @def rblnk_reverse_foreach(_rbegin, _rend, fn, type, member)
 * @brief 逆向遍历_begin到_end的节点，并为每一个节点调用回调函数或宏
 * @param[in] _rbegin 逆向起始位置
 * @param[in] _rend 逆向终止位置
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define rblnk_reverse_foreach(_rbegin, _rend, fn, type, member) do {\
		rblnk_iter _q_e = (_rend);							\
		rblnk_iter _q_f = (_rbegin);						\
		while (_q_f != _q_e) {								\
			type * _q_t = container_of(_q_f, type, member);	\
			rblnk_dec(_q_f);								\
			fn(_q_t);										\
		}													\
	} while (0)

NV_API void __do_rblnk_recurse_foreach(rblnk_iter cur, 
						void (*fn)(void*), size_t offset);

NV_API void __do_rblnk_recurse_reverse_foreach(rblnk_iter cur, 
						void (*fn)(void*), size_t offset);

static inline void __rblnk_recurse_foreach(rblnk_root* root, 
						void (*fn)(void*), size_t offset)
{
	if (fn)
		__do_rblnk_recurse_foreach(root->node, fn, offset);
}

static inline void __rblnk_recurse_reverse_foreach(rblnk_root* root, 
							void (*fn)(void*), size_t offset)
{
	if (fn)
		__do_rblnk_recurse_reverse_foreach(root->node, fn, offset);
}

/**
 * @def RBLNK_RECURSE_PROC(name, type, param)
 * @brief 定义递归遍历回调函数
 * @param name 函数名
 * @param type 节点拥有者类型
 * @param param 参数名
 * @see rblnk_recurse_foreach, rblnk_recurse_reverse_foreach
 */
#define RBLNK_RECURSE_PROC(name, type, param) void name(type * param)

/**
 * @def rblnk_recurse_foreach(root, fn, type, member)
 * @brief 正向递归遍历整个树
 * @param[in] root 树根
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @par 示例:
 * @code
 	RBLNK_RECURSE_PROC(PrintNode, DATA, node)
 	{
 		printf("%3d ", node->data);
 	}

	// 打印整个树
 	void PrintAVLTree(rblnk_root* root)
 	{
 		rblnk_recurse_foreach(root, PrintNode, DATA, node);
 		putchar('\n');
 	}
 * @endcode
 * @see rblnk_recurse_reverse_foreach, RBLNK_RECURSE_PROC
 */
#define rblnk_recurse_foreach(root, fn, type, member) \
	__rblnk_recurse_foreach(root, (void (*)(void*))(fn), \
			offset_of(type, member))

/**
 * @def rblnk_recurse_reverse_foreach(root, fn, type, member)
 * @brief 逆向递归遍历整个树
 * @param[in] root 树根
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see rblnk_recurse_foreach, RBLNK_RECURSE_PROC
 */
#define rblnk_recurse_reverse_foreach(root, fn, type, member) \
	__rblnk_recurse_reverse_foreach(root, (void (*)(void*))(fn), \
			offset_of(type, member))

/**
 * @def rblnk_search(root, newly, greater, res, type, member)
 * @brief 搜索树中的某个节点
 * @param[in] root 树根
 * @param[in] newly 用于搜索的临时节点，类型是type，该节点使用与搜索的值相同的值
 * @param[in] greater 比较函数或宏。
 						升序:(i->data > j->data) 
 						降序:(i->data < j->data)
 * @param[out] res 返回值。返回目标节点，若为NULL表示找不到节点
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @par 示例:
 * @code
 	rblnk_iter node_15;
 	DATA* p = (DATA*)malloc(sizeof(DATA));
 	if (!p)
 		return NULL;

 	p->data = 15;

	//查找值为15的节点
 	rblnk_search(&root, p, DATA_Greater, node_15, DATA, node);
 	if (!node_15)
 		return NULL;

 	return node_15;
 * @endcode
 */
#define rblnk_search(root, newly, greater, res, type, member) do {\
		rblnk_root* _k_r = (root);								\
		type * _k_pn = (newly);									\
		rblnk_iter _k_p = _k_r->node;							\
		(res) = NULL;											\
		while (_k_p) {											\
			type * _k_pr = container_of(_k_p, type, member);	\
			if (greater(_k_pr, _k_pn))							\
				_k_p = _k_p->left;								\
			else if (greater(_k_pn, _k_pr))						\
				_k_p = _k_p->right;								\
			else {												\
				(res) = _k_p; break;							\
			}													\
		}														\
	} while (0)

/**
 * @def rblnk_insert(root, newly, greater, res, type, member)
 * @brief 插入新节点
 * @param[in] root 树根
 * @param[in] newly 新节点
 * @param[in] greater 比较函数或宏。
 						升序:(i->data > j->data) 
 						降序:(i->data < j->data)
 * @param[out] res 返回值。返回1表示成功；若为0表示节点有冲突，无法插入
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @par 示例:
 * @code
 	int ret;
 	DATA* p = (DATA*)malloc(sizeof(DATA));
 	if (!p)
 		return 0;

 	p->data = 88;

	//插入值为88的节点
 	rblnk_insert(&root, &p->node, DATA_Greater, ret, DATA, node);
 	if (!ret)
 		return 0;

 	return 1;
 * @endcode
 */
#define rblnk_insert(root, newly, greater, res, type, member) do {\
		rblnk_root* _k_r = (root);										\
		rblnk_iter* _k_pp = &_k_r->node;								\
		rblnk_iter _k_n = (newly);										\
		rblnk_iter _k_p;												\
		type * _k_pn = container_of(_k_n, type, member);				\
		(res) = 1;														\
		rblnk_node_init(_k_n);											\
		while (*_k_pp) {												\
			type * _k_pr = container_of(_k_p = *_k_pp, type, member);	\
			if (greater(_k_pr, _k_pn))									\
				_k_pp = &_k_p->left;									\
			else if (greater(_k_pn, _k_pr))								\
				_k_pp = &_k_p->right;									\
			else {														\
				(res) = 0; break;										\
			}															\
		}																\
		if (res) {														\
			rblnk_link(_k_n, _k_p, _k_pp);								\
			rblnk_coloring(_k_r, _k_n, _k_p);							\
		}																\
	} while (0)

NV_API void __rblnk_serialize(rblnk_root* root, void** buf, size_t offset);

NV_API void __rblnk_deserialize(rblnk_root* root, void** buf, 
									size_t cnt, size_t offset);

/**
 * @def rblnk_serialize(root, buf, type, member)
 * @brief 将树进行序列化为一个有序序列
 * @param[in] root 树根
 * @param[out] buf 序列缓存，元素类型为type指针类型
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see rblnk_deserialize, rblnk_serialable
 */
#define rblnk_serialize(root, buf, type, member) \
	__rblnk_serialize(root, (void**)(buf), offset_of(type, member))

/**
 * @def rblnk_deserialize(root, buf, cnt, type, member)
 * @brief 根据树的序列进行反序列化为一个完整的树
 * @param[out] root 树根
 * @param[in] buf 序列缓存，元素类型为type指针类型
 * @param[in] cnt 序列长度(元素数)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see rblnk_serialize, rblnk_serialable
 */
#define rblnk_deserialize(root, buf, cnt, type, member) \
	__rblnk_deserialize(root, (void**)(buf), cnt, offset_of(type, member))

/** @} */

#ifdef __cplusplus
}
#endif	/* #ifdef __cplusplus */

#endif	/* #ifndef __NV_RBLNK__ */

