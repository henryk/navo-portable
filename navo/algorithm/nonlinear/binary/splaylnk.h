/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/nonlinear/binary/splaylnk.h
 * 
 * @brief Splay linked tree.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_SPLAYLNK__
#define __NV_SPLAYLNK__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"

#ifdef __cplusplus
extern "C" {
#endif	/* #ifdef __cplusplus */

/**
 * @defgroup 伸展树
 * @ingroup 二叉树
 * @{
 */

/**
 * @struct __splaylnk_node
 * @brief 伸展树节点
 * @typedef splaylnk_node
 * @brief 节点
 * @typedef splaylnk_iter
 * @brief 迭代器
 */
typedef struct __splaylnk_node {
	/** @brief 父节点 */
	struct __splaylnk_node* parent;
	/** @brief 左子节点 */
	struct __splaylnk_node* left;
	/** @brief 右子节点 */
	struct __splaylnk_node* right;
}splaylnk_node, *splaylnk_iter;

/**
 * @struct splaylnk_root
 * @brief 树根
 */
typedef struct {
	/** @brief 根节点 */
	splaylnk_node* node;
}splaylnk_root;

/**
 * @struct splaylnk_info
 * @brief 树信息表
 */
typedef struct {
	/** @brief 节点数 */
	size_t count;
	/** @brief 深度 */
	size_t depth;
	/** @brief 叶子数 */
	size_t leafs;
}splaylnk_info;

/**
 * @def SPLAYLNK_ROOT(name)
 * @brief 定义一个名为name的树根，并对其初始化
 * @param name 树根名称
 * @def SPLAYLNK_NODE(name)
 * @brief 定义一个名为name的节点，并对其初始化
 * @param name 节点名称
 * @def SPLAYLNK_INFO(name)
 * @brief 定义一个名为name的树信息表，并对其初始化
 * @param name 名称
 */
#define SPLAYLNK_ROOT(name) splaylnk_root name = {NULL}
#define SPLAYLNK_NODE(name) splaylnk_node name = {NULL, NULL, NULL}
#define SPLAYLNK_INFO(name) splaylnk_info name = {0, 0, 0}

/**
 * @typedef splaylnk_pr1
 * @brief 单参数回调函数指针
 * @typedef splaylnk_pr2
 * @brief 双参数回调函数指针
 */
typedef int (*splaylnk_pr1)(splaylnk_node*);
typedef int (*splaylnk_pr2)(splaylnk_node*, splaylnk_node*);

/**
 * @fn size_t splaylnk_count(splaylnk_root* root);
 * @brief 获取树的节点总数
 * @param[in] root 树根
 * @return size_t 返回节点数
 * @see splaylnk_get_info
 */
NV_API size_t splaylnk_count(splaylnk_root* root);

/**
 * @fn size_t splaylnk_distance(splaylnk_iter first, splaylnk_iter last);
 * @brief 计算两个节点的迭代距离
 * @param[in] first 首节点
 * @param[in] last 尾节点
 * @note 以索引为例，2到4的距离是4 (4-2+1=3)
 * @return size_t 返回距离
 */
NV_API size_t splaylnk_distance(splaylnk_iter first, splaylnk_iter last);

/**
 * @fn size_t splaylnk_index_of(splaylnk_root* root, splaylnk_iter itr);
 * @brief 计算迭代器的正向索引
 * @param[in] root 树根
 * @param[in] itr 迭代器
 * @return size_t 返回索引
 * @see splaylnk_reverse_index_of
 */
NV_API size_t splaylnk_index_of(splaylnk_root* root, splaylnk_iter itr);

/**
 * @fn size_t splaylnk_reverse_index_of(splaylnk_root* root, splaylnk_iter itr);
 * @brief 计算迭代器的逆向索引
 * @param[in] root 树根
 * @param[in] itr 迭代器
 * @return size_t 返回索引
 * @see splaylnk_index_of
 */
NV_API size_t splaylnk_reverse_index_of(splaylnk_root* root, 
									splaylnk_iter itr);

/**
 * @fn size_t splaylnk_depth(splaylnk_root* root);
 * @brief 计算树的深度
 * @param[in] root 树根
 * @return size_t 返回深度
 * @see splaylnk_get_info
 */
NV_API size_t splaylnk_depth(splaylnk_root* root);

/**
 * @fn size_t splaylnk_leafs(splaylnk_root* root);
 * @brief 计算树的叶子数
 * @param[in] root 树根
 * @return size_t 返回叶子数
 * @see splaylnk_get_info
 */
NV_API size_t splaylnk_leafs(splaylnk_root* root);

/**
 * @fn void splaylnk_get_info(splaylnk_root* root, splaylnk_info* info);
 * @brief 获取树的信息，包括节点数、深度、叶子数
 * @param[in] root 树根
 * @param[out] info 保存信息的结构体
 * @see splaylnk_count, splaylnk_depth, splaylnk_leafs
 */
NV_API void splaylnk_get_info(splaylnk_root* root, splaylnk_info* info);

/**
 * @fn void splaylnk_splay(splaylnk_root* root, splaylnk_node* node, 
								splaylnk_node* parent);
 * @brief 对节点进行伸展调整
 * @param[in,out] root 树根
 * @param[in,out] node 目标节点
 * @param[in,out] parent node的父节点
 * @note 常用于节点插入、节点访问之后的调整
 * @par 示例:
 * @code
 	int insert_node(splaylnk_root* root, DATA* newly)
 	{
	 	DATA* pdat;
 		splaylnk_iter cur;
 		splaylnk_iter* p = &root->node;
 		
 		// 初始化新节点
 		splaylnk_node_init(&newly->node);
 		
 		while (*p) {
 			cur = *p;
 			pdat = container_of(cur, DATA, node);
 			if (pdat->data > newly->data)
 				p = &cur->left;
 			else if (pdat->data < newly->data)
 				p = &cur->right;
 			else {
 				// 有相同值的节点，无法插入
 				return 0;
 			}
 		}
 		
 		// 链接新节点到树中
 		splaylnk_link(&newly->node, cur, p);
 		
 		// 伸展
 		splaylnk_splay(root, &newly->node, cur);

 		return 1;
 	}
 * @endcode
 * @see splaylnk_insert, splaylnk_link, splaylnk_semisplay
 */
NV_API void splaylnk_splay(splaylnk_root* root, splaylnk_node* node, 
							splaylnk_node* parent);

/**
 * @fn void splaylnk_semisplay(splaylnk_root* root, splaylnk_node* node, 
								splaylnk_node* parent);
 * @brief 对节点进行半伸展调整
 * @param[in,out] root 树根
 * @param[in,out] node 目标节点
 * @param[in,out] parent node的父节点
 * @note 常用于节点插入、节点访问之后的调整
 * @par 示例:
 * @code
 	int insert_node(splaylnk_root* root, DATA* newly)
 	{
	 	DATA* pdat;
 		splaylnk_iter cur;
 		splaylnk_iter* p = &root->node;
 		
 		// 初始化新节点
 		splaylnk_node_init(&newly->node);
 		
 		while (*p) {
 			cur = *p;
 			pdat = container_of(cur, DATA, node);
 			if (pdat->data > newly->data)
 				p = &cur->left;
 			else if (pdat->data < newly->data)
 				p = &cur->right;
 			else {
 				// 有相同值的节点，无法插入
 				return 0;
 			}
 		}
 		
 		// 链接新节点到树中
 		splaylnk_link(&newly->node, cur, p);
 		
 		// 半伸展
 		splaylnk_semisplay(root, &newly->node, cur);

 		return 1;
 	}
 * @endcode
 * @see splaylnk_insert, splaylnk_link, splaylnk_semisplay
 */
NV_API void splaylnk_semisplay(splaylnk_root* root, splaylnk_node* node, 
									splaylnk_node* parent);

/**
 * @fn void splaylnk_erase(splaylnk_root* root, splaylnk_node* entry);
 * @brief 移除某个节点(伸展)
 * @param[in,out] root 树根
 * @param[in,out] entry 待移除节点
 */
NV_API void splaylnk_erase(splaylnk_root* root, splaylnk_node* entry);

/**
 * @fn void splaylnk_semierase(splaylnk_root* root, splaylnk_node* entry);
 * @brief 移除某个节点(半伸展)
 * @param[in,out] root 树根
 * @param[in,out] entry 待移除节点
 */
NV_API void splaylnk_semierase(splaylnk_root* root, splaylnk_node* entry);

/**
 * @fn void splaylnk_erases(splaylnk_root* root, splaylnk_iter begin, splaylnk_iter end, 
							splaylnk_pr1 erase_handler);
 * @brief 移除区域内的节点(伸展)
 * @param[in,out] root 树根

 * @param[in,out] begin 起始位置
 * @param[in,out] end 终止位置
 * @param[in] erase_handler 移除响应函数，通常用于监视和释放节点，可以传递NULL,
 							表示不为删除的节点调用响应来处理
 * @note 函数会为每一个被移除的节点调用erase_handler来处理节点
 * @see splaylnk_clear
 */
NV_API void splaylnk_erases(splaylnk_root* root, splaylnk_iter begin, 
							splaylnk_iter end, splaylnk_pr1 erase_handler);

/**
 * @fn void splaylnk_semierases(splaylnk_root* root, splaylnk_iter begin, 
 						splaylnk_iter end, splaylnk_pr1 erase_handler);
 * @brief 移除区域内的节点(半伸展)
 * @param[in,out] root 树根

 * @param[in,out] begin 起始位置
 * @param[in,out] end 终止位置
 * @param[in] erase_handler 移除响应函数，通常用于监视和释放节点，可以传递NULL,
 							表示不为删除的节点调用响应来处理
 * @note 函数会为每一个被移除的节点调用erase_handler来处理节点
 * @see splaylnk_clear
 */
NV_API void splaylnk_semierases(splaylnk_root* root, splaylnk_iter begin, 
								splaylnk_iter end, splaylnk_pr1 erase_handler);

/**
 * @fn static inline void splaylnk_init(splaylnk_root* root);
 * @brief 初始化树根
 * @param[out] root 目标树根
 * @par 示例:
 * @code
	splaylnk_root r;
	// 初始化
	splaylnk_init(&r);
 * @endcode
 */
static inline void splaylnk_init(splaylnk_root* root)
{
	root->node = NULL;
}

/**
 * @fn static inline int splaylnk_empty(splaylnk_root* root);
 * @brief 判断树是否为空
 * @param[in] root 树根
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	if (!splaylnk_empty(&r)) {
 		......
 	}
 * @endcode
 */
static inline int splaylnk_empty(splaylnk_root* root)
{
	return !root->node;
}

static inline int __splaylnk_singular(splaylnk_node* root)
{
	return root && !root->left && !root->right;
}

/**
 * @fn static inline int splaylnk_singular(splaylnk_root* root);
 * @brief 判断树是否只有单个节点
 * @param[in] root 树根
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	if (splaylnk_singular(&r)) {
 		......
 	}
 * @endcode
 */
static inline int splaylnk_singular(splaylnk_root* root)
{
	return __splaylnk_singular(root->node);
}

/**
 * @fn static inline int splaylnk_serialable(splaylnk_root* root);
 * @brief 判断树是否可以序列化
 * @param[in] root 树根
 * @note 即判断是否至少有一个节点
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	// 判断是否可以序列化
 	if (splaylnk_serialable(&r)) {
 		slseq_head seq;
 		// 分配序列缓存
 		DATA** buf = (DATA**)malloc(sizeof(DATA*) * avl_cnt);
 		// 序列化
 		splaylnk_serialize(&r, buf, DATA, node);
 		SLSEQ_INIT_SERIAL(&seq, buf, avl_cnt);
 		......
 		// do something
 		......
 		// 反序列化
 		splaylnk_deserialize(&r, buf, avl_cnt, DATA, node);
 		// 释放缓存
 		free(buf);
 	}
 * @endcode
 * @see splaylnk_serialize, splaylnk_deserialize
 */
static inline int splaylnk_serialable(splaylnk_root* root)
{
	return !!root->node;
}

/**
 * @fn static inline splaylnk_iter splaylnk_left(splaylnk_node* node);
 * @brief 得到左子节点
 * @param[in] node 当前节点
 * @return splaylnk_iter 返回左子节点
 * @see splaylnk_right
 */
static inline splaylnk_iter splaylnk_left(splaylnk_node* node)
{
	return node->left;
}

/**
 * @fn static inline splaylnk_iter splaylnk_right(splaylnk_node* node);
 * @brief 得到右子节点
 * @param[in] node 当前节点
 * @return splaylnk_iter 返回右子节点
 * @see splaylnk_left
 */
static inline splaylnk_iter splaylnk_right(splaylnk_node* node)
{
	return node->right;
}

/**
 * @fn static inline splaylnk_iter splaylnk_parent(splaylnk_node* node);
 * @brief 得到父节点
 * @param[in] node 当前节点
 * @return splaylnk_iter 返回父节点
 */
static inline splaylnk_iter splaylnk_parent(splaylnk_node* node)
{
	return node->parent;
}

/**
 * @fn static inline int splaylnk_is_root(splaylnk_iter itr);
 * @brief 判断当前节点是否是根节点
 * @param[in] itr 当前节点
 * @return int 返回结果
 * @retval 1 是根节点
 * @retval 0 不是根节点
 */
static inline int splaylnk_is_root(splaylnk_iter itr)
{
	return !itr->parent;
}

/**
 * @fn static inline int splaylnk_is_leaf(splaylnk_iter itr);
 * @brief 判断当前节点是否是叶子节点
 * @param[in] itr 当前节点
 * @return int 返回结果
 * @retval 1 是叶子节点
 * @retval 0 不是叶子节点
 */
static inline int splaylnk_is_leaf(splaylnk_iter itr)
{
	return !itr->left && !itr->right;
}

/**
 * @fn static inline void splaylnk_link(splaylnk_iter newly, splaylnk_iter parent, 
								splaylnk_iter* plink);
 * @brief 链接两个节点
 * @param[in,out] newly 新节点
 * @param[in,out] parent 父节点
 * @param[in,out] plink 父节点对应孩子指针。若newly加入parent的左子，则plink指向
 						parent的left成员，若newly加入parent的右子，则plink指向
 						parent的right成员
 * @note 链接newly和parent节点
 */
static inline void splaylnk_link(splaylnk_iter newly, 
				splaylnk_iter parent, splaylnk_iter* plink)
{
	/* 简单地将newly节点链接到parent节点下 */
	*plink = newly;
	newly->left = NULL;
	newly->right = NULL;
	newly->parent = parent;
}

/**
 * @fn static inline void splaylnk_set_parent_if(splaylnk_node* node, 
									splaylnk_node* parent);
 * @brief 仅在node不为空时，设置node的父节点
 * @param[in,out] node 当前节点
 * @param[in] parent 新的父节点
 */
static inline void splaylnk_set_parent_if(splaylnk_node* node, 
										  splaylnk_node* parent)
{
	if (node)
		node->parent = parent;
}

/**
 * @fn static inline void splaylnk_node_init(splaylnk_node* node);
 * @brief 初始化节点
 * @param[out] node 指定节点
 */
static inline void splaylnk_node_init(splaylnk_node* node)
{
	node->parent = NULL;
	node->left = NULL;
	node->right = NULL;
}

/**
 * @fn static inline void splaylnk_iter_replace(splaylnk_root* root, 
							splaylnk_iter victim,  splaylnk_iter newly);
 * @brief 用新节点替换旧节点
 * @param[in,out] root 树根
 * @param[in,out] victim 受害目标节点
 * @param[out] newly 新节点
 */
static inline void splaylnk_iter_replace(splaylnk_root* root, 
					splaylnk_iter victim, splaylnk_iter newly)
{
	splaylnk_iter parent = splaylnk_parent(victim);
	
	/* 将victim父节点的孩子设为newly */
	if (parent) {
		if (victim == parent->left)
			parent->left = newly;
		else
			parent->right = newly;
	} else
		root->node = newly;
	
	/* 重新确定victim孩子节点的父节点为newly */
	splaylnk_set_parent_if(victim->left, newly);
	splaylnk_set_parent_if(victim->right, newly);
	
	/* 替换节点成员 */
	*newly = *victim;
}

/**
 * @fn static inline void splaylnk_replace(splaylnk_root* victim, splaylnk_root* newly);
 * @brief 用新的树根替换旧树根
 * @param[in,out] victim 受害目标树根
 * @param[out] newly 新树根
 */
static inline void splaylnk_replace(splaylnk_root* victim, 
										splaylnk_root* newly)
{
	newly->node = victim->node;
}

/**
 * @fn static inline void splaylnk_swap(splaylnk_root* a, splaylnk_root* b);
 * @brief 交换两个树
 * @param[in,out] a 第一个树
 * @param[in,out] b 第二个树
 */
static inline void splaylnk_swap(splaylnk_root* a, splaylnk_root* b)
{
	splaylnk_iter t = a->node; a->node = b->node; b->node = t;
}

/**
 * @fn int splaylnk_exist(splaylnk_root* root, splaylnk_iter itr);
 * @brief 判断一个节点是否存在于树中
 * @param[in] root 树根
 * @param[in] itr 指定节点
 * @return int 返回结果
 * @retval 1 存在
 * @retval 0 不存在
 */
NV_API int splaylnk_exist(splaylnk_root* root, splaylnk_iter itr);

/**
 * @fn static inline splaylnk_iter splaylnk_at(splaylnk_root* root, size_t index);
 * @brief 得到正向索引对应的节点的迭代器
 * @param[in] root 树根
 * @param[in] index 指定正向索引
 * @return splaylnk_iter 返回迭代器
 * @see splaylnk_reverse_at
 */
static inline splaylnk_iter splaylnk_at(splaylnk_root* root, size_t index)
{
	return splaylnk_advance(splaylnk_front(root), index);
}

/**
 * @fn static inline splaylnk_iter splaylnk_reverse_at(splaylnk_root* root, 
 												size_t index);
 * @brief 得到逆向索引对应的节点的迭代器
 * @param[in] root 树根
 * @param[in] index 指定逆向索引
 * @return splaylnk_iter 返回迭代器
 * @see splaylnk_at
 */
static inline splaylnk_iter splaylnk_reverse_at(splaylnk_root* root, size_t index)
{
	return splaylnk_advance(splaylnk_back(root), -(int)(index));
}

/**
 * @fn splaylnk_iter splaylnk_front(splaylnk_root* root);
 * @brief 得到树的最左端节点
 * @param[in] root 树根
 * @return splaylnk_iter 返回迭代器
 * @see splaylnk_back
 */
NV_API splaylnk_iter splaylnk_front(splaylnk_root* root);

/**
 * @fn splaylnk_iter splaylnk_back(splaylnk_root* root);
 * @brief 得到树的最右端节点
 * @param[in] root 树根
 * @return splaylnk_iter 返回迭代器
 * @see splaylnk_front
 */
NV_API splaylnk_iter splaylnk_back(splaylnk_root* root);

/**
 * @fn static inline splaylnk_iter splaylnk_begin(splaylnk_root* root);
 * @brief 得到指向正向起始位置的迭代器
 * @param[in] root 树根
 * @return splaylnk_iter 返回迭代器
 * @see splaylnk_end, splaylnk_end_of, splaylnk_rbegin, splaylnk_rend
 */
static inline splaylnk_iter splaylnk_begin(splaylnk_root* root)
{
	return splaylnk_front(root);
}

/**
 * @fn static inline splaylnk_iter splaylnk_end();
 * @brief 得到指向正向终止位置的迭代器
 * @return splaylnk_iter 返回迭代器，永远返回NULL
 * @see splaylnk_begin, splaylnk_end_of, splaylnk_rbegin, splaylnk_rend
 */
static inline splaylnk_iter splaylnk_end()
{
	return NULL;
}

/**
 * @fn static inline splaylnk_iter splaylnk_end_of(splaylnk_iter cur);
 * @brief 得到当前迭代器的正向终止位置
 * @param[in] itr 迭代器
 * @return splaylnk_iter 返回迭代器
 * @see splaylnk_begin, splaylnk_end
 */
static inline splaylnk_iter splaylnk_end_of(splaylnk_iter cur)
{
	return splaylnk_next(cur);
}

/**
 * @fn static inline splaylnk_iter splaylnk_rbegin(splaylnk_root* root);
 * @brief 得到指向逆向起始位置的迭代器
 * @param[in] root 树根
 * @return splaylnk_iter 返回迭代器
 * @see splaylnk_rend, splaylnk_rend_of, splaylnk_begin, splaylnk_end
 */
static inline splaylnk_iter splaylnk_rbegin(splaylnk_root* root)
{
	return splaylnk_back(root);
}

/**
 * @fn static inline splaylnk_iter splaylnk_rend();
 * @brief 得到指向逆向终止位置的迭代器
 * @return splaylnk_iter 返回迭代器，永远返回NULL
 * @see splaylnk_rbegin, splaylnk_rend_of, splaylnk_begin, splaylnk_end
 */
static inline splaylnk_iter splaylnk_rend()
{
	return NULL;
}

/**
 * @fn static inline splaylnk_iter splaylnk_rend_of(splaylnk_iter cur);
 * @brief 得到当前迭代器的逆向终止位置
 * @param[in] itr 迭代器
 * @return splaylnk_iter 返回迭代器
 * @see splaylnk_rbegin, splaylnk_rend
 */
static inline splaylnk_iter splaylnk_rend_of(splaylnk_iter cur)
{
	return splaylnk_prev(cur);
}

/**
 * @fn static inline splaylnk_iter splaylnk_rend_of(splaylnk_iter cur);
 * @brief 得到当前迭代器的逆向终止位置
 * @param[in] itr 迭代器
 * @return splaylnk_iter 返回迭代器
 * @see splaylnk_rbegin, splaylnk_rend
 */
static inline splaylnk_iter splaylnk_rend_of(splaylnk_iter cur)
{
	return splaylnk_prev(cur);
}

/**
 * @fn splaylnk_iter splaylnk_next(splaylnk_iter cur);
 * @brief 得到当前节点的中序后继节点
 * @param[in] cur 当前节点
 * @return splaylnk_iter 返回后继节点，若无后继，则返回NULL
 * @see splaylnk_prev
 */
NV_API splaylnk_iter splaylnk_next(splaylnk_iter cur);

/**
 * @fn splaylnk_iter splaylnk_prev(splaylnk_iter cur);
 * @brief 得到当前节点的中序前驱节点
 * @param[in] cur 当前节点
 * @return splaylnk_iter 返回前驱节点，若无前驱，则返回NULL
 * @see splaylnk_next
 */
NV_API splaylnk_iter splaylnk_prev(splaylnk_iter cur);

/**
 * @fn splaylnk_iter splaylnk_advance(splaylnk_iter cur, int dist);
 * @brief 给迭代器增加一段距离或减小一段距离
 * @param[in] cur 迭代器
 * @param[in] dist 增加的距离。正数时，表示增加；负数时，表示减小
 * @return splaylnk_iter 返回新迭代器
 */
NV_API splaylnk_iter splaylnk_advance(splaylnk_iter cur, int dist);

/**
 * @fn static inline void splaylnk_clear(splaylnk_root* root, 
 								splaylnk_pr1 erase_handler);
 * @brief 清空树
 * @param[in,out] root 树根
 * @param[in] erase_handler 移除响应函数，通常用于监视和释放节点，可以传递NULL,
 							表示不为删除的节点调用响应来处理
 * @note 函数会为每一个被移除的节点调用erase_handler来处理节点
 * @see splaylnk_erases
 */
static inline void splaylnk_clear(splaylnk_root* root, 
								splaylnk_pr1 erase_handler)
{
	if (!splaylnk_empty(root))
		splaylnk_erases(root, splaylnk_front(root), NULL, erase_handler);
}

static inline splaylnk_iter __splaylnk_inc_later(splaylnk_iter cur, 
													splaylnk_iter* p)
{
	*p = splaylnk_next(cur);
	return cur;
}

static inline splaylnk_iter __splaylnk_dec_later(splaylnk_iter cur, 
													splaylnk_iter* p)
{
	*p = splaylnk_prev(cur);
	return cur;
}

/**
 * @def splaylnk_inc(itr)
 * @brief 迭代器递增
 * @def splaylnk_dec(itr)
 * @brief 迭代器递减
 * @def splaylnk_inc_later(itr)
 * @brief 迭代器后自增
 * @def splaylnk_dec_later(itr)
 * @brief 迭代器后自减
 */
#define splaylnk_inc(itr)			((itr) = splaylnk_next(itr))
#define splaylnk_dec(itr)			((itr) = splaylnk_prev(itr))
#define splaylnk_inc_later(itr)	__splaylnk_inc_later(itr, &(itr))
#define splaylnk_dec_later(itr)	__splaylnk_dec_later(itr, &(itr))

/**
 * @def splaylnk_foreach(_begin, _end, fn, type, member)
 * @brief 正向遍历_begin到_end的节点，并为每一个节点调用回调函数或宏
 * @param[in] _begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define splaylnk_foreach(_begin, _end, fn, type, member) do {\
		splaylnk_iter _q_e = (_end);						\
		splaylnk_iter _q_f = (_begin);						\
		while (_q_f != _q_e) {								\
			type * _q_t = container_of(_q_f, type, member);	\
			splaylnk_inc(_q_f);								\
			fn(_q_t);										\
		}													\
	} while (0)

/**
 * @def splaylnk_reverse_foreach(_rbegin, _rend, fn, type, member)
 * @brief 逆向遍历_begin到_end的节点，并为每一个节点调用回调函数或宏
 * @param[in] _rbegin 逆向起始位置
 * @param[in] _rend 逆向终止位置
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define splaylnk_reverse_foreach(_rbegin, _rend, fn, type, member) do {\
		splaylnk_iter _q_e = (_rend);						\
		splaylnk_iter _q_f = (_rbegin);						\
		while (_q_f != _q_e) {								\
			type * _q_t = container_of(_q_f, type, member);	\
			splaylnk_dec(_q_f);								\
			fn(_q_t);										\
		}													\
	} while (0)

NV_API void __do_splaylnk_recurse_foreach(splaylnk_iter cur, 
								void (*fn)(void*), size_t offset);

NV_API void __do_splaylnk_recurse_reverse_foreach(
					splaylnk_iter cur, void (*fn)(void*), size_t offset);

static inline void __splaylnk_recurse_foreach(splaylnk_root* root, 
								void (*fn)(void*), size_t offset)
{
	if (fn)
		__do_splaylnk_recurse_foreach(root->node, fn, offset);
}

static inline void __splaylnk_recurse_reverse_foreach(
	splaylnk_root* root, void (*fn)(void*), size_t offset)
{
	if (fn)
		__do_splaylnk_recurse_reverse_foreach(root->node, fn, offset);
}

/**
 * @def SPLAYLNK_RECURSE_PROC(name, type, param)
 * @brief 定义递归遍历回调函数
 * @param name 函数名
 * @param type 节点拥有者类型
 * @param param 参数名
 * @see splaylnk_recurse_foreach, splaylnk_recurse_reverse_foreach
 */
#define SPLAYLNK_RECURSE_PROC(name, type, param) void name(type * param)

/**
 * @def splaylnk_recurse_foreach(root, fn, type, member)
 * @brief 正向递归遍历整个树
 * @param[in] root 树根
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @par 示例:
 * @code
 	SPLAYLNK_RECURSE_PROC(PrintNode, DATA, node)
 	{
 		printf("%3d ", node->data);
 	}

	// 打印整个树
 	void PrintAVLTree(splaylnk_root* root)
 	{
 		splaylnk_recurse_foreach(root, PrintNode, DATA, node);
 		putchar('\n');
 	}
 * @endcode
 * @see splaylnk_recurse_reverse_foreach, SPLAYLNK_RECURSE_PROC
 */
#define splaylnk_recurse_foreach(root, fn, type, member) \
	__splaylnk_recurse_foreach(root, (void (*)(void*))(fn), \
			offset_of(type, member))

/**
 * @def splaylnk_recurse_reverse_foreach(root, fn, type, member)
 * @brief 逆向递归遍历整个树
 * @param[in] root 树根
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see splaylnk_recurse_foreach, SPLAYLNK_RECURSE_PROC
 */
#define splaylnk_recurse_reverse_foreach(root, fn, type, member) \
	__splaylnk_recurse_reverse_foreach(root, (void (*)(void*))(fn), \
			offset_of(type, member))

/**
 * @def splaylnk_search(root, newly, greater, res, type, member)
 * @brief 搜索树中的某个节点
 * @param[in] root 树根
 * @param[in] newly 用于搜索的临时节点，类型是type，该节点使用与搜索的值相同的值
 * @param[in] greater 比较函数或宏。
 						升序:(i->data > j->data) 
 						降序:(i->data < j->data)
 * @param[out] res 返回值。返回目标节点，若为NULL表示找不到节点
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @par 示例:
 * @code
 	splaylnk_iter node_15;
 	DATA* p = (DATA*)malloc(sizeof(DATA));
 	if (!p)
 		return NULL;

 	p->data = 15;

	//查找值为15的节点
 	splaylnk_search(&root, p, DATA_Greater, node_15, DATA, node);
 	if (!node_15)
 		return NULL;

 	return node_15;
 * @endcode
 */
#define splaylnk_search(root, newly, greater, res, type, member) do {\
		splaylnk_root* _k_r = (root);							\
		type * _k_pn = (newly);									\
		splaylnk_iter _k_p = _k_r->node;						\
		(res) = NULL;											\
		while (_k_p) {											\
			type * _k_pr = container_of(_k_p, type, member);	\
			if (greater(_k_pr, _k_pn))							\
				_k_p = _k_p->right;								\
			else if (greater(_k_pn, _k_pr))						\
				_k_p = _k_p->left;								\
			else {												\
				(res) = _k_p; break;							\
			}													\
		}														\
	} while (0)

/**
 * @def splaylnk_insert(root, newly, greater, res, type, member)
 * @brief 插入新节点(伸展)
 * @param[in] root 树根
 * @param[in] newly 新节点
 * @param[in] greater 比较函数或宏。
 						升序:(i->data > j->data) 
 						降序:(i->data < j->data)
 * @param[out] res 返回值。返回1表示成功；若为0表示节点有冲突，无法插入
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @par 示例:
 * @code
 	int ret;
 	DATA* p = (DATA*)malloc(sizeof(DATA));
 	if (!p)
 		return 0;

 	p->data = 88;

	//插入值为88的节点(伸展)
 	splaylnk_insert(&root, &p->node, DATA_Greater, ret, DATA, node);
 	if (!ret)
 		return 0;

 	return 1;
 * @endcode
 */
#define splaylnk_insert(root, newly, greater, res, type, member) do {\
		splaylnk_root* _k_r = (root);									\
		splaylnk_iter* _k_pp = &_k_r->node;								\
		splaylnk_iter _k_n = (newly);									\
		splaylnk_iter _k_p;												\
		type * _k_pn = container_of(_k_n, type, member);				\
		(res) = 1;														\
		splaylnk_node_init(_k_n);										\
		while (*_k_pp) {												\
			type * _k_pr = container_of(_k_p = *_k_pp, type, member);	\
			if (greater(_k_pr, _k_pn))									\
				_k_pp = &_k_p->left;									\
			else if (greater(_k_pn, _k_pr))								\
				_k_pp = &_k_p->right;									\
			else {														\
				(res) = 0; break;										\
			}															\
		}																\
		if (res) {														\
			splaylnk_link(_k_n, _k_p, _k_pp);							\
			splaylnk_splay(_k_r, _k_n, _k_p);							\
		}																\
	} while (0)

/**
 * @def splaylnk_semi_insert(root, newly, greater, res, type, member)
 * @brief 插入新节点(半伸展)
 * @param[in] root 树根
 * @param[in] newly 新节点
 * @param[in] greater 比较函数或宏。
 						升序:(i->data > j->data) 
 						降序:(i->data < j->data)
 * @param[out] res 返回值。返回1表示成功；若为0表示节点有冲突，无法插入
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @par 示例:
 * @code
 	int ret;
 	DATA* p = (DATA*)malloc(sizeof(DATA));
 	if (!p)
 		return 0;

 	p->data = 88;

	//插入值为88的节点(半伸展)
 	splaylnk_semi_insert(&root, &p->node, DATA_Greater, ret, DATA, node);
 	if (!ret)
 		return 0;

 	return 1;
 * @endcode
 */
#define splaylnk_semi_insert(root, newly, greater, res, type, member) do {\
		splaylnk_root* _k_r = (root);						\
		splaylnk_iter* _k_pp = &_k_r->node;					\
		splaylnk_iter _k_n = (newly);						\
		splaylnk_iter _k_p;									\
		type * _k_pn = container_of(_k_n, type, member);	\
		(res) = 1;											\
		splaylnk_node_init(_k_n);							\
		while (*_k_pp) {									\
			_k_p = *_k_pp;									\
			_k_pr = container_of(_k_p, type, member);		\
			if (greater(_k_pr, _k_pn))						\
				_k_pp = &_k_p->left;						\
			else if (greater(_k_pn, _k_pr))					\
				_k_pp = &_k_p->right;						\
			else {											\
				(res) = 0; break;							\
			}												\
		}													\
		if (res) {											\
			splaylnk_link(_k_n, _k_p, _k_pp);				\
			splaylnk_semisplay(_k_r, _k_n, _k_p);			\
		}													\
	} while (0)

NV_API void __splaylnk_serialize(splaylnk_root* root, 
									void** buf, size_t offset);

NV_API void __splaylnk_deserialize(splaylnk_root* root, char** buf, 
										size_t cnt, size_t offset);

/**
 * @def splaylnk_serialize(root, buf, type, member)
 * @brief 将树进行序列化为一个有序序列
 * @param[in] root 树根
 * @param[out] buf 序列缓存，元素类型为type指针类型
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see splaylnk_deserialize, splaylnk_serialable
 */
#define splaylnk_serialize(root, buf, type, member) \
	__splaylnk_serialize(root, (void**)(buf), offset_of(type, member))

/**
 * @def splaylnk_deserialize(root, buf, cnt, type, member)
 * @brief 根据树的序列进行反序列化为一个完整的树
 * @param[out] root 树根
 * @param[in] buf 序列缓存，元素类型为type指针类型
 * @param[in] cnt 序列长度(元素数)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see splaylnk_serialize, splaylnk_serialable
 */
#define splaylnk_deserialize(root, buf, cnt, type, member) \
	__splaylnk_deserialize(root, (char**)(buf), cnt, offset_of(type, member))

/** @} */

#ifdef __cplusplus
}
#endif	/* #ifdef __cplusplus */

#endif	/* #ifndef __NV_SPLAYLNK__ */

