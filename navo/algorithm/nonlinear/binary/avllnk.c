/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/nonlinear/binary/avllnk.c
 * 
 * @brief Implements for AVL linked tree.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#include "avllnk.h"

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

static inline avllnk_link_left(avllnk_iter parent, avllnk_iter child)
{
	parent->left = child;
	avllnk_set_parent_if(child, parent);
}

static inline avllnk_link_right(avllnk_iter parent, avllnk_iter child)
{
	parent->right = child;
	avllnk_set_parent_if(child, parent);
}

static inline avllnk_link_left_unsafe(avllnk_iter parent, avllnk_iter child)
{
	parent->left = child;
	avllnk_set_parent(child, parent);
}

static inline avllnk_link_right_unsafe(avllnk_iter parent, avllnk_iter child)
{
	parent->right = child;
	avllnk_set_parent(child, parent);
}

static inline void avllnk_replace_child(avllnk_root* root, avllnk_iter me, 
							avllnk_iter old_child, avllnk_iter new_child)
{
	if (me) {
		if (old_child == me->left)
			me->left = new_child;
		else
			me->right = new_child;
	} else
		root->node = new_child;
}

/* 对top节点进行左旋转 */
static inline void avllnk_rotate_left(avllnk_node* top, avllnk_root* root)
{
	avllnk_node* parent = avllnk_parent(top);
	avllnk_node* node = top->right;
	avllnk_link_right(top, node->left);
	avllnk_link_left_unsafe(node, top);
	avllnk_replace_child(root, parent, top, node);
	avllnk_set_parent(node, parent);
	/* top和node必然是平衡的 */
	avllnk_set_balanced(node);
	avllnk_set_balanced(top);
}

/* 对top节点进行右旋转 */
static inline void avllnk_rotate_right(avllnk_node* top, avllnk_root* root)
{
	avllnk_node* parent = avllnk_parent(top);
	avllnk_node* node = top->left;
	avllnk_link_left(top, node->right);
	avllnk_link_right_unsafe(node, top);
	avllnk_replace_child(root, parent, top, node);
	avllnk_set_parent(node, parent);
	/* top和node必然是平衡的 */
	avllnk_set_balanced(node);
	avllnk_set_balanced(top);
}

/* 对top节点进行左双旋转 */
static void avllnk_double_rotate_left(avllnk_node* top, avllnk_root* root)
{
	avllnk_node* parent = avllnk_parent(top);
	avllnk_node* first = top->right;
	avllnk_node* second = first->left;
	avllnk_node* third = second->right;
	uintptr_t height = avllnk_height(second);

	avllnk_link_left(first, third);
	avllnk_link_right(top, second->left);

	avllnk_set_parent(top, second);
	avllnk_set_parent(first, second);
	avllnk_set_parent(second, parent);

	second->left = top;
	second->right = first;

	avllnk_replace_child(root, parent, top, second);

	if (AVLLNK_LH == height) {
		/* 此时的情况如下，旋转后，second是平衡的，
		 * top是平衡的，first不平衡
		 *       (top)                     (second)
		 *       /   \                     /     \
		 *    (c1)  (first)     ===>   (top)     (first)
		 *           /    \            /   \          \
		 *       (second) (c4)      (c1)   (c2)       (c4)
		 *        /
		 *      (c2)
		 */
		avllnk_set_balanced(top);
		avllnk_set_right_high(first);
	} else if (AVLLNK_RH == height) {
		/* 此时的情况如下，旋转后，second是平衡的，
		 * first是平衡的，top不平衡
		 *       (top)                     (second)
		 *       /   \                     /     \
		 *    (c1)  (first)     ===>   (top)     (first)
		 *           /    \            /         /   \
		 *       (second) (c4)      (c1)       c3)   (c4)
		 *             \
		 *            (c3)
		 */
		avllnk_set_balanced(first);
		avllnk_set_left_high(top);
	} else {
		/* 此时的情况如下，旋转后，top、first和second是平衡的
		 *      (top)                  (second)
		 *          \                   /    \
		 *          (first)  ===>   (top)    (first)
		 *          /
		 *      (second)
		 */
		avllnk_set_balanced(top);
		avllnk_set_balanced(first);		
	}

	avllnk_set_balanced(second);
}

static void avllnk_double_rotate_right(avllnk_node* top, avllnk_root* root)
{
	avllnk_node* parent = avllnk_parent(top);
	avllnk_node* first = top->left;
	avllnk_node* second = first->right;
	avllnk_node* third = second->left;
	uintptr_t height = avllnk_height(second);

	avllnk_link_right(first, third);
	avllnk_link_left(top, second->right);

	avllnk_set_parent(top, second);
	avllnk_set_parent(first, second);
	avllnk_set_parent(second, parent);

	second->right = top;
	second->left = first;

	avllnk_replace_child(root, parent, top, second);
	
	if (AVLLNK_RH == height) {
	/* 此时的情况如下，旋转后，second是平衡的，
		 * top是平衡的，first不平衡
		 *        (top)                (second)
		 *        /   \                 /    \
		 *    (first)  (c1)  ===> (first)   (top)
		 *     /    \              /        /   \
		 *  (c4) (second)       (c4)      (c2)  (c1)
		 *		        \
		 *              (c2)
		 */
		avllnk_set_balanced(top);
		avllnk_set_left_high(first);
	} else if (AVLLNK_LH == height) {
		/* 此时的情况如下，旋转后，second是平衡的，
		 * top是平衡的，first不平衡
		 *        (top)               (second)
		 *        /   \                /    \
		 *    (first)  (c1) ===>  (first)  (top)
		 *     /    \              /   \      \
		 *  (c4) (second)       (c4)  (c3)    (c1)
		 *		   /
		 *      (c3)
		 */
		avllnk_set_balanced(first);
		avllnk_set_right_high(top);
	} else {
		/* 此时的情况如下，旋转后，top、first和second是平衡的
		 *      (top)           (second)
		 *       /               /    \
		 *  (first)     ===> (first)  (top)
		 *       \
		 *      (second)
		 */
		avllnk_set_balanced(top);
		avllnk_set_balanced(first);		
	}

	avllnk_set_balanced(second);
}

/* 对top节点进行收缩左旋转 */
static void avllnk_shrink_rotate_left(avllnk_node* top, avllnk_root* root)
{
	avllnk_node* parent = avllnk_parent(top);
	avllnk_node* node = top->right;
	avllnk_node* left = node->left;

	avllnk_link_right(top, left);
	avllnk_link_left_unsafe(node, top);

	avllnk_replace_child(root, parent, top, node);

	avllnk_set_parent(node, parent);
	if (avllnk_balanced(node)) {
		/* 如果node平衡，说明node有c1和c2两个子节点，
		 * 旋转后，top和node是不平衡的
		 *  (top)                  (node)
		 *      \                   /  \
		 *      (node)   ====>  (top)  (c1)
		 *       /  \               \
		 *    (c2)  (c1)            (c2)
		 */
		avllnk_set_left_high(node);
		avllnk_set_right_high(top);
	} else {
		/* 如果node不平衡，说明node有且只有c1子节点，
		 * 旋转后，top和node是平衡的
		 *  (top)                  (node)
		 *      \                   /  \
		 *      (node)   ====>  (top)  (c1)
		 *          \
		 *         (c1)
		 */
		avllnk_set_balanced(node);
		avllnk_set_balanced(top);
	}
}

/* 对top节点进行收缩右旋转 */
static void avllnk_shrink_rotate_right(avllnk_node* top, avllnk_root* root)
{
	avllnk_node* parent = avllnk_parent(top);
	avllnk_node* node = top->left;
	avllnk_node* right = node->right;

	avllnk_link_left(top, right);
	avllnk_link_right_unsafe(node, top);

	avllnk_replace_child(root, parent, top, node);
	
	avllnk_set_parent(node, parent);
	if (avllnk_balanced(node)) {
	/* 如果node平衡，说明node有c1和c2两个子节点，
	* 旋转后，top和node是不平衡的
		 *      (top)          (node)
		 *      /               /  \
		 *   (node)   ====>  (c1)  (top)
		 *    /  \                 /
		 * (c1)  (c2)            (c2)
		 */
		avllnk_set_right_high(node);
		avllnk_set_left_high(top);
	} else {
		/* 如果node不平衡，说明node有且只有c1子节点，
		 * 旋转后，top和node是平衡的
		 *      (top)          (node)
		 *      /               /  \
		 *    (node)  ====>  (c1)  (top)
		 *    /
		 * (c1)
		 */
		avllnk_set_balanced(node);
		avllnk_set_balanced(top);
	}
}

NV_IMPL void avllnk_rebalance(avllnk_root* root, avllnk_node* node, 
								avllnk_node* parent)
{
	int sdir = AVLLNK_LH;

	while (parent) {
		if (node == parent->left) {
			if (!avllnk_balanced(parent)) {
				/* 在不平衡节点路径上插入 */
				if (avllnk_left_high(parent)) {
					/* 在长路径上插入，需要进行旋转 */
					if (sdir != AVLLNK_LH && node->right)
						avllnk_double_rotate_right(parent, root);
					else
						avllnk_rotate_right(parent, root);
					break;
				} else {
					/* 在短路径上插入，将parent设为平衡即可 */
					avllnk_set_balanced(parent);
					break;
				}
			} else {
				/* 在平衡节点路径上插入，修改parent的高度值 */
				avllnk_set_left_high(parent);
			}
			sdir = AVLLNK_LH;
		} else {
			if (!avllnk_balanced(parent)) {
				/* 在不平衡节点路径上插入 */
				if (avllnk_right_high(parent)) {
					/* 在长路径上插入，需要进行旋转 */
					if (sdir != AVLLNK_RH && node->left)
						avllnk_double_rotate_left(parent, root);
					else
						avllnk_rotate_left(parent, root);
					break;
				} else {
					/* 在短路径上插入，将parent设为平衡即可 */
					avllnk_set_balanced(parent);
					break;
				}
			} else {
				/* 在平衡节点路径上插入，修改parent的高度值 */
				avllnk_set_right_high(parent);
			}
			sdir = AVLLNK_RH;
		}
		node = parent;
		parent = avllnk_parent(node);
	}
}

/* 删除时平衡化 */
static void __avllnk_rebalance_on_erase(avllnk_node* node, 
					avllnk_node* parent, avllnk_root* root, int which)
{
	if (parent) {
		if (!node) {
			/* node为空，且parent左右子均为空 */
			if (which == AVLLNK_LH)
				goto __AVLLNK_REBALANCE_ON_ERASE_LT;
			else
				goto __AVLLNK_REBALANCE_ON_ERASE_RT;
		} do {
			if (node == parent->left) {
__AVLLNK_REBALANCE_ON_ERASE_LT:
				if (!avllnk_balanced(parent)) {
					/* 在不平衡节点路径上删除 */
					if (avllnk_left_high(parent)) {
						/* 在长路径上删除，将parent设为平衡即可 */
						avllnk_set_balanced(parent);
					} else {
						/* 在短路径上删除，需要进行旋转 */
						if (avllnk_left_high(parent->right))
							avllnk_double_rotate_left(parent, root);
						else
							avllnk_shrink_rotate_left(parent, root);
						break;
					}
				} else {
					/* 在平衡节点路径上删除，修改parent的高度值 */
					avllnk_set_right_high(parent);
					break;
				}
				node = parent;
				parent = avllnk_parent(node);
			} else {
__AVLLNK_REBALANCE_ON_ERASE_RT:
				if (!avllnk_balanced(parent)) {
					/* 在不平衡节点路径上删除 */
					if (avllnk_right_high(parent)) {
						/* 在长路径上删除，将parent设为平衡即可 */
						avllnk_set_balanced(parent);
					} else {
						/* 在短路径上删除，需要进行旋转 */
						if (avllnk_right_high(parent->left))
							avllnk_double_rotate_right(parent, root);
						else
							avllnk_shrink_rotate_right(parent, root);
						break;
					}
				} else {
					/* 在平衡节点路径上删除，修改parent的高度值 */
					avllnk_set_left_high(parent);
					break;
				}
				node = parent;
				parent = avllnk_parent(node);
			}
		} while (parent);
	}
}

NV_IMPL void avllnk_erase(avllnk_root* root, avllnk_node* entry)
{
	int which;
	avllnk_node* child;
	avllnk_node* parent = avllnk_parent(entry);

	do {/* 如果entry节点最多只有一个孩子，则直接删除entry节点
		 * 并调整树，如果entry同时有两个孩子，需要将其转换成
		 * 只有一子或无子的情况
		 */
		if (!entry->left) {
			child = entry->right;
		} else if (!entry->right) {
			child = entry->left;
		} else {
			avllnk_node* node = entry->right;
			/* 找到最靠近entry节点右边的节点来交换，
			 * 方法是先找右节点，再循环向左找到最左
			 * 端的node节点，将该节点替换到entry节
			 * 点的位置，此时树性质不受影响，问题转
			 * 变成了删除找到的node节点的问题
			 */
			while (node->left)
				node = node->left;
			/* 将node节点设为entry父节点的孩子 */
			which = (entry == parent->right);
			avllnk_replace_child(root, parent, entry, node);
			/* 记录node节点原来的节点信息，用于树的调整 */
			child = node->right;
			parent = avllnk_parent(node);
			if (parent == entry) {
				/* 说明找到的node节点是entry的右子 */
				parent = node;
			} else {
				/* 替换entry节点到node节点，相当
				 * 于删除了原来node位置的节点
				 */
				avllnk_set_parent_if(child, parent);
				parent->left = child;
				avllnk_link_right_unsafe(node, entry->right);
			}

			node->parent_height = entry->parent_height;
			avllnk_link_left_unsafe(node, entry->left);

			/* 平衡树 */
			break;
		}
		/* 接上面的if和else if，删除entry节点 */
		which = (entry == parent->right);
		avllnk_set_parent_if(child, parent);
		avllnk_replace_child(root, parent, entry, child);
	} while (0);

	/* 平衡树 */
	__avllnk_rebalance_on_erase(child, parent, root, which);
}

NV_IMPL void avllnk_erases(avllnk_root* root, avllnk_iter begin, 
						avllnk_iter end, avllnk_pr1 erase_handler)
{
	if (erase_handler) {
		while (begin != end) {
			avllnk_iter cur = begin;
			avllnk_inc(begin);
			avllnk_erase(root, cur);
			erase_handler(cur);
		}

	} else {
		while (begin != end)
			avllnk_erase(root, avllnk_inc_later(begin));
	}
}

NV_IMPL avllnk_iter avllnk_advance(avllnk_iter cur, int dist)
{
	if (dist > 0) {
		while (cur && dist--)
			avllnk_inc(cur);
	} else {
		while (cur && dist++)
			avllnk_dec(cur);
	}
	return cur;
}

NV_IMPL int avllnk_exist(avllnk_root* root, avllnk_iter itr)
{
	avllnk_iter i = avllnk_front(root);
	
	while (i) {
		if (i == itr)
			return 1;
		avllnk_inc(i);
	}
	
	return 0;
}

NV_IMPL avllnk_iter avllnk_front(avllnk_root* root)
{
	avllnk_iter i = root->node;

	if (!i)
		return NULL;

	/* 一直往左边走，找到最左边的节点 */
	while (i->left)
		i = i->left;

	return i;
}

NV_IMPL avllnk_iter avllnk_back(avllnk_root* root)
{
	avllnk_iter i = root->node;
	
	if (!i)
		return NULL;
	
	/* 一直往右边走，找到最右边的节点 */
	while (i->right)
		i = i->right;
	
	return i;
}

NV_IMPL avllnk_iter avllnk_next(avllnk_iter cur)
{
	avllnk_iter parent;
	
	/* 如果有右孩子，往右边走 */
	if (cur->right) {
		cur = cur->right;
		/* 找右孩子下最靠左的节点 */
		while (cur->left)
			cur = cur->left;
		return cur;
	}
	/* 如果没有右孩子，向父节点走 */
	while ((parent = avllnk_parent(cur)) 
			&& cur == parent->right)
		cur = parent;
	
	return parent;
}

NV_IMPL avllnk_iter avllnk_prev(avllnk_iter cur)
{
	avllnk_iter parent;

	/* 如果有左孩子，往左边走 */
	if (cur->left) {
		cur = cur->left;
		/* 找左孩子下最靠右的节点 */
		while (cur->right)
			cur = cur->right;
		return cur;
	}
	/* 如果没有左孩子，向父节点走 */
	while ((parent = avllnk_parent(cur)) 
			&& cur == parent->left)
		cur = parent;

	return parent;
}

NV_IMPL size_t avllnk_depth(avllnk_root* root)
{
	size_t cur_depth;
	size_t max_depth = 0;
	avllnk_iter parent;
	avllnk_iter cur = root->node;
	
	if (cur) {
		cur_depth = 1;
		while (cur->left) {
			cur = cur->left;
			++cur_depth;
		}
		max_depth = cur_depth;
		do {/* 如果有右孩子，往右边走 */
			if (cur->right) {
				cur = cur->right;
				++cur_depth;
				/* 找右孩子下最靠左的节点 */
				while (cur->left) {
					cur = cur->left;
					++cur_depth;
				}
				if (cur_depth > max_depth)
					max_depth = cur_depth;
			} else {
				/* 如果没有右孩子，向父节点走 */
				while ((parent = avllnk_parent(cur)) 
						&& cur == parent->right) {
					cur = parent;
					--cur_depth;
				}
				cur = parent;
				--cur_depth;
			}
		} while (cur);
	}
	
	return max_depth;
}

NV_IMPL size_t avllnk_leafs(avllnk_root* root)
{
	size_t leafs = 0;
	avllnk_iter parent;
	avllnk_iter cur = avllnk_begin(root);
	
	while (cur) {
		/* 如果有右孩子，往右边走 */
		if (cur->right) {
			cur = cur->right;
			/* 找右孩子下最靠左的节点 */
			while (cur->left)
				cur = cur->left;
		} else {
			if (!cur->left)
				++leafs;
			/* 如果没有右孩子，向父节点走 */
			while ((parent = avllnk_parent(cur)) 
					&& cur == parent->right)
				cur = parent;
			cur = parent;
		}
	}
	
	return leafs;
}

NV_IMPL void avllnk_get_info(avllnk_root* root, avllnk_info* info)
{
	size_t cnt = 0;
	size_t leafs = 0;
	size_t cur_depth;
	size_t max_depth = 0;
	avllnk_iter parent;
	avllnk_iter cur = root->node;
	
	if (cur) {
		cur_depth = 1;
		while (cur->left) {
			cur = cur->left;
			++cur_depth;
		}
		max_depth = cur_depth;
		do {
			++cnt;
			/* 如果有右孩子，往右边走 */
			if (cur->right) {
				cur = cur->right;
				++cur_depth;
				/* 找右孩子下最靠左的节点 */
				while (cur->left) {
					cur = cur->left;
					++cur_depth;
				}
				if (cur_depth > max_depth)
					max_depth = cur_depth;
			} else {
				if (!cur->left)
					++leafs;
				/* 如果没有右孩子，向父节点走 */
				while ((parent = avllnk_parent(cur)) 
						&& cur == parent->right) {
					cur = parent;
					--cur_depth;
				}
				cur = parent;
				--cur_depth;
			}
		} while (cur);
	}
	
	info->count = cnt;
	info->depth = max_depth;
	info->leafs = leafs;
}

NV_IMPL size_t avllnk_count(avllnk_root* root)
{
	size_t cnt = 0;
	avllnk_iter i = avllnk_begin(root);

	while (i) {
		avllnk_inc(i);
		++cnt;
	}

	return cnt;
}

NV_IMPL size_t avllnk_distance(avllnk_iter first, avllnk_iter last)
{
	size_t dist = 1;
	
	while (first != last) {
		avllnk_inc(first);
		++dist;
	}
	
	return dist;
}

NV_IMPL size_t avllnk_index_of(avllnk_root* root, avllnk_iter itr)
{
	size_t cnt = 0;
	avllnk_iter i = avllnk_begin(root);
	
	while (i && i != itr) {
		avllnk_inc(i);
		++cnt;
	}
	
	return cnt;
}

NV_IMPL size_t avllnk_reverse_index_of(avllnk_root* root, avllnk_iter itr)
{
	size_t cnt = 0;
	avllnk_iter i = avllnk_rbegin(root);
	
	while (i && i != itr) {
		avllnk_dec(i);
		++cnt;
	}
	
	return cnt;
}

NV_IMPL void __do_avllnk_recurse_foreach(avllnk_iter cur, 
						void (*fn)(void*), size_t offset)
{
	if (cur) {
		__do_avllnk_recurse_foreach(cur->left, fn, offset);
		fn(((char*)cur) - offset);
		__do_avllnk_recurse_foreach(cur->right, fn, offset);
	}
}

NV_IMPL void __do_avllnk_recurse_reverse_foreach(avllnk_iter cur, 
						void (*fn)(void*), size_t offset)
{
	if (cur) {
		__do_avllnk_recurse_reverse_foreach(cur->right, fn, offset);
		fn(((char*)cur) - offset);
		__do_avllnk_recurse_reverse_foreach(cur->left, fn, offset);
	}
}

NV_IMPL void __avllnk_serialize(avllnk_root* root, void** buf, size_t offset)
{
	avllnk_iter i = avllnk_begin(root);

	while (i) {
		void* p = ((char*)i) - offset;
		*buf++ = p;
		avllnk_inc(i);
	}
}

static inline size_t AVLLNK_F(size_t i, size_t resv)
{
	if (i + 1 >= resv)
		return i + resv;
	return (i << 1) + 1;
}

static inline size_t AVLLNK_P(size_t i, size_t d, size_t b)
{
	return ((i >> d) << d) + b;
}

#define AVLLNK_UPDATE_HEIGHT(current, parent) do {	\
		uintptr_t height = avllnk_height(parent);	\
		if (current == parent->left) {				\
			if (height == AVLLNK_LH)				\
				break;								\
			else if (height == AVLLNK_BL)			\
				avllnk_set_left_high(parent);		\
			else									\
				avllnk_set_balanced(parent);		\
		} else {									\
			if (height == AVLLNK_RH)				\
				break;								\
			else if (height == AVLLNK_BL)			\
				avllnk_set_right_high(parent);		\
			else									\
				avllnk_set_balanced(parent);		\
		}											\
		current = parent;							\
		parent = avllnk_parent(parent);				\
	} while (parent)

NV_IMPL void __avllnk_deserialize(avllnk_root* root, char** buf, 
									size_t cnt, size_t offset)
{
	avllnk_iter pi, pj, pp;
	size_t i, r, h, c, t, s, d, lv;
	if (!cnt) {
		root->node = NULL;
		return;
	} else if (cnt == 1) {
		pp = (avllnk_iter)((*buf) + offset);
		root->node = pp;
		pp->parent_height = AVLLNK_BL;
		return;
	}
	h = 0; c = 1; lv = 0;
	s = 1; d = 2; t = cnt;
	while (t >>= 1) {
		++lv;
		c <<= 1;
	}
	r = cnt - (--c);
	while (++h < lv) {
		i = s - 1;
		s <<= 1;
		while (i < c) {
			size_t p = AVLLNK_P(i, h + 1, s - 1);
			pp = (avllnk_iter)
				(buf[AVLLNK_F(p, r)] + offset);
			pi = (avllnk_iter)
				(buf[AVLLNK_F(i, r)] + offset);
			pp->left = pi;
			avllnk_set_parent_height(pi, pp, AVLLNK_BL);
			i += d;
			pj = (avllnk_iter)
				(buf[AVLLNK_F(i, r)] + offset);
			pp->right = pj;
			avllnk_set_parent_height(pj, pp, AVLLNK_BL);
			if (h < 2) {
				pi->left = NULL;
				pi->right = NULL;
				pj->left = NULL;
				pj->right = NULL;
			}
			i += d;
		}
		d <<= 1;
	}
	root->node = pp;
	pp->parent_height = AVLLNK_BL;
	for (i = 0; i < r; ++i) {
		pp = (avllnk_iter)
			(buf[((i >> 1) << 2) + 1] + offset);
		pi = (avllnk_iter)
			(buf[i << 1] + offset);
		pi->left = NULL;
		pi->right = NULL;
		if (i & 1)
			pp->right = pi;
		else
			pp->left = pi;
		avllnk_set_parent_height(pi, pp, AVLLNK_BL);
		/* 更新高度信息 */
		AVLLNK_UPDATE_HEIGHT(pi, pp);
	}
}

#ifdef __cplusplus
}
#endif	/* #ifdef __cplusplus */
