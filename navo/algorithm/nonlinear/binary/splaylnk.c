/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/nonlinear/binary/splaylnk.c
 * 
 * @brief Implements for splay linked tree.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#include "splaylnk.h"

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

static inline splaylnk_link_left(splaylnk_iter parent, splaylnk_iter child)
{
	parent->left = child;
	splaylnk_set_parent_if(child, parent);
}

static inline splaylnk_link_right(splaylnk_iter parent, splaylnk_iter child)
{
	parent->right = child;
	splaylnk_set_parent_if(child, parent);
}

static inline splaylnk_link_left_unsafe(splaylnk_iter parent, 
										splaylnk_iter child)
{
	parent->left = child;
	child->parent = parent;
}

static inline splaylnk_link_right_unsafe(splaylnk_iter parent, 
										 splaylnk_iter child)
{
	parent->right = child;
	child->parent = parent;
}

static inline void splaylnk_replace_child(splaylnk_root* root, 
	splaylnk_iter me, splaylnk_iter old_child, splaylnk_iter new_child)
{
	if (me) {
		if (old_child == me->left)
			me->left = new_child;
		else
			me->right = new_child;
	} else
		root->node = new_child;
}

/* 对top节点进行zig左旋转 */
static inline void splaylnk_zig_rotate_left(splaylnk_node* top, 
											splaylnk_root* root)
{
	splaylnk_node* parent = top->parent;
	splaylnk_node* node = top->right;
	splaylnk_link_right(top, node->left);
	splaylnk_link_left_unsafe(node, top);
	splaylnk_replace_child(root, parent, top, node);
	node->parent = parent;
}

/* 对top节点进行zig右旋转 */
static inline void splaylnk_zig_rotate_right(splaylnk_node* top, 
											splaylnk_root* root)
{
	splaylnk_node* parent = top->parent;
	splaylnk_node* node = top->left;
	splaylnk_link_left(top, node->right);
	splaylnk_link_right_unsafe(node, top);
	splaylnk_replace_child(root, parent, top, node);
	node->parent = parent;
}

/* 对top节点进行zigzig左旋转 */
static void splaylnk_zigzig_rotate_left(splaylnk_node* gparent, 
	splaylnk_node* parent, splaylnk_node* node, splaylnk_root* root)
{
	splaylnk_iter ggparent = gparent->parent;
	splaylnk_link_right(gparent, parent->left);
	splaylnk_link_right(parent, node->left);
	splaylnk_link_left_unsafe(parent, gparent);
	splaylnk_link_left_unsafe(node, parent);
	splaylnk_replace_child(root, ggparent, gparent, node);
	node->parent = ggparent;
}

/* 对top节点进行zigzig右旋转 */
static void splaylnk_zigzig_rotate_right(splaylnk_node* gparent, 
	splaylnk_node* parent, splaylnk_node* node, splaylnk_root* root)
{
	splaylnk_iter ggparent = gparent->parent;
	splaylnk_link_left(gparent, parent->right);
	splaylnk_link_left(parent, node->right);
	splaylnk_link_right_unsafe(parent, gparent);
	splaylnk_link_right_unsafe(node, parent);
	splaylnk_replace_child(root, ggparent, gparent, node);
	node->parent = ggparent;
}

/* 对top节点进行v左旋转 */
static void splaylnk_zigzag_rotate_left(splaylnk_node* gparent, 
	splaylnk_node* parent, splaylnk_node* node, splaylnk_root* root)
{
	splaylnk_iter ggparent = gparent->parent;
	splaylnk_link_right(gparent, node->left);
	splaylnk_link_left(parent, node->right);
	splaylnk_link_left_unsafe(node, gparent);
	splaylnk_link_right_unsafe(node, parent);
	splaylnk_replace_child(root, ggparent, gparent, node);
	node->parent = ggparent;
}

/* 对top节点进行zigzag右旋转 */
static void splaylnk_zigzag_rotate_right(splaylnk_node* gparent, 
	splaylnk_node* parent, splaylnk_node* node, splaylnk_root* root)
{
	splaylnk_iter ggparent = gparent->parent;
	splaylnk_link_left(gparent, node->right);
	splaylnk_link_right(parent, node->left);
	splaylnk_link_right_unsafe(node, gparent);
	splaylnk_link_left_unsafe(node, parent);
	splaylnk_replace_child(root, ggparent, gparent, node);
	node->parent = ggparent;
}

NV_IMPL void splaylnk_splay(splaylnk_root* root, splaylnk_node* node, 
								splaylnk_node* parent)
{
	while (parent) {
		splaylnk_iter gparent = parent->parent;
		if (node == parent->left) {
			if (!gparent) {
				/* 如果父节点是根节点，直接进行单旋 */
				splaylnk_zig_rotate_right(parent, root);
			} else if (parent == gparent->left) {
				/* 如果父节点是祖父节点的左子且node是父节点的左子，
				 * 则'一'字形旋转
				 */
				splaylnk_zigzig_rotate_right(gparent, parent, node, root);
			} else {
				/* 如果父节点是祖父节点的左子且node是父节点的右子，
				 * 则'之'字形旋转
				 */
				splaylnk_zigzag_rotate_left(gparent, parent, node, root);
			}
			parent = node->parent;
		} else {
			if (!gparent) {
				/* 如果父节点是根节点，直接进行单旋 */
				splaylnk_zig_rotate_left(parent, root);
			} else if (parent == gparent->right) {
				/* 如果父节点是祖父节点的右子且node是父节点的右子，
				 * 则'一'字形旋转
				 */
				splaylnk_zigzig_rotate_left(gparent, parent, node, root);
			} else {
				/* 如果父节点是祖父节点的右子且node是父节点的左子，
				 * 则'之'字形旋转
				 */
				splaylnk_zigzag_rotate_right(gparent, parent, node, root);
			}
			parent = node->parent;
		}
	}
}

NV_IMPL void splaylnk_semisplay(splaylnk_root* root, splaylnk_node* node, 
									splaylnk_node* parent)
{
	while (parent) {
		splaylnk_iter gparent = parent->parent;
		if (node == parent->left) {
			if (!gparent || (parent == gparent->left)) {
				/* （1）如果父节点是根节点，直接进行单旋,
				 * （2）如果父节点是祖父节点的左子且node是父节点的左子，
				 * 直接对父节点进行右旋（或左旋）
				 */
				splaylnk_zig_rotate_right(parent, root);
			} else {
				/* 如果父节点是祖父节点的左子且node是父节点的右子，
				 * 则'之'字形旋转
				 */
				splaylnk_zigzag_rotate_left(gparent, parent, node, root);
			}
			parent = node->parent;
		} else {
			if (!gparent || (parent == gparent->right)) {
				/* （1）如果父节点是根节点，直接进行单旋,
				 * （2）如果父节点是祖父节点的右子且node是父节点的右子，
				 * 直接对父节点进行左旋
				 */
				splaylnk_zig_rotate_left(parent, root);
			} else {
				/* 如果父节点是祖父节点的右子且node是父节点的左子，
				 * 则'之'字形旋转
				 */
				splaylnk_zigzag_rotate_right(gparent, parent, node, root);
			}
			parent = node->parent;
		}
	}
}

NV_IMPL void splaylnk_erase(splaylnk_root* root, splaylnk_node* entry)
{
	/* 把待删除节点伸展到树根 */
	splaylnk_splay(root, entry, entry->parent);
	do {
		splaylnk_iter child;
		/* 如果entry节点最多只有一个孩子，则直接删除entry节点，
		 * 如果entry同时有两个孩子，需要将其转换成只有一子或无子的情况
		 */
		if (!entry->left)
			child = entry->right;
		else if (!entry->right)
			child = entry->left;
		else {
			splaylnk_iter left = entry->left;
			splaylnk_iter right = entry->right;
			splaylnk_iter node = left;
			/* 把树分割成左子树和右子树 */
			left->parent = NULL;
			right->parent = NULL;
			/* 将左子树最右节点伸展到左子树树根，将其代替entry树根 */
			while (node->right)
				node = node->right;
			splaylnk_splay(root, node, node->parent);
			/* 重新合并左子树和右子树 */
			right = entry->right;
			node->right = right;
			right->parent = node;
			/* 完成伸展 */
			break;
		}
		/* 接上面的if和else if，删除entry节点 */
		splaylnk_set_parent_if(child, NULL);
		root->node = child;
	} while (0);
}

NV_IMPL void splaylnk_semierase(splaylnk_root* root, splaylnk_node* entry)
{
	/* 把待删除节点伸展到树根 */
	splaylnk_semisplay(root, entry, entry->parent);
	do {
		splaylnk_iter child;
		/* 如果entry节点最多只有一个孩子，则直接删除entry节点，
		 * 如果entry同时有两个孩子，需要将其转换成只有一子或无子的情况
		 */
		if (!entry->left)
			child = entry->right;
		else if (!entry->right)
			child = entry->left;
		else {
			splaylnk_iter left = entry->left;
			splaylnk_iter right = entry->right;
			splaylnk_iter node = left;
			/* 把树分割成左子树和右子树 */
			left->parent = NULL;
			/* 将左子树最右节点伸展到左子树树根，将其代替entry树根 */
			while (node->right)
				node = node->right;
			splaylnk_semisplay(root, node, node->parent);
			/* 重新合并左子树和右子树 */
			right = entry->right;
			node->right = right;
			right->parent = node;
			/* 完成伸展 */
			break;
		}
		/* 接上面的if和else if，删除entry节点 */
		splaylnk_set_parent_if(child, NULL);
		root->node = child;
	} while (0);
}

NV_IMPL void splaylnk_erases(splaylnk_root* root, splaylnk_iter begin, 
								splaylnk_iter end, splaylnk_pr1 erase_handler)
{
	if (erase_handler) {
		while (begin != end) {
			splaylnk_iter cur = begin;
			splaylnk_inc(begin);
			splaylnk_erase(root, cur);
			erase_handler(cur);
		}

	} else {
		while (begin != end)
			splaylnk_erase(root, splaylnk_inc_later(begin));
	}
}

NV_IMPL void splaylnk_semierases(splaylnk_root* root, splaylnk_iter begin, 
									splaylnk_iter end, splaylnk_pr1 erase_handler)
{
	if (erase_handler) {
		while (begin != end) {
			splaylnk_iter cur = begin;
			splaylnk_inc(begin);
			splaylnk_semierase(root, cur);
			erase_handler(cur);
		}

	} else {
		while (begin != end)
			splaylnk_semierase(root, splaylnk_inc_later(begin));
	}
}

NV_IMPL splaylnk_iter splaylnk_advance(splaylnk_iter cur, int dist)
{
	if (dist > 0) {
		while (cur && dist--)
			splaylnk_inc(cur);
	} else {
		while (cur && dist++)
			splaylnk_dec(cur);
	}
	return cur;
}

NV_IMPL int splaylnk_exist(splaylnk_root* root, splaylnk_iter itr)
{
	splaylnk_iter i = splaylnk_front(root);
	
	while (i) {
		if (i == itr)
			return 1;
		splaylnk_inc(i);
	}
	
	return 0;
}

NV_IMPL splaylnk_iter splaylnk_front(splaylnk_root* root)
{
	splaylnk_iter i = root->node;

	if (!i)
		return NULL;

	/* 一直往左边走，找到最左边的节点 */
	while (i->left)
		i = i->left;

	return i;
}

NV_IMPL splaylnk_iter splaylnk_back(splaylnk_root* root)
{
	splaylnk_iter i = root->node;
	
	if (!i)
		return NULL;
	
	/* 一直往右边走，找到最右边的节点 */
	while (i->right)
		i = i->right;
	
	return i;
}

NV_IMPL splaylnk_iter splaylnk_next(splaylnk_iter cur)
{
	splaylnk_iter parent;
	
	/* 如果有右孩子，往右边走 */
	if (cur->right) {
		cur = cur->right;
		/* 找右孩子下最靠左的节点 */
		while (cur->left)
			cur = cur->left;
		return cur;
	}
	/* 如果没有右孩子，向父节点走 */
	while ((parent = splaylnk_parent(cur)) 
			&& cur == parent->right)
		cur = parent;
	
	return parent;
}

NV_IMPL splaylnk_iter splaylnk_prev(splaylnk_iter cur)
{
	splaylnk_iter parent;

	/* 如果有左孩子，往左边走 */
	if (cur->left) {
		cur = cur->left;
		/* 找左孩子下最靠右的节点 */
		while (cur->right)
			cur = cur->right;
		return cur;
	}
	/* 如果没有左孩子，向父节点走 */
	while ((parent = splaylnk_parent(cur)) 
			&& cur == parent->left)
		cur = parent;

	return parent;
}

NV_IMPL size_t splaylnk_depth(splaylnk_root* root)
{
	size_t cur_depth;
	size_t max_depth = 0;
	splaylnk_iter parent;
	splaylnk_iter cur = root->node;
	
	if (cur) {
		cur_depth = 1;
		while (cur->left) {
			cur = cur->left;
			++cur_depth;
		}
		max_depth = cur_depth;
		do {/* 如果有右孩子，往右边走 */
			if (cur->right) {
				cur = cur->right;
				++cur_depth;
				/* 找右孩子下最靠左的节点 */
				while (cur->left) {
					cur = cur->left;
					++cur_depth;
				}
				if (cur_depth > max_depth)
					max_depth = cur_depth;
			} else {
				/* 如果没有右孩子，向父节点走 */
				while ((parent = splaylnk_parent(cur)) 
						&& cur == parent->right) {
					cur = parent;
					--cur_depth;
				}
				cur = parent;
				--cur_depth;
			}
		} while (cur);
	}
	
	return max_depth;
}

NV_IMPL size_t splaylnk_leafs(splaylnk_root* root)
{
	size_t leafs = 0;
	splaylnk_iter parent;
	splaylnk_iter cur = splaylnk_begin(root);
	
	while (cur) {
		/* 如果有右孩子，往右边走 */
		if (cur->right) {
			cur = cur->right;
			/* 找右孩子下最靠左的节点 */
			while (cur->left)
				cur = cur->left;
		} else {
			if (!cur->left)
				++leafs;
			/* 如果没有右孩子，向父节点走 */
			while ((parent = splaylnk_parent(cur)) 
					&& cur == parent->right)
				cur = parent;
			cur = parent;
		}
	}
	
	return leafs;
}

NV_IMPL void splaylnk_get_info(splaylnk_root* root, splaylnk_info* info)
{
	size_t cnt = 0;
	size_t leafs = 0;
	size_t cur_depth;
	size_t max_depth = 0;
	splaylnk_iter parent;
	splaylnk_iter cur = root->node;
	
	if (cur) {
		cur_depth = 1;
		while (cur->left) {
			cur = cur->left;
			++cur_depth;
		}
		max_depth = cur_depth;
		do {
			++cnt;
			/* 如果有右孩子，往右边走 */
			if (cur->right) {
				cur = cur->right;
				++cur_depth;
				/* 找右孩子下最靠左的节点 */
				while (cur->left) {
					cur = cur->left;
					++cur_depth;
				}
				if (cur_depth > max_depth)
					max_depth = cur_depth;
			} else {
				if (!cur->left)
					++leafs;
				/* 如果没有右孩子，向父节点走 */
				while ((parent = splaylnk_parent(cur)) 
						&& cur == parent->right) {
					cur = parent;
					--cur_depth;
				}
				cur = parent;
				--cur_depth;
			}
		} while (cur);
	}
	
	info->count = cnt;
	info->depth = max_depth;
	info->leafs = leafs;
}

NV_IMPL size_t splaylnk_count(splaylnk_root* root)
{
	size_t cnt = 0;
	splaylnk_iter i = splaylnk_begin(root);

	while (i) {
		splaylnk_inc(i);
		++cnt;
	}

	return cnt;
}

NV_IMPL size_t splaylnk_distance(splaylnk_iter first, splaylnk_iter last)
{
	size_t dist = 1;
	
	while (first != last) {
		splaylnk_inc(first);
		++dist;
	}
	
	return dist;
}

NV_IMPL size_t splaylnk_index_of(splaylnk_root* root, splaylnk_iter itr)
{
	size_t cnt = 0;
	splaylnk_iter i = splaylnk_begin(root);
	
	while (i && i != itr) {
		splaylnk_inc(i);
		++cnt;
	}
	
	return cnt;
}

NV_IMPL size_t splaylnk_reverse_index_of(splaylnk_root* root, 
												splaylnk_iter itr)
{
	size_t cnt = 0;
	splaylnk_iter i = splaylnk_rbegin(root);
	
	while (i && i != itr) {
		splaylnk_dec(i);
		++cnt;
	}
	
	return cnt;
}

NV_IMPL void __do_splaylnk_recurse_foreach(splaylnk_iter cur, 
								void (*fn)(void*), size_t offset)
{
	if (cur) {
		__do_splaylnk_recurse_foreach(cur->left, fn, offset);
		fn(((char*)cur) - offset);
		__do_splaylnk_recurse_foreach(cur->right, fn, offset);
	}
}

NV_IMPL void __do_splaylnk_recurse_reverse_foreach(
						splaylnk_iter cur, void (*fn)(void*), size_t offset)
{
	if (cur) {
		__do_splaylnk_recurse_reverse_foreach(cur->right, fn, offset);
		fn(((char*)cur) - offset);
		__do_splaylnk_recurse_reverse_foreach(cur->left, fn, offset);
	}
}

NV_IMPL void __splaylnk_serialize(splaylnk_root* root, 
									void** buf, size_t offset)
{
	splaylnk_iter i = splaylnk_begin(root);

	while (i) {
		void* p = ((char*)i) - offset;
		*buf++ = p;
		splaylnk_inc(i);
	}
}

static inline size_t SPLAYLNK_F(size_t i, size_t resv)
{
	if (i + 1 >= resv)
		return i + resv;
	return (i << 1) + 1;
}

static inline size_t SPLAYLNK_P(size_t i, size_t d, size_t b)
{
	return ((i >> d) << d) + b;
}

NV_IMPL void __splaylnk_deserialize(splaylnk_root* root, char** buf, 
										size_t cnt, size_t offset)
{
	splaylnk_iter pi, pj, pp;
	size_t i, r, h, c, t, s, d, lv;
	if (!cnt) {
		root->node = NULL;
		return;
	} else if (cnt == 1) {
		pp = (splaylnk_iter)((*buf) + offset);
		root->node = pp;
		pp->parent = NULL;
		return;
	}
	h = 0; c = 1; lv = 0;
	s = 1; d = 2; t = cnt;
	while (t >>= 1) {
		++lv;
		c <<= 1;
	}
	r = cnt - (--c);
	while (++h < lv) {
		i = s - 1;
		s <<= 1;
		while (i < c) {
			size_t p = SPLAYLNK_P(i, h + 1, s - 1);
			pp = (splaylnk_iter)
				(buf[SPLAYLNK_F(p, r)] + offset);
			pi = (splaylnk_iter)
				(buf[SPLAYLNK_F(i, r)] + offset);
			splaylnk_link_left_unsafe(pp, pi);
			i += d;
			pj = (splaylnk_iter)
				(buf[SPLAYLNK_F(i, r)] + offset);
			splaylnk_link_right_unsafe(pp, pj);
			if (h < 2) {
				pi->left = NULL;
				pi->right = NULL;
				pj->left = NULL;
				pj->right = NULL;
			}
			i += d;
		}
		d <<= 1;
	}
	root->node = pp;
	pp->parent = NULL;
	for (i = 0; i < r; ++i) {
		pp = (splaylnk_iter)
			(buf[((i >> 1) << 2) + 1] + offset);
		pi = (splaylnk_iter)
			(buf[i << 1] + offset);
		pi->left = NULL;
		pi->right = NULL;
		if (i & 1)
			splaylnk_link_right_unsafe(pp, pi);
		else
			splaylnk_link_left_unsafe(pp, pi);
	}
}

#ifdef __cplusplus
}
#endif	/* #ifdef __cplusplus */
