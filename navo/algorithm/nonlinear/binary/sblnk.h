/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/nonlinear/binary/sblnk.h
 * 
 * @brief Size-balanced linked tree.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_SBLNK__
#define __NV_SBLNK__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"

#ifdef __cplusplus
extern "C" {
#endif	/* #ifdef __cplusplus */

/**
 * @defgroup 大小平衡树
 * @ingroup 二叉树
 * @{
 */

/**
 * @struct __sblnk_node
 * @brief 大小平衡树节点
 * @typedef sblnk_node
 * @brief 节点
 * @typedef sblnk_iter
 * @brief 迭代器
 */
typedef struct __sblnk_node {
	/** @brief 父节点 */
	struct __sblnk_node* parent;
	/** @brief 左子节点 */
	struct __sblnk_node* left;
	/** @brief 右子节点 */
	struct __sblnk_node* right;
	/** @brief 子树大小 */
	size_t size;
}sblnk_node, *sblnk_iter;

/**
 * @struct sblnk_root
 * @brief 树根
 */
typedef struct {
	/** @brief 树根节点 */
	sblnk_node* node;
}sblnk_root;

/**
 * @struct sblnk_info
 * @brief 树信息表
 */
typedef struct {
	/** @brief 节点数 */
	size_t count;
	/** @brief 深度 */
	size_t depth;
	/** @brief 叶子数 */
	size_t leafs;
}sblnk_info;

/**
 * @def SBLNK_ROOT(name)
 * @brief 定义一个名为name的树根，并对其初始化
 * @param name 树根名称
 * @def SBLNK_NODE(name)
 * @brief 定义一个名为name的节点，并对其初始化
 * @param name 节点名称
 * @def SBLNK_INFO(name)
 * @brief 定义一个名为name的树信息表，并对其初始化
 * @param name 名称
 */
#define SBLNK_ROOT(name) sblnk_root name = {NULL}
#define SBLNK_NODE(name) sblnk_node name = {NULL, NULL, NULL, 0}
#define SBLNK_INFO(name) sblnk_info name = {0, 0, 0}

/**
 * @typedef sblnk_pr1
 * @brief 单参数回调函数指针
 * @typedef sblnk_pr2
 * @brief 双参数回调函数指针
 */
typedef int (*sblnk_pr1)(sblnk_node*);
typedef int (*sblnk_pr2)(sblnk_node*, sblnk_node*);

/**
 * @fn size_t sblnk_distance(sblnk_iter first, sblnk_iter last);
 * @brief 计算两个节点的迭代距离
 * @param[in] first 首节点
 * @param[in] last 尾节点
 * @note 以索引为例，2到4的距离是4 (4-2+1=3)
 * @return size_t 返回距离
 */
NV_API size_t sblnk_distance(sblnk_iter first, sblnk_iter last);

/**
 * @fn size_t sblnk_index_of(sblnk_root* root, sblnk_iter itr);
 * @brief 计算迭代器的正向索引
 * @param[in] root 树根
 * @param[in] itr 迭代器
 * @return size_t 返回索引
 * @see sblnk_reverse_index_of
 */
NV_API size_t sblnk_index_of(sblnk_root* root, sblnk_iter itr);

/**
 * @fn size_t sblnk_reverse_index_of(sblnk_root* root, sblnk_iter itr);
 * @brief 计算迭代器的逆向索引
 * @param[in] root 树根
 * @param[in] itr 迭代器
 * @return size_t 返回索引
 * @see sblnk_index_of
 */
NV_API size_t sblnk_reverse_index_of(sblnk_root* root, sblnk_iter itr);

/**
 * @fn size_t sblnk_depth(sblnk_root* root);
 * @brief 计算树的深度
 * @param[in] root 树根
 * @return size_t 返回深度
 * @see sblnk_get_info
 */
NV_API size_t sblnk_depth(sblnk_root* root);

/**
 * @fn size_t sblnk_leafs(sblnk_root* root);
 * @brief 计算树的叶子数
 * @param[in] root 树根
 * @return size_t 返回叶子数
 * @see sblnk_get_info
 */
NV_API size_t sblnk_leafs(sblnk_root* root);

/**
 * @fn void sblnk_get_info(sblnk_root* root, sblnk_info* info);
 * @brief 获取树的信息，包括节点数、深度、叶子数
 * @param[in] root 树根
 * @param[out] info 保存信息的结构体
 * @see sblnk_count, sblnk_depth, sblnk_leafs
 */
NV_API void sblnk_get_info(sblnk_root* root, sblnk_info* info);

/**
 * @fn void sblnk_maintain(sblnk_root* root, sblnk_node* node, 
								sblnk_node* parent);
 * @brief 插入节点后，对树进行调整
 * @param[in,out] root 树根
 * @param[in,out] node 目标节点
 * @param[in,out] parent node的父节点
 * @note 常用于节点插入，在sblnk_link之后调用
 * @par 示例:
 * @code
 	int insert_node(sblnk_root* root, DATA* newly)
 	{
	 	DATA* pdat;
 		sblnk_iter cur;
 		sblnk_iter* p = &root->node;
 		
 		// 初始化新节点
 		sblnk_node_init(&newly->node);
 		
 		while (*p) {
 			cur = *p;
 			pdat = container_of(cur, DATA, node);
 			if (pdat->data > newly->data)
 				p = &cur->left;
 			else if (pdat->data < newly->data)
 				p = &cur->right;
 			else {
 				// 有相同值的节点，无法插入
 				return 0;
 			}
 		}
 		
 		// 链接新节点到树中
 		sblnk_link(&newly->node, cur, p);
 		
 		// 调整树
 		sblnk_maintain(root, &newly->node, cur);

 		return 1;
 	}
 * @endcode
 * @see sblnk_insert, sblnk_link
 */
NV_API void sblnk_maintain(sblnk_root* root, sblnk_node* node, 
								sblnk_node* parent);

/**
 * @fn void sblnk_erase(sblnk_root* root, sblnk_node* entry);
 * @brief 移除某个节点
 * @param[in,out] root 树根
 * @param[in,out] entry 待移除节点
 */
NV_API void sblnk_erase(sblnk_root* root, sblnk_node* entry);

/**
 * @fn void sblnk_erases(sblnk_root* root, sblnk_iter begin, sblnk_iter end, 
							sblnk_pr1 erase_handler);
 * @brief 移除区域内的节点
 * @param[in,out] root 树根

 * @param[in,out] begin 起始位置
 * @param[in,out] end 终止位置
 * @param[in] erase_handler 移除响应函数，通常用于监视和释放节点，可以传递NULL,
 							表示不为删除的节点调用响应来处理
 * @note 函数会为每一个被移除的节点调用erase_handler来处理节点
 * @see sblnk_clear
 */
NV_API void sblnk_erases(sblnk_root* root, sblnk_iter begin, 
							sblnk_iter end, sblnk_pr1 erase_handler);

/**
 * @fn static inline void sblnk_init(sblnk_root* root);
 * @brief 初始化树根
 * @param[out] root 目标树根
 * @par 示例:
 * @code
	sblnk_root r;
	// 初始化
	sblnk_init(&r);
 * @endcode
 */
static inline void sblnk_init(sblnk_root* root)
{
	root->node = NULL;
}

/**
 * @fn static inline int sblnk_empty(sblnk_root* root);
 * @brief 判断树是否为空
 * @param[in] root 树根
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	if (!sblnk_empty(&r)) {
 		......
 	}
 * @endcode
 */
static inline int sblnk_empty(sblnk_root* root)
{
	return !root->node;
}

static inline int __sblnk_singular(sblnk_node* root)
{
	return root && !root->left && !root->right;
}

/**
 * @fn static inline int sblnk_singular(sblnk_root* root);
 * @brief 判断树是否只有单个节点
 * @param[in] root 树根
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	if (sblnk_singular(&r)) {
 		......
 	}
 * @endcode
 */
static inline int sblnk_singular(sblnk_root* root)
{
	return __sblnk_singular(root->node);
}

/**
 * @fn static inline int sblnk_serialable(sblnk_root* root);
 * @brief 判断树是否可以序列化
 * @param[in] root 树根
 * @note 即判断是否至少有一个节点
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	// 判断是否可以序列化
 	if (sblnk_serialable(&r)) {
 		slseq_head seq;
 		// 分配序列缓存
 		DATA** buf = (DATA**)malloc(sizeof(DATA*) * avl_cnt);
 		// 序列化
 		sblnk_serialize(&r, buf, DATA, node);
 		SLSEQ_INIT_SERIAL(&seq, buf, avl_cnt);
 		......
 		// do something
 		......
 		// 反序列化
 		sblnk_deserialize(&r, buf, avl_cnt, DATA, node);
 		// 释放缓存
 		free(buf);
 	}
 * @endcode
 * @see sblnk_serialize, sblnk_deserialize
 */
static inline int sblnk_serialable(sblnk_root* root)
{
	return !!root->node;
}

static inline size_t __sblnk_count(sblnk_node* root)
{
	if (root)
		return root->size + 1;
	return 0;
}

/**
 * @fn size_t sblnk_count(sblnk_root* root);
 * @brief 获取树的节点总数
 * @param[in] root 树根
 * @return size_t 返回节点数
 */
static inline size_t sblnk_count(sblnk_root* root)
{
	return __sblnk_count(root->node);
}

/**
 * @fn static inline sblnk_iter sblnk_left(sblnk_node* node);
 * @brief 得到左子节点
 * @param[in] node 当前节点
 * @return sblnk_iter 返回左子节点
 * @see sblnk_right
 */
static inline sblnk_iter sblnk_left(sblnk_node* node)
{
	return node->left;
}

/**
 * @fn static inline sblnk_iter sblnk_right(sblnk_node* node);
 * @brief 得到右子节点
 * @param[in] node 当前节点
 * @return sblnk_iter 返回右子节点
 * @see sblnk_left
 */
static inline sblnk_iter sblnk_right(sblnk_node* node)
{
	return node->right;
}

/**
 * @fn static inline sblnk_iter sblnk_parent(sblnk_node* node);
 * @brief 得到父节点
 * @param[in] node 当前节点
 * @return sblnk_iter 返回父节点
 */
static inline sblnk_iter sblnk_parent(sblnk_node* node)
{
	return node->parent;
}

/**
 * @fn static inline size_t sblnk_size(sblnk_node* node);
 * @brief 得到节点的子树大小
 * @param[in] node 当前节点
 * @return size_t 返回子树大小
 */
static inline size_t sblnk_size(sblnk_node* node)
{
	return node->size;
}

/**
 * @fn static inline int sblnk_is_root(sblnk_iter itr);
 * @brief 判断当前节点是否是根节点
 * @param[in] itr 当前节点
 * @return int 返回结果
 * @retval 1 是根节点
 * @retval 0 不是根节点
 */
static inline int sblnk_is_root(sblnk_iter itr)
{
	return !itr->parent;
}

/**
 * @fn static inline int sblnk_is_leaf(sblnk_iter itr);
 * @brief 判断当前节点是否是叶子节点
 * @param[in] itr 当前节点
 * @return int 返回结果
 * @retval 1 是叶子节点
 * @retval 0 不是叶子节点
 */
static inline int sblnk_is_leaf(sblnk_iter itr)
{
	return !itr->left && !itr->right;
}

/**
 * @fn static inline void sblnk_link(sblnk_iter newly, sblnk_iter parent, 
								sblnk_iter* plink);
 * @brief 链接两个节点
 * @param[in,out] newly 新节点
 * @param[in,out] parent 父节点
 * @param[in,out] plink 父节点对应孩子指针。若newly加入parent的左子，则plink指向
 						parent的left成员，若newly加入parent的右子，则plink指向
 						parent的right成员
 * @note 链接newly和parent节点
 */
static inline void sblnk_link(sblnk_iter newly, sblnk_iter parent, 
							  sblnk_iter* plink)
{
	/* 简单地将newly节点链接到parent节点下 */
	*plink = newly;
	newly->left = NULL;
	newly->right = NULL;
	newly->parent = parent;
}

/**
 * @fn static inline void sblnk_set_size(sblnk_node* node, size_t size);
 * @brief 设置当前节点子树大小域
 * @param[in,out] node 当前节点
 * @param[in] size 新子树大小
 */
static inline void sblnk_set_size(sblnk_node* node, size_t size)
{
	node->size = size;
}

/**
 * @fn static inline void sblnk_set_parent_if(sblnk_node* node, 
									sblnk_node* parent);
 * @brief 仅在node不为空时，设置node的父节点
 * @param[in,out] node 当前节点
 * @param[in] parent 新的父节点
 */
static inline void sblnk_set_parent_if(sblnk_node* node, sblnk_node* parent)
{
	if (node)
		node->parent = parent;
}

/**
 * @fn static inline void sblnk_node_init(sblnk_node* node);
 * @brief 初始化节点
 * @param[out] node 指定节点
 */
static inline void sblnk_node_init(sblnk_node* node)
{
	node->parent = NULL;
	node->left = NULL;
	node->right = NULL;
	node->size = 0;
}

/**
 * @fn static inline void sblnk_iter_replace(sblnk_root* root, 
							sblnk_iter victim,  sblnk_iter newly);
 * @brief 用新节点替换旧节点
 * @param[in,out] root 树根
 * @param[in,out] victim 受害目标节点
 * @param[out] newly 新节点
 */
static inline void sblnk_iter_replace(sblnk_root* root, sblnk_iter victim, 
									 sblnk_iter newly)
{
	sblnk_iter parent = sblnk_parent(victim);
	
	/* 将victim父节点的孩子设为newly */
	if (parent) {
		if (victim == parent->left)
			parent->left = newly;
		else
			parent->right = newly;
	} else
		root->node = newly;
	
	/* 重新确定victim孩子节点的父节点为newly */
	sblnk_set_parent_if(victim->left, newly);
	sblnk_set_parent_if(victim->right, newly);
	
	/* 替换节点成员 */
	*newly = *victim;
}

/**
 * @fn static inline void sblnk_replace(sblnk_root* victim, sblnk_root* newly);
 * @brief 用新的树根替换旧树根
 * @param[in,out] victim 受害目标树根
 * @param[out] newly 新树根
 */
static inline void sblnk_replace(sblnk_root* victim, sblnk_root* newly)
{
	newly->node = victim->node;
}

/**
 * @fn static inline void sblnk_swap(sblnk_root* a, sblnk_root* b);
 * @brief 交换两个树
 * @param[in,out] a 第一个树
 * @param[in,out] b 第二个树
 */
static inline void sblnk_swap(sblnk_root* a, sblnk_root* b)
{
	sblnk_iter t = a->node; a->node = b->node; b->node = t;
}

/**
 * @fn int sblnk_exist(sblnk_root* root, sblnk_iter itr);
 * @brief 判断一个节点是否存在于树中
 * @param[in] root 树根
 * @param[in] itr 指定节点
 * @return int 返回结果
 * @retval 1 存在
 * @retval 0 不存在
 */
NV_API int sblnk_exist(sblnk_root* root, sblnk_iter itr);

/**
 * @fn static inline sblnk_iter sblnk_at(sblnk_root* root, size_t index);
 * @brief 得到正向索引对应的节点的迭代器
 * @param[in] root 树根
 * @param[in] index 指定正向索引
 * @return sblnk_iter 返回迭代器
 * @see sblnk_reverse_at
 */
static inline sblnk_iter sblnk_at(sblnk_root* root, size_t index)
{
	return sblnk_advance(sblnk_front(root), index);
}

/**
 * @fn static inline sblnk_iter sblnk_reverse_at(sblnk_root* root, 
 												size_t index);
 * @brief 得到逆向索引对应的节点的迭代器
 * @param[in] root 树根
 * @param[in] index 指定逆向索引
 * @return sblnk_iter 返回迭代器
 * @see sblnk_at
 */
static inline sblnk_iter sblnk_reverse_at(sblnk_root* root, size_t index)
{
	return sblnk_advance(sblnk_back(root), -(int)(index));
}

/**
 * @fn sblnk_iter sblnk_front(sblnk_root* root);
 * @brief 得到树的最左端节点
 * @param[in] root 树根
 * @return sblnk_iter 返回迭代器
 * @see sblnk_back
 */
NV_API sblnk_iter sblnk_front(sblnk_root* root);

/**
 * @fn sblnk_iter sblnk_back(sblnk_root* root);
 * @brief 得到树的最右端节点
 * @param[in] root 树根
 * @return sblnk_iter 返回迭代器
 * @see sblnk_front
 */
NV_API sblnk_iter sblnk_back(sblnk_root* root);

/**
 * @fn static inline sblnk_iter sblnk_begin(sblnk_root* root);
 * @brief 得到指向正向起始位置的迭代器
 * @param[in] root 树根
 * @return sblnk_iter 返回迭代器
 * @see sblnk_end, sblnk_end_of, sblnk_rbegin, sblnk_rend
 */
static inline sblnk_iter sblnk_begin(sblnk_root* root)
{
	return sblnk_front(root);
}

/**
 * @fn static inline sblnk_iter sblnk_end();
 * @brief 得到指向正向终止位置的迭代器
 * @return sblnk_iter 返回迭代器，永远返回NULL
 * @see sblnk_begin, sblnk_end_of, sblnk_rbegin, sblnk_rend
 */
static inline sblnk_iter sblnk_end()
{
	return NULL;
}

/**
 * @fn static inline sblnk_iter sblnk_end_of(sblnk_iter cur);
 * @brief 得到当前迭代器的正向终止位置
 * @param[in] itr 迭代器
 * @return sblnk_iter 返回迭代器
 * @see sblnk_begin, sblnk_end
 */
static inline sblnk_iter sblnk_end_of(sblnk_iter cur)
{
	return sblnk_next(cur);
}

/**
 * @fn static inline sblnk_iter sblnk_rbegin(sblnk_root* root);
 * @brief 得到指向逆向起始位置的迭代器
 * @param[in] root 树根
 * @return sblnk_iter 返回迭代器
 * @see sblnk_rend, sblnk_rend_of, sblnk_begin, sblnk_end
 */
static inline sblnk_iter sblnk_rbegin(sblnk_root* root)
{
	return sblnk_back(root);
}

/**
 * @fn static inline sblnk_iter sblnk_rend();
 * @brief 得到指向逆向终止位置的迭代器
 * @return sblnk_iter 返回迭代器，永远返回NULL
 * @see sblnk_rbegin, sblnk_rend_of, sblnk_begin, sblnk_end
 */
static inline sblnk_iter sblnk_rend()
{
	return NULL;
}

/**
 * @fn static inline sblnk_iter sblnk_rend_of(sblnk_iter cur);
 * @brief 得到当前迭代器的逆向终止位置
 * @param[in] itr 迭代器
 * @return sblnk_iter 返回迭代器
 * @see sblnk_rbegin, sblnk_rend
 */
static inline sblnk_iter sblnk_rend_of(sblnk_iter cur)
{
	return sblnk_prev(cur);
}

/**
 * @fn sblnk_iter sblnk_next(sblnk_iter cur);
 * @brief 得到当前节点的中序后继节点
 * @param[in] cur 当前节点
 * @return sblnk_iter 返回后继节点，若无后继，则返回NULL
 * @see sblnk_prev
 */
NV_API sblnk_iter sblnk_next(sblnk_iter cur);

/**
 * @fn sblnk_iter sblnk_prev(sblnk_iter cur);
 * @brief 得到当前节点的中序前驱节点
 * @param[in] cur 当前节点
 * @return sblnk_iter 返回前驱节点，若无前驱，则返回NULL
 * @see sblnk_next
 */
NV_API sblnk_iter sblnk_prev(sblnk_iter cur);

/**
 * @fn sblnk_iter sblnk_advance(sblnk_iter cur, int dist);
 * @brief 给迭代器增加一段距离或减小一段距离
 * @param[in] cur 迭代器
 * @param[in] dist 增加的距离。正数时，表示增加；负数时，表示减小
 * @return sblnk_iter 返回新迭代器
 */
NV_API sblnk_iter sblnk_advance(sblnk_iter cur, int dist);

/**
 * @fn static inline void sblnk_clear(sblnk_root* root, 
 								sblnk_pr1 erase_handler);
 * @brief 清空树
 * @param[in,out] root 树根
 * @param[in] erase_handler 移除响应函数，通常用于监视和释放节点，可以传递NULL,
 							表示不为删除的节点调用响应来处理
 * @note 函数会为每一个被移除的节点调用erase_handler来处理节点
 * @see sblnk_erases
 */
static inline void sblnk_clear(sblnk_root* root, sblnk_pr1 erase_handler)
{
	if (!sblnk_empty(root))
		sblnk_erases(root, sblnk_front(root), NULL, erase_handler);
}

static inline sblnk_iter __sblnk_inc_later(sblnk_iter cur, sblnk_iter* p)
{
	*p = sblnk_next(cur);
	return cur;
}

static inline sblnk_iter __sblnk_dec_later(sblnk_iter cur, sblnk_iter* p)
{
	*p = sblnk_prev(cur);
	return cur;
}

/**
 * @def sblnk_inc(itr)
 * @brief 迭代器递增
 * @def sblnk_dec(itr)
 * @brief 迭代器递减
 * @def sblnk_inc_later(itr)
 * @brief 迭代器后自增
 * @def sblnk_dec_later(itr)
 * @brief 迭代器后自减
 */
#define sblnk_inc(itr)			((itr) = sblnk_next(itr))
#define sblnk_dec(itr)			((itr) = sblnk_prev(itr))
#define sblnk_inc_later(itr)	__sblnk_inc_later(itr, &(itr))
#define sblnk_dec_later(itr)	__sblnk_dec_later(itr, &(itr))

/**
 * @def sblnk_foreach(_begin, _end, fn, type, member)
 * @brief 正向遍历_begin到_end的节点，并为每一个节点调用回调函数或宏
 * @param[in] _begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define sblnk_foreach(_begin, _end, fn, type, member) do {\
		sblnk_iter _q_e = (_end);							\
		sblnk_iter _q_f = (_begin);							\
		while (_q_f != _q_e) {								\
			type * _q_t = container_of(_q_f, type, member);	\
			sblnk_inc(_q_f);								\
			fn(_q_t);										\
		}													\
	} while (0)

/**
 * @def sblnk_reverse_foreach(_rbegin, _rend, fn, type, member)
 * @brief 逆向遍历_begin到_end的节点，并为每一个节点调用回调函数或宏
 * @param[in] _rbegin 逆向起始位置
 * @param[in] _rend 逆向终止位置
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define sblnk_reverse_foreach(_rbegin, _rend, fn, type, member) do {\
		sblnk_iter _q_e = (_rend);							\
		sblnk_iter _q_f = (_rbegin);						\
		while (_q_f != _q_e) {								\
			type * _q_t = container_of(_q_f, type, member);	\
			sblnk_dec(_q_f);								\
			fn(_q_t);										\
		}													\
	} while (0)

NV_API void __do_sblnk_recurse_foreach(sblnk_iter cur, 
							void (*fn)(void*), size_t offset);

NV_API void __do_sblnk_recurse_reverse_foreach(sblnk_iter cur, 
							void (*fn)(void*), size_t offset);

static inline void __sblnk_recurse_foreach(sblnk_root* root, 
							void (*fn)(void*), size_t offset)
{
	if (fn)
		__do_sblnk_recurse_foreach(root->node, fn, offset);
}

static inline void __sblnk_recurse_reverse_foreach(sblnk_root* root, 
								void (*fn)(void*), size_t offset)
{
	if (fn)
		__do_sblnk_recurse_reverse_foreach(root->node, fn, offset);
}

/**
 * @def SBLNK_RECURSE_PROC(name, type, param)
 * @brief 定义递归遍历回调函数
 * @param name 函数名
 * @param type 节点拥有者类型
 * @param param 参数名
 * @see sblnk_recurse_foreach, sblnk_recurse_reverse_foreach
 */
#define SBLNK_RECURSE_PROC(name, type, param) void name(type * param)

/**
 * @def sblnk_recurse_foreach(root, fn, type, member)
 * @brief 正向递归遍历整个树
 * @param[in] root 树根
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @par 示例:
 * @code
 	SBLNK_RECURSE_PROC(PrintNode, DATA, node)
 	{
 		printf("%3d ", node->data);
 	}

	// 打印整个树
 	void PrintAVLTree(sblnk_root* root)
 	{
 		sblnk_recurse_foreach(root, PrintNode, DATA, node);
 		putchar('\n');
 	}
 * @endcode
 * @see sblnk_recurse_reverse_foreach, SBLNK_RECURSE_PROC
 */
#define sblnk_recurse_foreach(root, fn, type, member) \
	__sblnk_recurse_foreach(root, (void (*)(void*))(fn), \
			offset_of(type, member))

/**
 * @def sblnk_recurse_reverse_foreach(root, fn, type, member)
 * @brief 逆向递归遍历整个树
 * @param[in] root 树根
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see sblnk_recurse_foreach, SBLNK_RECURSE_PROC
 */
#define sblnk_recurse_reverse_foreach(root, fn, type, member) \
	__sblnk_recurse_reverse_foreach(root, (void (*)(void*))(fn), \
			offset_of(type, member))

/**
 * @def sblnk_search(root, newly, greater, res, type, member)
 * @brief 搜索树中的某个节点
 * @param[in] root 树根
 * @param[in] newly 用于搜索的临时节点，类型是type，该节点使用与搜索的值相同的值
 * @param[in] greater 比较函数或宏。
 						升序:(i->data > j->data) 
 						降序:(i->data < j->data)
 * @param[out] res 返回值。返回目标节点，若为NULL表示找不到节点
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @par 示例:
 * @code
 	sblnk_iter node_15;
 	DATA* p = (DATA*)malloc(sizeof(DATA));
 	if (!p)
 		return NULL;

 	p->data = 15;

	//查找值为15的节点
 	sblnk_search(&root, p, DATA_Greater, node_15, DATA, node);
 	if (!node_15)
 		return NULL;

 	return node_15;
 * @endcode
 */
#define sblnk_search(root, newly, greater, res, type, member) do {\
		sblnk_root* _k_r = (root);								\
		type * _k_pn = (newly);									\
		sblnk_iter _k_p = _k_r->node;							\
		(res) = NULL;											\
		while (_k_p) {											\
			type * _k_pr = container_of(_k_p, type, member);	\
			if (greater(_k_pr, _k_pn))							\
				_k_p = _k_p->left;								\
			else if (greater(_k_pn, _k_pr))						\
				_k_p = _k_p->right;								\
			else {												\
				(res) = _k_p; break;							\
			}													\
		}														\
	} while (0)

/**
 * @def sblnk_insert(root, newly, greater, res, type, member)
 * @brief 插入新节点
 * @param[in] root 树根
 * @param[in] newly 新节点
 * @param[in] greater 比较函数或宏。
 						升序:(i->data > j->data) 
 						降序:(i->data < j->data)
 * @param[out] res 返回值。返回1表示成功；若为0表示节点有冲突，无法插入
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @par 示例:
 * @code
 	int ret;
 	DATA* p = (DATA*)malloc(sizeof(DATA));
 	if (!p)
 		return 0;

 	p->data = 88;

	//插入值为88的节点
 	sblnk_insert(&root, &p->node, DATA_Greater, ret, DATA, node);
 	if (!ret)
 		return 0;

 	return 1;
 * @endcode
 */
#define sblnk_insert(root, newly, greater, res, type, member) do {\
		sblnk_root* _k_r = (root);										\
		sblnk_iter* _k_pp = &_k_r->node;								\
		sblnk_iter _k_n = (newly);										\
		sblnk_iter _k_p;												\
		type * _k_pn = container_of(_k_n, type, member);				\
		(res) = 1;														\
		sblnk_node_init(_k_n);											\
		while (*_k_pp) {												\
			type * _k_pr = container_of(_k_p = *_k_pp, type, member);	\
			if (greater(_k_pr, _k_pn))									\
				_k_pp = &_k_p->left;									\
			else if (greater(_k_pn, _k_pr))								\
				_k_pp = &_k_p->right;									\
			else {														\
				(res) = 0; break;										\
			}															\
		}																\
		if (res) {														\
			sblnk_link(_k_n, _k_p, _k_pp);								\
			sblnk_maintain(_k_r, _k_n, _k_p);							\
		}																\
	} while (0)

NV_API void __sblnk_serialize(sblnk_root* root, void** buf, size_t offset);

NV_API void __sblnk_deserialize(sblnk_root* root, char** buf, 
									size_t cnt, size_t offset);

/**
 * @def sblnk_serialize(root, buf, type, member)
 * @brief 将树进行序列化为一个有序序列
 * @param[in] root 树根
 * @param[out] buf 序列缓存，元素类型为type指针类型
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see sblnk_deserialize, sblnk_serialable
 */
#define sblnk_serialize(root, buf, type, member) \
	__sblnk_serialize(root, (void**)(buf), offset_of(type, member))

/**
 * @def sblnk_deserialize(root, buf, cnt, type, member)
 * @brief 根据树的序列进行反序列化为一个完整的树
 * @param[out] root 树根
 * @param[in] buf 序列缓存，元素类型为type指针类型
 * @param[in] cnt 序列长度(元素数)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see sblnk_serialize, sblnk_serialable
 */
#define sblnk_deserialize(root, buf, cnt, type, member) \
	__sblnk_deserialize(root, (char**)(buf), cnt, offset_of(type, member))

/** @} */

#ifdef __cplusplus
}
#endif	/* #ifdef __cplusplus */

#endif	/* #ifndef __NV_SBLNK__ */

