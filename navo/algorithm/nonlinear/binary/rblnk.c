/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/nonlinear/binary/rblnk.c
 * 
 * @brief Implements for red-black linked tree.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#include "rblnk.h"

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

static inline rblnk_link_left(rblnk_iter parent, rblnk_iter child)
{
	parent->left = child;
	rblnk_set_parent_if(child, parent);
}

static inline rblnk_link_right(rblnk_iter parent, rblnk_iter child)
{
	parent->right = child;
	rblnk_set_parent_if(child, parent);
}

static inline rblnk_link_left_unsafe(rblnk_iter parent, rblnk_iter child)
{
	parent->left = child;
	rblnk_set_parent(child, parent);
}

static inline rblnk_link_right_unsafe(rblnk_iter parent, rblnk_iter child)
{
	parent->right = child;
	rblnk_set_parent(child, parent);
}

static inline void rblnk_replace_child(rblnk_root* root, rblnk_iter me, 
						rblnk_iter old_child, rblnk_iter new_child)
{
	if (me) {
		if (old_child == me->left)
			me->left = new_child;
		else
			me->right = new_child;
	} else
		root->node = new_child;
}

/* 对top节点进行左旋转 */
static void rblnk_rotate_left(rblnk_node* top, rblnk_root* root)
{
	rblnk_node* parent = rblnk_parent(top);
	rblnk_node* node = top->right;
	rblnk_link_right(top, node->left);
	rblnk_link_left_unsafe(node, top);
	rblnk_replace_child(root, parent, top, node);
	rblnk_set_parent(node, parent);
}

/* 对top节点进行右旋转 */
static void rblnk_rotate_right(rblnk_node* top, rblnk_root* root)
{
	rblnk_node* parent = rblnk_parent(top);
	rblnk_node* node = top->left;
	rblnk_link_left(top, node->right);
	rblnk_link_right_unsafe(node, top);
	rblnk_replace_child(root, parent, top, node);
	rblnk_set_parent(node, parent);
}

NV_IMPL void rblnk_coloring(rblnk_root* root, rblnk_node* node, 
								rblnk_node* parent)
{	/* 如果有父节点且父节点是红色的，则对树进行修正 */
	while (parent && rblnk_red(parent)) {
		rblnk_iter gparent = rblnk_parent(parent);
		if (parent == gparent->left) {
			rblnk_iter uncle = gparent->right;

			/* 如果有叔节点且叔节点为红色，则将叔节点和父节点涂黑，将祖父节点涂红 */
			if (uncle && rblnk_red(uncle)) {
				rblnk_set_black(uncle);
				rblnk_set_black(parent);
				rblnk_set_red(gparent);
				/* 祖父节点下的路径已经调整完成，进行下一轮调整 */
				parent = rblnk_parent(node = gparent);
				continue;
			}
			/* 如果没有叔节点或叔节点是黑色的 */
			if (node == parent->right) {
				/* 如果父节点是祖父节点的左子且新节点是父节点的右子，
				 * 对父节点进行左旋，此时新节点成了原父节点的父节点
				 * 且新节点和原父节点都是红色的
				 */
				rblnk_rotate_left(parent, root); {
					rblnk_iter tmp = parent;
					parent = node;
					node = tmp;
				}
			}
			/* 此时父节点和左子是红色的，将父节点涂黑，祖父节点涂红，
			 * 再对祖父节点进行右旋，即完成调整
			 */
			rblnk_set_black(parent);
			rblnk_set_red(gparent);
			rblnk_rotate_right(gparent, root);
			parent = rblnk_parent(node);
		} else {
			rblnk_iter uncle = gparent->left;

			/* 如果有叔节点且叔节点为红色，则将叔节点和父节点涂黑，将祖父节点涂红 */
			if (uncle && rblnk_red(uncle)) {
				rblnk_set_black(uncle);
				rblnk_set_black(parent);
				rblnk_set_red(gparent);
				/* 祖父节点下的路径已经调整完成，进行下一轮调整 */
				parent = rblnk_parent(node = gparent);
				continue;
			}
			/* 如果没有叔节点或叔节点是黑色的 */
			if (node == parent->left) {
				/* 或父节点是祖父节点的右子且新节点是父节点的左子，
				 * 对父节点进行右旋，此时新节点成了原父节点的父节点
				 * 且新节点和原父节点都是红色的
				 */
				rblnk_rotate_right(parent, root); {
					rblnk_iter tmp = parent;
					parent = node;
					node = tmp;
				}
			}
			/* 此时父节点和右子是红色的，将父节点涂黑，祖父节点涂红，
			 * 再对祖父节点进行左旋，即完成调整
			 */
			rblnk_set_black(parent);
			rblnk_set_red(gparent);
			rblnk_rotate_left(gparent, root);
			parent = rblnk_parent(node);
		}
	}
	/* (1)没有父节点，则直接将新节点涂黑作为根节点
	 * (2)父节点是黑色的，则直接插入新节点不影响树
	 * (3)经过对树的调整后，将新节点涂黑
	 */
	rblnk_set_black(root->node);
}

/* 判断是否存在节点node，且node是黑色的 */
static inline int rblnk_black_if(rblnk_iter node)
{
	return (!node || rblnk_black(node));
}

/* 擦除颜色 */
static void __rblnk_erase_color(rblnk_node* node, rblnk_node* parent, 
									rblnk_root* root)
{
	rblnk_iter sibling;

	/* node节点是黑色的且不为根节点 */
	while (rblnk_black_if(node) && node != root->node) {
		if (node == parent->left) {
			sibling = parent->right;
			if (rblnk_red(sibling)) {
				/* 兄弟节点是红色的 */
				rblnk_set_black(sibling);
				rblnk_set_red(parent);
				rblnk_rotate_left(parent, root);
				/* sibling指向新的兄弟节点且该节点一定是黑色的 */
				sibling = parent->right;
			}
			if (rblnk_black_if(sibling->left) && 
				rblnk_black_if(sibling->right)) {
				/* 兄弟节点是黑色的且它两个孩子节点也是黑色的 */
				rblnk_set_red(sibling);
				node = parent;
				parent = rblnk_parent(node);
			} else {
				/* 兄弟节点是黑色的且有红色孩子节点 */
				if (rblnk_black_if(sibling->right)) {
					/* 如果兄弟节点右子节点为空或是黑色的，则左子一定是红色，
					 * 利用旋转将其转换成右子节点为红色
					 */
					rblnk_set_black(sibling->left);
					rblnk_set_red(sibling);
					rblnk_rotate_right(sibling, root);
					sibling = parent->right;
				}
				/* 这里，兄弟节点有一个红色的右子节点 */
				rblnk_set_color(sibling, rblnk_color(parent));
				rblnk_set_black(parent);
				rblnk_set_black(sibling->right);
				rblnk_rotate_left(parent, root);
				/* 将根节点涂黑 */
				node = root->node;
				/* 完成调整 */
				break;
			}
		} else {
			sibling = parent->left;
			if (rblnk_red(sibling)) {
				/* 兄弟节点是红色的 */
				rblnk_set_black(sibling);
				rblnk_set_red(parent);
				rblnk_rotate_right(parent, root);
				/* sibling指向新的兄弟节点且该节点一定是黑色的 */
				sibling = parent->left;
			}
			if (rblnk_black_if(sibling->left) && 
				rblnk_black_if(sibling->right)) {
				/* 兄弟节点是黑色的且它两个孩子节点也是黑色的 */
				rblnk_set_red(sibling);
				node = parent;
				parent = rblnk_parent(node);
			} else {
				/* 兄弟节点是黑色的且有红色孩子节点 */
				if (rblnk_black_if(sibling->left)) {
					/* 如果兄弟节点左子节点为空或是黑色的，则右子一定是红色，
					 * 利用旋转将其转换成右子节点为红色
					 */
					rblnk_set_black(sibling->right);
					rblnk_set_red(sibling);
					rblnk_rotate_left(sibling, root);
					sibling = parent->left;
				}
				/* 这里，兄弟节点有一个红色的右子节点 */
				rblnk_set_color(sibling, rblnk_color(parent));
				rblnk_set_black(parent);
				rblnk_set_black(sibling->left);
				rblnk_rotate_right(parent, root);
				/* 将根节点涂黑 */
				node = root->node;
				/* 完成调整 */
				break;
			}
		}
	}
	/* 如果节点为红色或者是根节点，直接涂黑 */
	if (node)
		rblnk_set_black(node);
}

NV_IMPL void rblnk_erase(rblnk_root* root, rblnk_node* entry)
{
	int color;
	rblnk_node* child;
	rblnk_node* parent = rblnk_parent(entry);

	do {/* 如果entry节点最多只有一个孩子，则直接删除entry节点
		 * 并调整树，如果entry同时有两个孩子，需要将其转换成
		 * 只有一子或无子的情况
		 */
		if (!entry->left) {
			child = entry->right;
		} else if (!entry->right) {
			child = entry->left;
		} else {
			rblnk_node* node = entry->right;
			/* 找到最靠近entry节点右边的节点来交换，
			 * 方法是先找右节点，再循环向左找到最左
			 * 端的node节点，将该节点替换到entry节
			 * 点的位置，此时树性质不受影响，问题转
			 * 变成了删除找到的node节点的问题
			 */
			while (node->left)
				node = node->left;
			/* 将node节点设为entry父节点的孩子 */
			rblnk_replace_child(root, parent, entry, node);
			/* 记录node节点原来的节点信息，用于树的调整 */
			child = node->right;
			parent = rblnk_parent(node);
			color = rblnk_color(node);
			if (parent == entry) {
				/* 说明找到的node节点是entry的右子 */
				parent = node;
			} else {
				/* 替换entry节点到node节点，相当
				 * 于删除了原来node位置的节点
				 */
				rblnk_set_parent_if(child, parent);
				parent->left = child;
				rblnk_link_right_unsafe(node, entry->right);
			}

			node->parent_color = entry->parent_color;
			rblnk_link_left_unsafe(node, entry->left);

			/* 调整树 */
			break;
		}
		/* 接上面的if和else if，删除entry节点 */
		color = rblnk_color(entry);
		rblnk_set_parent_if(child, parent);
		rblnk_replace_child(root, parent, entry, child);
	} while (0);

	/* color是待删除节点的颜色，如果是红色，删除对树没有影响，
	 * 如果是黑色，需要对树进行调整
	 */
	if (color)
		__rblnk_erase_color(child, parent, root);
}

NV_IMPL void rblnk_erases(rblnk_root* root, rblnk_iter begin, 
							rblnk_iter end, rblnk_pr1 erase_handler)
{
	if (erase_handler) {
		while (begin != end) {
			rblnk_iter cur = begin;
			rblnk_inc(begin);
			rblnk_erase(root, cur);
			erase_handler(cur);
		}

	} else {
		while (begin != end)
			rblnk_erase(root, rblnk_inc_later(begin));
	}
}

NV_IMPL rblnk_iter rblnk_advance(rblnk_iter cur, int dist)
{
	if (dist > 0) {
		while (cur && dist--)
			rblnk_inc(cur);
	} else {
		while (cur && dist++)
			rblnk_dec(cur);
	}
	return cur;
}

NV_IMPL int rblnk_exist(rblnk_root* root, rblnk_iter itr)
{
	rblnk_iter i = rblnk_front(root);
	
	while (i) {
		if (i == itr)
			return 1;
		rblnk_inc(i);
	}
	
	return 0;
}

NV_IMPL rblnk_iter rblnk_front(rblnk_root* root)
{
	rblnk_iter i = root->node;

	if (!i)
		return NULL;

	/* 一直往左边走，找到最左边的节点 */
	while (i->left)
		i = i->left;

	return i;
}

NV_IMPL rblnk_iter rblnk_back(rblnk_root* root)
{
	rblnk_iter i = root->node;
	
	if (!i)
		return NULL;
	
	/* 一直往右边走，找到最右边的节点 */
	while (i->right)
		i = i->right;
	
	return i;
}

NV_IMPL rblnk_iter rblnk_next(rblnk_iter cur)
{
	rblnk_iter parent;
	
	/* 如果有右孩子，往右边走 */
	if (cur->right) {
		cur = cur->right;
		/* 找右孩子下最靠左的节点 */
		while (cur->left)
			cur = cur->left;
		return cur;
	}
	/* 如果没有右孩子，向父节点走 */
	while ((parent = rblnk_parent(cur)) 
			&& cur == parent->right)
		cur = parent;
	
	return parent;
}

NV_IMPL rblnk_iter rblnk_prev(rblnk_iter cur)
{
	rblnk_iter parent;

	/* 如果有左孩子，往左边走 */
	if (cur->left) {
		cur = cur->left;
		/* 找左孩子下最靠右的节点 */
		while (cur->right)
			cur = cur->right;
		return cur;
	}
	/* 如果没有左孩子，向父节点走 */
	while ((parent = rblnk_parent(cur)) 
			&& cur == parent->left)
		cur = parent;

	return parent;
}

NV_IMPL size_t rblnk_depth(rblnk_root* root)
{
	size_t cur_depth;
	size_t max_depth = 0;
	rblnk_iter parent;
	rblnk_iter cur = root->node;
	
	if (cur) {
		cur_depth = 1;
		while (cur->left) {
			cur = cur->left;
			++cur_depth;
		}
		max_depth = cur_depth;
		do {/* 如果有右孩子，往右边走 */
			if (cur->right) {
				cur = cur->right;
				++cur_depth;
				/* 找右孩子下最靠左的节点 */
				while (cur->left) {
					cur = cur->left;
					++cur_depth;
				}
				if (cur_depth > max_depth)
					max_depth = cur_depth;
			} else {
				/* 如果没有右孩子，向父节点走 */
				while ((parent = rblnk_parent(cur)) 
						&& cur == parent->right) {
					cur = parent;
					--cur_depth;
				}
				cur = parent;
				--cur_depth;
			}
		} while (cur);
	}
	
	return max_depth;
}

NV_IMPL size_t rblnk_leafs(rblnk_root* root)
{
	size_t leafs = 0;
	rblnk_iter parent;
	rblnk_iter cur = rblnk_begin(root);
	
	while (cur) {
		/* 如果有右孩子，往右边走 */
		if (cur->right) {
			cur = cur->right;
			/* 找右孩子下最靠左的节点 */
			while (cur->left)
				cur = cur->left;
		} else {
			if (!cur->left)
				++leafs;
			/* 如果没有右孩子，向父节点走 */
			while ((parent = rblnk_parent(cur)) 
					&& cur == parent->right)
				cur = parent;
			cur = parent;
		}
	}
	
	return leafs;
}

NV_IMPL void rblnk_get_info(rblnk_root* root, rblnk_info* info)
{
	size_t cnt = 0;
	size_t leafs = 0;
	size_t cur_depth;
	size_t max_depth = 0;
	rblnk_iter parent;
	rblnk_iter cur = root->node;
	
	if (cur) {
		cur_depth = 1;
		while (cur->left) {
			cur = cur->left;
			++cur_depth;
		}
		max_depth = cur_depth;
		do {
			++cnt;
			/* 如果有右孩子，往右边走 */
			if (cur->right) {
				cur = cur->right;
				++cur_depth;
				/* 找右孩子下最靠左的节点 */
				while (cur->left) {
					cur = cur->left;
					++cur_depth;
				}
				if (cur_depth > max_depth)
					max_depth = cur_depth;
			} else {
				if (!cur->left)
					++leafs;
				/* 如果没有右孩子，向父节点走 */
				while ((parent = rblnk_parent(cur)) 
						&& cur == parent->right) {
					cur = parent;
					--cur_depth;
				}
				cur = parent;
				--cur_depth;
			}
		} while (cur);
	}
	
	info->count = cnt;
	info->depth = max_depth;
	info->leafs = leafs;
}

NV_IMPL size_t rblnk_count(rblnk_root* root)
{
	size_t cnt = 0;
	rblnk_iter i = rblnk_begin(root);

	while (i) {
		rblnk_inc(i);
		++cnt;
	}

	return cnt;
}

NV_IMPL size_t rblnk_distance(rblnk_iter first, rblnk_iter last)
{
	size_t dist = 1;
	
	while (first != last) {
		rblnk_inc(first);
		++dist;
	}
	
	return dist;
}

NV_IMPL size_t rblnk_index_of(rblnk_root* root, rblnk_iter itr)
{
	size_t cnt = 0;
	rblnk_iter i = rblnk_begin(root);
	
	while (i && i != itr) {
		rblnk_inc(i);
		++cnt;
	}
	
	return cnt;
}

NV_IMPL size_t rblnk_reverse_index_of(rblnk_root* root, rblnk_iter itr)
{
	size_t cnt = 0;
	rblnk_iter i = rblnk_rbegin(root);
	
	while (i && i != itr) {
		rblnk_dec(i);
		++cnt;
	}
	
	return cnt;
}

NV_IMPL void __do_rblnk_recurse_foreach(rblnk_iter cur, 
								void (*fn)(void*), size_t offset)
{
	if (cur) {
		__do_rblnk_recurse_foreach(cur->left, fn, offset);
		fn(((char*)cur) - offset);
		__do_rblnk_recurse_foreach(cur->right, fn, offset);
	}
}

NV_IMPL void __do_rblnk_recurse_reverse_foreach(rblnk_iter cur, 
								void (*fn)(void*), size_t offset)
{
	if (cur) {
		__do_rblnk_recurse_reverse_foreach(cur->right, fn, offset);
		fn(((char*)cur) - offset);
		__do_rblnk_recurse_reverse_foreach(cur->left, fn, offset);
	}
}

NV_IMPL void __rblnk_serialize(rblnk_root* root, void** buf, size_t offset)
{
	rblnk_iter i = rblnk_begin(root);

	while (i) {
		void* p = ((char*)i) - offset;
		*buf++ = p;
		rblnk_inc(i);
	}
}

static inline size_t RBLNK_F(size_t i, size_t resv)
{
	if (i + 1 >= resv)
		return i + resv;
	return (i << 1) + 1;
}

static inline size_t RBLNK_P(size_t i, size_t d, size_t b)
{
	return ((i >> d) << d) + b;
}

NV_IMPL void __rblnk_deserialize(rblnk_root* root, char** buf, 
									size_t cnt, size_t offset)
{
	rblnk_iter pi, pj, pp;
	size_t i, r, h, c, t, s, d, lv;
	if (!cnt) {
		root->node = NULL;
		return;
	} else if (cnt == 1) {
		pp = (rblnk_iter)((*buf) + offset);
		root->node = pp;
		pp->parent_color = RBLNK_BLACK;
		return;
	}
	h = 0; c = 1; lv = 0;
	s = 1; d = 2; t = cnt;
	while (t >>= 1) {
		++lv;
		c <<= 1;
	}
	r = cnt - (--c);
	while (++h < lv) {
		i = s - 1;
		s <<= 1;
		while (i < c) {
			size_t p = RBLNK_P(i, h + 1, s - 1);
			pp = (rblnk_iter)
				(buf[RBLNK_F(p, r)] + offset);
			pi = (rblnk_iter)
				(buf[RBLNK_F(i, r)] + offset);
			pp->left = pi;
			i += d;
			pj = (rblnk_iter)
				(buf[RBLNK_F(i, r)] + offset);
			pp->right = pj;
			if (h & 1) {
				rblnk_set_parent_color(pi, pp, RBLNK_BLACK);
				rblnk_set_parent_color(pj, pp, RBLNK_BLACK);
			} else {
				rblnk_set_parent_color(pi, pp, RBLNK_RED);
				rblnk_set_parent_color(pj, pp, RBLNK_RED);
			}
			if (h < 2) {
				pi->left = NULL;
				pi->right = NULL;
				pj->left = NULL;
				pj->right = NULL;
			}
			i += d;
		}
		d <<= 1;
	}
	root->node = pp;
	pp->parent_color = RBLNK_BLACK;
	for (i = 0; i < r; ++i) {
		pp = (rblnk_iter)
			(buf[((i >> 1) << 2) + 1] + offset);
		pi = (rblnk_iter)
			(buf[i << 1] + offset);
		pi->left = NULL;
		pi->right = NULL;
		if (i & 1)
			pp->right = pi;
		else
			pp->left = pi;
		rblnk_set_parent_color(pi, pp, RBLNK_RED);
	}
}

#ifdef __cplusplus
}
#endif	/* #ifdef __cplusplus */
