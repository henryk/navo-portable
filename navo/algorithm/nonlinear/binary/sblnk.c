/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/nonlinear/binary/sblnk.c
 * 
 * @brief Implements for size-balanced linked tree.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#include "sblnk.h"

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

static inline sblnk_link_left(sblnk_iter parent, sblnk_iter child)
{
	parent->left = child;
	sblnk_set_parent_if(child, parent);
}

static inline sblnk_link_right(sblnk_iter parent, sblnk_iter child)
{
	parent->right = child;
	sblnk_set_parent_if(child, parent);
}

static inline sblnk_link_left_unsafe(sblnk_iter parent, sblnk_iter child)
{
	parent->left = child;
	child->parent = parent;
}

static inline sblnk_link_right_unsafe(sblnk_iter parent, sblnk_iter child)
{
	parent->right = child;
	child->parent = parent;
}

static inline sblnk_link_left_shift(sblnk_iter parent, sblnk_iter child)
{
	parent->left = child;
	if (child) {
		child->parent = parent;
		parent->size += (child->size + 1);
	}
}

static inline sblnk_link_right_shift(sblnk_iter parent, sblnk_iter child)
{
	parent->right = child;
	if (child) {
		child->parent = parent;
		parent->size += (child->size + 1);
	}
}

static inline void sblnk_replace_child(sblnk_root* root, sblnk_iter me, 
						sblnk_iter old_child, sblnk_iter new_child)
{
	if (me) {
		if (old_child == me->left)
			me->left = new_child;
		else
			me->right = new_child;
	} else
		root->node = new_child;
}

/* 对top节点进行左旋转 */
static void sblnk_rotate_left(sblnk_node* top, sblnk_root* root)
{
	sblnk_node* parent = top->parent;
	sblnk_node* node = top->right;
	size_t tsz = top->size;
	top->size -= (node->size + 1);
	sblnk_link_right_shift(top, node->left);
	node->size = tsz;
	sblnk_link_left_unsafe(node, top);
	sblnk_replace_child(root, parent, top, node);
	node->parent = parent;
}

/* 对top节点进行右旋转 */
static void sblnk_rotate_right(sblnk_node* top, sblnk_root* root)
{
	sblnk_node* parent = top->parent;
	sblnk_node* node = top->left;
	size_t tsz = top->size;
	top->size -= (node->size + 1);
	sblnk_link_left_shift(top, node->right);
	node->size = tsz;
	sblnk_link_right_unsafe(node, top);
	sblnk_replace_child(root, parent, top, node);
	node->parent = parent;
}

NV_IMPL void sblnk_maintain(sblnk_root* root, sblnk_node* node, 
								sblnk_node* parent)
{
	sblnk_iter sibling;

	while (parent) {
		++parent->size;
		if (node == parent->left) {
			if (sibling = parent->right) {
				/* 如果存在兄弟节点，而兄弟节点有子节点且子节点的大小
				 * 大于node节点的大小，则旋转树
				 */
				sblnk_iter left = sibling->left;
				sblnk_iter right = sibling->right;
				if (right && right->size > node->size) {
					sblnk_rotate_left(parent, root);
					parent = sibling;
				} else if (left && left->size > node->size) {
					sblnk_rotate_right(sibling, root);
					sblnk_rotate_left(parent, root);
					parent = left;
				} else {
					/* 如果兄弟节点没有子节点，或者子节点不大于node节点，
					 * 反过来判断node节点的子节点大小是否大于兄弟节点的
					 * 大小，如果大于，则旋转树
					 */
					left = node->left;
					right = node->right;
					if (left && left->size > sibling->size) {
						sblnk_rotate_right(parent, root);
						parent = node;
					} else if (right && right->size > sibling->size) {
						sblnk_rotate_left(node, root);
						sblnk_rotate_right(parent, root);
						parent = right;
					}
				}
			} else if (node->size) {
				/* 不存在兄弟节点，那么如果node节点存在子节点，则旋转树 */
				sblnk_rotate_right(parent, root);
				parent = node;
			}
			node = parent;
			parent = node->parent;
		} else {
			if (sibling = parent->left) {
				/* 如果存在兄弟节点，而兄弟节点有子节点且子节点的大小
				 * 大于node节点的大小，则旋转树
				 */
				sblnk_iter left = sibling->left;
				sblnk_iter right = sibling->right;
				if (left && left->size > node->size) {
					sblnk_rotate_right(parent, root);
					parent = sibling;
				} else if (right && right->size > node->size) {
					sblnk_rotate_left(sibling, root);
					sblnk_rotate_right(parent, root);
					parent = right;
				} else {
					/* 如果兄弟节点没有子节点，或者子节点不大于node节点，
					 * 反过来判断node节点的子节点大小是否大于兄弟节点的
					 * 大小，如果大于，则旋转树
					 */
					left = node->left;
					right = node->right;
					if (right && right->size > sibling->size) {
						sblnk_rotate_left(parent, root);
						parent = node;
					} else if (left && left->size > sibling->size) {
						sblnk_rotate_right(node, root);
						sblnk_rotate_left(parent, root);
						parent = left;
					}
				}
			} else if (node->size) {
				/* 不存在兄弟节点，那么如果node节点存在子节点，则旋转树 */
				sblnk_rotate_left(parent, root);
				parent = node;
			}
			node = parent;
			parent = node->parent;
		}
	}
}

static void __sblnk_maintain_on_erase(sblnk_node* node, 
					sblnk_node* parent, sblnk_root* root, int which)
{
	if (parent) {
		if (which)
			goto __SBLNK_MAINTAIN_ON_ERASE_RT;
		else
			goto __SBLNK_MAINTAIN_ON_ERASE_LT;

		do {
			sblnk_iter sibling;

			--parent->size;
			if (node == parent->left) {
__SBLNK_MAINTAIN_ON_ERASE_LT:
				sibling = parent->right;
				if (node) {
					/* 如果node不为空 */
					if (sibling) {
						/* 如果存在兄弟节点，而兄弟节点有子节点且子节点的大小
						 * 大于node节点的大小，则旋转树
						 */
						sblnk_iter left = sibling->left;
						sblnk_iter right = sibling->right;
						if (right && right->size > node->size) {
							sblnk_rotate_left(parent, root);
							parent = sibling;
						} else if (left && left->size > node->size) {
							sblnk_rotate_right(sibling, root);
							sblnk_rotate_left(parent, root);
							parent = left;
						} else {
							/* 如果兄弟节点没有子节点，或者子节点不大于node节点，
							 * 反过来判断node节点的子节点大小是否大于兄弟节点的
							 * 大小，如果大于，则旋转树
							 */
							left = node->left;
							right = node->right;
							if (left && left->size > sibling->size) {
								sblnk_rotate_right(parent, root);
								parent = node;
							} else if (right && right->size > sibling->size) {
								sblnk_rotate_left(node, root);
								sblnk_rotate_right(parent, root);
								parent = right;
							}
						}
					} else if (node->size) {
						/* 不存在兄弟节点，那么如果node节点存在子节点，则旋转树 */
						sblnk_rotate_right(parent, root);
						parent = node;
					}
				} else if (sibling) {
					/* 如果node为空且存在兄弟节点 */
					sblnk_iter left = sibling->left;
					sblnk_iter right = sibling->right;
					if (right) {
						sblnk_rotate_left(parent, root);
						parent = sibling;
					} else if (left) {
						sblnk_rotate_right(sibling, root);
						sblnk_rotate_left(parent, root);
						parent = left;
					}
				}
				node = parent;
				parent = node->parent;
			} else {
__SBLNK_MAINTAIN_ON_ERASE_RT:
				sibling = parent->left;
				if (node) {
					/* 如果node不为空 */
					if (sibling) {
						/* 如果存在兄弟节点，而兄弟节点有子节点且子节点的大小
						 * 大于node节点的大小，则旋转树
						 */
						sblnk_iter left = sibling->left;
						sblnk_iter right = sibling->right;
						if (left && left->size > node->size) {
							sblnk_rotate_right(parent, root);
							parent = sibling;
						} else if (right && right->size > node->size) {
							sblnk_rotate_left(sibling, root);
							sblnk_rotate_right(parent, root);
							parent = right;
						} else {
							/* 如果兄弟节点没有子节点，或者子节点不大于node节点，
							 * 反过来判断node节点的子节点大小是否大于兄弟节点的
							 * 大小，如果大于，则旋转树
							 */
							left = node->left;
							right = node->right;
							if (right && right->size > sibling->size) {
								sblnk_rotate_left(parent, root);
								parent = node;
							} else if (left && left->size > sibling->size) {
								sblnk_rotate_right(node, root);
								sblnk_rotate_left(parent, root);
								parent = left;
							}
						}
					} else if (node->size) {
						/* 不存在兄弟节点，那么如果node节点存在子节点，则旋转树 */
						sblnk_rotate_left(parent, root);
						parent = node;
					}
				} else if (sibling) {
					/* 如果node为空且存在兄弟节点 */
					sblnk_iter left = sibling->left;
					sblnk_iter right = sibling->right;
					if (left) {
						sblnk_rotate_right(parent, root);
						parent = sibling;
					} else if (right) {
						sblnk_rotate_left(sibling, root);
						sblnk_rotate_right(parent, root);
						parent = right;
					}
				}
				node = parent;
				parent = node->parent;
			}
		} while (parent);
	}
}

NV_IMPL void sblnk_erase(sblnk_root* root, sblnk_node* entry)
{
	int which = 1;
	sblnk_node* child;
	sblnk_node* parent = entry->parent;

	do {/* 如果entry节点最多只有一个孩子，则直接删除entry节点
		 * 并调整树，如果entry同时有两个孩子，需要将其转换成
		 * 只有一子或无子的情况
		 */
		if (!entry->left) {
			child = entry->right;
		} else if (!entry->right) {
			child = entry->left;
			which = 0;
		} else {
			sblnk_node* node = entry->right;
			/* 找到最靠近entry节点右边的节点来交换，
			 * 方法是先找右节点，再循环向左找到最左
			 * 端的node节点，将该节点替换到entry节
			 * 点的位置，此时树性质不受影响，问题转
			 * 变成了删除找到的node节点的问题
			 */
			while (node->left)
				node = node->left;
			/* 将node节点设为entry父节点的孩子 */
			sblnk_replace_child(root, parent, entry, node);
			/* 记录node节点原来的节点信息，用于树的调整 */
			child = node->right;
			parent = node->parent;
			if (parent == entry) {
				/* 说明找到的node节点是entry的右子 */
				parent = node;
			} else {
				/* 替换entry节点到node节点，相当
				 * 于删除了原来node位置的节点
				 */
				sblnk_set_parent_if(child, parent);
				parent->left = child;
				sblnk_link_right_unsafe(node, entry->right);
			}

			node->parent = entry->parent;
			node->size = entry->size;
			sblnk_link_left_unsafe(node, entry->left);

			/* 平衡树 */
			break;
		}
		/* 接上面的if和else if，删除entry节点 */
		sblnk_set_parent_if(child, parent);
		sblnk_replace_child(root, parent, entry, child);
	} while (0);

	/* 平衡树 */
	__sblnk_maintain_on_erase(child, parent, root, which);
}

NV_IMPL void sblnk_erases(sblnk_root* root, sblnk_iter begin, 
							sblnk_iter end, sblnk_pr1 erase_handler)
{
	if (erase_handler) {
		while (begin != end) {
			sblnk_iter cur = begin;
			sblnk_inc(begin);
			sblnk_erase(root, cur);
			erase_handler(cur);
		}

	} else {
		while (begin != end)
			sblnk_erase(root, sblnk_inc_later(begin));
	}
}

NV_IMPL sblnk_iter sblnk_advance(sblnk_iter cur, int dist)
{
	if (dist > 0) {
		while (cur && dist--)
			sblnk_inc(cur);
	} else {
		while (cur && dist++)
			sblnk_dec(cur);
	}
	return cur;
}

NV_IMPL int sblnk_exist(sblnk_root* root, sblnk_iter itr)
{
	sblnk_iter i = sblnk_front(root);
	
	while (i) {
		if (i == itr)
			return 1;
		sblnk_inc(i);
	}
	
	return 0;
}

NV_IMPL sblnk_iter sblnk_front(sblnk_root* root)
{
	sblnk_iter i = root->node;

	if (!i)
		return NULL;

	/* 一直往左边走，找到最左边的节点 */
	while (i->left)
		i = i->left;

	return i;
}

NV_IMPL sblnk_iter sblnk_back(sblnk_root* root)
{
	sblnk_iter i = root->node;
	
	if (!i)
		return NULL;
	
	/* 一直往右边走，找到最右边的节点 */
	while (i->right)
		i = i->right;
	
	return i;
}

NV_IMPL sblnk_iter sblnk_next(sblnk_iter cur)
{
	sblnk_iter parent;
	
	/* 如果有右孩子，往右边走 */
	if (cur->right) {
		cur = cur->right;
		/* 找右孩子下最靠左的节点 */
		while (cur->left)
			cur = cur->left;
		return cur;
	}
	/* 如果没有右孩子，向父节点走 */
	while ((parent = sblnk_parent(cur)) 
			&& cur == parent->right)
		cur = parent;
	
	return parent;
}

NV_IMPL sblnk_iter sblnk_prev(sblnk_iter cur)
{
	sblnk_iter parent;

	/* 如果有左孩子，往左边走 */
	if (cur->left) {
		cur = cur->left;
		/* 找左孩子下最靠右的节点 */
		while (cur->right)
			cur = cur->right;
		return cur;
	}
	/* 如果没有左孩子，向父节点走 */
	while ((parent = sblnk_parent(cur)) 
			&& cur == parent->left)
		cur = parent;

	return parent;
}

NV_IMPL size_t sblnk_depth(sblnk_root* root)
{
	size_t cur_depth;
	size_t max_depth = 0;
	sblnk_iter parent;
	sblnk_iter cur = root->node;
	
	if (cur) {
		cur_depth = 1;
		while (cur->left) {
			cur = cur->left;
			++cur_depth;
		}
		max_depth = cur_depth;
		do {/* 如果有右孩子，往右边走 */
			if (cur->right) {
				cur = cur->right;
				++cur_depth;
				/* 找右孩子下最靠左的节点 */
				while (cur->left) {
					cur = cur->left;
					++cur_depth;
				}
				if (cur_depth > max_depth)
					max_depth = cur_depth;
			} else {
				/* 如果没有右孩子，向父节点走 */
				while ((parent = sblnk_parent(cur)) 
						&& cur == parent->right) {
					cur = parent;
					--cur_depth;
				}
				cur = parent;
				--cur_depth;
			}
		} while (cur);
	}
	
	return max_depth;
}

NV_IMPL size_t sblnk_leafs(sblnk_root* root)
{
	size_t leafs = 0;
	sblnk_iter parent;
	sblnk_iter cur = sblnk_begin(root);
	
	while (cur) {
		/* 如果有右孩子，往右边走 */
		if (cur->right) {
			cur = cur->right;
			/* 找右孩子下最靠左的节点 */
			while (cur->left)
				cur = cur->left;
		} else {
			if (!cur->left)
				++leafs;
			/* 如果没有右孩子，向父节点走 */
			while ((parent = sblnk_parent(cur)) 
					&& cur == parent->right)
				cur = parent;
			cur = parent;
		}
	}
	
	return leafs;
}

NV_IMPL void sblnk_get_info(sblnk_root* root, sblnk_info* info)
{
	size_t cnt = 0;
	size_t leafs = 0;
	size_t cur_depth;
	size_t max_depth = 0;
	sblnk_iter parent;
	sblnk_iter cur = root->node;
	
	if (cur) {
		cur_depth = 1;
		while (cur->left) {
			cur = cur->left;
			++cur_depth;
		}
		max_depth = cur_depth;
		do {
			++cnt;
			/* 如果有右孩子，往右边走 */
			if (cur->right) {
				cur = cur->right;
				++cur_depth;
				/* 找右孩子下最靠左的节点 */
				while (cur->left) {
					cur = cur->left;
					++cur_depth;
				}
				if (cur_depth > max_depth)
					max_depth = cur_depth;
			} else {
				if (!cur->left)
					++leafs;
				/* 如果没有右孩子，向父节点走 */
				while ((parent = sblnk_parent(cur)) 
						&& cur == parent->right) {
					cur = parent;
					--cur_depth;
				}
				cur = parent;
				--cur_depth;
			}
		} while (cur);
	}
	
	info->count = cnt;
	info->depth = max_depth;
	info->leafs = leafs;
}

NV_IMPL size_t sblnk_distance(sblnk_iter first, sblnk_iter last)
{
	size_t dist = 1;
	
	while (first != last) {
		sblnk_inc(first);
		++dist;
	}
	
	return dist;
}

NV_IMPL size_t sblnk_index_of(sblnk_root* root, sblnk_iter itr)
{
	size_t cnt = 0;
	sblnk_iter i = sblnk_begin(root);
	
	while (i && i != itr) {
		sblnk_inc(i);
		++cnt;
	}
	
	return cnt;
}

NV_IMPL size_t sblnk_reverse_index_of(sblnk_root* root, sblnk_iter itr)
{
	size_t cnt = 0;
	sblnk_iter i = sblnk_rbegin(root);
	
	while (i && i != itr) {
		sblnk_dec(i);
		++cnt;
	}
	
	return cnt;
}

NV_IMPL void __do_sblnk_recurse_foreach(sblnk_iter cur, 
						void (*fn)(void*), size_t offset)
{
	if (cur) {
		__do_sblnk_recurse_foreach(cur->left, fn, offset);
		fn(((char*)cur) - offset);
		__do_sblnk_recurse_foreach(cur->right, fn, offset);
	}
}

NV_IMPL void __do_sblnk_recurse_reverse_foreach(sblnk_iter cur, 
						void (*fn)(void*), size_t offset)
{
	if (cur) {
		__do_sblnk_recurse_reverse_foreach(cur->right, fn, offset);
		fn(((char*)cur) - offset);
		__do_sblnk_recurse_reverse_foreach(cur->left, fn, offset);
	}
}

NV_IMPL void __sblnk_serialize(sblnk_root* root, void** buf, size_t offset)
{
	sblnk_iter i = sblnk_begin(root);

	while (i) {
		void* p = ((char*)i) - offset;
		*buf++ = p;
		sblnk_inc(i);
	}
}

static inline size_t SBLNK_F(size_t i, size_t resv)
{
	if (i + 1 >= resv)
		return i + resv;
	return (i << 1) + 1;
}

static inline size_t SBLNK_P(size_t i, size_t d, size_t b)
{
	return ((i >> d) << d) + b;
}

NV_IMPL void __sblnk_deserialize(sblnk_root* root, char** buf, 
						 			size_t cnt, size_t offset)
{
	sblnk_iter pi, pj, pp;
	size_t i, r, h, c, t, s, d, lv;
	if (!cnt) {
		root->node = NULL;
		return;
	} else if (cnt == 1) {
		pp = (sblnk_iter)((*buf) + offset);
		root->node = pp;
		pp->parent = NULL;
		pp->size = 0;
		return;
	}
	h = 0; c = 1; lv = 0;
	s = 1; d = 2; t = cnt;
	while (t >>= 1) {
		++lv;
		c <<= 1;
	}
	r = cnt - (--c);
	while (++h < lv) {
		i = s - 1;
		s <<= 1;
		while (i < c) {
			size_t p = SBLNK_P(i, h + 1, s - 1);
			pp = (sblnk_iter)
				(buf[SBLNK_F(p, r)] + offset);
			pi = (sblnk_iter)
				(buf[SBLNK_F(i, r)] + offset);
			pp->left = pi;
			i += d;
			pj = (sblnk_iter)
				(buf[SBLNK_F(i, r)] + offset);
			pp->right = pj;
			pi->size = s - 2;
			pj->size = s - 2;
			pi->parent = pp;
			pj->parent = pp;
			if (h < 2) {
				pi->left = NULL;
				pi->right = NULL;
				pj->left = NULL;
				pj->right = NULL;
			}
			i += d;
		}
		d <<= 1;
	}
	root->node = pp;
	pp->parent = NULL;
	pp->size = (s - 1) << 1;
	for (i = 0; i < r; ++i) {
		pp = (sblnk_iter)
			(buf[((i >> 1) << 2) + 1] + offset);
		pi = (sblnk_iter)
			(buf[i << 1] + offset);
		pi->left = NULL;
		pi->right = NULL;
		if (i & 1)
			pp->right = pi;
		else
			pp->left = pi;
		pi->parent = pp;
		pi->size = 0;
		do {/* 更新大小域 */
			++pp->size;
		} while (pp = pp->parent);
	}
}

#ifdef __cplusplus
}
#endif	/* #ifdef __cplusplus */
