/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/nonlinear/binary/avllnk.h
 * 
 * @brief AVL linked tree.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_AVLLNK__
#define __NV_AVLLNK__

#include "../../../port/cdef.h"
#include "../../../port/integer.h"

#ifdef __cplusplus
extern "C" {
#endif	/* #ifdef __cplusplus */

/**
 * @defgroup AVL树
 * @ingroup 二叉树
 * @{
 */

#if defined(_MSC_VER) || defined(__CYGWIN__)
#pragma pack(push, 4)
#endif /* #if defined(_MSC_VER) || defined(__CYGWIN__) */

/**
 * @struct __avllnk_node
 * @brief AVL树节点
 * @typedef avllnk_node
 * @brief 节点
 * @typedef avllnk_iter
 * @brief 迭代器
 */
typedef struct intrin_align(4) __avllnk_node{
	/** @brief 父节点和高度信息 */
	uintptr_t parent_height;
	/** @brief 左子节点 */
	struct __avllnk_node* left;
	/** @brief 右子节点 */
	struct __avllnk_node* right;
}avllnk_node, *avllnk_iter;

#if defined(_MSC_VER) || defined(__CYGWIN__)
#pragma pack(pop)
#endif /* #if defined(_MSC_VER) || defined(__CYGWIN__) */

#define AVLLNK_LH		0	/* 左子树高 */
#define AVLLNK_RH		1	/* 右子树高 */
#define AVLLNK_BL		3	/* 子树平衡 */
#define AVLLNK_MASK		3	/* 高度信息掩码 */

/**
 * @struct avllnk_root
 * @brief 树根
 */
typedef struct {
	/** @brief 树根节点 */
	avllnk_node* node;
}avllnk_root;

/**
 * @struct avllnk_info
 * @brief 树信息表
 */
typedef struct {
	/** @brief 节点数 */
	size_t count;
	/** @brief 深度 */
	size_t depth;
	/** @brief 叶子数 */
	size_t leafs;
}avllnk_info;

/**
 * @def AVLLNK_ROOT(name)
 * @brief 定义一个名为name的树根，并对其初始化
 * @param name 树根名称
 * @def AVLLNK_NODE(name)
 * @brief 定义一个名为name的节点，并对其初始化
 * @param name 节点名称
 * @def AVLLNK_INFO(name)
 * @brief 定义一个名为name的树信息表，并对其初始化
 * @param name 名称
 */
#define AVLLNK_ROOT(name) avllnk_root name = {NULL}
#define AVLLNK_NODE(name) avllnk_node name = {0, NULL, NULL}
#define AVLLNK_INFO(name) avllnk_info name = {0, 0, 0}

/**
 * @typedef avllnk_pr1
 * @brief 单参数回调函数指针
 * @typedef avllnk_pr2
 * @brief 双参数回调函数指针
 */
typedef int (*avllnk_pr1)(avllnk_node*);
typedef int (*avllnk_pr2)(avllnk_node*, avllnk_node*);

/**
 * @fn size_t avllnk_count(avllnk_root* root);
 * @brief 获取树的节点总数
 * @param[in] root 树根
 * @return size_t 返回节点数
 * @see avllnk_get_info
 */
NV_API size_t avllnk_count(avllnk_root* root);

/**
 * @fn size_t avllnk_distance(avllnk_iter first, avllnk_iter last);
 * @brief 计算两个节点的迭代距离
 * @param[in] first 首节点
 * @param[in] last 尾节点
 * @note 以索引为例，2到4的距离是4 (4-2+1=3)
 * @return size_t 返回距离
 */
NV_API size_t avllnk_distance(avllnk_iter first, avllnk_iter last);

/**
 * @fn size_t avllnk_index_of(avllnk_root* root, avllnk_iter itr);
 * @brief 计算迭代器的正向索引
 * @param[in] root 树根
 * @param[in] itr 迭代器
 * @return size_t 返回索引
 * @see avllnk_reverse_index_of
 */
NV_API size_t avllnk_index_of(avllnk_root* root, avllnk_iter itr);

/**
 * @fn size_t avllnk_reverse_index_of(avllnk_root* root, avllnk_iter itr);
 * @brief 计算迭代器的逆向索引
 * @param[in] root 树根
 * @param[in] itr 迭代器
 * @return size_t 返回索引
 * @see avllnk_index_of
 */
NV_API size_t avllnk_reverse_index_of(avllnk_root* root, avllnk_iter itr);

/**
 * @fn size_t avllnk_depth(avllnk_root* root);
 * @brief 计算树的深度
 * @param[in] root 树根
 * @return size_t 返回深度
 * @see avllnk_get_info
 */
NV_API size_t avllnk_depth(avllnk_root* root);

/**
 * @fn size_t avllnk_leafs(avllnk_root* root);
 * @brief 计算树的叶子数
 * @param[in] root 树根
 * @return size_t 返回叶子数
 * @see avllnk_get_info
 */
NV_API size_t avllnk_leafs(avllnk_root* root);

/**
 * @fn void avllnk_get_info(avllnk_root* root, avllnk_info* info);
 * @brief 获取树的信息，包括节点数、深度、叶子数
 * @param[in] root 树根
 * @param[out] info 保存信息的结构体
 * @see avllnk_count, avllnk_depth, avllnk_leafs
 */
NV_API void avllnk_get_info(avllnk_root* root, avllnk_info* info);

/**
 * @fn void avllnk_rebalance(avllnk_root* root, avllnk_node* node, 
								avllnk_node* parent);
 * @brief 插入节点后，对树进行再平衡调整
 * @param[in,out] root 树根
 * @param[in,out] node 目标节点
 * @param[in,out] parent node的父节点
 * @note 常用于节点插入，在avllnk_link之后调用
 * @par 示例:
 * @code
 	int insert_node(avllnk_root* root, DATA* newly)
 	{
	 	DATA* pdat;
 		avllnk_iter cur;
 		avllnk_iter* p = &root->node;
 		
 		// 初始化新节点
 		avllnk_node_init(&newly->node);
 		
 		while (*p) {
 			cur = *p;
 			pdat = container_of(cur, DATA, node);
 			if (pdat->data > newly->data)
 				p = &cur->left;
 			else if (pdat->data < newly->data)
 				p = &cur->right;
 			else {
 				// 有相同值的节点，无法插入
 				return 0;
 			}
 		}
 		
 		// 链接新节点到树中
 		avllnk_link(&newly->node, cur, p);
 		
 		// 再平衡
 		avllnk_rebalance(root, &newly->node, cur);

 		return 1;
 	}
 * @endcode
 * @see avllnk_insert, avllnk_link
 */
NV_API void avllnk_rebalance(avllnk_root* root, avllnk_node* node, 
								avllnk_node* parent);

/**
 * @fn void avllnk_erase(avllnk_root* root, avllnk_node* entry);
 * @brief 移除某个节点
 * @param[in,out] root 树根
 * @param[in,out] entry 待移除节点
 */
NV_API void avllnk_erase(avllnk_root* root, avllnk_node* entry);

/**
 * @fn void avllnk_erases(avllnk_root* root, avllnk_iter begin, 
 						avllnk_iter end, avllnk_pr1 erase_handler);
 * @brief 移除区域内的节点
 * @param[in,out] root 树根
 * @param[in,out] begin 起始位置
 * @param[in,out] end 终止位置
 * @param[in] erase_handler 移除响应函数，通常用于监视和释放节点，可以传递NULL,
 							表示不为删除的节点调用响应来处理
 * @note 函数会为每一个被移除的节点调用erase_handler来处理节点
 * @see avllnk_clear
 */
NV_API void avllnk_erases(avllnk_root* root, avllnk_iter begin, 
							avllnk_iter end, avllnk_pr1 erase_handler);

/**
 * @fn static inline void avllnk_init(avllnk_root* root);
 * @brief 初始化树根
 * @param[out] root 目标树根
 * @par 示例:
 * @code
	avllnk_root r;
	// 初始化
	avllnk_init(&r);
 * @endcode
 */
static inline void avllnk_init(avllnk_root* root)
{
	root->node = NULL;
}

/**
 * @fn static inline int avllnk_empty(avllnk_root* root);
 * @brief 判断树是否为空
 * @param[in] root 树根
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	if (!avllnk_empty(&r)) {
 		......
 	}
 * @endcode
 */
static inline int avllnk_empty(avllnk_root* root)
{
	return !root->node;
}

static inline int __avllnk_singular(avllnk_node* root)
{
	return root && !root->left && !root->right;
}

/**
 * @fn static inline int avllnk_singular(avllnk_root* root);
 * @brief 判断树是否只有单个节点
 * @param[in] root 树根
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	if (avllnk_singular(&r)) {
 		......
 	}
 * @endcode
 */
static inline int avllnk_singular(avllnk_root* root)
{
	return __avllnk_singular(root->node);
}

/**
 * @fn static inline int avllnk_serialable(avllnk_root* root);
 * @brief 判断树是否可以序列化
 * @param[in] root 树根
 * @note 即判断是否至少有一个节点
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	// 判断是否可以序列化
 	if (avllnk_serialable(&r)) {
 		slseq_head seq;
 		// 分配序列缓存
 		DATA** buf = (DATA**)malloc(sizeof(DATA*) * avl_cnt);
 		// 序列化
 		avllnk_serialize(&r, buf, DATA, node);
 		SLSEQ_INIT_SERIAL(&seq, buf, avl_cnt);
 		......
 		// do something
 		......
 		// 反序列化
 		avllnk_deserialize(&r, buf, avl_cnt, DATA, node);
 		// 释放缓存
 		free(buf);
 	}
 * @endcode
 * @see avllnk_serialize, avllnk_deserialize
 */
static inline int avllnk_serialable(avllnk_root* root)
{
	return !!root->node;
}

/**
 * @fn static inline avllnk_iter avllnk_left(avllnk_node* node);
 * @brief 得到左子节点
 * @param[in] node 当前节点
 * @return avllnk_iter 返回左子节点
 * @see avllnk_right
 */
static inline avllnk_iter avllnk_left(avllnk_node* node)
{
	return node->left;
}

/**
 * @fn static inline avllnk_iter avllnk_right(avllnk_node* node);
 * @brief 得到右子节点
 * @param[in] node 当前节点
 * @return avllnk_iter 返回右子节点
 * @see avllnk_left
 */
static inline avllnk_iter avllnk_right(avllnk_node* node)
{
	return node->right;
}

/**
 * @fn static inline avllnk_iter avllnk_parent(avllnk_node* node);
 * @brief 得到父节点
 * @param[in] node 当前节点
 * @return avllnk_iter 返回父节点
 */
static inline avllnk_iter avllnk_parent(avllnk_node* node)
{
	return (avllnk_iter)(node->parent_height & (~AVLLNK_MASK));
}

/**
 * @fn static inline uintptr_t avllnk_height(avllnk_node* node);
 * @brief 得到节点的高度值
 * @param[in] node 当前节点
 * @return uintptr_t 返回高度值
 * @retval AVLLNK_LH 左子树高
 * @retval AVLLNK_RH 右子树高
 * @retval AVLLNK_BL 平衡
 */
static inline uintptr_t avllnk_height(avllnk_node* node)
{
	return (node->parent_height & AVLLNK_MASK);
}

/**
 * @fn static inline int avllnk_left_high(avllnk_node* node);
 * @brief 判断节点是否是左子树高
 * @param[in] node 当前节点
 * @return int 返回结果
 * @retval 1 左子树高
 * @retval 0 平衡或右子树高
 */
static inline int avllnk_left_high(avllnk_node* node)
{
	return avllnk_height(node) == AVLLNK_LH;
}

/**
 * @fn static inline int avllnk_right_high(avllnk_node* node);
 * @brief 判断节点是否是右子树高
 * @param[in] node 当前节点
 * @return int 返回结果
 * @retval 1 右子树高
 * @retval 0 平衡或左子树高
 */
static inline int avllnk_right_high(avllnk_node* node)
{
	return avllnk_height(node) == AVLLNK_RH;
}

/**
 * @fn static inline int avllnk_balanced(avllnk_node* node);
 * @brief 判断节点是否是平衡的
 * @param[in] node 当前节点
 * @return int 返回结果
 * @retval 1 平衡
 * @retval 0 不平衡
 */
static inline int avllnk_balanced(avllnk_node* node)
{
	return avllnk_height(node) == AVLLNK_BL;
}

/**
 * @fn static inline int avllnk_is_root(avllnk_iter itr);
 * @brief 判断当前节点是否是根节点
 * @param[in] itr 当前节点
 * @return int 返回结果
 * @retval 1 是根节点
 * @retval 0 不是根节点
 */
 */
static inline int avllnk_is_root(avllnk_iter itr)
{
	return !avllnk_parent(itr);
}

/**
 * @fn static inline int avllnk_is_leaf(avllnk_iter itr);
 * @brief 判断当前节点是否是叶子节点
 * @param[in] itr 当前节点
 * @return int 返回结果
 * @retval 1 是叶子节点
 * @retval 0 不是叶子节点
 */
static inline int avllnk_is_leaf(avllnk_iter itr)
{
	return !itr->left && !itr->right;
}

/**
 * @fn static inline void avllnk_link(avllnk_iter newly, avllnk_iter parent, 
								avllnk_iter* plink);
 * @brief 链接两个节点
 * @param[in,out] newly 新节点
 * @param[in,out] parent 父节点
 * @param[in,out] plink 父节点对应孩子指针。若newly加入parent的左子，则plink指向
 						parent的left成员，若newly加入parent的右子，则plink指向
 						parent的right成员
 * @note 链接newly和parent节点
 */
static inline void avllnk_link(avllnk_iter newly, avllnk_iter parent, 
								avllnk_iter* plink)
{
	/* 简单地将newly节点链接到parent节点下 */
	*plink = newly;
	newly->left = NULL;
	newly->right = NULL;
	/* 新节点默认是平衡的 */
	newly->parent_height = ((uintptr_t)parent) | AVLLNK_BL;
}

/**
 * @fn static inline void avllnk_set_left_high(avllnk_node* node);
 * @brief 设置当前节点高度为左子树高
 * @param[in,out] node 当前节点
 */
static inline void avllnk_set_left_high(avllnk_node* node)
{
	node->parent_height &= (~AVLLNK_MASK);
}

/**
 * @fn static inline void avllnk_set_right_high(avllnk_node* node);
 * @brief 设置当前节点高度为右子树高
 * @param[in,out] node 当前节点
 */
static inline void avllnk_set_right_high(avllnk_node* node)
{
	node->parent_height = AVLLNK_RH | 
		(node->parent_height & (~AVLLNK_MASK));
}

/**
 * @fn static inline void avllnk_set_balanced(avllnk_node* node);
 * @brief 设置当前节点为平衡节点
 * @param[in,out] node 当前节点
 */
static inline void avllnk_set_balanced(avllnk_node* node)
{
	node->parent_height |= AVLLNK_MASK;
}

/**
 * @fn static inline void avllnk_set_parent(avllnk_node* node, 
 										avllnk_node* parent);
 * @brief 设置当前节点的父节点
 * @param[in,out] node 当前节点
 * @param[in] parent 新的父节点
 */
static inline void avllnk_set_parent(avllnk_node* node, 
										avllnk_node* parent)
{
	node->parent_height = (uintptr_t)(parent) | 
		(node->parent_height & AVLLNK_MASK);
}

/**
 * @fn static inline void avllnk_set_parent_if(avllnk_node* node, 
									avllnk_node* parent);
 * @brief 仅在node不为空时，设置node的父节点
 * @param[in,out] node 当前节点
 * @param[in] parent 新的父节点
 */
static inline void avllnk_set_parent_if(avllnk_node* node, 
									avllnk_node* parent)
{
	if (node)
		avllnk_set_parent(node, parent);
}

/**
 * @fn static inline void avllnk_set_parent_height(avllnk_node* node, 
							avllnk_node* parent, uintptr_t height);
 * @brief 同时设置节点的父节点和高度值
 * @param[out] node 当前节点
 * @param[in] parent 新的父节点
 * @param[in] height 新的高度值，可选高度值如下:\n
 						AVLLNK_LH 左子树高\n
 						AVLLNK_RH 右子树高\n
 						AVLLNK_BL 子树平衡
 */
static inline void avllnk_set_parent_height(avllnk_node* node, 
							avllnk_node* parent, uintptr_t height)
{
	node->parent_height = (uintptr_t)(parent) | height;
}

/**
 * @fn static inline void avllnk_node_init(avllnk_node* node);
 * @brief 初始化节点
 * @param[out] node 指定节点
 */
static inline void avllnk_node_init(avllnk_node* node)
{
	node->parent_height = 0;
	node->left = NULL;
	node->right = NULL;
}

/**
 * @fn static inline void avllnk_iter_replace(avllnk_root* root, 
							avllnk_iter victim,  avllnk_iter newly);
 * @brief 用新节点替换旧节点
 * @param[in,out] root 树根
 * @param[in,out] victim 受害目标节点
 * @param[out] newly 新节点
 */
static inline void avllnk_iter_replace(avllnk_root* root, 
							avllnk_iter victim,  avllnk_iter newly)
{
	avllnk_iter parent = avllnk_parent(victim);
	
	/* 将victim父节点的孩子设为newly */
	if (parent) {
		if (victim == parent->left)
			parent->left = newly;
		else
			parent->right = newly;
	} else
		root->node = newly;
	
	/* 重新确定victim孩子节点的父节点为newly */
	avllnk_set_parent_if(victim->left, newly);
	avllnk_set_parent_if(victim->right, newly);
	
	/* 替换节点成员 */
	*newly = *victim;
}

/**
 * @fn static inline void avllnk_replace(avllnk_root* victim, avllnk_root* newly);
 * @brief 用新的树根替换旧树根
 * @param[in,out] victim 受害目标树根
 * @param[out] newly 新树根
 */
static inline void avllnk_replace(avllnk_root* victim, avllnk_root* newly)
{
	newly->node = victim->node;
}

/**
 * @fn static inline void avllnk_swap(avllnk_root* a, avllnk_root* b);
 * @brief 交换两个树
 * @param[in,out] a 第一个树
 * @param[in,out] b 第二个树
 */
static inline void avllnk_swap(avllnk_root* a, avllnk_root* b)
{
	avllnk_iter t = a->node; a->node = b->node; b->node = t;
}

/**
 * @fn int avllnk_exist(avllnk_root* root, avllnk_iter itr);
 * @brief 判断一个节点是否存在于树中
 * @param[in] root 树根
 * @param[in] itr 指定节点
 * @return int 返回结果
 * @retval 1 存在
 * @retval 0 不存在
 */
NV_API int avllnk_exist(avllnk_root* root, avllnk_iter itr);

/**
 * @fn static inline avllnk_iter avllnk_at(avllnk_root* root, size_t index);
 * @brief 得到正向索引对应的节点的迭代器
 * @param[in] root 树根
 * @param[in] index 指定正向索引
 * @return avllnk_iter 返回迭代器
 * @see avllnk_reverse_at
 */
static inline avllnk_iter avllnk_at(avllnk_root* root, size_t index)
{
	return avllnk_advance(avllnk_front(root), index);
}

/**
 * @fn static inline avllnk_iter avllnk_reverse_at(avllnk_root* root, 
 												size_t index);
 * @brief 得到逆向索引对应的节点的迭代器
 * @param[in] root 树根
 * @param[in] index 指定逆向索引
 * @return avllnk_iter 返回迭代器
 * @see avllnk_at
 */
static inline avllnk_iter avllnk_reverse_at(avllnk_root* root, size_t index)
{
	return avllnk_advance(avllnk_back(root), -(int)(index));
}

/**
 * @fn avllnk_iter avllnk_front(avllnk_root* root);
 * @brief 得到树的最左端节点
 * @param[in] root 树根
 * @return avllnk_iter 返回迭代器
 * @see avllnk_back
 */
NV_API avllnk_iter avllnk_front(avllnk_root* root);

/**
 * @fn avllnk_iter avllnk_back(avllnk_root* root);
 * @brief 得到树的最右端节点
 * @param[in] root 树根
 * @return avllnk_iter 返回迭代器
 * @see avllnk_front
 */
NV_API avllnk_iter avllnk_back(avllnk_root* root);

/**
 * @fn avllnk_iter avllnk_next(avllnk_iter cur);
 * @brief 得到当前节点的中序后继节点
 * @param[in] cur 当前节点
 * @return avllnk_iter 返回后继节点，若无后继，则返回NULL
 * @see avllnk_prev
 */
NV_API avllnk_iter avllnk_next(avllnk_iter cur);

/**
 * @fn avllnk_iter avllnk_prev(avllnk_iter cur);
 * @brief 得到当前节点的中序前驱节点
 * @param[in] cur 当前节点
 * @return avllnk_iter 返回前驱节点，若无前驱，则返回NULL
 * @see avllnk_next
 */
NV_API avllnk_iter avllnk_prev(avllnk_iter cur);

/**
 * @fn static inline avllnk_iter avllnk_begin(avllnk_root* root);
 * @brief 得到指向正向起始位置的迭代器
 * @param[in] root 树根
 * @return avllnk_iter 返回迭代器
 * @see avllnk_end, avllnk_end_of, avllnk_rbegin, avllnk_rend
 */
static inline avllnk_iter avllnk_begin(avllnk_root* root)
{
	return avllnk_front(root);
}

/**
 * @fn static inline avllnk_iter avllnk_end();
 * @brief 得到指向正向终止位置的迭代器
 * @return avllnk_iter 返回迭代器，永远返回NULL
 * @see avllnk_begin, avllnk_end_of, avllnk_rbegin, avllnk_rend
 */
static inline avllnk_iter avllnk_end()
{
	return NULL;
}

/**
 * @fn static inline avllnk_iter avllnk_end_of(avllnk_iter cur);
 * @brief 得到当前迭代器的正向终止位置
 * @param[in] itr 迭代器
 * @return avllnk_iter 返回迭代器
 * @see avllnk_begin, avllnk_end
 */
static inline avllnk_iter avllnk_end_of(avllnk_iter cur)
{
	return avllnk_next(cur);
}

/**
 * @fn static inline avllnk_iter avllnk_rbegin(avllnk_root* root);
 * @brief 得到指向逆向起始位置的迭代器
 * @param[in] root 树根
 * @return avllnk_iter 返回迭代器
 * @see avllnk_rend, avllnk_rend_of, avllnk_begin, avllnk_end
 */
static inline avllnk_iter avllnk_rbegin(avllnk_root* root)
{
	return avllnk_back(root);
}

/**
 * @fn static inline avllnk_iter avllnk_rend();
 * @brief 得到指向逆向终止位置的迭代器
 * @return avllnk_iter 返回迭代器，永远返回NULL
 * @see avllnk_rbegin, avllnk_rend_of, avllnk_begin, avllnk_end
 */
static inline avllnk_iter avllnk_rend()
{
	return NULL;
}

/**
 * @fn static inline avllnk_iter avllnk_rend_of(avllnk_iter cur);
 * @brief 得到当前迭代器的逆向终止位置
 * @param[in] itr 迭代器
 * @return avllnk_iter 返回迭代器
 * @see avllnk_rbegin, avllnk_rend
 */
static inline avllnk_iter avllnk_rend_of(avllnk_iter cur)
{
	return avllnk_prev(cur);
}

/**
 * @fn avllnk_iter avllnk_advance(avllnk_iter cur, int dist);
 * @brief 给迭代器增加一段距离或减小一段距离
 * @param[in] cur 迭代器
 * @param[in] dist 增加的距离。正数时，表示增加；负数时，表示减小
 * @return avllnk_iter 返回新迭代器
 */
NV_API avllnk_iter avllnk_advance(avllnk_iter cur, int dist);

/**
 * @fn static inline void avllnk_clear(avllnk_root* root, 
 								avllnk_pr1 erase_handler);
 * @brief 清空树
 * @param[in,out] root 树根
 * @param[in] erase_handler 移除响应函数，通常用于监视和释放节点，可以传递NULL,
 							表示不为删除的节点调用响应来处理
 * @note 函数会为每一个被移除的节点调用erase_handler来处理节点
 * @see avllnk_erases
 */
static inline void avllnk_clear(avllnk_root* root, avllnk_pr1 erase_handler)
{
	if (!avllnk_empty(root))
		avllnk_erases(root, avllnk_front(root), NULL, erase_handler);
}

static inline avllnk_iter __avllnk_inc_later(avllnk_iter cur, avllnk_iter* p)
{
	*p = avllnk_next(cur);
	return cur;
}

static inline avllnk_iter __avllnk_dec_later(avllnk_iter cur, avllnk_iter* p)
{
	*p = avllnk_prev(cur);
	return cur;
}

/**
 * @def avllnk_inc(itr)
 * @brief 迭代器递增
 * @def avllnk_dec(itr)
 * @brief 迭代器递减
 * @def avllnk_inc_later(itr)
 * @brief 迭代器后自增
 * @def avllnk_dec_later(itr)
 * @brief 迭代器后自减
 */
#define avllnk_inc(itr)			((itr) = avllnk_next(itr))
#define avllnk_dec(itr)			((itr) = avllnk_prev(itr))
#define avllnk_inc_later(itr)	__avllnk_inc_later(itr, &(itr))
#define avllnk_dec_later(itr)	__avllnk_dec_later(itr, &(itr))

/**
 * @def avllnk_foreach(_begin, _end, fn, type, member)
 * @brief 正向遍历_begin到_end的节点，并为每一个节点调用回调函数或宏
 * @param[in] _begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define avllnk_foreach(_begin, _end, fn, type, member) do {\
		avllnk_iter _q_e = (_end);							\
		avllnk_iter _q_f = (_begin);						\
		while (_q_f != _q_e) {								\
			type * _q_t = container_of(_q_f, type, member);	\
			avllnk_inc(_q_f);								\
			fn(_q_t);										\
		}													\
	} while (0)

/**
 * @def avllnk_reverse_foreach(_rbegin, _rend, fn, type, member)
 * @brief 逆向遍历_begin到_end的节点，并为每一个节点调用回调函数或宏
 * @param[in] _rbegin 逆向起始位置
 * @param[in] _rend 逆向终止位置
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define avllnk_reverse_foreach(_rbegin, _rend, fn, type, member) do {\
		avllnk_iter _q_e = (_rend);							\
		avllnk_iter _q_f = (_rbegin);						\
		while (_q_f != _q_e) {								\
			type * _q_t = container_of(_q_f, type, member);	\
			avllnk_dec(_q_f);								\
			fn(_q_t);										\
		}													\
	} while (0)

NV_API void __do_avllnk_recurse_foreach(
				avllnk_iter cur, void (*fn)(void*), size_t offset);

NV_API void __do_avllnk_recurse_reverse_foreach(
				avllnk_iter cur, void (*fn)(void*), size_t offset);

static inline void __avllnk_recurse_foreach(
	avllnk_root* root, void (*fn)(void*), size_t offset)
{
	if (fn)
		__do_avllnk_recurse_foreach(root->node, fn, offset);
}

static inline void __avllnk_recurse_reverse_foreach(
	avllnk_root* root, void (*fn)(void*), size_t offset)
{
	if (fn)
		__do_avllnk_recurse_reverse_foreach(root->node, fn, offset);
}

/**
 * @def AVLLNK_RECURSE_PROC(name, type, param)
 * @brief 定义递归遍历回调函数
 * @param name 函数名
 * @param type 节点拥有者类型
 * @param param 参数名
 * @see avllnk_recurse_foreach, avllnk_recurse_reverse_foreach
 */
#define AVLLNK_RECURSE_PROC(name, type, param) void name(type * param)

/**
 * @def avllnk_recurse_foreach(root, fn, type, member)
 * @brief 正向递归遍历整个树
 * @param[in] root 树根
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @par 示例:
 * @code
 	AVLLNK_RECURSE_PROC(PrintNode, DATA, node)
 	{
 		printf("%3d ", node->data);
 	}

	// 打印整个树
 	void PrintAVLTree(avllnk_root* root)
 	{
 		avllnk_recurse_foreach(root, PrintNode, DATA, node);
 		putchar('\n');
 	}
 * @endcode
 * @see avllnk_recurse_reverse_foreach, AVLLNK_RECURSE_PROC
 */
#define avllnk_recurse_foreach(root, fn, type, member) \
	__avllnk_recurse_foreach(root, (void (*)(void*))(fn), \
		offset_of(type, member))

/**
 * @def avllnk_recurse_reverse_foreach(root, fn, type, member)
 * @brief 逆向递归遍历整个树
 * @param[in] root 树根
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see avllnk_recurse_foreach, AVLLNK_RECURSE_PROC
 */
#define avllnk_recurse_reverse_foreach(root, fn, type, member) \
	__avllnk_recurse_reverse_foreach(root, (void (*)(void*))(fn), \
			offset_of(type, member))

/**
 * @def avllnk_search(root, newly, greater, res, type, member)
 * @brief 搜索树中的某个节点
 * @param[in] root 树根
 * @param[in] newly 用于搜索的临时节点，类型是type，该节点使用与搜索的值相同的值
 * @param[in] greater 比较函数或宏。
 						升序:(i->data > j->data) 
 						降序:(i->data < j->data)
 * @param[out] res 返回值。返回目标节点，若为NULL表示找不到节点
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @par 示例:
 * @code
 	avllnk_iter node_15;
 	DATA* p = (DATA*)malloc(sizeof(DATA));
 	if (!p)
 		return NULL;

 	p->data = 15;

	//查找值为15的节点
 	avllnk_search(&root, p, DATA_Greater, node_15, DATA, node);
 	if (!node_15)
 		return NULL;

 	return node_15;
 * @endcode
 */
#define avllnk_search(root, newly, greater, res, type, member) do {\
		avllnk_root* _k_r = (root);								\
		type * _k_pn = (newly);									\
		avllnk_iter _k_p = _k_r->node;							\
		(res) = NULL;											\
		while (_k_p) {											\
			type * _k_pr = container_of(_k_p, type, member);	\
			if (greater(_k_pr, _k_pn))							\
				_k_p = _k_p->left;								\
			else if (greater(_k_pn, _k_pr))						\
				_k_p = _k_p->right;								\
			else {												\
				(res) = _k_p; break;							\
			}													\
		}														\
	} while (0)

/**
 * @def avllnk_insert(root, newly, greater, res, type, member)
 * @brief 插入新节点
 * @param[in] root 树根
 * @param[in] newly 新节点
 * @param[in] greater 比较函数或宏。
 						升序:(i->data > j->data) 
 						降序:(i->data < j->data)
 * @param[out] res 返回值。返回1表示成功；若为0表示节点有冲突，无法插入
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @par 示例:
 * @code
 	int ret;
 	DATA* p = (DATA*)malloc(sizeof(DATA));
 	if (!p)
 		return 0;

 	p->data = 88;

	//插入值为88的节点
 	avllnk_insert(&root, &p->node, DATA_Greater, ret, DATA, node);
 	if (!ret)
 		return 0;

 	return 1;
 * @endcode
 */
#define avllnk_insert(root, newly, greater, res, type, member) do {\
		avllnk_root* _k_r = (root);										\
		avllnk_iter* _k_pp = &_k_r->node;								\
		avllnk_iter _k_n = (newly);										\
		avllnk_iter _k_p;												\
		type * _k_pn = container_of(_k_n, type, member);				\
		(res) = 1;														\
		avllnk_node_init(_k_n);											\
		while (*_k_pp) {												\
			type * _k_pr = container_of(_k_p = *_k_pp, type, member);	\
			if (greater(_k_pr, _k_pn))									\
				_k_pp = &_k_p->left;									\
			else if (greater(_k_pn, _k_pr))								\
				_k_pp = &_k_p->right;									\
			else {														\
				(res) = 0; break;										\
			}															\
		}																\
		if (res) {														\
			avllnk_link(_k_n, _k_p, _k_pp);								\
			avllnk_rebalance(_k_r, _k_n, _k_p);							\
		}																\
	} while (0)

NV_API void __avllnk_serialize(avllnk_root* root, void** buf, size_t offset);

NV_API void __avllnk_deserialize(avllnk_root* root, void** buf, 
								 size_t cnt, size_t offset);


/**
 * @def avllnk_serialize(root, buf, type, member)
 * @brief 将树进行序列化为一个有序序列
 * @param[in] root 树根
 * @param[out] buf 序列缓存，元素类型为type指针类型
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see avllnk_deserialize, avllnk_serialable
 */
#define avllnk_serialize(root, buf, type, member) \
	__avllnk_serialize(root, (void**)(buf), offset_of(type, member))

/**
 * @def avllnk_deserialize(root, buf, cnt, type, member)
 * @brief 根据树的序列进行反序列化为一个完整的树
 * @param[out] root 树根
 * @param[in] buf 序列缓存，元素类型为type指针类型
 * @param[in] cnt 序列长度(元素数)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see avllnk_serialize, avllnk_serialable
 */
#define avllnk_deserialize(root, buf, cnt, type, member) \
	__avllnk_deserialize(root, (void**)(buf), cnt, offset_of(type, member))

/** @} */

#ifdef __cplusplus
}
#endif	/* #ifdef __cplusplus */

#endif /* #ifndef __NV_AVLLNK__ */

