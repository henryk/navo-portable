/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/nonlinear/binary/treaplnk.c
 * 
 * @brief Implements for linked treap.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#include "treaplnk.h"

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

static inline treaplnk_link_left(treaplnk_iter parent, treaplnk_iter child)
{
	parent->left = child;
	treaplnk_set_parent_if(child, parent);
}

static inline treaplnk_link_right(treaplnk_iter parent, treaplnk_iter child)
{
	parent->right = child;
	treaplnk_set_parent_if(child, parent);
}

static inline treaplnk_link_left_unsafe(treaplnk_iter parent, 
										treaplnk_iter child)
{
	parent->left = child;
	child->parent = parent;
}

static inline treaplnk_link_right_unsafe(treaplnk_iter parent, 
										 treaplnk_iter child)
{
	parent->right = child;
	child->parent = parent;
}

static inline void treaplnk_replace_child(treaplnk_root* root, 
	treaplnk_iter me, treaplnk_iter old_child, treaplnk_iter new_child)
{
	if (me) {
		if (old_child == me->left)
			me->left = new_child;
		else
			me->right = new_child;
	} else
		root->node = new_child;
}

/* 对top节点进行左旋转 */
static inline void treaplnk_rotate_left(treaplnk_node* top, 
										treaplnk_root* root)
{
	treaplnk_node* parent = top->parent;
	treaplnk_node* node = top->right;
	treaplnk_link_right(top, node->left);
	treaplnk_link_left_unsafe(node, top);
	treaplnk_replace_child(root, parent, top, node);
	node->parent = parent;
}

/* 对top节点进行右旋转 */
static inline void treaplnk_rotate_right(treaplnk_node* top, 
										 treaplnk_root* root)
{
	treaplnk_node* parent = top->parent;
	treaplnk_node* node = top->left;
	treaplnk_link_left(top, node->right);
	treaplnk_link_right_unsafe(node, top);
	treaplnk_replace_child(root, parent, top, node);
	node->parent = parent;
}

NV_IMPL void treaplnk_fix(treaplnk_root* root, treaplnk_node* node, 
							treaplnk_node* parent)
{
	while (parent && parent->priority > node->priority) {
		if (node == parent->left) {
			treaplnk_rotate_right(parent, root);
			parent = node->parent;
		} else {
			treaplnk_rotate_left(parent, root);
			parent = node->parent;
		}
	}
}

NV_IMPL void treaplnk_erase(treaplnk_root* root, treaplnk_node* entry)
{
	do {
		treaplnk_node* child;
		treaplnk_node* parent;
		treaplnk_node* left = entry->left;
		treaplnk_node* right = entry->right;
		/* 如果entry节点最多只有一个孩子，则直接删除entry节点
		 * 并调整树，如果entry同时有两个孩子，需要将其转换成
		 * 只有一子或无子的情况
		 */
		if (!left)
			child = right;
		else if (!right)
			child = left;
		else {
			/* 此时，entry节点有两个子节点，如果左子节点的修正值
			 * 小于右子节点的修正值，则右旋entry节点，如果左子节
			 * 点的修正值大于右子节点的修正值，则左旋entry节点
			 */
			if (left->priority < right->priority)
				treaplnk_rotate_right(entry, root);
			else
				treaplnk_rotate_left(entry, root);
			continue;
		}
		/* 接上面的if和else if，删除entry节点 */
		parent = entry->parent;
		treaplnk_set_parent_if(child, parent);
		treaplnk_replace_child(root, parent, entry, child);
	} while (0);
}

NV_IMPL void treaplnk_erases(treaplnk_root* root, treaplnk_iter begin, 
								treaplnk_iter end, treaplnk_pr1 erase_handler)
{
	if (erase_handler) {
		while (begin != end) {
			treaplnk_iter cur = begin;
			treaplnk_inc(begin);
			treaplnk_erase(root, cur);
			erase_handler(cur);
		}

	} else {
		while (begin != end)
			treaplnk_erase(root, treaplnk_inc_later(begin));
	}
}

NV_IMPL treaplnk_iter treaplnk_advance(treaplnk_iter cur, int dist)
{
	if (dist > 0) {
		while (cur && dist--)
			treaplnk_inc(cur);
	} else {
		while (cur && dist++)
			treaplnk_dec(cur);
	}
	return cur;
}

NV_IMPL int treaplnk_exist(treaplnk_root* root, treaplnk_iter itr)
{
	treaplnk_iter i = treaplnk_front(root);
	
	while (i) {
		if (i == itr)
			return 1;
		treaplnk_inc(i);
	}
	
	return 0;
}

NV_IMPL treaplnk_iter treaplnk_front(treaplnk_root* root)
{
	treaplnk_iter i = root->node;

	if (!i)
		return NULL;

	/* 一直往左边走，找到最左边的节点 */
	while (i->left)
		i = i->left;

	return i;
}

NV_IMPL treaplnk_iter treaplnk_back(treaplnk_root* root)
{
	treaplnk_iter i = root->node;
	
	if (!i)
		return NULL;
	
	/* 一直往右边走，找到最右边的节点 */
	while (i->right)
		i = i->right;
	
	return i;
}

NV_IMPL treaplnk_iter treaplnk_next(treaplnk_iter cur)
{
	treaplnk_iter parent;
	
	/* 如果有右孩子，往右边走 */
	if (cur->right) {
		cur = cur->right;
		/* 找右孩子下最靠左的节点 */
		while (cur->left)
			cur = cur->left;
		return cur;
	}
	/* 如果没有右孩子，向父节点走 */
	while ((parent = treaplnk_parent(cur)) 
			&& cur == parent->right)
		cur = parent;
	
	return parent;
}

NV_IMPL treaplnk_iter treaplnk_prev(treaplnk_iter cur)
{
	treaplnk_iter parent;

	/* 如果有左孩子，往左边走 */
	if (cur->left) {
		cur = cur->left;
		/* 找左孩子下最靠右的节点 */
		while (cur->right)
			cur = cur->right;
		return cur;
	}
	/* 如果没有左孩子，向父节点走 */
	while ((parent = treaplnk_parent(cur)) 
			&& cur == parent->left)
		cur = parent;

	return parent;
}

NV_IMPL size_t treaplnk_depth(treaplnk_root* root)
{
	size_t cur_depth;
	size_t max_depth = 0;
	treaplnk_iter parent;
	treaplnk_iter cur = root->node;
	
	if (cur) {
		cur_depth = 1;
		while (cur->left) {
			cur = cur->left;
			++cur_depth;
		}
		max_depth = cur_depth;
		do {/* 如果有右孩子，往右边走 */
			if (cur->right) {
				cur = cur->right;
				++cur_depth;
				/* 找右孩子下最靠左的节点 */
				while (cur->left) {
					cur = cur->left;
					++cur_depth;
				}
				if (cur_depth > max_depth)
					max_depth = cur_depth;
			} else {
				/* 如果没有右孩子，向父节点走 */
				while ((parent = treaplnk_parent(cur)) 
						&& cur == parent->right) {
					cur = parent;
					--cur_depth;
				}
				cur = parent;
				--cur_depth;
			}
		} while (cur);
	}
	
	return max_depth;
}

NV_IMPL size_t treaplnk_leafs(treaplnk_root* root)
{
	size_t leafs = 0;
	treaplnk_iter parent;
	treaplnk_iter cur = treaplnk_begin(root);
	
	while (cur) {
		/* 如果有右孩子，往右边走 */
		if (cur->right) {
			cur = cur->right;
			/* 找右孩子下最靠左的节点 */
			while (cur->left)
				cur = cur->left;
		} else {
			if (!cur->left)
				++leafs;
			/* 如果没有右孩子，向父节点走 */
			while ((parent = treaplnk_parent(cur)) 
					&& cur == parent->right)
				cur = parent;
			cur = parent;
		}
	}
	
	return leafs;
}

NV_IMPL void treaplnk_get_info(treaplnk_root* root, treaplnk_info* info)
{
	size_t cnt = 0;
	size_t leafs = 0;
	size_t cur_depth;
	size_t max_depth = 0;
	treaplnk_iter parent;
	treaplnk_iter cur = root->node;
	
	if (cur) {
		cur_depth = 1;
		while (cur->left) {
			cur = cur->left;
			++cur_depth;
		}
		max_depth = cur_depth;
		do {
			++cnt;
			/* 如果有右孩子，往右边走 */
			if (cur->right) {
				cur = cur->right;
				++cur_depth;
				/* 找右孩子下最靠左的节点 */
				while (cur->left) {
					cur = cur->left;
					++cur_depth;
				}
				if (cur_depth > max_depth)
					max_depth = cur_depth;
			} else {
				if (!cur->left)
					++leafs;
				/* 如果没有右孩子，向父节点走 */
				while ((parent = treaplnk_parent(cur)) 
						&& cur == parent->right) {
					cur = parent;
					--cur_depth;
				}
				cur = parent;
				--cur_depth;
			}
		} while (cur);
	}
	
	info->count = cnt;
	info->depth = max_depth;
	info->leafs = leafs;
}

NV_IMPL size_t treaplnk_count(treaplnk_root* root)
{
	size_t cnt = 0;
	treaplnk_iter i = treaplnk_begin(root);

	while (i) {
		treaplnk_inc(i);
		++cnt;
	}

	return cnt;
}

NV_IMPL size_t treaplnk_distance(treaplnk_iter first, 
						 treaplnk_iter last)
{
	size_t dist = 1;
	
	while (first != last) {
		treaplnk_inc(first);
		++dist;
	}
	
	return dist;
}

NV_IMPL size_t treaplnk_index_of(treaplnk_root* root, treaplnk_iter itr)
{
	size_t cnt = 0;
	treaplnk_iter i = treaplnk_begin(root);
	
	while (i && i != itr) {
		treaplnk_inc(i);
		++cnt;
	}
	
	return cnt;
}

NV_IMPL size_t treaplnk_reverse_index_of(treaplnk_root* root, treaplnk_iter itr)
{
	size_t cnt = 0;
	treaplnk_iter i = treaplnk_rbegin(root);
	
	while (i && i != itr) {
		treaplnk_dec(i);
		++cnt;
	}
	
	return cnt;
}

NV_IMPL void __do_treaplnk_recurse_foreach(treaplnk_iter cur, 
									void (*fn)(void*), size_t offset)
{
	if (cur) {
		__do_treaplnk_recurse_foreach(cur->left, fn, offset);
		fn(((char*)cur) - offset);
		__do_treaplnk_recurse_foreach(cur->right, fn, offset);
	}
}

NV_IMPL void __do_treaplnk_recurse_reverse_foreach(
						treaplnk_iter cur, void (*fn)(void*), size_t offset)
{
	if (cur) {
		__do_treaplnk_recurse_reverse_foreach(cur->right, fn, offset);
		fn(((char*)cur) - offset);
		__do_treaplnk_recurse_reverse_foreach(cur->left, fn, offset);
	}
}

NV_IMPL void __treaplnk_serialize(treaplnk_root* root, 
									void** buf, size_t offset)
{
	treaplnk_iter i = treaplnk_begin(root);

	while (i) {
		void* p = ((char*)i) - offset;
		*buf++ = p;
		treaplnk_inc(i);
	}
}

NV_IMPL void __treaplnk_deserialize(treaplnk_root* root, char** buf, 
										size_t cnt, size_t offset)
{
	size_t i;
	treaplnk_iter p, q;

	root->node = NULL;
	p = (treaplnk_iter)(buf[0] + offset);
	q = p;
	p->left = NULL;
	p->right = NULL;
	treaplnk_link(p, NULL, &root->node);
	for (i = 1; i < cnt; ++i) {
		p = (treaplnk_iter)(buf[i] + offset);
		p->left = NULL;
		p->right = NULL;
		treaplnk_link(p, q, &q->right);
		treaplnk_fix(root, p, q);
		q = p;
	}
}

#ifdef __cplusplus
}
#endif	/* #ifdef __cplusplus */
