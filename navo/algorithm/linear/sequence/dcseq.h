/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/linear/sequence/dcseq.h
 *
 * @brief Double-ended linear sequence list.
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_DCSEQ__
#define __NV_DCSEQ__

#include "cdef.h"

#ifdef __cplusplus
#include <cstring>
extern "C" {
#else
#include <string.h>
#endif /* #ifdef __cplusplus */

/**
 * @defgroup 双端环形序表
 * @ingroup 顺序表
 * @{
 */

/**
 * @def dcseq_iter(type)
 * @param type 节点类型
 * @brief 定义type类型迭代器
 */
#define dcseq_iter(type) type *

/**
 * @def dcseq_head(type)
 * @brief type类型表头
 * @param type 节点类型
 * @note base为缓存基地址，
        first为首节点
        last为末尾节点(末尾节点是空的)，
        end是缓存末尾
 */
#define dcseq_head(type) \
    struct { type * first; type * last; type * end; type * base; }
#define dcseq dcseq_head

/**
 * @def DCSEQ_HEAD(type, name, arr)
 * @brief 定义一个名为name的表头，并对其初始化
 * @param type 节点类型
 * @param name 表头名称
 * @param arr 缓存数组名
 */
#define DCSEQ_HEAD(type, name, arr) \
    dcseq_head(type) name = {arr, arr, &arr[count_of(arr)], arr}
#define DCSEQ DCSEQ_HEAD

/**
 * @def dcseq_pr1(type)
 * @brief 单参数回调函数指针类型
 * @param type 节点类型
 * @def dcseq_pr2(type)
 * @brief 双参数回调函数指针类型
 * @param type 节点类型
 */
#define dcseq_pr1(type) int (*)(type *)
#define dcseq_pr2(type) int (*)(type *, type *)

#define DCSEQ_CHECK_PASS 		0 /* 正常 */
#define DCSEQ_CHECK_INVALID 	1 /* 无效缓存 */
#define DCSEQ_CHECK_OVERFLOW 	2 /* 溢出 */

static inline int __dcseq_check(char* first, char* last,
                                char* end, char* base)
{
	if (!base || base >= end || first >= end)
		return DCSEQ_CHECK_INVALID;
	else if (last > end || first < base)
		return DCSEQ_CHECK_OVERFLOW;
    else
        return DCSEQ_CHECK_PASS;
}

/**
 * @def dcseq_check(head)
 * @brief 检查表
 * @param[in] head 表头
 * @return int 返回结果
 * @retval DCSEQ_CHECK_PASS 0 正常
 * @retval DCSEQ_CHECK_INVALID 1 无效缓存
 * @retval DCSEQ_CHECK_OVERFLOW 2 溢出
 */
#define dcseq_check(head) \
    __dcseq_check((char*)((head)->first),   \
                  (char*)((head)->last),    \
                  (char*)((head)->end),     \
                  (char*)((head)->base))

/**
 * @def dcseq_reset(head, _base, _first, _last, _end)
 * @brief 重置表头
 * @param[out] head 目标表头
 * @param[in] _base 缓存基址
 * @param[in] _first 表首节点
 * @param[in] _last 表尾节点
 * @param[in] _end 缓存终止处
 */
#define dcseq_reset(head, _base, _first, _last, _end) \
    do {(head)->base = (_base);         \
        (head)->first = (_first);       \
        (head)->last = (_last);         \
        (head)->end = (_end);           \
    } while (0)

/**
 * @def dcseq_init(type, head, base, size, _first)
 * @brief 初始化表
 * @param[in] type 节点类型
 * @param[out] head 目标表头
 * @param[in] base 缓存基址
 * @param[in] size 缓存长度(节点容量)
 * @param[in] _first 起始位置
 * @see DCSEQ_INIT_ARRAY
 */
#define dcseq_init(type, head, base, size, _first)  \
    dcseq_reset(head, (type *)(base),               \
                (type *)(base) + (_first),          \
                (type *)(base) + (_first),          \
                (type *)(base) + (size))            \

/**
 * @def dcseq_init_serial(type, head, base, size, cnt)
 * @brief 作为序列来初始化表
 * @param[in] type 节点类型
 * @param[out] head 目标表头
 * @param[in] base 缓存基址
 * @param[in] size 缓存长度(节点容量)
 * @param[in] cnt 序列长度
 * @par 示例:
 * @code
	//将dclnk链表通过序列化来进行堆排序
	dclnk_head* alives = &bulltes.alive_list;
	if (dclnk_sortable(alives)) {
		Bullet* buf[MAX_BULLETS];
		DCSEQ_INIT_SERIAL(int, &seq_head, buf, list_cnt);
		dclnk_serialize(alives, buf, Bullet, alive_node);
		dcseq_make_heap(Bullet, &head, dcseq_begin(&head),
						dcseq_end(&seq_head),
						CompareBulletAlive);
		dcseq_sort_heap(Bullet, &head, dcseq_begin(&head),
						dcseq_end(&seq_head),
						CompareBulletAlive);
		dclnk_deserialize(alives, dcseq_base(&seq_head),
						dcseq_count(&seq_head),
						Bullet, alive_node);
	}
 * @endcode
 * @see DCSEQ_INIT_SERIAL
 */
#define dcseq_init_serial(type, head, base, size, cnt)  \
    dcseq_reset(head, (type *)(base), (type *)(base),   \
                (type *)(base) + (cnt),                 \
                (type *)(base) + (size))

/**
 * @def DCSEQ_INIT(type, head, arr, _first)
 * @brief 初始化表
 * @param[in] type 节点类型
 * @param[out] head 目标表头
 * @param[in] arr 缓存数组
 * @param[in] _first 起始位置
 * @par 示例:
 * @code
 	dcseq_head(int) h;
 	int buf[100];

 	DCSEQ_INIT(int, &h, buf, 0);
 * @endcode
 * @see dcseq_init
 */
#define DCSEQ_INIT(type, head, arr, _first) \
    dcseq_init(type, head, arr, count_of(arr), _first)

/**
 * @def DCSEQ_INIT_SERIAL(type, head, arr, cnt)
 * @brief 初始化序列
 * @param[in] type 节点类型
 * @param[out] head 目标表头
 * @param[in] arr 缓存数组
 * @param[in] cnt 序列长度
 * @par 示例:
 * @code
 	dcseq_head(DATA*) h;
 	DATA* buf[100];

	dclnk_serialize(&lst, buf, DATA, node);
 	DCSEQ_INIT_SERIAL(int, &h, buf, dclnk_count(&lst));
 * @endcode
 * @see dcseq_init_serial
 */
#define DCSEQ_INIT_SERIAL(type, head, arr, cnt) \
	dcseq_init_serial(type, head, arr, count_of(arr), cnt)

/**
 * @def dcseq_clear(head)
 * @brief 清空表
 * @param[out] head 表头
 */
#define dcseq_clear(head) \
    ((head)->last = (head)->first = (head)->base)

/**
 * @def dcseq_base(head)
 * @brief 获得表的缓存基址
 * @param[in] head 表头
 * @return char* 返回基地址
 */
#define dcseq_base(head) ((head)->base)

/**
 * @def dcseq_count(head)
 * @brief 获取表的节点总数
 * @param[in] head 表头
 * @return size_t 返回节点总数
 */
#define dcseq_count(head) \
    __dcseq_count(dcseq_size(head), (head)->last - (head)->first)

static inline size_t __dcseq_count(int size, int dist)
{
    return (dist >= 0)? dist: dist + size;
}

/**
 * @def dcseq_size(head)
 * @brief 获取表的缓存长度
 * @param[in] head 表头
 * @return size_t 返回缓存长度
 * @see dcseq_capacity
 */
#define dcseq_size(head) ((size_t)((head)->end - (head)->base))

/**
 * @def dcseq_capacity(head)
 * @brief 获取表的节点容量
 * @param[in] head 表头
 * @return size_t 返回节点容量
 * @see dcseq_size
 */
#define dcseq_capacity(head) (dcseq_size(head) - 1)

/**
 * @def dcseq_remain(head)
 * @brief 获取表的剩余容量
 * @param[in] head 表头
 * @return size_t 返回剩余容量
 * @see dcseq_full, dcseq_empty
 */
#define dcseq_remain(head) (dcseq_capacity(head) - dcseq_count(head))

/**
 * @def dcseq_empty(head)
 * @brief 判断表是否为空
 * @param[in] head 表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @see dcseq_full, dcseq_remain
 */
#define dcseq_empty(head) \
    __dcseq_empty((char*)((head)->first), (char*)((head)->last))

static inline int __dcseq_empty(char* first, char* last)
{
    return first == last;
}

/**
 * @def dcseq_full(head)
 * @brief 判断表是否已满
 * @param[in] head 表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @see dcseq_empty, dcseq_remain
 */
#define dcseq_full(head) \
    __dcseq_full((char*)((head)->base), (char*)((head)->first),    \
                 (char*)((head)->end), (char*)((head)->last + 1))

static inline int __dcseq_full(char* base, char* first,
                               char* end, char* last_next)
{
    return (last_next == first) ||
        ((first = base) && (last_next == end));
}

/**
 * @def dcseq_singular(head)
 * @brief 判断表是否只有单个节点
 * @param[in] head 表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 */
#define dcseq_singular(head) \
    __dcseq_singular((char*)((head)->base), \
                     (char*)((head)->last), \
                     (char*)((head)->end),  \
                     (char*)((head)->first + 1))

static inline int __dcseq_singular(char* base, char* last,
                                   char* end, char* first_next)
{
    return (last == first_next) ||
        ((base == last) && (first_next >= end));
}

/**
 * @def dcseq_sortable(head)
 * @brief 判断表是否可以被排序
 * @param[in] head 表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 */
#define dcseq_sortable(head) \
    __dcseq_sortable(dcseq_size(head), (head)->last - (head)->first)

static inline int __dcseq_sortable(int size, int dist)
{
    return (dist > 1) || (__dcseq_count(size, dist) > 1);
}

/**
 * @def dcseq_insertable(head, cnt)
 * @brief 判断表是否可以插入一定数量的节点
 * @param[in] head 表头
 * @param[in] cnt 插入的节点数
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 */
#define dcseq_insertable(head, cnt) \
    __dcseq_insertable(dcseq_size(head), \
        (head)->last - (head)->first, (int)(cnt))

static inline int __dcseq_insertable(int size, int dist, int cnt)
{
    return size >= (int)__dcseq_count(size, dist) + cnt + 1;
}

/**
 * @def dcseq_serialable(head)
 * @brief 判断表是否可以序列化
 * @param[in] head 表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 */
#define dcseq_serialable(head) \
    __dcseq_serialable((char*)((head)->first), (char*)((head)->last))

static inline int __dcseq_serialable(char* first, char* last)
{
    return first != last;
}

/**
 * @def dcseq_copyable(dst, src)
 * @brief 判断源表是否可以深拷贝到目标表
 * @param[in] dst 目标表头
 * @param[in] src 源表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 */
#define dcseq_copyable(dst, src) \
    __dcseq_copyable((char*)(dst), (char*)(src),    \
                     (char*)((src)->first),         \
                     (char*)((src)->last),          \
                     (char*)((src)->base),          \
                     (char*)((src)->end),           \
                     (char*)((dst)->base),          \
                     (char*)((dst)->end))

static inline int __dcseq_copyable(char* dst, char* src,
                                   char* s_first, char* s_last,
                                   char* s_base, char* s_end,
                                   char* d_base, char* d_end)
{
    size_t sz;
    if (s_first > s_last)
        sz = (s_end - s_first) + (s_last - s_base);
    else
        sz = s_last - s_first;
	return (dst != src && (s_last != s_first) &&
         (d_end >= d_base + sz));
}

/**
 * @def dcseq_at(type, head, index)
 * @brief 根据正向索引获取迭代器
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] index 指定索引
 * @return dcseq_iter(type) 返回对应的迭代器
 */
#define dcseq_at(type, head, index) \
    __dcseq_at(type, (head)->base, (head)->end, (head)->first + (index))

#define __dcseq_at(type, base, end, dest) \
    ((type *)__do_dcseq_at((char*)(base), (char*)(end), (char*)(dest)))

static inline char* __do_dcseq_at(char* base, char* end, char* dest)
{
    return (dest >= end)? base + (dest - end): dest;
}

/**
 * @def dcseq_reverse_at(type, head, index)
 * @brief 根据逆向索引获取迭代器
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] index 指定索引
 * @return dcseq_iter(type) 返回对应的迭代器
 */
#define dcseq_reverse_at(type, head, index) \
    __dcseq_reverse_at(type, (head)->base, (head)->end, \
    					(head)->last - ((index) + 1))

#define __dcseq_reverse_at(type, base, end, dest) \
    ((type *)__do_dcseq_reverse_at((char*)(base), \
    					(char*)(end), (char*)(dest)))

static inline char* __do_dcseq_reverse_at(char* base,
										char* end,
										char* dest)
{
    return (base <= dest)? dest: dest + (end - base);
}

/**
 * @def dcseq_index_of(head, itr)
 * @brief 获取迭代器对应的正向索引
 * @param[in] head 表头
 * @param[in] itr 迭代器
 * @return dcseq_iter(type) 返回正向索引
 */
#define dcseq_index_of(head, itr) \
    __dcseq_index_of(dcseq_size(head), (itr) - (head)->first)

static inline size_t __dcseq_index_of(int size, int dist)
{
    return (dist >= 0)? dist: dist + size;
}

/**
 * @def dcseq_reverse_index_of(head, itr)
 * @brief 获取迭代器对应的逆向索引
 * @param[in] head 表头
 * @param[in] itr 迭代器
 * @return dcseq_iter(type) 返回逆向索引
 */
#define dcseq_reverse_index_of(head, itr) \
    __dcseq_reverse_index_of(dcseq_size(head), (head)->last - (itr))

static inline size_t __dcseq_reverse_index_of(int size, int dist)
{
    return (dist > 0)? dist - 1: size + dist - 1;
}

/**
 * @def dcseq_exist(head, itr)
 * @brief 判断迭代器对应的节点是否存在于表中
 * @param[in] head 表头
 * @param[in] itr 迭代器
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 */
#define dcseq_exist(head, itr) \
    __dcseq_exist((head)->base, (head)->first, (head)->last, (head)->end, itr)

#define __dcseq_exist(base, first, last, end, itr) \
    __do_dcseq_exist((char*)(base), (char*)(first), \
        (char*)(last), (char*)(end), (char*)(itr))

static inline int __do_dcseq_exist(char* base, char* first,
                                   char* last, char* end, char* itr)
{
    if (itr < base || itr >= end)
        return 0;
    else if (first > last)
        return (itr >= first || itr < last)? 1: 0;
    else if (itr >= first && itr < last)
        return 1;
    return 0;
}

/**
 * @def dcseq_distance(first, last)
 * @brief 计算两个节点的迭代距离
 * @param[in] first 首节点
 * @param[in] last 尾节点
 * @note 以索引为例，2到4的距离是4 (4-2+1=3)
 * @return size_t 返回距离
 */
#define dcseq_distance(head, first, last) \
    __dcseq_distance(dcseq_size(head), (last) - (first))

static inline size_t __dcseq_distance(int size, int dist)
{
    return (dist >= 0)? dist + 1: size + dist + 1;
}

/**
 * @def dcseq_insert(type, head, pos, newly)
 * @brief 在指定位置插入节点
 * @param[in] type 节点类型
 * @param[in,out] head 表头
 * @param[in,out] pos 插入位置
 * @param[in] newly 新节点
 * @note 插入完成之后，则pos迭代器失效
 */
#define dcseq_insert(type, head, pos, newly) do {                               \
        type * _q_p = (type *)(pos);                                            \
        type * _q_F = (head)->first;                                            \
        type * _q_L = (head)->last;                                             \
        type * _q_E = (head)->end;                                              \
        type * _q_B = (head)->base;                                             \
        if (_q_p == _q_F)                                                       \
            _q_p = ((head)->first = __dcseq_prev(_q_B, _q_E, _q_F));            \
        else if (_q_p == _q_L)                                                  \
            (head)->last = __dcseq_next(_q_B, _q_E, _q_L);                      \
        else if (_q_F < _q_L) {                                                 \
            if ((_q_p - _q_F) <= ((_q_L - _q_p) << 1)) {                       \
                if (_q_F == _q_B) {                                             \
                    *(_q_E - 1) = *_q_F;                                        \
                    memcpy(_q_F, _q_F + 1, sizeof(type) * ((--_q_p) - _q_F));  \
                    (head)->first = _q_E - 1;                                   \
                } else {                                                        \
                    memcpy(_q_F - 1, _q_F, sizeof(type) * ((_q_p--) - _q_F));  \
                    --(head)->first;                                            \
                }                                                               \
            } else {                                                            \
                memmove(_q_p + 1, _q_p, sizeof(type) * (_q_L - _q_p));          \
                (head)->last = __dcseq_next(_q_B, _q_E, _q_L);                  \
            }                                                                   \
        } else {                                                                \
            int _q_n = ((_q_E - _q_F) + (_q_L - _q_B)) >> 1;                    \
            if (_q_p > _q_F) {                                                  \
                if ((_q_p - _q_F) <= _q_n) {                                    \
                    memcpy(_q_F - 1, _q_F, sizeof(type) * ((_q_p--) - _q_F));  \
                    --(head)->first;                                            \
                } else {                                                        \
                    memmove(_q_B + 1, _q_B, sizeof(type) * (_q_L - _q_B));      \
                    *_q_B = *(_q_E - 1);                                        \
                    memmove(_q_p + 1, _q_p, sizeof(type) * (_q_E - _q_p - 1));  \
                    ++(head)->last;                                             \
                }                                                               \
            } else {                                                            \
                if ((_q_p - _q_B) > _q_n) {                                     \
                    memcpy(_q_F - 1, _q_F, sizeof(type) * (_q_E - _q_F));       \
                    *(_q_E - 1) = *_q_B;                                        \
                    memcpy(_q_B, _q_B + 1, sizeof(type) * ((--_q_p) - _q_B));  \
                    --(head)->first;                                            \
                } else {                                                        \
                    memmove(_q_p + 1, _q_p, sizeof(type) * (_q_L - _q_p));      \
                    ++(head)->last;                                             \
                }                                                               \
            }                                                                   \
        }                                                                       \
        *_q_p = (newly);                                                        \
	} while (0)

/**
 * @def dcseq_erase(type, head, pos)
 * @brief 删除指定位置的节点
 * @param[in] type 节点类型
 * @param[in,out] head 表头
 * @param[in] pos 删除位置
 * @note 删除后pos迭代器需要更新
 */
#define dcseq_erase(type, head, pos) do {                                       \
        type * _q_p = (type *)(pos);                                            \
        type * _q_F = (head)->first;                                            \
        type * _q_L = (head)->last;                                             \
        type * _q_E = (head)->end;                                              \
        type * _q_B = (head)->base;                                             \
        if (_q_F < _q_L) {                                                      \
            if ((_q_L - _q_p) <= ((_q_p - _q_F) >> 1)) {                       \
                memcpy(_q_p, _q_p + 1, sizeof(type) * (_q_L - _q_p - 1));       \
                --(head)->last;                                                 \
            } else {                                                            \
                memmove(_q_F + 1, _q_F, sizeof(type) * (_q_p - _q_F));          \
                ++(head)->first;                                                \
            }                                                                   \
        } else if (_q_p >= _q_F) {                                              \
            if ((_q_p - _q_F) <= (((_q_E - _q_p) + (_q_L - _q_B)) << 1)) {   \
                memmove(_q_F + 1, _q_F, sizeof(type) * (_q_p - _q_F));          \
                ++(head)->first;                                                \
            } else {                                                            \
                memcpy(_q_p, _q_p + 1, sizeof(type) * (_q_E - _q_p - 1));       \
                if (_q_L == _q_B) {                                             \
                    (head)->last = _q_E - 1;                                    \
                } else {                                                        \
                    *(_q_E - 1) = *_q_B;                                        \
                    memcpy(_q_B, _q_B + 1, sizeof(type) * (_q_L - _q_B - 1));   \
                    --(head)->last;                                             \
                }                                                               \
            }                                                                   \
        } else if ((_q_L - _q_p) <= (((_q_E - _q_F) + (_q_p - _q_B)) << 1)) {\
            memcpy(_q_p, _q_p + 1, sizeof(type) * (_q_L - _q_p - 1));           \
            --(head)->last;                                                     \
        } else {                                                                \
            memmove(_q_B + 1, _q_B, sizeof(type) * (_q_p - _q_B));              \
            *_q_B = *(_q_E - 1);                                                \
            memmove(_q_F + 1, _q_F, sizeof(type) * (_q_E - _q_F - 1));          \
            (head)->first = __dcseq_next(_q_B, _q_E, _q_F);                     \
        }                                                                       \
	} while (0)

/**
 * @def dcseq_inserts(type, head, pos, src, cnt)
 * @brief 在指定位置连续插入多个节点
 * @param[in] type 节点类型
 * @param[in,out] head 表头
 * @param[in,out] pos 插入位置
 * @param[in] src 新节点缓存
 * @param[in] cnt 新节点数
 * @note 插入完成之后，则pos迭代器失效
 * @par 示例:
 * @code
 	dcseq_iter(int) p = dcseq_at(&head, 8);
 	// 判断是否可以插入cnt个节点
 	if (dcseq_insertable(&head, cnt)) {
 		dcseq_inserts(int, &head, p, buf, cnt);
 	}
 * @endcode
 */
#define dcseq_inserts(type, head, pos, src, cnt) do {                           \
        size_t _q_c = (size_t)(cnt);                                            \
        type * _q_s = (type *)(src);                                            \
        type * _q_p = (type *)(pos);                                            \
        type * _q_F = (head)->first;                                            \
        type * _q_L = (head)->last;                                             \
        type * _q_E = (head)->end;                                              \
        type * _q_B = (head)->base;                                             \
        if (_q_p >= _q_F) {                                                     \
            size_t _q_fc = _q_F - _q_B;                                         \
            if (_q_fc >= _q_c) {                                                \
                memcpy(_q_F - _q_c, _q_F, sizeof(type) * (_q_p - _q_F));        \
                memcpy(_q_p - _q_c, _q_s, sizeof(type) * _q_c);                 \
                (head)->first = _q_F - _q_c;                                    \
            } else {                                                            \
                size_t _q_n = _q_c - _q_fc;                                     \
                size_t _q_m = _q_p - _q_F;                                      \
                if (_q_n >= _q_m) {                                             \
                    size_t _q_r = _q_n - _q_m;                                  \
                    memcpy(_q_E - _q_n, _q_F, sizeof(type) * _q_m);             \
                    memcpy(_q_E - _q_r, _q_s, sizeof(type) * _q_r);             \
                    memcpy(_q_B, _q_s + _q_r, sizeof(type) * (_q_c - _q_r));    \
                } else {                                                        \
                    memcpy(_q_E - _q_n, _q_F, sizeof(type) * _q_n);             \
                    memcpy(_q_B, _q_F + _q_n, sizeof(type) * (_q_m - _q_n));    \
                    memcpy(_q_B + (_q_m - _q_n), _q_s, sizeof(type) * _q_c);    \
                }                                                               \
                (head)->first = _q_E - _q_n;                                    \
            }                                                                   \
        } else {                                                                \
            memmove(_q_p + _q_c, _q_p, sizeof(type) * (_q_L - _q_p));           \
            memcpy(_q_p, _q_s, sizeof(type) * _q_c);                            \
            (head)->last = _q_L + _q_c;                                         \
        }                                                                       \
	} while (0)

/**
 * @def dcseq_erases(type, head, begin, end)
 * @brief 连续删除指定位置的多个节点
 * @param[in] type 节点类型
 * @param[in,out] head 表头
 * @param[in,out] begin 起始位置
 * @param[in,out] end 终止位置
 */
#define dcseq_erases(type, head, _begin, _end) do {                         \
        type * _q_e = (type *)(_end);                                       \
        type * _q_f = (type *)(_begin);                                     \
        type * _q_F = (head)->first;                                        \
        type * _q_L = (head)->last;                                         \
        type * _q_E = (head)->end;                                          \
        type * _q_B = (head)->base;                                         \
        if (_q_f < _q_L) {                                                  \
            memcpy(_q_f, _q_e, sizeof(type) * (_q_e - _q_f));               \
            (head)->last = _q_L - (_q_e - _q_f);                            \
        } else if (_q_e < _q_E) {                                           \
            memmove(_q_F + (_q_e - _q_f), _q_F,                             \
                    sizeof(type) * (_q_f - _q_F));                          \
            (head)->first = _q_F + (_q_e - _q_f);                           \
        } else {                                                            \
            size_t _q_n = _q_E - _q_f;                                      \
            size_t _q_m = _q_L - _q_e;                                      \
            if (_q_n > _q_m) {                                              \
                memcpy(_q_f, _q_e, sizeof(type) * _q_m);                    \
                (head)->last = _q_f + _q_m;                                 \
            } else {                                                        \
                memcpy(_q_f, _q_e, sizeof(type) * _q_n);                    \
                memcpy(_q_B, _q_e + _q_n, sizeof(type) * (_q_m - _q_n));    \
                (head)->last = _q_B + (_q_m - _q_n);                        \
            }                                                               \
        }                                                                   \
	} while (0)

/**
 * @def dcseq_splice(type, dst, pos, src, _first, _last)
 * @brief 移接一段序列[first, last]到目标位置。本方法只可以在不同的序表之间
 * @param[in] type 节点类型
 * @param[in,out] dst 目标表头
 * @param[in,out] pos 指定位置
 * @param[in,out] src 源表头
 * @param[in,out] _first 起始位置，包括_first
 * @param[in,out] _last 末尾位置，包括_last
 */
#define dcseq_splice(type, dst, pos, src, _first, _last) do {               \
        type * _q1_l = (_last);                                             \
        type * _q1_f = (_first);                                            \
        type * _q1_p = (pos);                                               \
        type * _q1_B = (src)->base;                                         \
        type * _q1_E = (src)->end;                                          \
        size_t _q_cs[2] = {0, 0};                                           \
        type * _q_ss[2] = {_q1_f, _q1_B};                                   \
        int _q1_i, _q1_k;                                                   \
        if (dst == src) break;                                              \
        if (_q1_f <= _q1_l)                                                 \
            _q_cs[0] = _q1_l - _q1_f + 1;                                   \
        else {                                                              \
            _q_cs[0] = _q1_E - _q1_f;                                       \
            _q_cs[1] = _q1_l - _q1_B + 1;                                   \
            _q1_i = dcseq_index_of(dst, _q1_p) + _q_cs[0];                  \
        }                                                                   \
        for (_q1_k = 0; _q1_k < 2 && _q_cs[_q1_k]; ++_q1_k) {               \
            dcseq_inserts(type, dst, _q1_p, _q_ss[_q1_k], _q_cs[_q1_k]);    \
            if (_q_cs[1])                                                   \
                _q1_p = dcseq_at(type, dst, _q1_i);                         \
        }                                                                   \
        dcseq_erases(type, src, _q1_f, __dcseq_next(_q1_B, _q1_E, _q1_l));  \
	} while (0)

/**
 * @def dcseq_appends(type, head, src, cnt)
 * @brief 在表尾添加多个节点
 * @param[in] type 节点类型
 * @param[in,out] head 表头
 * @param[in] src 新节点缓存
 * @param[in] cnt 新节点数
 * @par 示例:
 * @code
 	// 判断是否可以插入cnt个节点
 	if (dcseq_insertable(&head, cnt)) {
 		dcseq_appends(int, &head, buf, cnt);
 	}
 * @endcode
 * @see dcseq_append
 */
#define dcseq_appends(type, head, src, cnt) do {                        \
        size_t _q_c = (cnt);                                            \
        type * _q_s = (type *)(src);                                    \
        type * _q_E = (head)->end;                                      \
        type * _q_B = (head)->base;                                     \
        type * _q_L = (head)->last;                                     \
        if (_q_L + _q_c > _q_E) {                                       \
            size_t _q_n = _q_E - _q_L;                                  \
            memcpy(_q_L, _q_s, sizeof(type) * _q_n);                    \
            memcpy(_q_B, _q_s + _q_n, sizeof(type) * (_q_c - _q_n));    \
            (head)->last = _q_B + (_q_c - _q_n);                        \
        } else {                                                        \
            memcpy(_q_L, _q_s, sizeof(type) * _q_c);                    \
            _q_L += _q_c;                                               \
            if (_q_L == _q_E)                                           \
                _q_L = _q_B;                                            \
            (head)->last = _q_L;                                        \
        }                                                               \
	} while (0)

/**
 * @def dcseq_append(head, newly)
 * @brief 在表尾添加新节点
 * @param[in,out] head 表头
 * @param[in] newly 新节点
 * @see dcseq_appends
 */
#define dcseq_append(head, newly) \
    (*((head)->last++) = (newly))

/**
 * @def dcseq_copy(type, dst, src)
 * @brief 深拷贝源表的节点到目标表
 * @param[in] type 节点类型
 * @param[out] dst 目标表
 * @param[in] src 源表
 * @par 示例:
 * @code
 	// 判断是否可以从src拷贝到dst
 	if (dcseq_copyable(&dst, &src)) {
 		dcseq_copy(int, &dst, &src);
 	}
 * @endcode
 * @see dcseq_copyable
 */
#define dcseq_copy(type, dst, src) do {                                 \
        type * _q_B = (src)->base;                                      \
		type * _q_F = (src)->first;                                     \
		type * _q_L = (src)->last;                                      \
		type * _q_E = (src)->end;                                       \
		type * _q_dB = (dst)->base;                                     \
		if (_q_F < _q_L) {                                              \
            size_t _q_c = _q_L - _q_F;                                  \
            memcpy(_q_dB, _q_F, sizeof(type) * (_q_L - _q_F));          \
            (dst)->last = _q_dB + _q_c;                                 \
		} else {                                                        \
            size_t _q_c = _q_E - _q_F;                                  \
            memcpy(_q_dB, _q_F, sizeof(type) * _q_c);                   \
            memcpy(_q_dB + _q_c, _q_B, sizeof(type) * (_q_L - _q_B));   \
            (dst)->last = _q_dB + (_q_c + (_q_L - _q_B));               \
        }                                                               \
        (dst)->first = _q_dB;                                           \
	} while (0)

/**
 * @def dcseq_push_front(head)
 * @brief 获取指向表首插入节点位置的迭代器
 * @param[in,out] head 表头
 * @return dcseq_iter(type) 返回迭代器
 * @par 示例:
 * @code
 	// 在表首插入i
 	*dcseq_push_front(&head) = i;
 * @endcode
 * @see dcseq_pop_front, dcseq_push_back, dcseq_pop_back
 */
#define dcseq_push_front(type, head) \
    ((type *)dcseq_dec(head, (head)->first))

/**
 * @def dcseq_push_back(head)
 * @brief 获取指向表尾插入节点位置的迭代器
 * @param[in,out] head 表头
 * @return dcseq_iter(type) 返回迭代器
 * @par 示例:
 * @code
 	// 在表尾插入i
 	*dcseq_push_back(&head) = i;
 * @endcode
 * @see dcseq_pop_back, dcseq_push_front, dcseq_pop_front
 */
#define dcseq_push_back(type, head) \
    dcseq_inc_later(type, head, (head)->last)

/**
 * @def dcseq_pop_front(head)
 * @brief 获取指向表首节点位置的迭代器
 * @param[in,out] head 表头
 * @return dcseq_iter(type) 返回迭代器
 * @par 示例:
 * @code
 	// 移除表首节点
 	int d = *dcseq_pop_front(&head);
 * @endcode
 * @see dcseq_push_front, dcseq_push_back, dcseq_pop_back
 */
#define dcseq_pop_front(type, head) \
    dcseq_inc_later(type, head, (head)->first)

/**
 * @def dcseq_pop_back(head)
 * @brief 获取指向表尾节点位置的迭代器
 * @param[in,out] head 表头
 * @return dcseq_iter(type) 返回迭代器
 * @par 示例:
 * @code
 	// 移除表尾节点
 	int d = *dcseq_pop_back(&head);
 * @endcode
 * @see dcseq_push_back, dcseq_push_front, dcseq_pop_front
 */
#define dcseq_pop_back(type, head) \
    ((type *)dcseq_dec(head, (head)->last))

/**
 * @def dcseq_iter_replace(victim, newly)
 * @brief 用新节点替换目标节点
 * @param[in,out] victim 受害目标节点
 * @param[in,out] newly 新节点
 */
#define dcseq_iter_replace(victim, newly) (*(victim) = (newly))

#define __dcseq_iter_swap(type, a, b) do {\
	type _q_t = *(a); *(a) = *(b); *(b) = _q_t; } while (0)

/**
 * @def dcseq_iter_swap(type, a, b)
 * @brief 用新节点替换目标节点
 * @param[in] type 节点类型
 * @param[in,out] a 第一个节点
 * @param[in,out] b 第二个节点
 */
#define dcseq_iter_swap(type, a, b) do {        \
		type * _q_b = (b); type * _q_a = (a);	\
		__dcseq_iter_swap(type, _q_a, _q_b);    \
	} while (0)

/**
 * @def dcseq_replace(victim, newly)
 * @brief 用新表头替换目标表头
 * @param[in,out] victim 受害目标表头
 * @param[out] newly 新表头
 */
#define dcseq_replace(victim, newly) (*(victim) = *(newly))

/**
 * @def dcseq_swap(type, a, b)
 * @brief 交换两个表头
 * @param[in] type 节点类型
 * @param[in,out] a 第一个表头
 * @param[in,out] b 第二个表头
 */
#define dcseq_swap(type, a, b) do { \
        dcseq_head(type) t = *(a);  \
        *(a) = *(b); *(b) = t;      \
    } while (0)

/**
 * @def dcseq_front(head)
 * @brief 得到表首节点
 * @return dcseq_iter(type) 返回节点
 * @see dcseq_back
 */
#define dcseq_front(head) ((head)->first)

/**
 * @def dcseq_back(head)
 * @brief 得到表首节点
 * @param[in] head 表头
 * @return dcseq_iter(type) 返回节点
 * @see dcseq_front
 */
#define dcseq_back(head) \
    __dcseq_back((head)->base, (head)->last, (head)->end)

#define __dcseq_back(base, last, end) \
    (((last) > (base))? ((last) - 1): ((end) - 1))

/**
 * @def dcseq_begin(head)
 * @brief 得到指向正向起始位置的迭代器
 * @return dcseq_iter(type) 返回迭代器
 * @par 示例:
 * @code
 	// 遍历序表
 	dcseq_iter(int) e = dcseq_end(&head);
 	dcseq_iter(int) i = dcseq_begin(&head);
 	while (i != e) {
 		printf("[%d]=%d\n", dcseq_index_of(&head, i), *i);
 		dcseq_inc(i);
 	}
 * @endcode
 * @see dcseq_end, dcseq_rbegin, dcseq_rend
 */
#define dcseq_begin(head) dcseq_front(head)

/**
 * @def dcseq_end(head)
 * @brief 得到指向正向终止位置的迭代器
 * @param[in] head 表头
 * @return dcseq_iter(type) 返回迭代器
 * @see dcseq_begin, dcseq_rbegin, dcseq_rend
 */
#define dcseq_end(head) ((head)->last)

/**
 * @def dcseq_end_of(head, last)
 * @brief 得到指向当前节点的正向终止位置的迭代器
 * @param[in] head 表头
 * @param[in] last 当前节点
 * @return dcseq_iter(type) 返回终止迭代器
 * @see dcseq_begin, dcseq_end
 */
#define dcseq_end_of(head, last) dcseq_next(head, last)

/**
 * @def dcseq_rbegin(head)
 * @brief 得到指向逆向起始位置的迭代器
 * @param[in] head 表头
 * @return dcseq_iter(type) 返回迭代器
 * @see dcseq_end, dcseq_begin, dcseq_rend
 */
#define dcseq_rbegin(head) dcseq_back(head)

/**
 * @def dcseq_rend(head)
 * @brief 得到指向逆向起始位置的迭代器
 * @return dcseq_iter(type) 返回迭代器
 * @see dcseq_end, dcseq_rbegin, dcseq_rend
 */
#define dcseq_rend(head) dcseq_prev(head, (head)->first)

/**
 * @def dcseq_rend_of(head, rlast)
 * @brief 得到指向当前节点的逆向终止位置的迭代器
 * @param[in] head 表头
 * @param[in] rlast 当前节点
 * @return dcseq_iter(type) 返回终止迭代器
 * @see dcseq_rbegin, dcseq_rend
 */
#define dcseq_rend_of(head, rlast) dcseq_prev(head, rlast)

/**
 * @def dcseq_next(head, itr)
 * @brief 获取当前节点的后继节点
 * @param[in] head 表头
 * @param[in] itr 当前节点
 * @return dcseq_iter(type) 返回后继节点
 * @see dcseq_prev
 */
#define dcseq_next(head, itr) \
    __dcseq_next((head)->base, (head)->end, itr)

#define __dcseq_next(base, end, itr) \
    (((itr) + 1 >= (end))? (base): ((itr) + 1))

/**
 * @def dcseq_prev(head, itr)
 * @brief 获取当前节点的前驱节点
 * @param[in] head 表头
 * @param[in] itr 当前节点
 * @return dcseq_iter(type) 返回前驱节点
 * @see dcseq_next
 */
#define dcseq_prev(head, itr) \
    __dcseq_prev((head)->base, (head)->end, itr)

#define __dcseq_prev(base, end, itr) \
    (((itr) < (base) + 1)? ((end) - 1): ((itr) - 1))

/**
 * @def dcseq_advance(type, head, cur, dist)
 * @brief 给迭代器增加一段距离或减小一段距离
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] cur 迭代器
 * @param[in] dist 增加的距离。正数时，表示增加；负数时，表示减小
 * @return dcseq_iter(type) 返回新迭代器
 */
#define dcseq_advance(type, head, cur, dist) \
    __dcseq_advance(type, (head)->base, (head)->end, (type *)(cur) + (dist))

#define __dcseq_advance(type, base, end, dest) \
    ((type *)__do_dcseq_advance((char*)(base), (char*)(end), (char*)(dest)))

static inline char* __do_dcseq_advance(char* base, char* end, char* dest)
{
    if (dest >= end)
        return base + (dest - end);
    else if (dest < base)
        return end - (base - dest);
    else
        return dest;
}

/**
 * @def DCSEQ_NOT_END(itr, end)
 * @brief 迭代结束条件
 * @par 示例:
 * @code
 	dcseq_iter(int) e = dcseq_end(&head);
 	dcseq_iter(int) i = dcseq_begin(&head);
 	while (DCSEQ_NOT_END(i, e)) {
 		......
 		dcseq_inc(&head, i);
 	}
 * @endcode
 * @def DCSEQ_NOT_REND(itr, rend)
 * @brief 逆向迭代结束条件
 * @par 示例:
 * @code
 	dcseq_iter(int) e = dcseq_rend(&head);
 	dcseq_iter(int) i = dcseq_rbegin(&head);
 	while (DCSEQ_NOT_REND(i, e)) {
 		......
 		dcseq_dec(&head, i);
 	}
 * @endcode
 */
#define DCSEQ_NOT_END(itr, end)     ((itr) != (end))
#define DCSEQ_NOT_REND(itr, rend)	((itr) != (rend))

/**
 * @def dcseq_inc(head, itr)
 * @brief 迭代器递增
 * @param[in] head 表头
 * @param[in,out] 迭代器
 * @def dcseq_dec(head, itr)
 * @brief 迭代器递减
 * @param[in] head 表头
 * @param[in,out] 迭代器
 * @def dcseq_add(type, head, itr, cnt)
 * @brief 迭代器增加
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in,out] 迭代器
 * @param[in] cnt 增加量
 * @def dcseq_sub(head, itr, cnt)
 * @brief 迭代器减小
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in,out] 迭代器
 * @param[in] cnt 减小量
 * @def dcseq_inc_later(type, head, itr)
 * @brief 迭代器后递增
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in,out] 迭代器
 * @def dcseq_dec_later(type, head, itr)
 * @brief 迭代器后递减
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in,out] 迭代器
 * @def dcseq_add_later(type, head, itr, cnt)
 * @brief 迭代器后增加
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in,out] 迭代器
 * @param[in] cnt 增加量
 * @def dcseq_sub_later(type, head, itr, cnt)
 * @brief 迭代器后减少
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in,out] 迭代器
 * @param[in] cnt 减小量
 */
#define dcseq_inc(head, itr)                __dcseq_inc((head)->base, (head)->end, itr)
#define dcseq_dec(head, itr)                __dcseq_dec((head)->base, (head)->end, itr)
#define dcseq_add(type, head, itr, cnt)     __dcseq_add(type, (head)->base, (head)->end, itr, cnt)
#define dcseq_sub(type, head, itr, cnt)     __dcseq_sub(type, (head)->base, (head)->end, itr, cnt)

#define dcseq_inc_later(type, head, itr) \
    __dcseq_inc_later(type, (head)->base, (head)->end, itr)

#define dcseq_dec_later(type, head, itr) \
    __dcseq_dec_later(type, (head)->base, (head)->end, itr)

#define dcseq_add_later(type, head, itr, cnt) \
    __dcseq_add_later(type, (head)->base, (head)->end, itr, cnt)

#define dcseq_sub_later(type, head, itr, cnt) \
    __dcseq_sub_later(type, (head)->base, (head)->end, itr, cnt)

#define __dcseq_inc(base, end, itr)	((itr) = __dcseq_next(base, end, itr))
#define __dcseq_dec(base, end, itr)	((itr) = __dcseq_prev(base, end, itr))

#define __dcseq_add(type, base, end, itr, cnt) \
    ((itr) = __dcseq_advance(type, base, end, (itr) + (cnt)))
#define __dcseq_sub(type, base, end, itr, cnt) \
    ((itr) = __dcseq_advance(type, base, end, (itr) - (cnt)))

#define __dcseq_inc_later(type, base, end, itr) \
    ((type *)__do_dcseq_inc_later((char*)(base), (char*)(end), \
            (char*)((itr) + 1), (char*)(itr), (char**)(&(itr))))

#define __dcseq_dec_later(type, base, end, itr) \
    ((type *)__do_dcseq_dec_later((char*)(base), (char*)((type *)(end) - 1), \
            (char*)((itr) - 1), (char*)(itr), (char**)(&(itr))))

#define __dcseq_add_later(type, base, end, itr, cnt) \
    ((type *)__do_dcseq_add_sub_later((char*)(base), (char*)(end),  \
            (char*)((itr) + (cnt)), (char*)(itr), (char**)(&(itr))))

#define __dcseq_sub_later(type, base, end, itr, cnt) \
    ((type *)__do_dcseq_add_sub_later((char*)(base), (char*)(end),  \
            (char*)((itr) - (cnt)), (char*)(itr), (char**)(&(itr))))

static inline char* __do_dcseq_inc_later(char* base, char* end,
                                         char* dest, char* cur,
                                         char** p)
{
    *p = (dest >= end)? base: dest;
	return cur;
}

static inline char* __do_dcseq_dec_later(char* base, char* nend,
                                         char* dest, char* cur,
                                         char** p)
{
	*p = (dest < base)? nend: dest;
	return cur;
}

static inline char* __do_dcseq_add_sub_later(char* base, char* end,
                                             char* dest, char* cur,
                                             char** p)
{
	*p = __do_dcseq_advance(base, end, dest);
	return cur;
}

/**
 * @def dcseq_iter_less(first, last, itr1, itr2)
 * @brief 判断itr1是否小于itr2
 * @param[in] first 表头的first字段
 * @param[in] last 表头的last字段
 * @param[in] itr1 迭代器1
 * @param[in] itr2 迭代器2
 * @return 返回判断结果
 * @retval 1 itr1小于itr2
 * @retval 0 itr1大于等于itr2
 */
#define dcseq_iter_less(first, last, itr1, itr2) \
    __dcseq_iter_less((char*)(first), (char*)(last), \
                      (char*)(itr1), (char*)(itr2))

static inline int __dcseq_iter_less(char* first, char* last,
                                    char* itr1, char* itr2)
{
    if ((itr1 < itr2 && (first <= last || itr1 >= first || itr2 <= last)) ||
        (itr1 > itr2 && first > last && itr1 >= first && itr2 <= last))
        return 1;
    else
        return 0;
}

/**
 * @def dcseq_iter_less_equal(first, last, itr1, itr2)
 * @brief 判断itr1是否小于等于itr2
 * @param[in] first 表头的first字段
 * @param[in] last 表头的last字段
 * @param[in] itr1 迭代器1
 * @param[in] itr2 迭代器2
 * @return 返回判断结果
 * @retval 1 itr1小于等于itr2
 * @retval 0 itr1大于itr2
 */
#define dcseq_iter_less_equal(first, last, itr1, itr2) \
    __dcseq_iter_less_equal((char*)(first), (char*)(last), \
                            (char*)(itr1), (char*)(itr2))

static inline int __dcseq_iter_less_equal(char* first, char* last,
                                          char* itr1, char* itr2)
{
    if (itr1 == itr2 ||
        (itr1 < itr2 && (first <= last || itr1 >= first || itr2 <= last)) ||
        (itr1 > itr2 && first > last && itr1 >= first && itr2 <= last))
        return 1;
    else
        return 0;
}

/**
 * @def dcseq_iter_greater(first, last, itr1, itr2)
 * @brief 判断itr1是否大于itr2
 * @param[in] first 表头的first字段
 * @param[in] last 表头的last字段
 * @param[in] itr1 迭代器1
 * @param[in] itr2 迭代器2
 * @return 返回判断结果
 * @retval 1 itr1大于itr2
 * @retval 0 itr1小于等于itr2
 */
#define dcseq_iter_greater(first, last, itr1, itr2) \
    __dcseq_iter_greater((char*)(first), (char*)(last), \
                         (char*)(itr1), (char*)(itr2))

static inline int __dcseq_iter_greater(char* first, char* last,
                                       char* itr1, char* itr2)
{
    if ((itr1 > itr2 && (first <= last || itr2 >= first || itr1 <= last)) ||
        (itr1 < itr2 && first > last && itr2 >= first && itr1 <= last))
        return 1;
    else
        return 0;
}

/**
 * @def dcseq_iter_greater_equal(first, last, itr1, itr2)
 * @brief 判断itr1是否大于等于itr2
 * @param[in] first 表头的first字段
 * @param[in] last 表头的last字段
 * @param[in] itr1 迭代器1
 * @param[in] itr2 迭代器2
 * @return 返回判断结果
 * @retval 1 itr1大于等于itr2
 * @retval 0 itr1小于itr2
 */
#define dcseq_iter_greater_equal(first, last, itr1, itr2) \
    __dcseq_iter_greater_equal((char*)(first), (char*)(last), \
                               (char*)(itr1), (char*)(itr2))

static inline int __dcseq_iter_greater_equal(char* first, char* last,
                                             char* itr1, char* itr2)
{
    if (itr1 == itr2 ||
        (itr1 > itr2 && (first <= last || itr2 >= first || itr1 <= last)) ||
        (itr1 < itr2 && first > last && itr2 >= first && itr1 <= last))
        return 1;
    else
        return 0;
}

/**
 * @def dcseq_foreach(type, head, begin, end, fn)
 * @brief 正向遍历begin到end的节点，
        并为每一个节点调用回调函数或宏
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] fn 回调函数或宏
 */
#define dcseq_foreach(type, head, begin, _end, fn)  \
    do {type* _q_B = (head)->base;                  \
		type* _q_E = (head)->end;                   \
        type * _q_e = (type *)(_end);               \
        type * _q_f = (type *)(begin);              \
		while (_q_f != _q_e) {				        \
			fn(_q_f);						        \
			__dcseq_inc(_q_B, _q_E, _q_f);          \
		}									        \
	} while(0)

/**
 * @def dcseq_reverse_foreach(type, head, rbegin, rend, fn)
 * @brief 逆向遍历rbegin到rend的节点，
        并为每一个节点调用回调函数或宏
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] rbegin 逆向起始位置
 * @param[in] rend 逆向终止位置
 * @param[in] fn 回调函数或宏
 */
#define dcseq_reverse_foreach(type, head, rbegin, rend, fn) \
    do {type* _q_B = (head)->base;                  \
		type* _q_E = (head)->end;                   \
        type * _q_e = (type *)(rend);               \
        type * _q_f = (type *)(rbegin);             \
		while (_q_f != _q_e) {				        \
			fn(_q_f);						        \
			__dcseq_dec(_q_B, _q_E, _q_f);          \
		}									        \
	} while(0)

/**
 * @def dcseq_search(type, head, begin,_end, equal, var, res)
 * @brief 顺序查找区域内的指定节点
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in,out] begin 起始位置
 * @param[in,out] end 终止位置
 * @param[in] equal 比较函数或宏。相等时返回1，不相等返回0
 * @param[in] var 搜索的键值，常量，变量均可
 * @param[out] res 保存搜索结果的变量，类型是节点指针。
                结果为NULL表示找不到节点
 * @see dcseq_binary_search
 */
#define dcseq_search(type, head, begin, _end, equal, var, res) \
    do {type _q_t = (var);                              \
		type * _q_e = (_end);					        \
		type * _q_f = (begin);					        \
		type* _q_B = (head)->base;                      \
		type* _q_E = (head)->end;                       \
		(res) = NULL;							        \
		while (_q_f != _q_e) {					        \
			if (equal(_q_f, (&_q_t))) {		            \
				(res) = _q_f;					        \
				break;							        \
			}									        \
			__dcseq_inc(_q_B, _q_E, _q_f);              \
		}		                						\
	} while (0)

/**
 * @def dcseq_binary_search(type, head, begin, end, greater, var, res)
 * @brief 二分查找区域内的指定节点
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in,out] begin 起始位置
 * @param[in,out] end 终止位置
 * @param[in] greater 比较函数或宏。升序:(*i > *j) 降序:(*i < *j)
 * @param[in] var 搜索的键值，常量，变量均可
 * @param[out] res 保存搜索结果的变量，类型是节点指针。结果为NULL表示找不到节点
 * @note 必须保证，表在查找之前是升序的
 * @see dcseq_search, dcseq_is_sorted
 */
#define dcseq_binary_search(type, head, begin, _end, greater, var, res) \
    do {type _q_t = (var);									        \
		type * _q_e = (_end);							            \
		type * _q_f = (begin);							            \
		type* _q_B = (head)->base;                                  \
		type* _q_E = (head)->end;                                   \
		int _q_s = _q_E - _q_B;                                     \
		(res) = NULL;										        \
        while (_q_f != _q_e) {                                      \
			type * _q_m = __dcseq_advance(type, _q_B, _q_E, _q_f +  \
                ((__dcseq_distance(_q_s, _q_e - _q_f) - 1) >> 1));  \
			if (greater(_q_m, (&_q_t)))					            \
				_q_e = _q_m;								        \
			else if (greater((&_q_t), _q_m))			            \
				_q_f = __dcseq_next(_q_B, _q_E, _q_m);              \
			else {										            \
				(res) = _q_m;								        \
				break;										        \
			}												        \
		}													        \
	} while (0)

/* 反转序表的两种方式：
 * (1) 直接使用 dcseq_reverse(元素类型, head); (推荐)
 * (2) 按照如下方式迭代
 *     dcseq_iter(int) i = dcseq_vbegin(head);
 *     dcseq_iter(int) j = dcseq_rvbegin(head);
 *     dcseq_iter(int) e = dcseq_vend(int, head);
 *     while (i != e) {
 *			dcseq_vswap(int, i, j);
 *     }
 *     当迭代完成时，反转完成
 */

/**
 * @def dcseq_vbegin(head)
 * @brief 得到正反转的起始迭代器
 * @return dcseq_iter(type) 返回正反转迭代器
 * @par 示例:
 * @code
 	// 反转表
 	dcseq_iter(int) ve = dcseq_vend(int, &head);
 	dcseq_iter(int) vi = dcseq_vbegin(&head);
 	dcseq_iter(int) vr = dcseq_rvbegin(&head);
 	while (vi < ve) {
 		dcseq_vswap(int, head, vi, vr);
 	}
 * @endcode
 * @see dcseq_rvbegin, dcseq_vend, dcseq_vswap, dcseq_reverse
 * @def dcseq_rvbegin(head)
 * @brief 得到逆反转的起始迭代器
 * @param[in] head 表头
 * @return dcseq_iter(type) 返回迭代器
 * @see dcseq_vbegin, dcseq_vend, dcseq_vswap, dcseq_reverse
 */
#define dcseq_vbegin(head)		dcseq_begin(head)
#define dcseq_rvbegin(head)		dcseq_rbegin(head)

/**
 * @def dcseq_vend(type, head)
 * @brief 得到用于反转的终止位置的迭代器
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @return dcseq_iter(type) 返回迭代器
 * @par 示例:
 * @code
 	// 反转表
 	dcseq_iter(int) ve = dcseq_vend(&head);
 	dcseq_iter(int) vi = dcseq_vbegin(&head);
 	dcseq_iter(int) vr = dcseq_rvbegin(&head);
 	while (vi != ve) {
 		dcseq_vswap(int, &head, vi, vr);
 	}
 * @endcode
 * @see dcseq_vbegin, dcseq_rvbegin, dcseq_vswap, dcseq_reverse
 */
#define dcseq_vend(type, head) \
    dcseq_at(type, head, dcseq_count(head) >> 1)

#define __dcseq_vnext(base, end, i, r) do { \
        __dcseq_inc(base, end, i);          \
        __dcseq_dec(base, end, r);          \
    } while (0)

#define dcseq_vnext(head, i, r) do {    \
        dcseq_inc(head, i);             \
        dcseq_dec(head, r);             \
    } while (0)

#define __dcseq_vswap(type, base, end, i, r) do {   \
        __dcseq_iter_swap(type, i, r);              \
        __dcseq_vnext(base, end, i, r);             \
    } while (0)

/**
 * @def dcseq_vswap(type, head, i, r)
 * @brief 交换反转操作
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in,out] i 正反转迭代器
 * @param[in,out] r 逆反转迭代器
 * @par 示例:
 * @code
 	// 反转表
 	dcseq_iter(int) ve = sllnk_vend(int, &head);
 	dcseq_iter(int) vi = sllnk_vbegin(&head);
 	dcseq_iter(int) vr = sllnk_rvbegin(&head);
 	while (vi != ve) {
 		sllnk_vswap(int, &head, vi, vr);
 	}
 * @endcode
 * @see sllnk_vbegin, sllnk_vend, sllnk_reverse
 */
#define dcseq_vswap(type, head, i, r) do {  \
        __dcseq_iter_swap(type, i, r);      \
        dcseq_vnext(head, i, r);            \
    } while (0)

/**
 * @def dcseq_reverse(type, head)
 * @brief 反转表
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @see dcseq_vbegin, dcseq_rvbegin, dcseq_vend, dcseq_vswap
 */
#define dcseq_reverse(type, head) do {                      \
        type * _q_B = (head)->base;                         \
        type * _q_E = (head)->end;                          \
		type * _q_e = dcseq_vend(type, head);               \
		type * _q_i = dcseq_vbegin(head);		            \
		type * _q_j = dcseq_rvbegin(head);		            \
		while (_q_i != _q_e)                                \
			__dcseq_vswap(type, _q_B, _q_E, _q_i, _q_j);    \
	} while (0)

/**
 * @def dcseq_serialize(type, head, buf)
 * @brief 序列化
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[out] buf 序列缓存
 * @see 若要进行反序列化还原表，请使用dcseq_init_serial, DCSEQ_INIT_SERIAL
 */
#define dcseq_serialize(type, head, buf) do {           \
        type * _q_d = (type *)(buf);                    \
        type * _q_f = (head)->first;                    \
        type * _q_e = (head)->end;                      \
        size_t _q_c = dcseq_count(head);                \
        if (_q_f + _q_c > _q_e) {                       \
            size_t _q_fs = _q_e - _q_f;                 \
            memcpy(_q_d, _q_f, sizeof(type) * _q_fs);   \
            memcpy(_q_d + _q_fs, (head)->base,          \
                   sizeof(type) * (_q_c - _q_fs));      \
        } else                                          \
            memcpy(_q_d, _q_f, sizeof(type) * _q_c);    \
    } while (0)

/**
 * @def dcseq_fill(type, head, begin, _end, val)
 * @brief 用val填充区域
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] val 填充值
 * @see dcseq_generate
 */
#define dcseq_fill(type, head, begin, _end, val) do {   \
        type _q_v = (val);                              \
		type* _q_B = (head)->base;                      \
		type* _q_E = (head)->end;                       \
        type * _q_e = (type *)(_end);                   \
        type * _q_f = (type *)(begin);                  \
		while (_q_f != _q_e) {				            \
			*_q_f = _q_v;					            \
			__dcseq_inc(_q_B, _q_E, _q_f);              \
		}									            \
	} while(0)

/**
 * @def dcseq_generate(type, head, begin, _end, generator)
 * @brief 使用输入产生器来填充区域
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] generator 输入产生器
 * @see dcseq_fill
 */
#define dcseq_generate(type, head, begin, _end, generator) \
    do {type* _q_B = (head)->base;                  \
		type* _q_E = (head)->end;                   \
        type * _q_e = (type *)(_end);               \
        type * _q_f = (type *)(begin);              \
		while (_q_f != _q_e) {				        \
			*_q_f = (generator);					\
			__dcseq_inc(_q_B, _q_E, _q_f);          \
		}									        \
	} while(0)

/**
 * @def dcseq_is_heap(type, head, begin, _end, greater, res)
 * @brief 判断区域是否是一个最大堆
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] greater 比较函数或宏
 					最大堆:(*i > *j) 最小堆:(*i < *j)
 * @param[out] res 保存结果的变量，1为真，0为假
 * @note 方法:逐个比较每个非根节点与其父节点的值
 * @see dcseq_make_heap, dcseq_push_heap, dcseq_pop_heap, dcseq_sort_heap
 */
#define dcseq_is_heap(type, head, begin, _end, greater, res) do {               \
        type * _q_e = (_end);				                                    \
		type * _q_p = (begin);						                            \
		type * _q_B = (head)->base;                                             \
        type * _q_E = (head)->end;                                              \
		size_t _q_d = (__dcseq_distance(_q_E - _q_B, _q_e - _q_p) - 2) >> 1;    \
		type * _q_i = __dcseq_next(_q_B, _q_E, _q_p);                           \
		for ((res) = 1; _q_d--; _q_p = __dcseq_next(_q_B, _q_E, _q_p)) {        \
			__dcseq_ret_gt(_q_B, _q_E, _q_i, _q_p, res, greater);               \
			__dcseq_ret_gt(_q_B, _q_E, _q_i, _q_p, res, greater);               \
		}												                        \
		if (_q_i != _q_e && (greater(_q_i, _q_p))) {                            \
			(res) = 0; break;							                        \
		}												                        \
	} while (0)

#define __dcseq_ret_gt(B, E, i, j, r, greater) \
    if (greater((i), (j))) { (r) = 0; break; } __dcseq_inc(B, E, i)

/**
 * @def dcseq_push_heap(type, head, begin, _end, greater)
 * @brief 将区域最后一个节点插入到堆中
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] greater 比较函数或宏
 					最大堆:(*i > *j) 最小堆:(*i < *j)
 * @note 使用上滤插入法将最后一个节点last插入到(first,last-1)堆中，
 	所以插入的前提是(first,last-1)是一个堆\n
	上滤插入算法如下：\n
	(1) 将last记为current节点.\n
	(2) 若current无父节点(即current为根节点)，则退出.\n
	(3) 若current的值小于等于父节点的值，则退出.\n
	(4) 交换current和父节点的值，并记父节点为current节点，转(2)
 * @see dcseq_make_heap, dcseq_is_heap, dcseq_pop_heap, dcseq_sort_heap
 */
#define dcseq_push_heap(type, head, begin, _end, greater) do {              \
		type * _q_F = (head)->first;                                        \
		type * _q_B = (head)->base;                                         \
        type * _q_E = (head)->end;                                          \
		size_t _q_i = __dcseq_distance(_q_E - _q_B, (_end) - (begin)) - 2;  \
		size_t _q_p = (_q_i - 1) >> 1;				                        \
		type * _q_ib = __dcseq_at(type, _q_B, _q_E, _q_F + _q_i);           \
		type * _q_pb = __dcseq_at(type, _q_B, _q_E, _q_F + _q_p);           \
		type _q_t = *_q_ib;								                    \
		while (_q_i && (greater((&_q_t), _q_pb))) {                         \
			*_q_ib = *_q_pb;							                    \
			_q_ib = _q_pb;								                    \
			_q_i = _q_p;								                    \
			_q_p = (_q_p - 1) >> 1;						                    \
			_q_pb = __dcseq_at(type, _q_B, _q_E, _q_F + _q_p);              \
		}												                    \
		*_q_ib = _q_t;									                    \
	} while (0)

/**
 * @def dcseq_pop_heap(type, head, begin, _end, greater)
 * @brief 弹出区域中堆的顶点节点
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] greater 比较函数或宏
 					最大堆:(*i > *j) 最小堆:(*i < *j)
 * @note 使用下滤删除法将first顶点节点从堆中删除，并移动至last位置，
 	所以删除的前提是(first,last)是一个堆\n
	下滤删除算法如下：\n
	(1)交换根节点begin和最后子节点last的值.\n
	(2)对根节点begin进行下滤调整.
 * @see dcseq_make_heap, dcseq_is_heap, dcseq_push_heap, dcseq_sort_heap
 */
#define dcseq_pop_heap(type, head, begin, _end, greater) do {           \
        type * _q_B = (head)->base;                                     \
        type * _q_E = (head)->end;                                      \
        __dcseq_pop_heap(type, _q_B, _q_E, begin, _end, greater);       \
	} while (0)

#define __dcseq_pop_heap(type, base, _End, begin, _end, greater) do {       \
        type * _q_l = (type *)(_end);                                       \
        type * _q_F = (type *)(begin);		                                \
		type * _q_ib, * _q_jb, * _q_f = _q_F;                               \
		type _q_t = *_q_f;					                                \
		size_t _q_L, _q_p = 0, _q_j = 2;					                \
		__dcseq_dec(base, _End, _q_l);                                      \
		_q_L = __dcseq_distance(_End - base, _q_l - _q_F) - 1;              \
        for (; _q_j <= _q_L; _q_j = (_q_p + 1) << 1) {                      \
			_q_jb = __dcseq_at(type, base, _End, _q_F + _q_j);              \
			_q_ib = __dcseq_prev(base, _End, _q_jb);                        \
			if (greater(_q_jb, _q_ib)) {	                                \
				*_q_f = *_q_jb;			                                    \
				_q_f = _q_jb;				                                \
				_q_p = _q_j;				                                \
			} else {						                                \
				*_q_f = *_q_ib;			                                    \
				_q_f = _q_ib;				                                \
				_q_p = _q_j - 1;			                                \
			}								                                \
		}									                                \
		_q_jb = __dcseq_at(type, base, _End, _q_F + ((_q_p - 1) >> 1));     \
		if (greater(_q_l, _q_jb)) {			                                \
			*_q_f = *_q_jb;				                                    \
			*_q_jb = *_q_l;					                                \
		} else 							                                    \
			*_q_f = *_q_l;					                                \
		*_q_l = _q_t;						                                \
	} while (0)

/* 下滤调整：
 * (1)将first记为parent节点.
 * (2)若parent节点有两个子节点，则选出值最大节点，记为max，
 *    若parent节点仅一个子节点，则选唯一节点为最大节点，记为max，
 *    若parent节点没有子节点，则退出算法.
 * (3)若parent节点的值大于等于max节点的值，则退出算法.
 * (4)将max节点与parent节点的值进行交换.
 * (5)将max节点记为parent节点，转(2).
 * greater(i, j) 最大堆 => (*i > *j) 最小堆 => (*i < *j)
 */
#define __dcseq_adjust_heap(type, base, end, begin, first, last, greater) \
    do {size_t _q_j = ((first) + 1) << 1;			                    \
		type * _q_pb = __dcseq_at(type, base, end, (begin) + (first));  \
		type * _q_lb = __dcseq_at(type, base, end, (begin) + (last));   \
		type * _q_ib, * _q_jb, * _q_mb;				                    \
		type _q_t = *_q_pb;							                    \
		while (_q_j <= (last)) {					                    \
			_q_jb = __dcseq_at(type, base, end, (begin) + _q_j);        \
			_q_ib = __dcseq_prev(base, end, _q_jb);                     \
			if (greater(_q_jb, _q_ib)) {			                    \
				_q_mb = _q_jb;						                    \
				_q_j = (_q_j + 1) << 1;				                    \
			} else {								                    \
				_q_mb = _q_ib;						                    \
				_q_j = _q_j << 1;					                    \
			}										                    \
			if (greater((&_q_t), _q_mb))			                    \
				break;								                    \
			*_q_pb = *_q_mb;						                    \
			_q_pb = _q_mb;							                    \
		}											                    \
		if (_q_j == (last) + 1 && (greater(_q_lb, (&_q_t)))) {          \
			*_q_pb = *_q_lb;						                    \
			*_q_lb = _q_t;							                    \
		} else										                    \
			*_q_pb = _q_t;							                    \
	} while (0)

/**
 * @def dcseq_make_heap(type, head, begin, _end, greater)
 * @brief 将一段区域建立成最大堆
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] greater 比较函数或宏
 					最大堆:(*i > *j) 最小堆:(*i < *j)
 * @note 建立堆的算法如下：\n
	(1)将最后一个子节点的父节点记为current节点.\n
	(2)对current节点进行下滤调整.\n
	(3)若current节点向左有同一高度的节点，则将该节点设为current节点并转(2).\n
	(4)若current节点有父节点，则将其父节点记为current节点并转(2).\n
	(5)若current没有父节点(即current节点为根节点)，则退出算法.
 * @see dcseq_make_heap, dcseq_is_heap, dcseq_pop_heap, dcseq_sort_heap
 */
#define dcseq_make_heap(type, head, begin, _end, greater) do {                      \
        type * _q1_B = (head)->base;                                                \
        type * _q1_E = (head)->end;                                                 \
        type * _q1_f = (type *)(begin);                                             \
		size_t _q1_l = __dcseq_distance(_q1_E - _q1_B, (_end) - _q1_f) - 2;         \
        size_t _q1_i = ((_q1_l - 1) >> 1) + 1;                                      \
		while (_q1_i--)													            \
			__dcseq_adjust_heap(type, _q1_B, _q1_E, _q1_f, _q1_i, _q1_l, greater);  \
	} while (0)

/**
 * @def dcseq_sort_heap(type, head, begin, _end, greater)
 * @brief 对最大堆进行排序
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] greater 比较函数或宏
 					最大堆:(*i > *j) 最小堆:(*i < *j)
 * @note 排序堆的算法如下(升序)：\n
	(1)若first等于last，则排序完成.\n
	(2)对(first, last)区间进行pop_heap.\n
	(3)last递减，转(1),
 * @see dcseq_make_heap, dcseq_is_heap, dcseq_pop_heap, dcseq_push_heap
 */
#define dcseq_sort_heap(type, head, begin, _end, greater) do {                  \
        type * _q1_B = (head)->base;                                            \
        type * _q1_E = (head)->end;                                             \
		type * _q1_e = (type *)(_end);							                \
		type * _q1_f = (type *)(begin);							                \
		while (_q1_e != _q1_f) {							                    \
			__dcseq_pop_heap(type, _q1_B, _q1_E, _q1_f, _q1_e, greater);        \
			__dcseq_dec(_q1_B, _q1_E, _q1_e);                                   \
		}													                    \
	} while (0)

/**
 * @def dcseq_shuffle(type, head, begin, _end, _RAND)
 * @brief 对区域内的节点进行随机排列
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] _RAND 随机数产生器
 */
#define dcseq_shuffle(type, head, begin, _end, _RAND) do {                  \
        type * _q_F = (head)->first;                                        \
        type * _q_B = (head)->base;                                         \
        type * _q_E = (head)->end;                                          \
		type * _q_l = (type *)(_end);				                        \
		type * _q_f = (type *)(begin);					                    \
		size_t _q_j;                                                        \
		__dcseq_dec(_q_B, _q_E, _q_l);                                      \
		_q_j = __dcseq_distance(_q_E - _q_B, _q_l - _q_f) - 1;              \
		while (_q_j) {                                                      \
			size_t _q_n = (_RAND) % (_q_j + 1);                             \
			if (_q_j-- > _q_n) {							                \
                type * _q_N = __dcseq_at(type, _q_B, _q_E, _q_F + _q_n);    \
				__dcseq_iter_swap(type, _q_l, _q_N);	                    \
			}                                                               \
            __dcseq_dec(_q_B, _q_E, _q_l);                                  \
		}												                    \
	} while (0)

/**
 * @def dcseq_sort_insert(type, head, newly, greater_equal)
 * @brief 按序插入
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] newly 新节点
 * @param[in] greater_equal 比较函数或宏 升序:(*i >= *j)
 */
#define dcseq_sort_insert(type, head, begin, _end, newly, greater_equal) \
    do {type _q1_n = (newly);					        \
		type * _q1_f = (begin);		                    \
		do {                                            \
            type * _q1_B = (head)->base;                \
            type * _q1_E = (head)->end;                 \
            type * _q1_e = (_end);		                \
            while (_q1_f != _q1_e) {                    \
                if (greater_equal(_q1_f, (&_q1_n)))	    \
                    break;							    \
                __dcseq_inc(_q1_B, _q1_E, _q1_f);       \
            }										    \
		} while (0);                                    \
		dcseq_insert(type, head, _q1_f, _q1_n);         \
	} while (0)

/**
 * @def dcseq_is_sorted(type, head, begin, end, greater_equal, res)
 * @brief 判断区域是否是已排序的
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] greater_equal 比较函数或宏 升序:(*i >= *j)
 * @param[out] res 保存结果的变量
 * @see dcseq_bubble_sort, dcseq_select_sort, dcseq_insert_sort,
 		dcseq_shell_sort, dcseq_oddeven_sort, dcseq_comb_sort,
 		dcseq_cocktail_sort, dcseq_counting_sort, dcseq_pigeonhole_sort
 */
#define dcseq_is_sorted(type, head, begin, _end, greater_equal, res) \
    do {type * _q_B = (head)->base;                         \
        type * _q_E = (head)->end;                          \
        type * _q_e = (_end);                            	\
		type * _q_i = (begin);								\
		type * _q_j = __dcseq_next(_q_B, _q_E, _q_i);       \
		(res) = 1;                                          \
		while (_q_j != _q_e) {                              \
			if (!(greater_equal(_q_j, _q_i))) {             \
				(res) = 0; break;							\
			}												\
			_q_i = _q_j;                                    \
			__dcseq_inc(_q_B, _q_E, _q_j);                  \
		}													\
	} while (0)

/**
 * @def dcseq_bubble_sort(type, head, begin, _end, greater)
 * @brief 冒泡排序
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] greater 升序:(*i > *j) 降序:(*i < *j)
 * @see dcseq_is_sorted
 */
#define dcseq_bubble_sort(type, head, begin, _end, greater) do {  \
		int _q_pc = 1;									        \
		type * _q_B = (head)->base;                             \
		type * _q_E = (head)->end;                              \
		type * _q_i = (begin);                                  \
		type * _q_f = __dcseq_prev(_q_B, _q_E, _q_i);           \
		type * _q_l = __dcseq_prev(_q_B, _q_E, _end);           \
		while (_q_pc && _q_i != _q_l) {			                \
			type * _q_j = __dcseq_prev(_q_B, _q_E, _q_l);	    \
			_q_pc = 0;                                          \
			while ( _q_j != _q_f) {		                        \
                type * _q_k = __dcseq_next(_q_B, _q_E, _q_j);   \
				if (greater(_q_j, _q_k)) {				        \
                    __dcseq_iter_swap(type, _q_j, _q_k);        \
					_q_pc = 1;							        \
				}										        \
				__dcseq_dec(_q_B, _q_E, _q_j);                  \
			}											        \
			__dcseq_inc(_q_B, _q_E, _q_i);                      \
		}												        \
	} while (0)

/**
 * @def dcseq_oddeven_sort(type, head, begin, _end, greater)
 * @brief 奇偶排序
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] greater 升序:(*i > *j) 降序:(*i < *j)
 * @see dcseq_is_sorted
 */
#define dcseq_oddeven_sort(type, head, begin, _end, greater) do {   \
		int _q_pc = 1;										        \
		type * _q_B = (head)->base;                                 \
		type * _q_E = (head)->end;                                  \
		type * _q_l = __dcseq_prev(_q_B, _q_E, _end);	            \
		type * _q_f = (begin);		                                \
		while (_q_pc) {										        \
            int _q_w; _q_pc = 0;								    \
			for (_q_w = 0; _q_w < 2; ++_q_w)                        \
			    __dcseq_oddeven_scan(_q_w, type, greater);          \
		}													        \
	} while (0)

#define __dcseq_oddeven_scan(start, type, greater) do {                 \
		type * _q_i = __dcseq_advance(type, _q_B, _q_E, _q_f + start);  \
		type * _q_p = _q_i;                                             \
		while (_q_i != _q_l && _q_p != _q_l) {			                \
            type * _q_j = __dcseq_next(_q_B, _q_E, _q_i);               \
			if (greater(_q_i, _q_j)) {	                                \
				__dcseq_iter_swap(type, _q_i, _q_j);                    \
				_q_pc = 1;				                                \
			}							                                \
			_q_p = _q_j;                                                \
			_q_i = __dcseq_advance(type, _q_B, _q_E, _q_i + 2);         \
		}								                                \
	} while (0)

/**
 * @def dcseq_comb_sort(type, head, begin, _end, greater)
 * @brief 梳子排序
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] greater 升序:(*i > *j) 降序:(*i < *j)
 * @see dcseq_is_sorted
 */
#define dcseq_comb_sort(type, head, begin, _end, greater) do {              \
		int _q_swap = 1;										            \
		type * _q_B = (head)->base;                                         \
		type * _q_E = (head)->end;                                          \
		type * _q_e = (_end);						                        \
		type * _q_f = (begin);						                        \
		size_t _q_gap = __dcseq_distance(_q_E - _q_B, _q_e - _q_f) - 1;     \
		while (_q_gap > 1 || _q_swap) {							            \
			type * _q_i, * _q_j;								            \
			if (_q_gap > 1)										            \
				_q_gap = (size_t)(((double)_q_gap) / 1.247330950103979);    \
            _q_swap = 0;                                                    \
			_q_i = _q_f;										            \
			_q_j = __dcseq_advance(type, _q_B, _q_E, _q_f + _q_gap);        \
			while (_q_j != _q_e) {	                                        \
				if (greater(_q_i, _q_j)) {						            \
					__dcseq_iter_swap(type, _q_i, _q_j);                    \
					_q_swap = 1;								            \
				}												            \
				__dcseq_inc(_q_B, _q_E, _q_i);                              \
				__dcseq_inc(_q_B, _q_E, _q_j);                              \
			}													            \
		}														            \
	} while (0)

/**
 * @def dcseq_select_sort(type, head, begin, _end, greater)
 * @brief 选择排序
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] greater 升序:(*i > *j) 降序:(*i < *j)
 * @see dcseq_is_sorted
 */
#define dcseq_select_sort(type, head, begin, _end, greater) do {    \
        type * _q_B = (head)->base;                                 \
		type * _q_E = (head)->end;                                  \
        type * _q_e = (_end);	                                    \
		type * _q_i = (begin);	                                    \
		type * _q_j = __dcseq_next(_q_B, _q_E, _q_i);               \
		while (_q_j != _q_e) {                                      \
			type * _q_m = _q_i;							            \
			type * _q_k = _q_j;                                     \
			while (_q_k != _q_e) {				                    \
				if (greater(_q_m, _q_k))				            \
					_q_m = _q_k;						            \
                __dcseq_inc(_q_B, _q_E, _q_k);                      \
			}											            \
			if (_q_i != _q_m)							            \
                __dcseq_iter_swap(type, _q_m, _q_i);                \
            _q_i = _q_j;                                            \
            _q_j = __dcseq_next(_q_B, _q_E, _q_j);                  \
		}												            \
	} while (0)

/**
 * @def dcseq_insert_sort(type, head, begin, _end, greater)
 * @brief 插入排序
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] greater 升序:(*i > *j) 降序:(*i < *j)
 * @see dcseq_is_sorted
 */
#define dcseq_insert_sort(type, head, begin, _end, greater) do {        \
        type * _q_B = (head)->base;                                     \
		type * _q_E = (head)->end;                                      \
		type * _q_l = __dcseq_prev(_q_B, _q_E, _end);                   \
		type * _q_f = (begin);                                          \
		type * _q_i = _q_l;								                \
		while (_q_i != _q_f) {						                    \
			type * _q_j = _q_l;								            \
			__dcseq_dec(_q_B, _q_E, _q_i);                              \
			while (_q_j != _q_i) {					                    \
				if (greater(_q_i, _q_j)) {					            \
					type _q_t = *_q_i;						            \
                    if (_q_i > _q_j) {                                  \
                        memcpy(_q_i, _q_i + 1, sizeof(type) *           \
                               (_q_E - _q_i - 1));                      \
                        *(_q_E - 1) = *_q_B;                            \
                        memcpy(_q_B, _q_B + 1, sizeof(type) *           \
                               (_q_j - _q_B));                          \
                    } else                                              \
                        memcpy(_q_i, _q_i + 1, sizeof(type) *           \
                               (__dcseq_distance(_q_E - _q_B,           \
                                                _q_j - _q_i) - 1));     \
					*_q_j = _q_t;							            \
					break;									            \
				}											            \
				__dcseq_dec(_q_B, _q_E, _q_j);                          \
			}												            \
		}													            \
	} while (0)

/**
 * @def dcseq_shell_sort(type, head, begin, _end, greater)
 * @brief 希尔排序
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] greater 升序:(*i > *j) 降序:(*i < *j)
 * @see dcseq_is_sorted
 */
#define dcseq_shell_sort(type, head, begin, _end, greater) do {                 \
        type * _q_B = (head)->base;                                             \
		type * _q_E = (head)->end;                                              \
        type * _q_e = (_end);                                                   \
		type * _q_f = (begin);	                                                \
		size_t _q_gap = (__dcseq_distance(_q_E - _q_B, _q_e - _q_f) - 1) >> 1;  \
		for (; _q_gap; _q_gap >>= 1) {			                                \
			type * _q_i = __dcseq_advance(type, _q_B, _q_E, _q_f + _q_gap);     \
			while (_q_i != _q_e) {			                                    \
				type _q_t = *_q_i;					                            \
				type * _q_k = _q_i;                                             \
				type * _q_j = __dcseq_advance(type, _q_B, _q_E, _q_i - _q_gap); \
				while (__dcseq_exist(_q_B, _q_f, _q_e, _q_E, _q_j)              \
                        && (greater(_q_j, (&_q_t)))) {                          \
					*_q_k = *_q_j;		                                        \
					_q_k = _q_j;                                                \
					__dcseq_sub(type, _q_B, _q_E, _q_j, _q_gap);                \
				}									                            \
				*_q_k = _q_t;			                                        \
				__dcseq_inc(_q_B, _q_E, _q_i);                                  \
			}										                            \
		}											                            \
	} while (0)

/* 计数排序和鸽巢排序(仅限于有限整数序列，需事先将hole清零) */
#ifndef COUNTING_SIZE
/**
 * @def COUNTING_SIZE(_min, _max)
 * @brief 计数排序计数缓存大小
 * @param[in] _min 节点最小值
 * @param[in] _max 节点最大值
 * @def PIGEONHOLE_SIZE(_max)
 * @brief 鸽巢排序计数缓存大小
 * @param[in] _max 节点最大值
 */
#define COUNTING_SIZE(_min, _max) ((_max) - (_min) + 1)
#define PIGEONHOLE_SIZE(_max) ((_max) + 1)
#endif	/* #ifndef COUNTING_SIZE */

/**
 * @def dcseq_counting_sort(type, head, begin, _end, hole, _min, _max)
 * @brief 计数排序
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] hole 计数巢
 * @param[in] _min 节点最小值
 * @param[in] _max 节点最大值
 * @note 要求节点类型必须是整数类型
 * @par 示例:
 * @code
 	int hole[COUNTING_SIZE(10, 60)];
 	dcseq_iter(int) e = dcseq_end(&head);
 	dcseq_iter(int) b = dcseq_begin(&head);

 	// 计数排序
 	dcseq_counting_sort(int, &head, b, e, hole, 10, 60);
 * @endcode
 * @see dcseq_is_sorted, dcseq_pigeonhole_sort
 */
#define dcseq_counting_sort(type, head, begin, _end, hole, _min, _max) \
    do {type * _q_B = (head)->base;                         \
		type * _q_E = (head)->end;                          \
        type * _q_hole = (type *)(hole);				    \
		type * _q_e = (_end);	                            \
		type * _q_f = (begin);	                            \
		type _q_min = (_min);							    \
		type _q_cnt = (_max) - _q_min + 1;				    \
		type _q_i = 0;									    \
		memset(_q_hole, 0, sizeof(type) * _q_cnt);		    \
		while (_q_e != _q_f) {							    \
            __dcseq_dec(_q_B, _q_E, _q_e);                  \
			++_q_hole[*_q_e - _q_min];				        \
        }                                                   \
		for (; _q_i < _q_cnt; ++_q_i) {					    \
			size_t _q_c = _q_hole[_q_i];				    \
			while (_q_c--) {							    \
				*_q_f = _q_i;							    \
				__dcseq_inc(_q_B, _q_E, _q_f);              \
			}                                               \
		}												    \
	} while (0)

/**
 * @def dcseq_pigeonhole_sort(type, head, begin, _end, hole, _max)
 * @brief 鸽巢排序
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] hole 计数巢
 * @param[in] _max 节点最大值
 * @note 要求节点类型必须是整数类型，与计数排序不同的是，鸽巢排序
 * @par 示例:
 * @code
 	int hole[PIGEONHOLE_SIZE(50)];
 	dcseq_iter(int) e = dcseq_end(&head);
 	dcseq_iter(int) b = dcseq_begin(&head);

 	// 鸽巢排序
 	dcseq_pigeonhole_sort(int, &head, b, e, hole, 50);
 * @endcode
 * @see dcseq_is_sorted, dcseq_counting_sort
 */
#define dcseq_pigeonhole_sort(type, head, begin, _end, hole, _max) \
	dcseq_counting_sort(type, head, begin, _end, hole, 0, _max)

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_DCSEQ__ */
