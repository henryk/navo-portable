/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/linear/sequence/dlseq.h
 *
 * @brief Double-ended linear sequence list.
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_DLSEQ__
#define __NV_DLSEQ__

#include "../../../port/cdef.h"

#ifdef __cplusplus
#include <cstring>
extern "C" {
#else
#include <string.h>
#endif /* #ifdef __cplusplus */

/**
 * @defgroup 双端线形序表
 * @ingroup 顺序表
 * @{
 */

/**
 * @def dlseq_iter(type)
 * @param type 节点类型
 * @brief 定义type类型迭代器
 */
#define dlseq_iter(type) type *

/**
 * @def dlseq_head(type)
 * @brief type类型表头
 * @param type 节点类型
 * @note first为缓存基地址，
        last为末尾节点(末尾节点是空的)，
        end是缓存末尾
 * @def dlseq
 * @brief 同dlseq_head
 * @see dlseq_head
 */
#define dlseq_head(type) \
    struct { type * first; type * last; type * end; type * base; }
#define dlseq dlseq_head

/**
 * @def DLSEQ_HEAD(type, name, ar)
 * @brief 定义一个名为name的表头，并对其初始化
 * @param type 节点类型
 * @param name 表头名称
 * @param arr 缓存数组名
 * @def DLSEQ
 * @brief 同DLSEQ_HEAD
 * @see DLSEQ_HEAD
 */
#define DLSEQ_HEAD(type, name, arr) \
    dlseq_head(type) name = {arr, arr, &arr[count_of(arr)], arr}
#define DLSEQ DLSEQ_HEAD

/**
 * @def dlseq_pr1(type)
 * @brief 单参数回调函数指针类型
 * @param type 节点类型
 * @def dlseq_pr2(type)
 * @brief 双参数回调函数指针类型
 * @param type 节点类型
 */
#define dlseq_pr1(type) int (*)(type *)
#define dlseq_pr2(type) int (*)(type *, type *)

#define DLSEQ_CHECK_PASS 		0 /* 正常 */
#define DLSEQ_CHECK_INVALID 	1 /* 无效缓存 */
#define DLSEQ_CHECK_OVERFLOW 	2 /* 溢出 */

static inline int __dlseq_check(char* first, char* last,
                                char* end, char* base)
{
	if (!base || base >= end || first >= end)
		return DLSEQ_CHECK_INVALID;
	else if (last > end || first < base)
		return DLSEQ_CHECK_OVERFLOW;
    else
        return DLSEQ_CHECK_PASS;
}

/**
 * @def dlseq_check(head)
 * @brief 检查表
 * @param[in] head 表头
 * @return int 返回结果
 * @retval DLSEQ_CHECK_PASS 0 正常
 * @retval DLSEQ_CHECK_INVALID 1 无效缓存
 * @retval DLSEQ_CHECK_OVERFLOW 2 溢出
 */
#define dlseq_check(head) \
    __dlseq_check((char*)((head)->first),   \
                  (char*)((head)->last),    \
                  (char*)((head)->end),     \
                  (char*)((head)->base))

/**
 * @def dlseq_reset(head, _base, _first, _last, _end)
 * @brief 重置表头
 * @param[out] head 目标表头
 * @param[in] _base 缓存基址
 * @param[in] _first 表首节点
 * @param[in] _last 表尾节点
 * @param[in] _end 缓存终止处
 */
#define dlseq_reset(head, _base, _first, _last, _end) \
    do {(head)->base = (_base);         \
        (head)->first = (_first);       \
        (head)->last = (_last);         \
        (head)->end = (_end);           \
    } while (0)

/**
 * @def dlseq_init(type, head, base, size, _first)
 * @brief 初始化表
 * @param[in] type 节点类型
 * @param[out] head 目标表头
 * @param[in] base 缓存基址
 * @param[in] size 缓存长度(节点容量)
 * @param[in] _first 起始位置
 * @see DLSEQ_INIT
 */
#define dlseq_init(type, head, base, size, _first)  \
    dlseq_reset(head, (type *)(base),               \
                (type *)(base) + (_first),          \
                (type *)(base) + (_first),          \
                (type *)(base) + (size))            \

/**
 * @def dlseq_init_serial(type, head, base, size, cnt)
 * @brief 作为序列来初始化表
 * @param[in] type 节点类型
 * @param[out] head 目标表头
 * @param[in] base 缓存基址
 * @param[in] size 缓存长度(节点容量)
 * @param[in] cnt 序列长度
 * @par 示例:
 * @code
	//将dclnk链表通过序列化来进行堆排序
	dclnk_head* alives = &bulltes.alive_list;
	if (dclnk_sortable(alives)) {
		Bullet* buf[MAX_BULLETS];
		DLSEQ_INIT_SERIAL(int, &seq_head, buf, list_cnt);
		dclnk_serialize(alives, buf, Bullet, alive_node);
		dlseq_make_heap(Bullet, dlseq_begin(&head),
						dlseq_end(&seq_head),
						CompareBulletAlive);
		dlseq_sort_heap(Bullet, dlseq_begin(&head),
						dlseq_end(&seq_head),
						CompareBulletAlive);
		dclnk_deserialize(alives, dlseq_base(&seq_head),
						dlseq_count(&seq_head),
						Bullet, alive_node);
	}
 * @endcode
 * @see DLSEQ_INIT_SERIAL
 */
#define dlseq_init_serial(type, head, base, size, cnt)  \
    dlseq_reset(head, (type *)(base), (type *)(base),   \
                (type *)(base) + (cnt),                 \
                (type *)(base) + (size))

/**
 * @def DLSEQ_INIT(type, head, arr, _first)
 * @brief 初始化表
 * @param[in] type 节点类型
 * @param[out] head 目标表头
 * @param[in] arr 缓存数组
 * @param[in] _first 起始位置
 * @par 示例:
 * @code
 	dlseq_head(int) h;
 	int buf[100];

 	DLSEQ_INIT(int, &h, buf, 0);
 * @endcode
 * @see dlseq_init
 * @def DLSEQ_INIT_SERIAL(type, head, arr, cnt)
 * @brief 初始化序列
 * @param[in] type 节点类型
 * @param[out] head 目标表头
 * @param[in] arr 缓存数组
 * @param[in] cnt 序列长度
 * @par 示例:
 * @code
 	dlseq_head(DATA*) h;
 	DATA* buf[100];

	dclnk_serialize(&lst, buf, DATA, node);
 	DLSEQ_INIT_SERIAL(int, &h, buf, dclnk_count(&lst));
 * @endcode
 * @see dlseq_init_serial
 */
#define DLSEQ_INIT(type, head, arr, _first) \
    dlseq_init(type, head, arr, count_of(arr), _first)
#define DLSEQ_INIT_SERIAL(type, head, arr, cnt) \
	dlseq_init_serial(type, head, arr, count_of(arr), cnt)

/**
 * @def dlseq_clear(head)
 * @brief 清空表
 * @param[out] head 表头
 */
#define dlseq_clear(head) \
    ((head)->last = (head)->first = (head)->base)

/**
 * @def dlseq_base(head)
 * @brief 获得表的缓存基址
 * @param[in] head 表头
 * @return char* 返回基地址
 */
#define dlseq_base(head) ((head)->base)

/**
 * @def dlseq_count(head)
 * @brief 获取表的节点总数
 * @param[in] head 表头
 * @return size_t 返回节点总数
 */
#define dlseq_count(head) \
    ((size_t)((head)->last - (head)->first))

/**
 * @def dlseq_size(head)
 * @brief 获取表的缓存长度
 * @param[in] head 表头
 * @return size_t 返回缓存长度
 * @see dlseq_capacity
 */
#define dlseq_size(head) \
    ((size_t)((head)->end - (head)->base))

/**
 * @def dlseq_capacity(head)
 * @brief 获取表的节点容量
 * @param[in] head 表头
 * @return size_t 返回节点容量
 * @see dlseq_size
 */
#define dlseq_capacity(head) dlseq_size(head)

/**
 * @def dlseq_remain(head)
 * @brief 获取表的剩余容量
 * @param[in] head 表头
 * @return size_t 返回剩余容量
 * @see dlseq_full, dlseq_empty
 */
#define dlseq_remain(head) \
    (dlseq_size(head) - dlseq_count(head))

/**
 * @def dlseq_front_remain(head)
 * @brief 获取表的首部剩余容量
 * @param[in] head 表头
 * @return size_t 返回剩余容量
 * @see dlseq_back_remain
 */
#define dlseq_front_remain(head) \
    ((size_t)((head)->first - (head)->base))

/**
 * @def dlseq_back_remain(head)
 * @brief 获取表的尾部剩余容量
 * @param[in] head 表头
 * @return size_t 返回剩余容量
 * @see dlseq_front_remain
 */
#define dlseq_back_remain(head) \
    ((size_t)((head)->end - (head)->last))

/**
 * @def dlseq_empty(head)
 * @brief 判断表是否为空
 * @param[in] head 表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @see dlseq_full, dlseq_remain
 */
#define dlseq_empty(head) ((head)->first >= (head)->last)

/**
 * @def dlseq_full(head)
 * @brief 判断表是否已满
 * @param[in] head 表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @see dlseq_empty, dlseq_remain
 */
#define dlseq_full(head) \
    ((head)->last >= (head)->end && \
     (head)->first <= (head)->base)

/**
 * @def dlseq_singular(head)
 * @brief 判断表是否只有单个节点
 * @param[in] head 表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 */
#define dlseq_singular(head) \
    ((head)->last == ((head)->first + 1))

/**
 * @def dlseq_sortable(head)
 * @brief 判断表是否可以被排序
 * @param[in] head 表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 */
#define dlseq_sortable(head) \
    ((head)->last > ((head)->first + 1))

/**
 * @def dlseq_insertable(head, cnt)
 * @brief 判断表是否可以插入一定数量的节点
 * @param[in] head 表头
 * @param[in] cnt 插入的节点数
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 */
#define dlseq_insertable(head, cnt) \
    (dlseq_size(head) >= (dlseq_count(head) + (cnt)))

/**
 * @def dlseq_serialable(head)
 * @brief 判断表是否可以序列化
 * @param[in] head 表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 */
#define dlseq_serialable(head) \
    ((head)->last > (head)->first)

/**
 * @def dlseq_copyable(dst, src)
 * @brief 判断源表是否可以深拷贝到目标表
 * @param[in] dst 目标表头
 * @param[in] src 源表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 */
#define dlseq_copyable(dst, src) \
    __dlseq_copyable((char*)(dst),              \
                     (char*)(src),              \
                     (char*)((src)->first),     \
                     (char*)((src)->last),      \
                     (char*)((dst)->base),      \
                     (char*)((dst)->end))

static inline int __dlseq_copyable(char* dst, char* src,
                                   char* s_first, char* s_last,
                                   char* d_base, char* d_end)
{
	return (dst != src && (s_last > s_first) &&
         (d_end - d_base) >= (s_last - s_first));
}

/**
 * @def dlseq_at(head, index)
 * @brief 根据正向索引获取迭代器
 * @param[in] index 指定索引
 * @return dlseq_iter 返回对应的迭代器
 */
#define dlseq_at(head, index) ((head)->first + (index))

/**
 * @def dlseq_reverse_at(head, index)
 * @brief 根据逆向索引获取迭代器
 * @param[in] head 表头
 * @param[in] index 指定索引
 * @return dlseq_iter 返回对应的迭代器
 */
#define dlseq_reverse_at(head, index) \
    ((head)->last - ((index) + 1))

/**
 * @def dlseq_index_of(head, itr)
 * @brief 获取迭代器对应的正向索引
 * @param[in] itr 迭代器
 * @return dlseq_iter 返回正向索引
 */
#define dlseq_index_of(head, itr) ((itr) - (head)->first)

/**
 * @def dlseq_reverse_index_of(head, itr)
 * @brief 获取迭代器对应的逆向索引
 * @param[in] head 表头
 * @param[in] itr 迭代器
 * @return dlseq_iter 返回逆向索引
 */
#define dlseq_reverse_index_of(head, itr) \
    ((head)->last - ((itr) + 1))

/**
 * @def dlseq_exist(head, itr)
 * @brief 判断迭代器对应的节点是否存在于表中
 * @param[in] head 表头
 * @param[in] itr 迭代器
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 */
#define dlseq_exist(head, itr) \
    __dlseq_exist((char*)((head)->first),   \
                  (char*)((head)->last),    \
                  (char*)(itr))

static inline int __dlseq_exist(char* first, char* last, char* itr)
{
	return itr >= first && itr < last;
}

/**
 * @def dlseq_distance(first, last)
 * @brief 计算两个节点的迭代距离
 * @param[in] first 首节点
 * @param[in] last 尾节点
 * @note 以索引为例，2到4的距离是4 (4-2+1=3)
 * @return size_t 返回距离
 */
#define dlseq_distance(first, last) ((size_t)((last) - (first)) + 1)

/**
 * @def dlseq_insert(type, head, pos, newly)
 * @brief 在指定位置插入节点
 * @param[in] type 节点类型
 * @param[in,out] head 表头
 * @param[in,out] pos 插入位置
 * @param[in] newly 新节点
 * @note 插入完成之后，则pos迭代器失效
 */
#define dlseq_insert(type, head, pos, newly) do {   \
		type * _q_p = (pos);                        \
		type * _q_f = (head)->first;                \
		type * _q_b = (head)->base;                 \
		if (_q_f > _q_b) {                          \
            memcpy(_q_f - 1, _q_f, sizeof(type) *   \
                ((_q_p--) - ((head)->first--)));    \
		} else {                                    \
            memmove(_q_p + 1, _q_p, sizeof(type) *  \
                (((head)->last++) - _q_p));          \
		}                                           \
		*_q_p = (newly);                            \
	} while (0)

/**
 * @def dlseq_erase(type, head, pos)
 * @brief 删除指定位置的节点
 * @param[in] type 节点类型
 * @param[in,out] head 表头
 * @param[in] pos 删除位置
 */
#define dlseq_erase(type, head, pos) do {                       \
		type * _q_p = (pos);                                    \
		type * _q_f = (head)->first;                            \
		if (((_q_p - _q_f) << 2) <= ((head)->last - _q_f)) {    \
            memmove(_q_f + 1, _q_f, sizeof(type) *              \
                (_q_p - ((head)->first++)));                    \
		} else {                                                \
            memcpy(_q_p, _q_p + 1, sizeof(type) *               \
                ((--(head)->last) - _q_p));                     \
		}                                                       \
	} while (0)

/**
 * @def dlseq_inserts(type, head, pos, src, cnt)
 * @brief 在指定位置连续插入多个节点
 * @param[in] type 节点类型
 * @param[in,out] head 表头
 * @param[in,out] pos 插入位置
 * @param[in] src 新节点缓存
 * @param[in] cnt 新节点数
 * @note 插入完成之后，则pos迭代器失效
 * @par 示例:
 * @code
 	dlseq_iter(int) p = dlseq_at(&head, 8);
 	// 判断是否可以插入cnt个节点
 	if (dlseq_insertable(&head, cnt)) {
 		dlseq_inserts(int, &head, p, buf, cnt);
 	}
 * @endcode
 */
#define dlseq_inserts(type, head, pos, src, cnt) do {                   \
        size_t _q_c = (cnt);                                            \
        type * _q_p = (pos);                                            \
        type * _q_f = (head)->first;                                    \
        type * _q_b = (head)->base;                                     \
        if (_q_f >= _q_b + _q_c) {                                      \
            memcpy(_q_f - _q_c, _q_f, sizeof(type) * (_q_p - _q_f));    \
            memcpy(_q_p - _q_c, src, sizeof(type) * _q_c);              \
            (head)->first -= _q_c;                                      \
        } else if (_q_f > _q_b) {                                       \
            memcpy(_q_b, _q_f, sizeof(type) * (_q_p - _q_f));           \
            memmove(_q_p + (_q_c - (_q_f - _q_b)), _q_p,                \
                sizeof(type) * ((head)->last - _q_p));                  \
            memcpy(_q_p + (_q_p - _q_f), src, sizeof(type) * _q_c);     \
            (head)->first = _q_b;                                       \
            (head)->last += (_q_c - (_q_f - _q_b));                     \
        } else {                                                        \
            memmove(_q_p + _q_c, _q_p, sizeof(type) *                   \
                ((head)->last - _q_p));                                 \
            memcpy(_q_p, src, sizeof(type) * _q_c);                     \
            (head)->last += _q_c;                                       \
        }                                                               \
	} while (0)

/**
 * @def dlseq_erases(type, head, begin, end)
 * @brief 连续删除指定位置的多个节点
 * @param[in] type 节点类型
 * @param[in,out] head 表头
 * @param[in,out] begin 起始位置
 * @param[in,out] end 终止位置
 */
#define dlseq_erases(type, head, _begin, _end) do {             \
		type * _q_e = (_end);                                   \
		type * _q_f = (_begin);                                 \
		size_t _q_fr = _q_f - (head)->first;                    \
		size_t _q_br = (head)->end - _q_e;                      \
		size_t _q_c = _q_e - _q_f;                              \
		if ((_q_fr << 1) <= _q_br) {                            \
            memmove((head)->first + _q_fr, (head)->first,       \
                sizeof(type) * _q_fr);                          \
            (head)->first += _q_c;                              \
		} else {                                                \
            memcpy(_q_e - _q_c, _q_e, sizeof(type) * _q_br);    \
            (head)->last -= _q_c;                               \
		}                                                       \
	} while (0)

/**
 * @def dlseq_splice(type, dst, pos, src, _first, _last)
 * @brief 移接一段序列[first, last]到目标位置。
 			这可以发生在同一个序表中，也可以在不同的序表之间
 * @param[in] type 节点类型
 * @param[in,out] dst 目标表头
 * @param[in,out] pos 指定位置
 * @param[in,out] src 源表头
 * @param[in,out] _first 起始位置，包括_first
 * @param[in,out] _last 末尾位置，包括_last
 */
#define dlseq_splice(type, dst, pos, src, _first, _last) do {       \
        type * _q1_l = (_last);                                     \
        type * _q1_f = (_first);                                    \
        dlseq_inserts(type, dst, pos, _q1_f, _q1_l - _q1_f + 1);    \
        dlseq_erases(type, src, _q1_f, _q1_l + 1);                  \
	} while (0)

/**
 * @def dlseq_appends(type, head, src, cnt)
 * @brief 在表尾添加多个节点
 * @param[in] type 节点类型
 * @param[in,out] head 表头
 * @param[in] src 新节点缓存
 * @param[in] cnt 新节点数
 * @par 示例:
 * @code
 	// 判断是否可以插入cnt个节点
 	if (dlseq_insertable(&head, cnt)) {
 		dlseq_appends(int, &head, buf, cnt);
 	}
 * @endcode
 * @see dlseq_append
 */
#define dlseq_appends(type, head, src, cnt) do {        \
		size_t _q_c = (cnt);							\
		memcpy((head)->last, src, sizeof(type) * _q_c);	\
		(head)->last += _q_c;					        \
	} while (0)

/**
 * @def dlseq_append(head, newly)
 * @brief 在表尾添加新节点
 * @param[in,out] head 表头
 * @param[in] newly 新节点
 * @see dlseq_appends
 */
#define dlseq_append(head, newly) \
    (*((head)->last++) = (newly))

/**
 * @def dlseq_copy(type, dst, src)
 * @brief 深拷贝源表的节点到目标表
 * @param[in] type 节点类型
 * @param[out] dst 目标表
 * @param[in] src 源表
 * @par 示例:
 * @code
 	// 判断是否可以从src拷贝到dst
 	if (dlseq_copyable(&dst, &src)) {
 		dlseq_copy(int, &dst, &src);
 	}
 * @endcode
 * @see dlseq_copyable
 */
#define dlseq_copy(type, dst, src) do {                         \
		size_t _q_c = dlseq_count(src);                         \
		memcpy((dst)->base, (src)->first, sizeof(type) * _q_c); \
		(dst)->last = ((dst)->first = (dst)->base) + _q_c;		\
	} while (0)

/**
 * @def dlseq_push_front(head)
 * @brief 获取指向表首插入节点位置的迭代器
 * @param[in,out] head 表头
 * @return dlseq_iter 返回迭代器
 * @par 示例:
 * @code
 	// 在表首插入i
 	*dlseq_push_front(&head) = i;
 * @endcode
 * @see dlseq_pop_front, dlseq_push_back, dlseq_pop_back
 */
#define dlseq_push_front(head) (--(head)->first)

/**
 * @def dlseq_push_back(head)
 * @brief 获取指向表尾插入节点位置的迭代器
 * @param[in,out] head 表头
 * @return dlseq_iter 返回迭代器
 * @par 示例:
 * @code
 	// 在表尾插入i
 	*dlseq_push_back(&head) = i;
 * @endcode
 * @see dlseq_pop_back, dlseq_push_front, dlseq_pop_front
 */
#define dlseq_push_back(head) ((head)->last++)

/**
 * @def dlseq_pop_front(head)
 * @brief 获取指向表首节点位置的迭代器
 * @param[in,out] head 表头
 * @return dlseq_iter 返回迭代器
 * @par 示例:
 * @code
 	// 移除表首节点
 	int d = *dlseq_pop_front(&head);
 * @endcode
 * @see dlseq_push_front, dlseq_push_back, dlseq_pop_back
 */
#define dlseq_pop_front(head) ((head)->first++)

/**
 * @def dlseq_pop_back(head)
 * @brief 获取指向表尾节点位置的迭代器
 * @param[in,out] head 表头
 * @return dlseq_iter 返回迭代器
 * @par 示例:
 * @code
 	// 移除表尾节点
 	int d = *dlseq_pop_back(&head);
 * @endcode
 * @see dlseq_push_back, dlseq_push_front, dlseq_pop_front
 */
#define dlseq_pop_back(head) (--(head)->last)

/**
 * @def dlseq_iter_replace(victim, newly)
 * @brief 用新节点替换目标节点
 * @param[in,out] victim 受害目标节点
 * @param[in,out] newly 新节点
 */
#define dlseq_iter_replace(victim, newly) (*(victim) = (newly))

#define __dlseq_iter_swap(type, a, b) do {\
	type _q_t = *(a); *(a) = *(b); *(b) = _q_t; } while (0)

/**
 * @def dlseq_iter_swap(type, a, b)
 * @brief 用新节点替换目标节点
 * @param[in] type 节点类型
 * @param[in,out] a 第一个节点
 * @param[in,out] b 第二个节点
 */
#define dlseq_iter_swap(type, a, b) do {        \
		type * _q_b = (b); type * _q_a = (a);	\
		__dlseq_iter_swap(type, _q_a, _q_b);    \
	} while (0)

/**
 * @def dlseq_replace(victim, newly)
 * @brief 用新表头替换目标表头
 * @param[in,out] victim 受害目标表头
 * @param[out] newly 新表头
 */
#define dlseq_replace(victim, newly) (*(victim) = *(newly))

/**
 * @def dlseq_swap(type, a, b)
 * @brief 交换两个表头
 * @param[in] type 节点类型
 * @param[in,out] a 第一个表头
 * @param[in,out] b 第二个表头
 */
#define dlseq_swap(type, a, b) do { \
        dlseq_head(type) t = *(a);  \
        *(a) = *(b); *(b) = t;      \
    } while (0)

/**
 * @def dlseq_front(head)
 * @brief 得到表首节点
 * @return dlseq_iter 返回节点
 * @see dlseq_back
 */
#define dlseq_front(head) ((head)->first)

/**
 * @def dlseq_back(head)
 * @brief 得到表首节点
 * @param[in] head 表头
 * @return dlseq_iter 返回节点
 * @see dlseq_front
 */
#define dlseq_back(head) ((head)->last - 1)

/**
 * @def dlseq_begin(head)
 * @brief 得到指向正向起始位置的迭代器
 * @return dlseq_iter 返回迭代器
 * @par 示例:
 * @code
 	// 遍历序表
 	dlseq_iter(int) e = dlseq_end(&head);
 	dlseq_iter(int) i = dlseq_begin(&head);
 	while (i < e) {
 		printf("[%d]=%d\n", dlseq_index_of(&head, i), *i);
 		dlseq_inc(i);
 	}
 * @endcode
 * @see dlseq_end, dlseq_rbegin, dlseq_rend
 */
#define dlseq_begin(head) dlseq_front(head)

/**
 * @def dlseq_end(head)
 * @brief 得到指向正向终止位置的迭代器
 * @param[in] head 表头
 * @return dlseq_iter 返回迭代器
 * @see dlseq_begin, dlseq_rbegin, dlseq_rend
 */
#define dlseq_end(head) ((head)->last)

/**
 * @def dlseq_end_of(itr)
 * @brief 得到指向当前节点的正向终止位置的迭代器
 * @param[in] last 当前节点
 * @return dlseq_iter 返回终止迭代器
 * @see dlseq_begin, dlseq_end
 */
#define dlseq_end_of(itr) ((itr) + 1)

/**
 * @def dlseq_rbegin(head)
 * @brief 得到指向逆向起始位置的迭代器
 * @param[in] head 表头
 * @return dlseq_iter 返回迭代器
 * @see dlseq_end, dlseq_begin, dlseq_rend
 */
#define dlseq_rbegin(head) dlseq_back(head)

/**
 * @def dlseq_rend(head)
 * @brief 得到指向逆向起始位置的迭代器
 * @return dlseq_iter 返回迭代器
 * @see dlseq_end, dlseq_rbegin, dlseq_rend
 */
#define dlseq_rend(head) ((head)->first - 1)

/**
 * @def dlseq_rend_of(itr)
 * @brief 得到指向当前节点的逆向终止位置的迭代器
 * @param[in] last 当前节点
 * @return dlseq_iter 返回终止迭代器
 * @see dlseq_rbegin, dlseq_rend
 */
#define dlseq_rend_of(itr) ((itr) - 1)

/**
 * @def dlseq_next(itr)
 * @brief 获取当前节点的后继节点
 * @param[in] itr 当前节点
 * @return dlseq_iter 返回后继节点
 * @see dlseq_prev
 */
#define dlseq_next(itr) ((itr) + 1)

/**
 * @def dlseq_prev(itr)
 * @brief 获取当前节点的前驱节点
 * @param[in] itr 当前节点
 * @return dlseq_iter 返回前驱节点
 * @see dlseq_next
 */
#define dlseq_prev(itr) ((itr) - 1)

/**
 * @def dlseq_advance(cur, dist)
 * @brief 给迭代器增加一段距离或减小一段距离
 * @param[in] cur 迭代器
 * @param[in] dist 增加的距离。正数时，表示增加；负数时，表示减小
 * @return dlseq_iter 返回新迭代器
 */
#define dlseq_advance(cur, dist) ((cur) + (dist))

/**
 * @def DLSEQ_NOT_END(itr, end)
 * @brief 迭代结束条件
 * @par 示例:
 * @code
 	dlseq_iter(int) e = dlseq_end(&head);
 	dlseq_iter(int) i = dlseq_begin(&head);
 	while (DLSEQ_NOT_END(i, e)) {
 		......
 		dlseq_inc(i);
 	}
 * @endcode
 * @def DLSEQ_NOT_REND(itr, rend)
 * @brief 逆向迭代结束条件
 * @par 示例:
 * @code
 	dlseq_iter(int) e = dlseq_rend(&head);
 	dlseq_iter(int) i = dlseq_rbegin(&head);
 	while (DLSEQ_NOT_REND(i, e)) {
 		......
 		dlseq_dec(i);
 	}
 * @endcode
 */
#define DLSEQ_NOT_END(itr, end)     ((itr) < (end))
#define DLSEQ_NOT_REND(itr, rend)	((itr) > (rend))

/**
 * @def dlseq_inc(itr)
 * @brief 迭代器递增
 * @param[in,out] 迭代器
 * @def dlseq_dec(itr)
 * @brief 迭代器递减
 * @param[in,out] 迭代器
 * @def dlseq_add(itr, cnt)
 * @brief 迭代器增加
 * @param[in,out] 迭代器
 * @param[in] cnt 增加量
 * @def dlseq_sub(itr, cnt)
 * @brief 迭代器减小
 * @param[in,out] 迭代器
 * @param[in] cnt 减小量
 * @def dlseq_inc_later(itr)
 * @brief 迭代器后递增
 * @param[in,out] 迭代器
 * @def dlseq_dec_later(itr)
 * @brief 迭代器后递减
 * @param[in,out] 迭代器
 * @def dlseq_add_later(itr, cnt)
 * @brief 迭代器后增加
 * @param[in,out] 迭代器
 * @param[in] cnt 增加量
 * @def dlseq_sub_later(itr, cnt)
 * @brief 迭代器后减少
 * @param[in,out] 迭代器
 * @param[in] cnt 减小量
 */
#define dlseq_inc(itr)				(++(itr))
#define dlseq_dec(itr)				(--(itr))
#define dlseq_add(itr, cnt)			((itr) += (cnt))
#define dlseq_sub(itr, cnt)			((itr) -= (cnt))
#define dlseq_inc_later(itr)		((itr)++)
#define dlseq_dec_later(itr)		((itr)--)
#define dlseq_add_later(itr, cnt)	(dlseq_add(itr, cnt) - (cnt))
#define dlseq_sub_later(itr, cnt)	(dlseq_sub(itr, cnt) + (cnt))

/**
 * @def dlseq_foreach(type, begin, end, fn)
 * @brief 正向遍历begin到end的节点，
        并为每一个节点调用回调函数或宏
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] fn 回调函数或宏
 */
#define dlseq_foreach(type, begin, end, fn) \
    do {type * _q_e = (type *)(end);        \
        type * _q_f = (type *)(begin);      \
		while (_q_f < _q_e) {				\
			fn(_q_f);						\
			++_q_f;							\
		}									\
	} while(0)

/**
 * @def dlseq_reverse_foreach(type, rbegin, rend, fn)
 * @brief 逆向遍历rbegin到rend的节点，
        并为每一个节点调用回调函数或宏
 * @param[in] type 节点类型
 * @param[in] rbegin 逆向起始位置
 * @param[in] rend 逆向终止位置
 * @param[in] fn 回调函数或宏
 */
#define dlseq_reverse_foreach(type, rbegin, rend, fn) \
    do {type * _q_e = (type *)(rend);       \
        type * _q_f = (type *)(rbegin);     \
		while (_q_f > _q_e) {				\
			fn(_q_f);						\
			--_q_f;							\
		}									\
	} while(0)

/**
 * @def dlseq_search(type, begin, end, equal, var, res)
 * @brief 顺序查找区域内的指定节点
 * @param[in] type 节点类型
 * @param[in,out] begin 起始位置
 * @param[in,out] end 终止位置
 * @param[in] equal 比较函数或宏。相等时返回1，不相等返回0
 * @param[in] var 搜索的键值，常量，变量均可
 * @param[out] res 保存搜索结果的变量，类型是节点指针。
                结果为NULL表示找不到节点
 * @see dlseq_binary_search
 */
#define dlseq_search(type, begin, end, equal, var, res) \
    do {type _q_t = (var);						\
		type * _q_e = (end);					\
		type * _q_f = (begin);					\
		(res) = NULL;							\
		while (_q_f < _q_e) {					\
			type * _q_cb = _q_f++;			    \
			if (equal(_q_cb, (&_q_t))) {		\
				(res) = _q_cb;					\
				break;							\
			}									\
		}										\
	} while (0)

/**
 * @def dlseq_binary_search(type, begin, end, greater, var, res)
 * @brief 二分查找区域内的指定节点
 * @param[in] type 节点类型
 * @param[in,out] begin 起始位置
 * @param[in,out] end 终止位置
 * @param[in] greater 比较函数或宏。升序:(*i > *j) 降序:(*i < *j)
 * @param[in] var 搜索的键值，常量，变量均可
 * @param[out] res 保存搜索结果的变量，类型是节点指针。结果为NULL表示找不到节点
 * @note 必须保证，表在查找之前是升序的
 * @see dlseq_search, dlseq_is_sorted
 */
#define dlseq_binary_search(type, begin, end, greater, var, res) \
    do {type _q_t = (var);									\
		type * _q_e = (end);							    \
		type * _q_f = (begin);							    \
		(res) = NULL;										\
		while (_q_f < _q_e) {								\
			type * _q_m = _q_f + ((_q_e - _q_f) >> 1);	    \
			if (greater(_q_m, (&_q_t))) {					\
				_q_e = _q_m;								\
			} else if (greater((&_q_t), _q_m)) {			\
				_q_f = _q_m + 1;							\
			} else {										\
				(res) = _q_m;								\
				break;										\
			}												\
		}													\
	} while (0)

/* 反转序表的两种方式：
 * (1) 直接使用 dlseq_reverse(元素类型, head); (推荐)
 * (2) 按照如下方式迭代
 *     dlseq_iter(int) i = dlseq_vbegin(head);
 *     dlseq_iter(int) j = dlseq_rvbegin(head);
 *     dlseq_iter(int) e = dlseq_vend(head);
 *     while (i < e) {
 *			dlseq_vswap(元素类型, i, j);
 *     }
 *     当迭代完成时，反转完成
 */

/**
 * @def dlseq_vbegin(head)
 * @brief 得到正反转的起始迭代器
 * @return dlseq_iter 返回正反转迭代器
 * @par 示例:
 * @code
 	// 反转表
 	dlseq_iter(int) ve = dlseq_vend(&head);
 	dlseq_iter(int) vi = dlseq_vbegin(&head);
 	dlseq_iter(int) vr = dlseq_rvbegin(&head);
 	while (vi < ve) {
 		dlseq_vswap(int, vi, vr);
 	}
 * @endcode
 * @see dlseq_rvbegin, dlseq_vend, dlseq_vswap, dlseq_reverse
 * @def dlseq_rvbegin(head)
 * @brief 得到逆反转的起始迭代器
 * @param[in] head 表头
 * @return dlseq_iter 返回迭代器
 * @see dlseq_vbegin, dlseq_vend, dlseq_vswap, dlseq_reverse
 */
#define dlseq_vbegin(head)		dlseq_begin(head)
#define dlseq_rvbegin(head)		dlseq_rbegin(head)

/**
 * @def dlseq_vend(head)
 * @brief 得到用于反转的终止位置的迭代器
 * @param[in] head 表头
 * @return dlseq_iter 返回迭代器
 * @par 示例:
 * @code
 	// 反转表
 	dlseq_iter(int) ve = dlseq_vend(&head);
 	dlseq_iter(int) vi = dlseq_vbegin(&head);
 	dlseq_iter(int) vr = dlseq_rvbegin(&head);
 	while (vi < ve) {
 		dlseq_vswap(int, vi, vr);
 	}
 * @endcode
 * @see dlseq_vbegin, dlseq_rvbegin, dlseq_vswap, dlseq_reverse
 */
#define dlseq_vend(head) \
    ((head)->first + (dlseq_count(head) >> 1))

#define dlseq_vnext(i, r) do { ++(i); --(r); } while (0)

/**
 * @def dlseq_vswap(type, i, r)
 * @brief 交换反转操作
 * @param[in] type 节点类型
 * @param[in,out] i 正反转迭代器
 * @param[in,out] r 逆反转迭代器
 * @par 示例:
 * @code
 	// 反转表
 	dlseq_iter(int) ve = sllnk_vend(&head);
 	dlseq_iter(int) vi = sllnk_vbegin(&head);
 	dlseq_iter(int) vr = sllnk_rvbegin(&head);
 	while (vi < ve) {
 		sllnk_vswap(int, vi, vr);
 	}
 * @endcode
 * @see sllnk_vbegin, sllnk_vend, sllnk_reverse
 */
#define dlseq_vswap(type, i, r) do {    \
		__dlseq_iter_swap(type, i, r);  \
        dlseq_vnext(i, r);              \
    } while (0)

/**
 * @def dlseq_reverse(type, head)
 * @brief 反转表
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @see dlseq_vbegin, dlseq_rvbegin, dlseq_vend, dlseq_vswap
 */
#define dlseq_reverse(type, head) do {          \
		type * _q_e = dlseq_vend(head);			\
		type * _q_i = dlseq_vbegin(head);		\
		type * _q_j = dlseq_rvbegin(head);		\
		while (_q_i < _q_e)						\
			dlseq_vswap(type, _q_i, _q_j);      \
	} while (0)

/**
 * @def dlseq_serialize(type, head, buf)
 * @brief 序列化
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[out] buf 序列缓存
 * @see 若要进行反序列化还原表，请使用dlseq_init_serial, DLSEQ_INIT_SERIAL
 */
#define dlseq_serialize(type, head, buf) \
    memcpy(buf, (head)->first, sizeof(type) * dlseq_count(head))

/**
 * @def dlseq_fill(type, begin, end, val)
 * @brief 用val填充区域
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] val 填充值
 * @see dlseq_generate
 */
#define dlseq_fill(type, begin, end, val) \
    do {type _q_v = (val);      \
		type * _q_e = (end);    \
		type * _q_f = (begin);  \
		while (_q_f < _q_e)     \
			*_q_f++ = _q_v;     \
	} while (0)

/**
 * @def dlseq_generate(type, begin, end, generator)
 * @brief 使用输入产生器来填充区域
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] generator 输入产生器
 * @see dlseq_fill
 */
#define dlseq_generate(type, begin, end, generator) \
    do {type * _q_e = (end);        \
		type * _q_f = (begin);      \
		while (_q_f < _q_e)         \
			*_q_f++ = (generator);  \
	} while (0)

/**
 * @def dlseq_is_heap(type, begin, end, greater, res)
 * @brief 判断区域是否是一个最大堆
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] greater 比较函数或宏
 					最大堆:(*i > *j) 最小堆:(*i < *j)
 * @param[out] res 保存结果的变量，1为真，0为假
 * @note 方法:逐个比较每个非根节点与其父节点的值
 * @see dlseq_make_heap, dlseq_push_heap, dlseq_pop_heap, dlseq_sort_heap
 */
#define dlseq_is_heap(type, begin, end, greater, res)   \
    do {type * _q_e = (end);				            \
		type * _q_f = (begin);						    \
		size_t _q_c = _q_e - _q_f;                      \
		type * _q_b = _q_f + ((_q_c - 1) >> 1);		    \
		type * _q_p = _q_f;							    \
		type * _q_i = _q_f + 1;						    \
		for ((res) = 1; _q_b-- > _q_f; ++_q_p) {		\
			__dlseq_ret_gt(_q_i, _q_p, res, greater);   \
			__dlseq_ret_gt(_q_i, _q_p, res, greater);   \
		}												\
		if (_q_i < _q_e && (greater(_q_i, _q_p))) {    \
			(res) = 0; break;							\
		}												\
	} while (0)

#define __dlseq_ret_gt(i, j, r, greater) \
    if (greater((i), (j))) { (r) = 0; break; } ++(i)

/**
 * @def dlseq_push_heap(type, begin, end, greater)
 * @brief 将区域最后一个节点插入到堆中
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] greater 比较函数或宏
 					最大堆:(*i > *j) 最小堆:(*i < *j)
 * @note 使用上滤插入法将最后一个节点last插入到(first,last-1)堆中，
 	所以插入的前提是(first,last-1)是一个堆\n
	上滤插入算法如下：\n
	(1) 将last记为current节点.\n
	(2) 若current无父节点(即current为根节点)，则退出.\n
	(3) 若current的值小于等于父节点的值，则退出.\n
	(4) 交换current和父节点的值，并记父节点为current节点，转(2)
 * @see dlseq_make_heap, dlseq_is_heap, dlseq_pop_heap, dlseq_sort_heap
 */
#define dlseq_push_heap(type, begin, end, greater) do { \
		type * _q_l = (type *)(end) - 1;				\
		type * _q_f = (type *)(begin);					\
		size_t _q_i = _q_l - _q_f;					    \
		size_t _q_p = (_q_i - 1) >> 1;				    \
		type * _q_ib = _q_f + _q_i;						\
		type * _q_pb = _q_f + _q_p;						\
		type _q_t = *_q_ib;								\
		while (_q_i && (greater((&_q_t), _q_pb))) {   \
			*_q_ib = *_q_pb;							\
			_q_ib = _q_pb;								\
			_q_i = _q_p;								\
			_q_p = (_q_p - 1) >> 1;						\
			_q_pb = _q_f + _q_p;						\
		}												\
		*_q_ib = _q_t;									\
	} while (0)

/**
 * @def dlseq_pop_heap(type, begin, end, greater)
 * @brief 弹出区域中堆的顶点节点
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] greater 比较函数或宏
 					最大堆:(*i > *j) 最小堆:(*i < *j)
 * @note 使用下滤删除法将first顶点节点从堆中删除，并移动至last位置，
 	所以删除的前提是(first,last)是一个堆\n
	下滤删除算法如下：\n
	(1)交换根节点begin和最后子节点last的值.\n
	(2)对根节点begin进行下滤调整.
 * @see dlseq_make_heap, dlseq_is_heap, dlseq_push_heap, dlseq_sort_heap
 */
#define dlseq_pop_heap(type, begin, end, greater) do {  \
        type * _q_l = (type *)(end) - 1;	            \
		type * _q_f = (type *)(begin);		            \
		type * _q_pb = _q_f;				            \
		type * _q_ib, * _q_jb;				            \
		type _q_t = *_q_pb;					            \
		size_t _q_p = 0, _q_j = 2;					    \
		size_t _q_L = _q_l - _q_f;			            \
        for (; _q_j <= _q_L; _q_j = (_q_p + 1) << 1) {  \
			_q_jb = _q_f + _q_j;			            \
			_q_ib = _q_jb - 1;				            \
			if (greater(_q_jb, _q_ib)) {	            \
				*_q_pb = *_q_jb;			            \
				_q_pb = _q_jb;				            \
				_q_p = _q_j;				            \
			} else {						            \
				*_q_pb = *_q_ib;			            \
				_q_pb = _q_ib;				            \
				_q_p = _q_j - 1;			            \
			}								            \
		}									            \
		_q_jb = _q_f + ((_q_p - 1) >> 1);	            \
		if (greater(_q_l, _q_jb)) {			            \
			*_q_pb = *_q_jb;				            \
			*_q_jb = *_q_l;					            \
		} else 							                \
			*_q_pb = *_q_l;					            \
		*_q_l = _q_t;						            \
	} while (0)

/* 下滤调整：
 * (1)将first记为parent节点.
 * (2)若parent节点有两个子节点，则选出值最大节点，记为max，
 *    若parent节点仅一个子节点，则选唯一节点为最大节点，记为max，
 *    若parent节点没有子节点，则退出算法.
 * (3)若parent节点的值大于等于max节点的值，则退出算法.
 * (4)将max节点与parent节点的值进行交换.
 * (5)将max节点记为parent节点，转(2).
 * greater(i, j) 最大堆 => (*i > *j) 最小堆 => (*i < *j)
 */
#define __dlseq_adjust_heap(type, base, first, last, greater) \
    do {size_t _q_j = ((first) + 1) << 1;			\
		type * _q_pb = (base) + (first);			\
		type * _q_lb = (base) + (last);				\
		type * _q_ib, * _q_jb, * _q_mb;				\
		type _q_t = *_q_pb;							\
		while (_q_j <= (last)) {					\
			_q_jb = (base) + _q_j;					\
			_q_ib = _q_jb - 1;						\
			if (greater(_q_jb, _q_ib)) {			\
				_q_mb = _q_jb;						\
				_q_j = (_q_j + 1) << 1;				\
			} else {								\
				_q_mb = _q_ib;						\
				_q_j = _q_j << 1;					\
			}										\
			if (greater((&_q_t), _q_mb))			\
				break;								\
			*_q_pb = *_q_mb;						\
			_q_pb = _q_mb;							\
		}											\
		if (_q_j == (last) + 1 &&					\
			(greater(_q_lb, (&_q_t)))) {			\
			*_q_pb = *_q_lb;						\
			*_q_lb = _q_t;							\
		} else										\
			*_q_pb = _q_t;							\
	} while (0)

/**
 * @def dlseq_make_heap(type, begin, end, greater)
 * @brief 将一段区域建立成最大堆
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] greater 比较函数或宏
 					最大堆:(*i > *j) 最小堆:(*i < *j)
 * @note 建立堆的算法如下：\n
	(1)将最后一个子节点的父节点记为current节点.\n
	(2)对current节点进行下滤调整.\n
	(3)若current节点向左有同一高度的节点，则将该节点设为current节点并转(2).\n
	(4)若current节点有父节点，则将其父节点记为current节点并转(2).\n
	(5)若current没有父节点(即current节点为根节点)，则退出算法.
 * @see dlseq_make_heap, dlseq_is_heap, dlseq_pop_heap, dlseq_sort_heap
 */
#define dlseq_make_heap(type, begin, end, greater) do {\
		type * _q1_f = (type *)(begin);									\
		size_t _q1_l = (type *)(end) - _q1_f - 1;						\
		size_t _q1_i = ((_q1_l - 1) >> 1) + 1;				            \
		while (_q1_i--)													\
			__dlseq_adjust_heap(type, _q1_f, _q1_i, _q1_l, greater);	\
	} while (0)

/**
 * @def dlseq_sort_heap(type, begin, end, greater)
 * @brief 对最大堆进行排序
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] greater 比较函数或宏
 					最大堆:(*i > *j) 最小堆:(*i < *j)
 * @note 排序堆的算法如下(升序)：\n
	(1)若first等于last，则排序完成.\n
	(2)对(first, last)区间进行pop_heap.\n
	(3)last递减，转(1),
 * @see dlseq_make_heap, dlseq_is_heap, dlseq_pop_heap, dlseq_push_heap
 */
#define dlseq_sort_heap(type, begin, end, greater) do {     \
		type * _q1_e = (end);								\
		type * _q1_f = (begin);							    \
		while (_q1_e > _q1_f + 1) {							\
			dlseq_pop_heap(type, _q1_f, _q1_e--, greater);	\
		}													\
	} while (0)

/**
 * @def dlseq_shuffle(type, head, begin, end, _RAND)
 * @brief 对区域内的节点进行随机排列
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] _RAND 随机数产生器
 */
#define dlseq_shuffle(type, begin, end, _RAND) do {		\
		type * _q_l = (type *)(end) - 1;				\
		type * _q_f = (type *)(begin);					\
		for (; _q_l > _q_f; --_q_l) {					\
			type * _q_n = _q_f + 						\
                ((_RAND) % ((_q_l - _q_f) + 1));		\
			if (_q_l > _q_n)							\
				__dlseq_iter_swap(type, _q_l, _q_n);	\
		}												\
	} while (0)

/**
 * @def dlseq_sort_insert(type, head, newly, greater_equal)
 * @brief 按序插入
 * @param[in] type 节点类型
 * @param[in] head 表头
 * @param[in] newly 新节点
 * @param[in] greater_equal 比较函数或宏 升序:(*i >= *j)
 */
#define dlseq_sort_insert(type, head, begin, end, newly, greater_equal) \
    do {type _q_n = (newly);					\
		type * _q_e = (end);		            \
		type * _q_f = (begin);		            \
		for (; _q_f < _q_e; ++_q_f) {			\
			if (greater_equal(_q_f, (&_q_n)))	\
				break;							\
		}										\
		dlseq_insert(type, head, _q_f, _q_n);   \
	} while (0)

/**
 * @def dlseq_is_sorted(type, begin, end, greater_equal, res)
 * @brief 判断区域是否是已排序的
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] greater_equal 比较函数或宏 升序:(*i >= *j)
 * @param[out] res 保存结果的变量
 * @see dlseq_bubble_sort, dlseq_select_sort, dlseq_insert_sort,
 		dlseq_shell_sort, dlseq_oddeven_sort, dlseq_comb_sort,
 		dlseq_cocktail_sort, dlseq_counting_sort, dlseq_pigeonhole_sort
 */
#define dlseq_is_sorted(type, begin, end, greater_equal, res) \
    do {type * _q_e = (end);                            	\
		type * _q_i = (begin);								\
		for ((res) = 1; _q_i + 1 < _q_e; ++_q_i) {			\
			if (!(greater_equal((_q_i + 1), _q_i))) {		\
				(res) = 0; break;							\
			}												\
		}													\
	} while (0)

/**
 * @def dlseq_bubble_sort(type, begin, end, greater)
 * @brief 冒泡排序
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] greater 升序:(*i > *j) 降序:(*i < *j)
 * @see dlseq_is_sorted
 */
#define dlseq_bubble_sort(type, begin, end, greater) do {   \
		int _q_pc = 1;									    \
		type * _q_l = (end) - 1;                            \
		type * _q_i = (begin);                              \
		for (; _q_pc && _q_i < _q_l; ++_q_i) {			    \
			type * _q_j = _q_l - 1;						    \
			for (_q_pc = 0; _q_j >= _q_i; --_q_j) {		    \
                type * _q_k = _q_j + 1;                     \
				if (greater(_q_j, _q_k)) {				    \
                    __dlseq_iter_swap(type, _q_j, _q_k);    \
					_q_pc = 1;							    \
				}										    \
			}											    \
		}												    \
	} while (0)

/**
 * @def dlseq_oddeven_sort(type, begin, end, greater)
 * @brief 奇偶排序
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] greater 升序:(*i > *j) 降序:(*i < *j)
 * @see dlseq_is_sorted
 */
#define dlseq_oddeven_sort(type, begin, end, greater) do {  \
		int _q_pc = 1;										\
		type * _q_l = (end) - 1;	                        \
		type * _q_f = (begin);		                        \
		while (_q_pc) {										\
            int _q_w; _q_pc = 0;							\
			for (_q_w = 0; _q_w < 2; ++_q_w)                \
			    __dlseq_oddeven_scan(_q_w, type, greater);	\
		}													\
	} while (0)

#define __dlseq_oddeven_scan(start, type, greater) do { \
		type * _q_i = _q_f + start;	                    \
		while (_q_i < _q_l) {			                \
			type * _q_j = _q_i + 1;		                \
			if (greater(_q_i, _q_j)) {	                \
				__dlseq_iter_swap(type, _q_i, _q_j);    \
				_q_pc = 1;				                \
			}							                \
			_q_i += 2;					                \
		}								                \
	} while (0)

/**
 * @def dlseq_cocktail_sort(type, begin, end, greater)
 * @brief 鸡尾酒排序
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] greater 升序:(*i > *j) 降序:(*i < *j)
 * @see dlseq_is_sorted
 */
#define dlseq_cocktail_sort(type, begin, end, greater) do { \
		int _q_pc;										    \
		type * _q_l = (end) - 1;	                        \
		type * _q_f = (begin);		                        \
		do {										        \
			type * _q_i = _q_f;								\
			for (_q_pc = 0; _q_i < _q_l; ++_q_i) {			\
				type * _q_j = _q_i + 1;						\
				if (greater(_q_i, _q_j)) {					\
                    __dlseq_iter_swap(type, _q_i, _q_j);    \
					_q_pc = 1;								\
				}											\
			}												\
			for (_q_i = --_q_l; _q_i > _q_f; --_q_i) {		\
				type * _q_j = _q_i - 1;						\
				if (greater(_q_j, _q_i)) {					\
					__dlseq_iter_swap(type, _q_i, _q_j);    \
					_q_pc = 1;								\
				}											\
			}												\
			++_q_f;											\
		} while (_q_pc);									\
	} while (0)

/**
 * @def dlseq_comb_sort(type, begin, end, greater)
 * @brief 梳子排序
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] greater 升序:(*i > *j) 降序:(*i < *j)
 * @see dlseq_is_sorted
 */
#define dlseq_comb_sort(type, begin, end, greater) do {         \
		int _q_swap = 1;										\
		type * _q_e = (end);						            \
		type * _q_f = (begin);						            \
		size_t _q_gap = _q_e - _q_f;							\
		while (_q_gap > 1 || _q_swap) {							\
			type * _q_i, * _q_j;								\
			if (_q_gap > 1)										\
				_q_gap = (size_t)(((double)_q_gap)             \
                      / 1.247330950103979);                     \
			_q_i = _q_f;										\
			_q_j = _q_f + _q_gap;                               \
			for (_q_swap = 0; _q_j < _q_e; ++_q_i, ++_q_j) {	\
				if (greater(_q_i, _q_j)) {						\
					__dlseq_iter_swap(type, _q_i, _q_j);        \
					_q_swap = 1;								\
				}												\
			}													\
		}														\
	} while (0)

/**
 * @def dlseq_select_sort(type, begin, end, greater)
 * @brief 选择排序
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] greater 升序:(*i > *j) 降序:(*i < *j)
 * @see dlseq_is_sorted
 */
#define dlseq_select_sort(type, begin, end, greater)    \
    do {type * _q_e = (end);	                        \
		type * _q_i = (begin);	                        \
		for (; _q_i + 1 < _q_e; ++_q_i) {				\
			type * _q_m = _q_i;							\
			type * _q_j = _q_i + 1;						\
			for (; _q_j < _q_e; ++_q_j) {				\
				if (greater(_q_m, _q_j))				\
					_q_m = _q_j;						\
			}											\
			if (_q_i != _q_m)							\
                __dlseq_iter_swap(type, _q_m, _q_i);    \
		}												\
	} while (0)

/**
 * @def dlseq_insert_sort(type, begin, end, greater)
 * @brief 插入排序
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] greater 升序:(*i > *j) 降序:(*i < *j)
 * @see dlseq_is_sorted
 */
#define dlseq_insert_sort(type, begin, end, greater) do {   \
		type * _q_l = (end) - 1;                            \
		type * _q_f = (begin);                              \
		type * _q_i = _q_l - 1;								\
		for (; _q_i >= _q_f; --_q_i) {						\
			type * _q_j = _q_l;								\
			for (; _q_j > _q_i; --_q_j) {					\
				if (greater(_q_i, _q_j)) {					\
					type _q_t = *_q_i;						\
					memcpy(_q_i, _q_i + 1, sizeof(type) *   \
                        (_q_j - _q_i));		                \
					*_q_j = _q_t;							\
					break;									\
				}											\
			}												\
		}													\
	} while (0)

/**
 * @def dlseq_shell_sort(type, begin, end, greater)
 * @brief 希尔排序
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] greater 升序:(*i > *j) 降序:(*i < *j)
 * @see dlseq_is_sorted
 */
#define dlseq_shell_sort(type, begin, end, greater) \
    do {type * _q_e = (end);                        \
		type * _q_f = (begin);	                    \
		size_t _q_gap = (_q_e - _q_f) >> 1;		    \
		for (; _q_gap; _q_gap >>= 1) {			    \
			type * _q_i = _q_f + _q_gap;			\
			for (; _q_i < _q_e; ++_q_i) {			\
				type _q_t = *_q_i;					\
				type * _q_j = _q_i - _q_gap;		\
				while (_q_j >= _q_f &&				\
					(greater(_q_j, (&_q_t)))) {	\
					_q_j[_q_gap] = *_q_j;		    \
					_q_j -= _q_gap;					\
				}									\
				_q_j[_q_gap] = _q_t;				\
			}										\
		}											\
	} while (0)

/* 计数排序和鸽巢排序(仅限于有限整数序列，需事先将hole清零) */
#ifndef COUNTING_SIZE
/**
 * @def COUNTING_SIZE(_min, _max)
 * @brief 计数排序计数缓存大小
 * @param[in] _min 节点最小值
 * @param[in] _max 节点最大值
 * @def PIGEONHOLE_SIZE(_max)
 * @brief 鸽巢排序计数缓存大小
 * @param[in] _max 节点最大值
 */
#define COUNTING_SIZE(_min, _max) ((_max) - (_min) + 1)
#define PIGEONHOLE_SIZE(_max) ((_max) + 1)
#endif	/* #ifndef COUNTING_SIZE */

/**
 * @def dlseq_counting_sort(type, begin, end, hole, _min, _max)
 * @brief 计数排序
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] hole 计数巢
 * @param[in] _min 节点最小值
 * @param[in] _max 节点最大值
 * @note 要求节点类型必须是整数类型
 * @par 示例:
 * @code
 	int hole[COUNTING_SIZE(10, 60)];
 	dlseq_iter(int) e = dlseq_end(&head);
 	dlseq_iter(int) b = dlseq_begin(&head);

 	// 计数排序
 	dlseq_counting_sort(int, b, e, hole, 10, 60);
 * @endcode
 * @see dlseq_is_sorted, dlseq_pigeonhole_sort
 */
#define dlseq_counting_sort(type, begin, end, hole, _min, _max) \
    do {type * _q_hole = (type *)(hole);				\
		type * _q_e = (end);	                        \
		type * _q_f = (begin);	                        \
		type _q_min = (_min);							\
		type _q_cnt = (_max) - _q_min + 1;				\
		type _q_i = 0;									\
		memset(_q_hole, 0, sizeof(type) * _q_cnt);		\
		while (_q_e > _q_f)								\
			++_q_hole[*(--_q_e) - _q_min];				\
		for (; _q_i < _q_cnt; ++_q_i) {					\
			size_t _q_c = _q_hole[_q_i];				\
			while (_q_c--)								\
				*_q_f++ = _q_i;							\
		}												\
	} while (0)

/**
 * @def dlseq_pigeonhole_sort(type, begin, end, hole, _max)
 * @brief 鸽巢排序
 * @param[in] type 节点类型
 * @param[in] begin 起始位置
 * @param[in] end 终止位置
 * @param[in] hole 计数巢
 * @param[in] _max 节点最大值
 * @note 要求节点类型必须是整数类型，与计数排序不同的是，鸽巢排序
 * @par 示例:
 * @code
 	int hole[PIGEONHOLE_SIZE(50)];
 	dlseq_iter(int) e = dlseq_end(&head);
 	dlseq_iter(int) b = dlseq_begin(&head);

 	// 鸽巢排序
 	dlseq_pigeonhole_sort(int, b, e, hole, 50);
 * @endcode
 * @see dlseq_is_sorted, dlseq_counting_sort
 */
#define dlseq_pigeonhole_sort(type, begin, end, hole, _max) \
	dlseq_counting_sort(type, begin, end, hole, 0, _max)

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_DLSEQ__ */

