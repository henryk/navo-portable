/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/.h
 * 
 * @brief .
 * 
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_SAMPLE__
#define __NV_SAMPLE__

#include "../../../port/cdef.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_SAMPLE__ */
