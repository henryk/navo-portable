/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/linear/linked/sllnk.h
 * 
 * @brief Singly linear linked list.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_SLLNK__
#define __NV_SLLNK__

#include "../../../port/cdef.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup 单向线形链表
 * @ingroup 链式表
 * @{
 */

/**
 * @struct __sllnk_node
 * @brief 单向线形链表节点
 * @typedef sllnk_head
 * @brief 表头
 * @typedef sllnk_node
 * @brief 节点
 * @typedef sllnk_iter
 * @brief 迭代器
 */
typedef struct __sllnk_node {
	/** @brief 后继节点 */
	struct __sllnk_node* next;
}sllnk_node, sllnk_head, *sllnk_iter;

/**
 * @def SLLNK_HEAD(name)
 * @brief 定义一个名为name的表头，并对其初始化
 * @param name 表头名称
 * @def SLLNK_NODE(name)
 * @brief 定义一个名为name的节点，并对其初始化
 * @param name 节点名称
 */
#define SLLNK_HEAD(name) sllnk_head name = {NULL}
#define SLLNK_NODE(name) sllnk_node name = {NULL}

/**
 * @typedef sllnk_pr1
 * @brief 单参数回调函数指针
 * @typedef sllnk_pr2
 * @brief 双参数回调函数指针
 */
typedef int (*sllnk_pr1)(sllnk_node*);
typedef int (*sllnk_pr2)(sllnk_node*, sllnk_node*);

/**
 * @fn int sllnk_check(sllnk_head* head, sllnk_iter* loop_entry);
 * @brief 判断链表结构是否正常
 * @param[in] head 表头
 * @param[out] loop_entry 指向保存环路入口的迭代器，可以为NULL。
 				若为空，则不返回环路入口；
 				若不为空，则函数将环路入口保存到loop_entry
 * @return int 返回结果
 * @retval SLLNK_CHECK_PASS 0 正常
 * @retval SLLNK_CHECK_LOOP 1 有非法环路
 */
NV_API int sllnk_check(sllnk_head* head, sllnk_iter* loop_entry);

#define SLLNK_CHECK_PASS 	0 /* 正常 */
#define SLLNK_CHECK_LOOP 	1 /* 有非法环路 */

/**
 * @fn static inline void sllnk_init(sllnk_head* head);
 * @brief 初始化表头
 * @param[out] head 目标表头
 * @par 示例:
 * @code
	sllnk_head head;
	// 初始化
	sllnk_init(&head);
 * @endcode
 */
static inline void sllnk_init(sllnk_head* head)
{
	head->next = NULL;
}

/**
 * @fn static inline int sllnk_empty(sllnk_head* head);
 * @brief 判断表是否为空
 * @param[in] head 表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	if (!sllnk_empty(&head)) {
 		......
 	}
 * @endcode
 */
static inline int sllnk_empty(sllnk_head* head)
{
	return !head->next;
}

static inline int __sllnk_singular(sllnk_node* front)
{
	return (front && !front->next);
}

/**
 * @fn static inline int sllnk_singular(sllnk_head* head);
 * @brief 判断表是否只有单个节点
 * @param[in] head 表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	if (sllnk_singular(&head)) {
 		......
 	}
 * @endcode
 */
static inline int sllnk_singular(sllnk_head* head)
{
	return __sllnk_singular(head->next);
}

/**
 * @fn static inline int sllnk_sortable(sllnk_iter begin, sllnk_iter end);
 * @brief 判断表是否可以排序
 * @param[in] begin 起始位置
 * @param[in] end 结束位置
 * @note 即判断是否至少有两个节点
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	sllnk_iter begin = sllnk_begin(&head);
 	sllnk_iter end = sllnk_end(&head);
 	// 判断是否可以排序
 	if (sllnk_sortable(begin, end)) {
 		// 插入排序
 		sllnk_insert_sort(begin, end, DATA_GreaterEqual, DATA, node);
 	}
 * @endcode
 * @see sllnk_bubble_sort, sllnk_oddeven_sort, 
 		sllnk_select_sort, sllnk_insert_sort, sllnk_is_sorted
 */
static inline int sllnk_sortable(sllnk_iter begin, sllnk_iter end)
{
	return (begin != end && begin->next != end);
}

/**
 * @fn static inline int sllnk_serialable(sllnk_head* head);
 * @brief 判断表是否可以序列化
 * @param[in] head 表头
 * @note 即判断是否至少有一个节点
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	// 判断是否可以序列化
 	if (sllnk_serialable(&head)) {
 		sllnk_head(DATA*) seq;
 		// 分配序列缓存
 		DATA** buf = (DATA**)malloc(sizeof(DATA*) * list_cnt);
 		// 序列化
 		sllnk_serialize(&head, buf, DATA, node);
 		SLSEQ_INIT_SERIAL(DATA*, &seq, buf, list_cnt);
 		// 希尔排序
 		slseq_shell_sort(DATA*, slseq_begin(&seq), 
 			slseq_end(&seq), DATA_Greater);
 		// 反序列化
 		sllnk_deserialize(&head, buf, list_cnt, DATA, node);
 		// 释放缓存
 		free(buf);
 	}
 * @endcode
 * @see sllnk_serialize, sllnk_deserialize
 */
static inline int sllnk_serialable(sllnk_head* head)
{
	return !!head->next;
}

/**
 * @fn static inline void sllnk_link(sllnk_iter prev, sllnk_iter next);
 * @brief 链接两个节点
 * @param[in,out] prev 前驱节点
 * @param[in,out] next 后继节点
 * @note 链接prev和next节点
 */
static inline void sllnk_link(sllnk_iter prev, sllnk_iter next)
{	/* 强制连接prev节点和next节点
	 * --(prev)-> <-(next)-- ==> --(prev)--(next)--
	 */
	prev->next = next;
}

/**
 * @fn static inline void sllnk_insert_between(sllnk_node* newly, 
												sllnk_iter prev, 
												sllnk_iter next);
 * @brief 在两个节点之间插入一个新节点
 * @param[in,out] newly 新节点
 * @param[in,out] prev 前驱节点
 * @param[in,out] next 后继节点
 * @note 将newly节点插入到prev和next节点之间。如果prev和next不是相邻节点，
 *       则newly将替换掉prev和next之间的所有节点
 */
static inline void sllnk_insert_between(sllnk_node* newly, 
												sllnk_iter prev, 
												sllnk_iter next)
{	/* 强制连接prev节点和next节点，并在两者之间插入newly节点
	 * (1) prev节点和next节点原来未连接 
	 *       (newly)
	 *                   ===> --(prev)-(newly)-(next)--
	 * --(prev) (next)--
	 * (2) prev节点和next节点原来已连接 
	 *       (newly)
	 *                   ===> --(prev)-(newly)-(next)--
	 * --(prev)-(next)--
	 */
	prev->next = newly;
	newly->next = next;
	/* (1)连接prev节点与newly节点  (2)再连接newly节点与next节点
	 *          (newly)                    (newly)
	 *          /                          /    \
	 *    --(prev)  (next)--         --(prev)  (next)--
	 */
}

/**
 * @fn static inline void sllnk_insert(sllnk_iter prev, 
 								sllnk_iter pos, sllnk_node* newly);
 * @brief 插入节点到指定位置
 * @param[in,out] prev pos的前驱节点
 * @param[in,out] pos 插入位置
 * @param[in,out] newly 新节点
 * @note 将newly节点插入到pos的位置上，pos是从表头开始的位置
 * @par 示例:
 * @code
 	sllnk_iter pp = sllnk_at(&head, 7);
 	sllnk_iter pos = pp->next;
 	DATA* p = (DATA*)malloc(sizeof(DATA));
 	// 将p节点插入到pos所在的位置
 	sllnk_insert(pp, pos, &p->node);
 * @endcode
 */
static inline void sllnk_insert(sllnk_iter prev, 
						sllnk_iter pos, sllnk_node* newly)
{
	sllnk_insert_between(newly, prev, pos);
}

/**
 * @fn static inline void sllnk_erase(sllnk_iter prev, sllnk_iter entry);
 * @brief 擦除节点
 * @param[in,out] prev 目标节点前驱
 * @param[in,out] entry 目标节点
 * @par 示例:
 * @code
  	sllnk_iter p = sllnk_pbegin(&head);
 	sllnk_iter e = sllnk_end(&head);
 	sllnk_iter i = sllnk_begin(&head);
 	while (i != e) {
 		if (is_bad_data(container_of(i, DATA, node)))
 			sllnk_erase(p, sllnk_inc_later(i));
 		else
 			sllnk_prev_inc(p, i);
 	}
 * @endcode
 */
static inline void sllnk_erase(sllnk_iter prev, sllnk_iter entry)
{	/* 将entry节点从表中移除
	 *                              (entry)
	 *  -(prev)-(entry)-()- ===> 
	 *                           --(prev)--()--
	 */
	prev->next = entry->next;
}

NV_API void __sllnk_free(sllnk_iter begin, sllnk_iter end, 
						sllnk_pr1 erase_handler);

/**
 * @fn static inline void sllnk_erases(sllnk_iter bprev, sllnk_iter begin, 
							sllnk_iter end, sllnk_pr1 erase_handler);
 * @brief 清空区域
 * @param[in,out] bprev 区域起始位置前驱
 * @param[in,out] begin 区域起始位置
 * @param[in,out] end 区域终止位置
 * @param[in] erase_handler 移除响应函数，通常用于监视和释放节点，可以传递NULL,
 							表示不为删除的节点调用响应来处理
 * @note 函数会为每一个被移除的节点调用erase_handler来处理节点
 */
static inline void sllnk_erases(sllnk_iter bprev, sllnk_iter begin, 
							sllnk_iter end, sllnk_pr1 erase_handler)
{
	bprev->next = end;
	if (erase_handler)
		__sllnk_free(begin, end, erase_handler);
}

/**
 * @fn static inline void sllnk_clear(sllnk_head* head, sllnk_pr1 erase_handler);
 * @brief 清空表
 * @param[in,out] head 表头
 * @param[in] erase_handler 移除响应函数，通常用于监视和释放节点，可以传递NULL,
 							表示不为删除的节点调用响应来处理
 * @note 函数会为每一个被移除的节点调用erase_handler来处理节点
 */
static inline void sllnk_clear(sllnk_head* head, sllnk_pr1 erase_handler)
{
	sllnk_erases(head, head->next, NULL, erase_handler);
}

/**
 * @fn static inline void sllnk_push_front(sllnk_head* head, sllnk_node* newly);
 * @brief 在表首添加节点
 * @param[in,out] head 表头
 * @param[in,out] newly 新节点
 */
static inline void sllnk_push_front(sllnk_head* head, sllnk_node* newly)
{
	sllnk_insert_between(newly, head, head->next);
}

/**
 * @fn static inline void sllnk_pop_front(sllnk_head* head);
 * @brief 移除表首节点
 * @param[in,out] head 表头
 */
static inline void sllnk_pop_front(sllnk_head* head)
{
	sllnk_link(head, head->next->next);
}

/**
 * @fn static inline void sllnk_splice_between(
			sllnk_iter prev, sllnk_iter next, 
			sllnk_iter first_prev, sllnk_iter first, 
			sllnk_iter last, sllnk_iter last_next);
 * @brief 在两个节点之间插入多个连续节点
 * @param[in,out] prev 前驱节点
 * @param[in,out] next 后继节点
 * @param[in,out] first_prev 首节点的前驱节点
 * @param[in,out] first 首节点
 * @param[in,out] last 尾节点
 * @param[in,out] last_next 尾节点的后继节点
 * @note 将从first至last的节点嫁接到prev和next之间。如果prev和next之间有节点，
 * 		则它们之间的节点将会被替换
 */
static inline void sllnk_splice_between(
	sllnk_iter prev, sllnk_iter next, 
	sllnk_iter first_prev, sllnk_iter first, 
	sllnk_iter last, sllnk_iter last_next)
{	/* 将first节点至last节点的部分从旧表(B表)移接到新表(A表)中 
	 * (从first到last是按next方向)
	 * A:  -(prev)-(next)-      
	 * B: -(first_prev)-(first)-...-(last)-(last_next)-
	 * to:
	 * A: -(prev)-(first)-...-(last)-(next)-
	 * B: -(first_prev)-(last_next)-
	 */
	last->next = next;
	prev->next = first;
	first_prev->next = last_next;
}

/**
 * @fn static inline void sllnk_splice(sllnk_iter prev, sllnk_iter pos, 
				sllnk_iter first_prev, sllnk_iter first, sllnk_iter last);
 * @brief 在两个节点之间插入多个连续节点
 * @param[in,out] prev pos的前驱节点
 * @param[in,out] pos 插入位置
 * @param[in,out] first_prev 首节点的前驱节点
 * @param[in,out] first 首节点
 * @param[in,out] last 尾节点
 * @note 将从first至last的节点嫁接到pos位置。可以跨链表移动
 * @par 示例:
 * @code
 	// 将a链表的节点全部嫁接到b链表
 	sllnk_splice(bprev, b, a, sllnk_front(a), sllnk_back(b));
 	// 将b到e的节点们移动到第4个节点的位置
 	sllnk_splice(sllnk_at(head, 2), sllnk_at(head, 3), bprev, b, e);
 * @endcode
 */
static inline void sllnk_splice(sllnk_iter prev, sllnk_iter pos, 
				sllnk_iter first_prev, sllnk_iter first, sllnk_iter last)
{
	sllnk_splice_between(prev, pos, first_prev, first, last, last->next);
}

/**
 * @fn size_t sllnk_count(sllnk_head* head);
 * @brief 统计表的节点数量
 * @param[in] head 表头
 * @return size_t 返回表的节点总数
 */
NV_API size_t sllnk_count(sllnk_head* head);

/**
 * @fn size_t sllnk_distance(sllnk_iter first, sllnk_iter last);
 * @brief 计算两个节点的迭代距离
 * @param[in] first 首节点
 * @param[in] last 尾节点
 * @note 以索引为例，2到4的距离是4 (4-2+1=3)
 * @return size_t 返回距离
 */
NV_API size_t sllnk_distance(sllnk_iter first, sllnk_iter last);

/**
 * @fn sllnk_iter sllnk_advance(sllnk_iter cur, size_t dist);
 * @brief 给迭代器增加一段距离或减小一段距离
 * @param[in] cur 迭代器
 * @param[in] dist 增加的距离。正数时，表示增加；负数时，表示减小
 * @return sllnk_iter 返回新迭代器
 */
NV_API sllnk_iter sllnk_advance(sllnk_iter cur, size_t dist);

/**
 * @fn static inline sllnk_iter sllnk_at(sllnk_head* head, const size_t index);
 * @brief 根据正向索引得到迭代器
 * @param[in] head 表头
 * @param[in] index 索引，从0开始
 * @return sllnk_iter 返回对应的迭代器
 */
static inline sllnk_iter sllnk_at(sllnk_head* head, const size_t index)
{
	return sllnk_advance(head->next, index);
}

/**
 * @fn size_t sllnk_index_of(sllnk_head* head, sllnk_iter itr);
 * @brief 计算迭代器的正向索引
 * @param[in] head 表头
 * @param[in] itr 迭代器
 * @return size_t 返回索引
 */
NV_API size_t sllnk_index_of(sllnk_head* head, sllnk_iter itr);

/**
 * @fn static inline void sllnk_node_init(sllnk_node* entry);
 * @brief 初始化节点
 * @param[out] entry 节点
 */
static inline void sllnk_node_init(sllnk_node* entry)
{
	entry->next = NULL;
}

/**
 * @fn static inline void sllnk_iter_replace(sllnk_node* vprev, 
							sllnk_node* victim, sllnk_node* newly);
 * @brief 用新节点替换目标节点
 * @param[in,out] vprev 目标节点前驱
 * @param[in,out] victim 受害目标节点
 * @param[in,out] newly 新节点
 */
static inline void sllnk_iter_replace(sllnk_node* vprev, 
							sllnk_node* victim, sllnk_node* newly)
{	/* 用newly节点替换表中的victim节点
	 *      (newly)                     (victim)
	 *                        ===>
	 * --(vprev)-(victim)-()--      --(vprev)-(newly)-()--
	 */
	sllnk_insert_between(newly, vprev, victim->next);
}

NV_API void __sllnk_iter_swap(
	sllnk_iter aprev, sllnk_iter a, sllnk_iter anext, 
	sllnk_iter bprev, sllnk_iter b, sllnk_iter bnext);

/**
 * @fn static inline void sllnk_iter_swap(sllnk_iter aprev, sllnk_iter a, 
									sllnk_iter bprev, sllnk_iter b);
 * @brief 交换两个节点
 * @param[in,out] aprev 第一个节点前驱
 * @param[in,out] a 第一个节点
 * @param[in,out] bprev 第二个节点前驱
 * @param[in,out] b 第二个节点
 */
static inline void sllnk_iter_swap(sllnk_iter aprev, sllnk_iter a, 
									sllnk_iter bprev, sllnk_iter b)
{
	__sllnk_iter_swap(aprev, a, a->next, bprev, b, b->next);
}

/**
 * @fn static inline void sllnk_replace(sllnk_head* victim, sllnk_head* newly);
 * @brief 用新表头替换目标表头
 * @param[in,out] victim 受害目标表头
 * @param[in,out] newly 新表头
 */
static inline void sllnk_replace(sllnk_head* victim, sllnk_head* newly)
{	/* 用newly头替换表中的victim头
	 *      [newly]                 [victim]
	 *                    ===>
	 * --()-[victim]-()--      --()-[newly]-()--
	 */
	newly->next = victim->next;
}

static inline void __sllnk_swap(sllnk_iter a, sllnk_iter afront, 
								sllnk_iter b, sllnk_iter bfront)
{	/* 交换a、b两个头
	 * [a]-(afront)--       [b]-(afront)--
	 *                         ===>
	 * [b]-(bfront)--       [a]-(afront)--
	 */
	a->next = bfront;
	b->next = afront;
}

/**
 * @fn static inline void sllnk_swap(sllnk_head* a, sllnk_head* alast, 
 								sllnk_head* b, sllnk_head* blast);
 * @brief 交换两个表头
 * @param[in,out] a 第一个表头
 * @param[in,out] b 第二个表头
 */
static inline void sllnk_swap(sllnk_head* a, sllnk_head* b)
{
	__sllnk_swap(a, a->next, b, b->next);
}

/**
 * @fn static inline int sllnk_exist(sllnk_head* head, sllnk_iter itr);
 * @brief 判断一个节点是否存在于表中
 * @param[in] head 表头
 * @param[in] itr 目标节点
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 */
static inline int sllnk_exist(sllnk_head* head, sllnk_iter itr)
{
	return sllnk_index_of(head, itr) != (size_t)(-1);
}

/**
 * @fn static inline sllnk_iter sllnk_front(sllnk_head* head);
 * @brief 得到表首节点
 * @param[in] head 表头
 * @return sllnk_iter 返回表首节点
 */
static inline sllnk_iter sllnk_front(sllnk_head* head)
{
	return head->next;
}

/**
 * @fn static inline sllnk_iter sllnk_begin(sllnk_head* head);
 * @brief 得到指向正向起始位置的迭代器
 * @param[in] head 表头
 * @return sllnk_iter 返回迭代器
 * @par 示例:
 * @code
 	// 遍历链表
 	int index = 0;
 	sllnk_iter e = sllnk_end();
 	sllnk_iter i = sllnk_begin(&head);
 	while (i != e) {
 		printf("[%d]=%d\n", index++, owner_of(i, DATA, node).data);
 		sllnk_inc(i);
 	}
 * @endcode
 * @see sllnk_end, sllnk_pbegin
 */
static inline sllnk_iter sllnk_begin(sllnk_head* head)
{
	return head->next;
}

/**
 * @fn static inline sllnk_iter sllnk_pbegin(sllnk_head* head);
 * @brief 得到指向正向起始位置前驱的迭代器
 * @param[in] head 表头
 * @return sllnk_iter 返回迭代器
 * @par 示例:
 * @code
  	sllnk_iter p = sllnk_pbegin(&head);
 	sllnk_iter e = sllnk_end();
 	sllnk_iter i = sllnk_begin(&head);
 	while (i != e) {
 		if (is_bad_data(container_of(i, DATA, node)))
 			sllnk_erase(p, sllnk_inc_later(i));
 		else
 			sllnk_prev_inc(p, i);
 	}
 * @endcode
 * @see sllnk_begin, sllnk_end
 */
static inline sllnk_iter sllnk_pbegin(sllnk_head* head)
{
	return head;
}

/**
 * @fn static inline sllnk_iter sllnk_end();
 * @brief 得到指向正向终止位置的迭代器
 * @return sllnk_iter 返回迭代器
 * @see sllnk_begin, sllnk_pbegin
 */
static inline sllnk_iter sllnk_end()
{
	return NULL;
}

/**
 * @fn static inline sllnk_iter sllnk_end_of(sllnk_iter itr);
 * @brief 得到当前迭代器的正向终止位置
 * @param[in] itr 迭代器
 * @return sllnk_iter 返回迭代器
 */
static inline sllnk_iter sllnk_end_of(sllnk_iter itr)
{
	return itr->next;
}

/**
 * @fn static inline sllnk_iter sllnk_next(sllnk_iter itr);
 * @brief 得到当前节点的后继节点
 * @param[in] itr 当前节点
 * @return sllnk_iter 返回后继节点
 */
static inline sllnk_iter sllnk_next(sllnk_iter itr)
{
	return itr->next;
}

static inline sllnk_iter __sllnk_inc_later(sllnk_iter cur, sllnk_iter* p)
{
	*p = cur->next;
	return cur;
}

/**
 * @def sllnk_inc(itr)
 * @brief 迭代器递增
 * @def sllnk_prev_inc(itr, prev)
 * @brief 迭代器递增，并更新前驱迭代器
 * @def sllnk_inc_later(itr)
 * @brief 迭代器后自增
 */
#define sllnk_inc(itr) ((itr) = (itr)->next)
#define sllnk_prev_inc(itr, prev) ((prev) = (itr), sllnk_inc(itr))
#define sllnk_inc_later(itr) __sllnk_inc_later(itr, &(itr))

/**
 * @def sllnk_foreach(_begin, _end, fn, type, member)
 * @brief 正向遍历_begin到_end的节点，并为每一个节点调用回调函数或宏
 * @param[in] _begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define sllnk_foreach(_begin, _end, fn, type, member) \
	do {sllnk_iter _q_e = (_end);							\
		sllnk_iter _q_f = (_begin);							\
		while (_q_f != _q_e) {								\
			type * _q_t = container_of(_q_f, type, member);	\
			_q_f = _q_f->next;								\
			fn(_q_t);										\
		}													\
	} while (0)

/**
 * @def sllnk_search(head, _begin, _end, equal, var, res, type, member)
 * @brief 顺序查找区域内的指定节点
 * @param[in,out] head 表头
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] equal 比较函数或宏。相等时返回1，不相等返回0
 * @param[in] var 搜索的键值，常量，变量均可
 * @param[out] res 保存搜索结果的变量，类型是节点指针。结果为NULL表示找不到节点
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define sllnk_search(head, _begin, _end, equal, var, res, type, member) do {\
		sllnk_iter _q_e = (_end);								\
		sllnk_iter _q_f = (_begin);								\
		sllnk_head* _q_h = (head);								\
		(res) = NULL;											\
		while (_q_f != _q_e) {									\
			type * _q_cb = container_of(_q_f, type, member);	\
			if (equal(_q_cb, (val))) {							\
				(res) = _q_cb;									\
				break;											\
			}													\
			_q_f = _q_f->next;									\
		}														\
	} while (0)

/* 反转链表的两种方式：
 * (1) 直接使用 sllnk_reverse(head); (推荐)
 * (2) 按照如下方式迭代
 *     sllnk_iter i = sllnk_vbegin(head);
 *     sllnk_iter e = sllnk_vend(head);
 *     while (i != e) {
 *			sllnk_vpush(head, i);
 *     }
 *     当迭代完成时，反转完成
 */

static inline sllnk_iter __sllnk_vbegin(sllnk_head* head, sllnk_iter front)
{
	head->next = NULL;
	return front;
}

/**
 * @fn static inline sllnk_iter sllnk_vbegin(sllnk_head* head);
 * @brief 得到用于反转的起始位置的迭代器
 * @param[in] head 表头
 * @return sllnk_iter 返回迭代器
 * @par 示例:
 * @code
 	// 反转表
 	sllnk_iter ve = sllnk_vend(&head);
 	sllnk_iter vi = sllnk_vbegin(&head);
 	while (vi != ve) {
 		sllnk_vpush(&head, vi);
 	}
 * @endcode
 * @see sllnk_vend, sllnk_vpush, sllnk_reverse
 */
static inline sllnk_iter sllnk_vbegin(sllnk_head* head)
{
	return __sllnk_vbegin(head, head->next);
}

/**
 * @fn static inline sllnk_iter sllnk_vend(sllnk_head* head);
 * @brief 得到用于反转的终止位置的迭代器
 * @param[in] head 表头
 * @return sllnk_iter 返回迭代器
 * @par 示例:
 * @code
 	// 反转表
 	sllnk_iter ve = sllnk_vend(&head);
 	sllnk_iter vi = sllnk_vbegin(&head);
 	while (vi != ve) {
 		sllnk_vpush(&head, vi);
 	}
 * @endcode
 * @see sllnk_vbegin, sllnk_vpush, sllnk_reverse
 */
static inline sllnk_iter sllnk_vend(sllnk_head* head)
{
	return head;
}

/**
 * @def sllnk_vpush(head, itr)
 * @brief 入栈反转操作
 * @param[in,out] head 表头
 * @param[in,out] itr 迭代器
 * @par 示例:
 * @code
 	// 反转表
 	sllnk_iter ve = sllnk_vend(&head);
 	sllnk_iter vi = sllnk_vbegin(&head);
 	while (vi != ve) {
 		sllnk_vpush(&head, vi);
 	}
 * @endcode
 * @see sllnk_vbegin, sllnk_vend, sllnk_reverse
 */
#define sllnk_vpush(head, itr) sllnk_push_front(head, sllnk_inc_later(itr))

/**
 * @fn void sllnk_reverse(sllnk_head* head);
 * @brief 反转表
 * @param[in,out] head 表头
 * @see sllnk_vbegin, sllnk_vend, sllnk_vpush
 */
NV_API void sllnk_reverse(sllnk_head* head);

NV_API void __sllnk_serialize(sllnk_head* head, char** buf, 
							const size_t offset);

NV_API void __sllnk_deserialize(sllnk_head* head, char** buf, 
							size_t cnt, const size_t offset);

/**
 * @def sllnk_serialize(head, buf, type, member)
 * @brief 表的序列化
 * @param[in] head 表头
 * @param[out] buf 序列缓存
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @note 函数会遍历整个表，逐个将节点的拥有者地址存入缓存
 * @par 示例:
 * @code
 	// 判断是否可以序列化
 	if (sllnk_serialable(&head)) {
 		sllnk_head(DATA*) seq;
 		// 分配序列缓存
 		DATA** buf = (DATA**)malloc(sizeof(DATA*) * list_cnt);
 		// 序列化
 		sllnk_serialize(&head, buf, DATA, node);
 		SLSEQ_INIT_SERIAL(DATA*, &seq, buf, list_cnt);
 		// 希尔排序
 		slseq_shell_sort(DATA*, slseq_begin(&seq), 
 			slseq_end(&seq), DATA_Greater);
 		// 反序列化
 		sllnk_deserialize(&head, buf, list_cnt, DATA, node);
 		// 释放缓存
 		free(buf);
 	}
 * @endcode
 * @see sllnk_deserialize, sllnk_serialable
 */
#define sllnk_serialize(head, buf, type, member) \
	__sllnk_serialize(head, (char**)(buf), offset_of(type, member))

/**
 * @def sllnk_deserialize(head, buf, cnt, type, member)
 * @brief 表的反序列化
 * @param[out] head 表头
 * @param[out] buf 序列缓存
 * @param[in] cnt 序列长度
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @note 函数会遍历整个序列来还原整个表
 * @par 示例:
 * @code
 	// 判断是否可以序列化
 	if (sllnk_serialable(&head)) {
 		slseq_head seq(DATA*);
 		// 分配序列缓存
 		DATA** buf = (DATA**)malloc(sizeof(DATA*) * list_cnt);
 		// 序列化
 		sllnk_serialize(&head, buf, DATA, node);
 		SLSEQ_INIT_SERIAL(DATA*, &seq, buf, list_cnt);
 		// 希尔排序
 		slseq_shell_sort(DATA*, slseq_begin(&seq), 
 			slseq_end(&seq), DATA_Greater);
 		// 反序列化
 		sllnk_deserialize(&head, buf, list_cnt, DATA, node);
 		// 释放缓存
 		free(buf);
 	}
 * @endcode
 * @see sllnk_serialize, sllnk_serialable
 */
#define sllnk_deserialize(head, buf, cnt, type, member) \
	__sllnk_deserialize(head, (char**)(buf), cnt, offset_of(type, member))

/**
 * @def sllnk_sort_insert(head, newly, greater_equal, type, member)
 * @brief 按升序插入新节点
 * @param[in,out] head 表头
 * @param[in,out] newly 新节点
 * @param[in] greater_equal 比较函数或宏，
 							升序:((*i)->x >= (*j)->x) 
 							降序:((*i)->x <= (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define sllnk_sort_insert(head, newly, greater_equal, type, member) do {\
		sllnk_iter _k_n = (newly);								\
		sllnk_head* _k_h = (head);								\
		sllnk_iter _k_j = _k_h;									\
		sllnk_iter _k_i = _k_h->next;							\
		while (_k_i != _k_h) {									\
			type * _k_pi = container_of(_k_i, type, member);	\
			type * _k_pn = container_of(_k_n, type, member);	\
			if (greater_equal(_k_pi, _k_pn))					\
				break;											\
			_k_j = _k_i;										\
			_k_i = _k_i->next;									\
		}														\
		sllnk_insert(_k_j, _k_i, _k_n);							\
	} while (0)

/**
 * @def sllnk_is_sorted(_begin, _end, greater_equal, res, type, member)
 * @brief 判断表是否按序
 * @param[in] _begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] greater_equal 比较函数或宏，
 							升序:((*i)->x >= (*j)->x) 
 							降序:((*i)->x <= (*j)->x)
 * @param[out] res 保存输出结果的变量
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see sllnk_bubble_sort, sllnk_oddeven_sort, 
 		sllnk_select_sort, sllnk_insert_sort, sllnk_sortable
 */
#define sllnk_is_sorted(_begin, _end, greater_equal, res, type, member) do {\
		sllnk_iter _k_e = (_end);								\
		sllnk_iter _k_i = (_begin);								\
		sllnk_iter _k_j = _k_i->next;							\
		(res) = 1;												\
		while (_k_j != _k_e) {									\
			type * _k_pi = container_of(_k_i, type, member);	\
			type * _k_pj = container_of(_k_j, type, member);	\
			if (!(greater_equal(_k_pj, _k_pi))) {				\
				(res) = 0;										\
				break;											\
			}													\
			_k_i = _k_j;										\
			_k_j = _k_j->next;									\
		}														\
	} while (0)

/* 单向线形链表排序性能比较
 * 随机数据：(最高) 插入排序 >> 选择排序 >> 冒泡排序 > 鸡尾酒排序 > 奇偶排序 (最低)
 * 顺序数据：(最高) 冒泡排序 > 奇偶排序 > 鸡尾酒排序 >> 插入排序 > 选择排序  (最低)
 * 逆序数据：(最高) 插入排序 >> 选择排序 >> 冒泡排序 > 鸡尾酒排序 > 奇偶排序  (最低)
 */

/**
 * @def sllnk_bubble_sort(bprev, _begin, _end, greater, type, member)
 * @brief 对表进行冒泡排序
 * @param[in,out] bprev 起始位置前驱
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] greater 比较函数或宏
 					升序:((*i)->x > (*j)->x)
 					降序:((*i)->x < (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see sllnk_sortable, sllnk_is_sorted
 */
#define sllnk_bubble_sort(bprev, _begin, _end, greater, type, member) do {\
		int _k_pc = 1;												\
		sllnk_iter _k_e = (_end);									\
		sllnk_iter _k_i = (_begin);									\
		sllnk_iter _k_ip = (bprev);									\
		for (; _k_pc && _k_i != _k_e; _k_i = _k_i->next) {			\
			sllnk_iter _k_jp = _k_i;								\
			sllnk_iter _k_j = _k_i->next;							\
			for (_k_pc = 0; _k_j != _k_e; _k_j = _k_j->next) {		\
				type * _k_pi = container_of(_k_i, type, member);	\
				type * _k_pj = container_of(_k_j, type, member);	\
				if (greater(_k_pi, _k_pj)) {						\
					sllnk_iter _k_t = _k_i;							\
					_k_i = _k_j;									\
					_k_j = _k_t;									\
					sllnk_iter_swap(_k_jp, _k_i, _k_ip, _k_j);		\
					_k_pc = 1;										\
				}													\
				_k_jp = _k_j;										\
			}														\
			_k_ip = _k_i;											\
		}															\
	} while (0)

/**
 * @def sllnk_oddeven_sort(bprev, _begin, _end, greater, type, member)
 * @brief 对表进行奇偶排序
 * @param[in,out] bprev 起始位置前驱
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] greater 比较函数或宏
 					升序:((*i)->x > (*j)->x)
 					降序:((*i)->x < (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see sllnk_sortable, sllnk_is_sorted
 */
#define sllnk_oddeven_sort(bprev, _begin, _end, greater, type, member) do {\
		int _k_pc = 1;														\
		sllnk_iter _k_e = (_end);											\
		sllnk_iter _k_f = (_begin);											\
		sllnk_iter _k_p = (bprev);											\
		while (_k_pc) {														\
			_k_pc = 0;														\
			_k_f = _k_p->next;												\
			__sllnk_oddeven_scan(greater, type, member, _k_p, _k_f);		\
			__sllnk_oddeven_scan(greater, type, member, _k_f, _k_f->next);	\
		}																	\
	} while (0)

#define __sllnk_oddeven_scan(greater, type, member, prev, _start) do {\
		sllnk_iter _k_ip = prev;								\
		sllnk_iter _k_i = _start;								\
		sllnk_iter _k_jp = _k_i;								\
		sllnk_iter _k_j = _k_i->next;							\
		while (_k_j != _k_e) {									\
			type * _k_pi = container_of(_k_i, type, member);	\
			type * _k_pj = container_of(_k_j, type, member);	\
			if (greater(_k_pi, _k_pj)) {						\
				sllnk_iter _k_t = _k_i;							\
				_k_i = _k_j;									\
				_k_j = _k_t;									\
				sllnk_iter_swap(_k_jp, _k_i, _k_ip, _k_j);		\
				_k_pc = 1;										\
			}													\
			if (!_k_j) break;									\
			_k_ip = _k_j;										\
			_k_i = _k_j->next;									\
			if (!_k_i) break;									\
			_k_jp = _k_i;										\
			_k_j = _k_i->next;									\
		}														\
	} while (0)

/**
 * @def sllnk_select_sort(bprev, _begin, _end, greater, type, member)
 * @brief 对表进行选择排序
 * @param[in,out] bprev 起始位置前驱
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] greater 比较函数或宏
 					升序:((*i)->x > (*j)->x)
 					降序:((*i)->x < (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see sllnk_sortable, sllnk_is_sorted
 */
#define sllnk_select_sort(bprev, _begin, _end, greater, type, member) do {\
		sllnk_iter _k_e = (_end);									\
		sllnk_iter _k_i = (_begin);									\
		sllnk_iter _k_ip = (bprev);									\
		sllnk_iter _k_jp = _k_i;									\
		sllnk_iter _k_j = _k_i->next;								\
		while (_k_j != _k_e) {										\
			sllnk_iter _k_mp = _k_ip;								\
			sllnk_iter _k_m = _k_i;									\
			while (_k_j != _k_e) {									\
				type * _k_pj = container_of(_k_j, type, member);	\
				type * _k_pm = container_of(_k_m, type, member);	\
				if (greater(_k_pm, _k_pj)) {						\
					_k_mp = _k_jp;									\
					_k_m = _k_j;									\
				}													\
				_k_jp = _k_j;										\
				_k_j = _k_j->next;									\
			}														\
			if (_k_i != _k_m) {										\
				sllnk_iter _k_t = _k_m;								\
				_k_m = _k_i;										\
				_k_i = _k_t;										\
				sllnk_iter_swap(_k_ip, _k_m, _k_mp, _k_i);			\
			}														\
			_k_ip = _k_i;											\
			_k_i = _k_i->next;										\
			_k_jp = _k_i;											\
			_k_j = _k_i->next;										\
		}															\
	} while (0)
/**
 * @def sllnk_insert_sort(bprev, _begin, _end, greater, type, member)
 * @brief 对表进行插入排序
 * @param[in,out] bprev 起始位置前驱
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] greater 比较函数或宏
 					升序:((*i)->x > (*j)->x)
 					降序:((*i)->x < (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see sllnk_sortable, sllnk_is_sorted
 */
#define sllnk_insert_sort(bprev, _begin, _end, greater_equal, type, member) \
	do {sllnk_iter _k_e = (_end);									\
		sllnk_iter _k_f = (_begin);									\
		sllnk_iter _k_p = (bprev);									\
		sllnk_iter _k_ip = _k_f;									\
		sllnk_iter _k_i = _k_f->next;								\
		for (; _k_i != _k_e; _k_i = _k_i->next) {					\
			type * _k_pi = container_of(_k_i, type, member);		\
			sllnk_iter _k_jp = _k_p;								\
			sllnk_iter _k_j = _k_p->next;							\
			while (_k_j != _k_i) {									\
				type * _k_pj = container_of(_k_j, type, member);	\
				if (greater_equal(_k_pj, _k_pi)) {					\
					sllnk_iter _k_t = _k_i;							\
					_k_i = _k_ip;									\
					sllnk_erase(_k_ip, _k_t);						\
					sllnk_insert(_k_jp, _k_j, _k_t);				\
					break;											\
				}													\
				_k_jp = _k_j;										\
				_k_j = _k_j->next;									\
			}														\
			_k_ip = _k_i;											\
		}															\
	} while (0)

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_SLLNK__ */
