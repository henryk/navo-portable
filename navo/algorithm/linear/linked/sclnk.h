/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/linear/linked/sclnk.h
 * 
 * @brief Singly circular linked list.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_SCLNK__
#define __NV_SCLNK__

#include "../../../port/cdef.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup 单向环形链表
 * @ingroup 链式表
 * @{
 */

/**
 * @struct __sclnk_node
 * @brief 单向环形链表节点
 * @typedef sclnk_head
 * @brief 表头
 * @typedef sclnk_node
 * @brief 节点
 * @typedef sclnk_iter
 * @brief 迭代器
 */
typedef struct __sclnk_node {
	/** @brief 后继节点 */
	struct __sclnk_node* next;
}sclnk_node, sclnk_head, *sclnk_iter;

/**
 * @def SCLNK_HEAD(name)
 * @brief 定义一个名为name的表头，并对其初始化
 * @param name 表头名称
 * @def SCLNK_NODE(name)
 * @brief 定义一个名为name的节点，并对其初始化
 * @param name 节点名称
 */
#define SCLNK_HEAD(name) sclnk_head name = {&(name)}
#define SCLNK_NODE(name) sclnk_node name = {&(name)}

/**
 * @typedef sclnk_pr1
 * @brief 单参数回调函数指针
 * @typedef sclnk_pr2
 * @brief 双参数回调函数指针
 */
typedef int (*sclnk_pr1)(sclnk_node*);
typedef int (*sclnk_pr2)(sclnk_node*, sclnk_node*);

/**
 * @fn int sclnk_check(sclnk_head* head, sclnk_iter* loop_entry);
 * @brief 判断链表结构是否正常
 * @param[in] head 表头
 * @param[out] loop_entry 指向保存环路入口的迭代器，可以为NULL。
 			若为空，则不返回环路入口；
 			若不为空且仅有环路问题，则函数将环路入口保存到loop_entry
 * @return int 返回结果
 * @retval SCLNK_CHECK_PASS 0 正常
 * @retval SCLNK_CHECK_LOOP 1 有非法环路
 * @retval SCLNK_CHECK_NULL 2 有节点指向NULL
 */
NV_API int sclnk_check(sclnk_head* head, sclnk_iter* loop_entry);

#define SCLNK_CHECK_PASS 	0 /* 正常 */
#define SCLNK_CHECK_LOOP 	1 /* 有非法环路 */
#define SCLNK_CHECK_NULL 	2 /* 有节点指向NULL */

/**
 * @fn static inline void sclnk_init(sclnk_head* head);
 * @brief 初始化表头
 * @param[out] head 目标表头
 * @par 示例:
 * @code
	sclnk_head head;
	// 初始化
	sclnk_init(&head);
 * @endcode
 */
static inline void sclnk_init(sclnk_head* head)
{
	head->next = head;
}

/**
 * @fn static inline int sclnk_empty(sclnk_head* head);
 * @brief 判断表是否为空
 * @param[in] head 表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	if (!sclnk_empty(&head)) {
 		......
 	}
 * @endcode
 */
static inline int sclnk_empty(sclnk_head* head)
{
	return head->next == head;
}

static inline int __sclnk_singular(sclnk_node* front, sclnk_node* end)
{
	return (front != end && front->next == end);
}

/**
 * @fn static inline int sclnk_singular(sclnk_head* head);
 * @brief 判断表是否只有单个节点
 * @param[in] head 表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	if (sclnk_singular(&head)) {
 		......
 	}
 * @endcode
 */
static inline int sclnk_singular(sclnk_head* head)
{
	return __sclnk_singular(head->next, head);
}

/**
 * @fn static inline int sclnk_sortable(sclnk_iter begin, sclnk_iter end);
 * @brief 判断表是否可以排序
 * @param[in] begin 起始位置
 * @param[in] end 结束位置
 * @note 即判断是否至少有两个节点
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	sclnk_iter begin = sclnk_begin(&head);
 	sclnk_iter end = sclnk_end(&head);
 	// 判断是否可以排序
 	if (sclnk_sortable(begin, end)) {
 		// 插入排序
 		sclnk_insert_sort(begin, end, DATA_GreaterEqual, DATA, node);
 	}
 * @endcode
 * @see sclnk_bubble_sort, sclnk_oddeven_sort, 
 		sclnk_select_sort, sclnk_insert_sort, sclnk_is_sorted
 */
static inline int sclnk_sortable(sclnk_iter begin, sclnk_iter end)
{
	return (begin != end && begin->next != end);
}

/**
 * @fn static inline int sclnk_serialable(sclnk_head* head);
 * @brief 判断表是否可以序列化
 * @param[in] head 表头
 * @note 即判断是否至少有一个节点
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	// 判断是否可以序列化
 	if (sclnk_serialable(&head)) {
 		slseq_head(DATA*) seq;
 		// 分配序列缓存
 		DATA** buf = (DATA**)malloc(sizeof(DATA*) * list_cnt);
 		// 序列化
 		sclnk_serialize(&head, buf, DATA, node);
 		SLSEQ_INIT_SERIAL(DATA*, &seq, buf, list_cnt);
 		// 希尔排序
 		slseq_shell_sort(DATA*, slseq_begin(&seq), 
 			slseq_end(&seq), DATA_Greater);
 		// 反序列化
 		sclnk_deserialize(&head, buf, list_cnt, DATA, node);
 		// 释放缓存
 		free(buf);
 	}
 * @endcode
 */
static inline int sclnk_serialable(sclnk_head* head)
{
	return !!head->next;
}

/**
 * @fn static inline void sclnk_link(sclnk_iter prev, sclnk_iter next);
 * @brief 链接两个节点
 * @param[in,out] prev 前驱节点
 * @param[in,out] next 后继节点
 * @note 链接prev和next节点
 */
static inline void sclnk_link(sclnk_iter prev, sclnk_iter next)
{	/* 强制连接prev节点和next节点
	 * --(prev)-> <-(next)-- ==> --(prev)--(next)--
	 */
	prev->next = next;
}

/**
 * @fn static inline void sclnk_insert_between(sclnk_node* newly, 
												sclnk_iter prev, 
												sclnk_iter next);
 * @brief 在两个节点之间插入一个新节点
 * @param[in,out] newly 新节点
 * @param[in,out] prev 前驱节点
 * @param[in,out] next 后继节点
 * @note 将newly节点插入到prev和next节点之间。如果prev和next不是相邻节点，
 *       则newly将替换掉prev和next之间的所有节点
 */
static inline void sclnk_insert_between(sclnk_node* newly, 
												sclnk_iter prev, 
												sclnk_iter next)
{	/* 强制连接prev节点和next节点，并在两者之间插入newly节点
	 * (1) prev节点和next节点原来未连接 
	 *       (newly)
	 *                   ===> --(prev)-(newly)-(next)--
	 * --(prev) (next)--
	 * (2) prev节点和next节点原来已连接 
	 *       (newly)
	 *                   ===> --(prev)-(newly)-(next)--
	 * --(prev)-(next)--
	 */
	prev->next = newly;
	newly->next = next;
	/* (1)连接prev节点与newly节点  (2)再连接newly节点与next节点
	 *          (newly)                    (newly)
	 *          /                          /    \
	 *    --(prev)  (next)--         --(prev)  (next)--
	 */
}

/**
 * @fn static inline void sclnk_insert(sclnk_iter prev, 
 								sclnk_iter pos, sclnk_node* newly);
 * @brief 插入节点到指定位置
 * @param[in,out] prev pos的前驱节点
 * @param[in,out] pos 插入位置
 * @param[in,out] newly 新节点
 * @note 将newly节点插入到pos的位置上，pos是从表头开始的位置
 * @par 示例:
 * @code
 	sclnk_iter pp = sclnk_at(&head, 7);
 	sclnk_iter pos = pp->next;
 	DATA* p = (DATA*)malloc(sizeof(DATA));
 	// 将p节点插入到pos所在的位置
 	sclnk_insert(pp, pos, &p->node);
 * @endcode
 */
static inline void sclnk_insert(sclnk_iter prev, 
						sclnk_iter pos, sclnk_node* newly)
{
	sclnk_insert_between(newly, prev, pos);
}

/**
 * @fn static inline void sclnk_erase(sclnk_iter prev, sclnk_iter entry);
 * @brief 擦除节点
 * @param[in,out] prev 目标节点前驱
 * @param[in,out] entry 目标节点
 * @par 示例:
 * @code
  	sclnk_iter p = sclnk_pbegin(&head);
 	sclnk_iter e = sclnk_end(&head);
 	sclnk_iter i = sclnk_begin(&head);
 	while (i != e) {
 		if (is_bad_data(container_of(i, DATA, node)))
 			sclnk_erase(p, sclnk_inc_later(i));
 		else
 			sclnk_prev_inc(p, i);
 	}
 * @endcode
 */
static inline void sclnk_erase(sclnk_iter prev, sclnk_iter entry)
{	/* 将entry节点从表中移除
	 *                              (entry)
	 *  -(prev)-(entry)-()- ===> 
	 *                           --(prev)--()--
	 */
	prev->next = entry->next;
}

NV_API void __sclnk_free(sclnk_iter begin, sclnk_iter end, 
						sclnk_pr1 erase_handler);

/**
 * @fn static inline void sclnk_erases(sclnk_iter bprev, sclnk_iter begin, 
							sclnk_iter end, sclnk_pr1 erase_handler);
 * @brief 清空区域
 * @param[in,out] bprev 区域起始位置前驱
 * @param[in,out] begin 区域起始位置
 * @param[in,out] end 区域终止位置
 * @param[in] erase_handler 移除响应函数，通常用于监视和释放节点，可以传递NULL,
 							表示不为删除的节点调用响应来处理
 * @note 函数会为每一个被移除的节点调用erase_handler来处理节点
 */
static inline void sclnk_erases(sclnk_iter bprev, sclnk_iter begin, 
							sclnk_iter end, sclnk_pr1 erase_handler)
{
	bprev->next = end;
	if (erase_handler)
		__sclnk_free(begin, end, erase_handler);
}

/**
 * @fn static inline void sclnk_clear(sclnk_head* head, sclnk_pr1 erase_handler);
 * @brief 清空表
 * @param[in,out] head 表头
 * @param[in] erase_handler 移除响应函数，通常用于监视和释放节点，可以传递NULL,
 							表示不为删除的节点调用响应来处理
 * @note 函数会为每一个被移除的节点调用erase_handler来处理节点
 */
static inline void sclnk_clear(sclnk_head* head, sclnk_pr1 erase_handler)
{
	sclnk_erases(head, head->next, head, erase_handler);
}

/**
 * @fn static inline void sclnk_push_front(sclnk_head* head, sclnk_node* newly);
 * @brief 在表首添加节点
 * @param[in,out] head 表头
 * @param[in,out] newly 新节点
 */
static inline void sclnk_push_front(sclnk_head* head, sclnk_node* newly)
{
	sclnk_insert_between(newly, head, head->next);
}

/**
 * @fn static inline void sclnk_pop_front(sclnk_head* head);
 * @brief 移除表首节点
 * @param[in,out] head 表头
 */
static inline void sclnk_pop_front(sclnk_head* head)
{
	sclnk_link(head, head->next->next);
}

/**
 * @fn static inline void sclnk_splice_between(
			sclnk_iter prev, sclnk_iter next, 
			sclnk_iter first_prev, sclnk_iter first, 
			sclnk_iter last, sclnk_iter last_next);
 * @brief 在两个节点之间插入多个连续节点
 * @param[in,out] prev 前驱节点
 * @param[in,out] next 后继节点
 * @param[in,out] first_prev 首节点的前驱节点
 * @param[in,out] first 首节点
 * @param[in,out] last 尾节点
 * @param[in,out] last_next 尾节点的后继节点
 * @note 将从first至last的节点嫁接到prev和next之间。如果prev和next之间有节点，
 * 		则它们之间的节点将会被替换
 */
static inline void sclnk_splice_between(
	sclnk_iter prev, sclnk_iter next, 
	sclnk_iter first_prev, sclnk_iter first, 
	sclnk_iter last, sclnk_iter last_next)
{	/* 将first节点至last节点的部分从旧表(B表)移接到新表(A表)中 
	 * (从first到last是按next方向)
	 * A:  -(prev)-(next)-      
	 * B: -(first_prev)-(first)-...-(last)-(last_next)-
	 * to:
	 * A: -(prev)-(first)-...-(last)-(next)-
	 * B: -(first_prev)-(last_next)-
	 */
	last->next = next;
	prev->next = first;
	first_prev->next = last_next;
}

/**
 * @fn static inline void sclnk_splice(sclnk_iter prev, sclnk_iter pos, 
				sclnk_iter first_prev, sclnk_iter first, sclnk_iter last);
 * @brief 在两个节点之间插入多个连续节点
 * @param[in,out] prev pos的前驱节点
 * @param[in,out] pos 插入位置
 * @param[in,out] first_prev 首节点的前驱节点
 * @param[in,out] first 首节点
 * @param[in,out] last 尾节点
 * @note 将从first至last的节点嫁接到pos位置。可以跨链表移动
 * @par 示例:
 * @code
 	// 将a链表的节点全部嫁接到b链表
 	sclnk_splice(bprev, b, a, sclnk_front(a), sclnk_back(b));
 	// 将b到e的节点们移动到第4个节点的位置
 	sclnk_splice(sclnk_at(head, 2), sclnk_at(head, 3), bprev, b, e);
 * @endcode
 */
static inline void sclnk_splice(sclnk_iter prev, sclnk_iter pos, 
				sclnk_iter first_prev, sclnk_iter first, sclnk_iter last)
{
	sclnk_splice_between(prev, pos, first_prev, first, last, last->next);
}

/**
 * @fn size_t sclnk_count(sclnk_head* head);
 * @brief 统计表的节点数量
 * @param[in] head 表头
 * @return size_t 返回表的节点总数
 */
NV_API size_t sclnk_count(sclnk_head* head);

/**
 * @fn size_t sclnk_distance(sclnk_iter first, sclnk_iter last);
 * @brief 计算两个节点的迭代距离
 * @param[in] first 首节点
 * @param[in] last 尾节点
 * @note 以索引为例，2到4的距离是4 (4-2+1=3)
 * @return size_t 返回距离
 */
NV_API size_t sclnk_distance(sclnk_iter first, sclnk_iter last);

/**
 * @fn sclnk_iter sclnk_advance(sclnk_head* head, sclnk_iter cur, size_t dist);
 * @brief 给迭代器增加一段距离或减小一段距离
 * @param[in] head 表头
 * @param[in] cur 迭代器
 * @param[in] dist 增加的距离。正数时，表示增加；负数时，表示减小
 * @return sclnk_iter 返回新迭代器
 */
NV_API sclnk_iter sclnk_advance(sclnk_head* head, sclnk_iter cur, size_t dist);

/**
 * @fn static inline sclnk_iter sclnk_at(sclnk_head* head, const size_t index);
 * @brief 根据正向索引得到迭代器
 * @param[in] head 表头
 * @param[in] index 索引，从0开始
 * @return sclnk_iter 返回对应的迭代器
 */
static inline sclnk_iter sclnk_at(sclnk_head* head, const size_t index)
{
	return sclnk_advance(head, head->next, index);
}

/**
 * @fn size_t sclnk_index_of(sclnk_head* head, sclnk_iter itr);
 * @brief 计算迭代器的正向索引
 * @param[in] head 表头
 * @param[in] itr 迭代器
 * @return size_t 返回索引
 */
NV_API size_t sclnk_index_of(sclnk_head* head, sclnk_iter itr);

/**
 * @fn static inline void sclnk_node_init(sclnk_node* entry);
 * @brief 初始化节点
 * @param[out] entry 节点
 */
static inline void sclnk_node_init(sclnk_node* entry)
{
	entry->next = entry;
}

/**
 * @fn static inline void sclnk_iter_replace(sclnk_node* vprev, 
							sclnk_node* victim, sclnk_node* newly);
 * @brief 用新节点替换目标节点
 * @param[in,out] vprev 目标节点前驱
 * @param[in,out] victim 受害目标节点
 * @param[in,out] newly 新节点
 */
static inline void sclnk_iter_replace(sclnk_node* vprev, 
							sclnk_node* victim, sclnk_node* newly)
{	/* 用newly节点替换表中的victim节点
	 *      (newly)                     (victim)
	 *                        ===>
	 * --(vprev)-(victim)-()--      --(vprev)-(newly)-()--
	 */
	sclnk_insert_between(newly, vprev, victim->next);
}

NV_API void __sclnk_iter_swap(
	sclnk_iter aprev, sclnk_iter a, sclnk_iter anext, 
	sclnk_iter bprev, sclnk_iter b, sclnk_iter bnext);

/**
 * @fn static inline void sclnk_iter_swap(sclnk_iter aprev, sclnk_iter a, 
									sclnk_iter bprev, sclnk_iter b);
 * @brief 交换两个节点
 * @param[in,out] aprev 第一个节点前驱
 * @param[in,out] a 第一个节点
 * @param[in,out] bprev 第二个节点前驱
 * @param[in,out] b 第二个节点
 */
static inline void sclnk_iter_swap(sclnk_iter aprev, sclnk_iter a, 
									sclnk_iter bprev, sclnk_iter b)
{
	__sclnk_iter_swap(aprev, a, a->next, bprev, b, b->next);
}

/**
 * @fn static inline void sclnk_replace(sclnk_head* victim, 
							sclnk_iter vlast, sclnk_head* newly);
 * @brief 用新表头替换目标表头
 * @param[in,out] victim 受害目标表头
 * @param[in,out] vlast 目标表表尾节点
 * @param[in,out] newly 新表头
 */
static inline void sclnk_replace(sclnk_head* victim, 
							sclnk_iter vlast, sclnk_head* newly)
{	/* 用newly头替换表中的victim头
	 *      [newly]                   [victim]
	 *                        ===>
	 * --(vlast)-[victim]-()--    --(vlast)-[newly]-()--
	 */
	vlast->next = newly;
	newly->next = victim->next;
}

static inline void __sclnk_swap(
	sclnk_iter a, sclnk_iter afront, sclnk_iter alast, 
	sclnk_iter b, sclnk_iter bfront, sclnk_iter blast)
{	/* 交换a、b两个头
	 * --(alast)-[a]-(afront)--       --(alast)-[b]-(afront)--
	 *                         ===>
	 * --(blast)-[b]-(bfront)--       --(blast)-[a]-(afront)--
	 */
	blast->next = a;
	a->next = bfront;
	alast->next = b;
	b->next = afront;
}

/**
 * @fn static inline void sclnk_swap(sclnk_head* a, sclnk_head* alast, 
 								sclnk_head* b, sclnk_head* blast);
 * @brief 交换两个表头
 * @param[in,out] a 第一个表头
 * @param[in,out] alast 第一个表尾节点
 * @param[in,out] b 第二个表头
 * @param[in,out] alast 第二个表尾节点
 */
static inline void sclnk_swap(sclnk_head* a, sclnk_head* alast, 
 								sclnk_head* b, sclnk_head* blast)
{
	__sclnk_swap(a, a->next, alast, b, b->next, blast);
}

/**
 * @fn static inline int sclnk_exist(sclnk_head* head, sclnk_iter itr);
 * @brief 判断一个节点是否存在于表中
 * @param[in] head 表头
 * @param[in] itr 目标节点
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 */
static inline int sclnk_exist(sclnk_head* head, sclnk_iter itr)
{
	return sclnk_index_of(head, itr) != (size_t)(-1);
}

/**
 * @fn static inline sclnk_iter sclnk_front(sclnk_head* head);
 * @brief 得到表首节点
 * @param[in] head 表头
 * @return sclnk_iter 返回表首节点
 */
static inline sclnk_iter sclnk_front(sclnk_head* head)
{
	return head->next;
}

/**
 * @fn static inline sclnk_iter sclnk_begin(sclnk_head* head);
 * @brief 得到指向正向起始位置的迭代器
 * @param[in] head 表头
 * @return sclnk_iter 返回迭代器
 * @par 示例:
 * @code
 	// 遍历链表
 	int index = 0;
 	sclnk_iter e = sclnk_end(&head);
 	sclnk_iter i = sclnk_begin(&head);
 	while (i != e) {
 		printf("[%d]=%d\n", index++, owner_of(i, DATA, node).data);
 		sclnk_inc(i);
 	}
 * @endcode
 * @see sclnk_end, sclnk_pbegin
 */
static inline sclnk_iter sclnk_begin(sclnk_head* head)
{
	return head->next;
}

/**
 * @fn static inline sclnk_iter sclnk_pbegin(sclnk_head* head);
 * @brief 得到指向正向起始位置前驱的迭代器
 * @param[in] head 表头
 * @return sclnk_iter 返回迭代器
 * @par 示例:
 * @code
  	sclnk_iter p = sclnk_pbegin(&head);
 	sclnk_iter e = sclnk_end(&head);
 	sclnk_iter i = sclnk_begin(&head);
 	while (i != e) {
 		if (is_bad_data(container_of(i, DATA, node)))
 			sclnk_erase(p, sclnk_inc_later(i));
 		else
 			sclnk_prev_inc(p, i);
 	}
 * @endcode
 * @see sclnk_begin, sclnk_end
 */
static inline sclnk_iter sclnk_pbegin(sclnk_head* head)
{
	return head;
}

/**
 * @fn static inline sclnk_iter sclnk_end(sclnk_head* head);
 * @brief 得到指向正向终止位置的迭代器
 * @param[in] head 表头
 * @return sclnk_iter 返回迭代器
 * @see sclnk_begin, sclnk_pbegin
 */
static inline sclnk_iter sclnk_end(sclnk_head* head)
{
	return head;
}

/**
 * @fn static inline sclnk_iter sclnk_end_of(sclnk_iter itr);
 * @brief 得到当前迭代器的正向终止位置
 * @param[in] itr 迭代器
 * @return sclnk_iter 返回迭代器
 */
static inline sclnk_iter sclnk_end_of(sclnk_iter itr)
{
	return itr->next;
}

/**
 * @fn static inline sclnk_iter sclnk_next(sclnk_iter itr);
 * @brief 得到当前节点的后继节点
 * @param[in] itr 当前节点
 * @return sclnk_iter 返回后继节点
 */
static inline sclnk_iter sclnk_next(sclnk_iter itr)
{
	return itr->next;
}

static inline sclnk_iter __sclnk_inc_later(sclnk_iter cur, sclnk_iter* p)
{
	*p = cur->next;
	return cur;
}

/**
 * @def sclnk_inc(itr)
 * @brief 迭代器递增
 * @def sclnk_prev_inc(itr, prev)
 * @brief 迭代器递增，并更新前驱迭代器
 * @def sclnk_inc_later(itr)
 * @brief 迭代器后自增
 */
#define sclnk_inc(itr) ((itr) = (itr)->next)
#define sclnk_prev_inc(itr, prev) ((prev) = (itr), sclnk_inc(itr))
#define sclnk_inc_later(itr) __sclnk_inc_later(itr, &(itr))

/**
 * @def sclnk_foreach(_begin, _end, fn, type, member)
 * @brief 正向遍历_begin到_end的节点，并为每一个节点调用回调函数或宏
 * @param[in] _begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define sclnk_foreach(_begin, _end, fn, type, member) \
	do {sclnk_iter _q_e = (_end);							\
		sclnk_iter _q_f = (_begin);							\
		while (_q_f != _q_e) {								\
			type * _q_t = container_of(_q_f, type, member);	\
			_q_f = _q_f->next;								\
			fn(_q_t);										\
		}													\
	} while (0)

/**
 * @def sclnk_search(head, _begin, _end, equal, var, res, type, member)
 * @brief 顺序查找区域内的指定节点
 * @param[in,out] head 表头
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] equal 比较函数或宏。相等时返回1，不相等返回0
 * @param[in] var 搜索的键值，常量，变量均可
 * @param[out] res 保存搜索结果的变量，类型是节点指针。结果为NULL表示找不到节点
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define sclnk_search(head, _begin, _end, equal, var, res, type, member) do {\
		sclnk_iter _q_e = (_end);								\
		sclnk_iter _q_f = (_begin);								\
		sclnk_head* _q_h = (head);								\
		(res) = NULL;											\
		while (_q_f != _q_e) {									\
			type * _q_cb = container_of(_q_f, type, member);	\
			if (equal(_q_cb, (val))) {							\
				(res) = _q_cb;									\
				break;											\
			}													\
			_q_f = _q_f->next;									\
		}														\
	} while (0)

/* 反转链表的两种方式：
 * (1) 直接使用 sclnk_reverse(head); (推荐)
 * (2) 按照如下方式迭代
 *     sclnk_iter i = sclnk_vbegin(head);
 *     sclnk_iter e = sclnk_vend(head);
 *     while (i != e) {
 *			sclnk_vpush(head, i);
 *     }
 *     当迭代完成时，反转完成
 */

static inline sclnk_iter __sclnk_vbegin(sclnk_head* head, sclnk_iter front)
{
	head->next = head;
	return front;
}

/**
 * @fn static inline sclnk_iter sclnk_vbegin(sclnk_head* head);
 * @brief 得到用于反转的起始位置的迭代器
 * @param[in] head 表头
 * @return sclnk_iter 返回迭代器
 * @par 示例:
 * @code
 	// 反转表
 	sclnk_iter ve = sclnk_vend(&head);
 	sclnk_iter vi = sclnk_vbegin(&head);
 	while (vi != ve) {
 		sclnk_vpush(&head, vi);
 	}
 * @endcode
 * @see sclnk_vend, sclnk_vpush, sclnk_reverse
 */
static inline sclnk_iter sclnk_vbegin(sclnk_head* head)
{
	return __sclnk_vbegin(head, head->next);
}

/**
 * @fn static inline sclnk_iter sclnk_vend(sclnk_head* head);
 * @brief 得到用于反转的终止位置的迭代器
 * @param[in] head 表头
 * @return sclnk_iter 返回迭代器
 * @par 示例:
 * @code
 	// 反转表
 	sclnk_iter ve = sclnk_vend(&head);
 	sclnk_iter vi = sclnk_vbegin(&head);
 	while (vi != ve) {
 		sclnk_vpush(&head, vi);
 	}
 * @endcode
 * @see sclnk_vbegin, sclnk_vpush, sclnk_reverse
 */
static inline sclnk_iter sclnk_vend(sclnk_head* head)
{
	return head;
}

/**
 * @def sclnk_vpush(head, itr)
 * @brief 入栈反转
 * @param[in,out] head 表头
 * @param[in,out] itr 迭代器
 * @par 示例:
 * @code
 	// 反转表
 	sclnk_iter ve = sclnk_vend(&head);
 	sclnk_iter vi = sclnk_vbegin(&head);
 	while (vi != ve) {
 		sclnk_vpush(&head, vi);
 	}
 * @endcode
 * @see sclnk_vbegin, sclnk_vend, sclnk_reverse
 */
#define sclnk_vpush(head, itr) sclnk_push_front(head, sclnk_inc_later(itr))

/**
 * @fn void sclnk_reverse(sclnk_head* head);
 * @brief 反转表
 * @param[in,out] head 表头
 * @see sclnk_vbegin, sclnk_vend, sclnk_vpush
 */
NV_API void sclnk_reverse(sclnk_head* head);

NV_API void __sclnk_serialize(sclnk_head* head, char** buf, const size_t offset);

NV_API void __sclnk_deserialize(sclnk_head* head, char** buf, 
							size_t cnt, const size_t offset);

/**
 * @def sclnk_serialize(head, buf, type, member)
 * @brief 表的序列化
 * @param[in] head 表头
 * @param[out] buf 序列缓存
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @note 函数会遍历整个表，逐个将节点的拥有者地址存入缓存
 * @par 示例:
 * @code
 	// 判断是否可以序列化
 	if (sclnk_serialable(&head)) {
 		slseq_head(DATA*) seq;
 		// 分配序列缓存
 		DATA** buf = (DATA**)malloc(sizeof(DATA*) * list_cnt);
 		// 序列化
 		sclnk_serialize(&head, buf, DATA, node);
 		SLSEQ_INIT_SERIAL(DATA*, &seq, buf, list_cnt);
 		// 希尔排序
 		slseq_shell_sort(DATA*, slseq_begin(&seq), 
 			slseq_end(&seq), DATA_Greater);
 		// 反序列化
 		sclnk_deserialize(&head, buf, list_cnt, DATA, node);
 		// 释放缓存
 		free(buf);
 	}
 * @endcode
 * @see sclnk_deserialize, sclnk_serialable
 */
#define sclnk_serialize(head, buf, type, member) \
	__sclnk_serialize(head, (char**)(buf), offset_of(type, member))

/**
 * @def sclnk_deserialize(head, buf, cnt, type, member)
 * @brief 表的反序列化
 * @param[out] head 表头
 * @param[out] buf 序列缓存
 * @param[in] cnt 序列长度
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @note 函数会遍历整个序列来还原整个表
 * @par 示例:
 * @code
 	// 判断是否可以序列化
 	if (sclnk_serialable(&head)) {
 		slseq_head(DATA*) seq;
 		// 分配序列缓存
 		DATA** buf = (DATA**)malloc(sizeof(DATA*) * list_cnt);
 		// 序列化
 		sclnk_serialize(&head, buf, DATA, node);
 		SLSEQ_INIT_SERIAL(DATA*, &seq, buf, list_cnt);
 		// 希尔排序
 		slseq_shell_sort(DATA*, slseq_begin(&seq), 
 			slseq_end(&seq), DATA_Greater);
 		// 反序列化
 		sclnk_deserialize(&head, buf, list_cnt, DATA, node);
 		// 释放缓存
 		free(buf);
 	}
 * @endcode
 * @see sclnk_serialize, sclnk_serialable
 */
#define sclnk_deserialize(head, buf, cnt, type, member) \
	__sclnk_deserialize(head, (char**)(buf), cnt, offset_of(type, member))

/**
 * @def sclnk_sort_insert(head, newly, greater_equal, type, member)
 * @brief 按升序插入新节点
 * @param[in,out] head 表头
 * @param[in,out] newly 新节点
 * @param[in] greater_equal 比较函数或宏，
 							升序:((*i)->x >= (*j)->x) 
 							降序:((*i)->x <= (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define sclnk_sort_insert(head, newly, greater_equal, type, member) do {\
		sclnk_iter _k_n = (newly);								\
		sclnk_head* _k_h = (head);								\
		sclnk_iter _k_j = _k_h;									\
		sclnk_iter _k_i = _k_h->next;							\
		while (_k_i != _k_h) {									\
			type * _k_pi = container_of(_k_i, type, member);	\
			type * _k_pn = container_of(_k_n, type, member);	\
			if (greater_equal(_k_pi, _k_pn))					\
				break;											\
			_k_j = _k_i;										\
			_k_i = _k_i->next;									\
		}														\
		sclnk_insert(_k_j, _k_i, _k_n);							\
	} while (0)

/**
 * @def sclnk_is_sorted(_begin, _end, greater_equal, res, type, member)
 * @brief 判断表是否按序
 * @param[in] _begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] greater_equal 比较函数或宏，
 							升序:((*i)->x >= (*j)->x) 
 							降序:((*i)->x <= (*j)->x)
 * @param[out] res 保存输出结果的变量
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see sclnk_bubble_sort, sclnk_oddeven_sort, 
 		sclnk_select_sort, sclnk_insert_sort, sclnk_sortable
 */
#define sclnk_is_sorted(_begin, _end, greater_equal, res, type, member) do {\
		sclnk_iter _k_e = (_end);								\
		sclnk_iter _k_i = (_begin);								\
		sclnk_iter _k_j = _k_i->next;							\
		(res) = 1;												\
		while (_k_j != _k_e) {									\
			type * _k_pi = container_of(_k_i, type, member);	\
			type * _k_pj = container_of(_k_j, type, member);	\
			if (!(greater_equal(_k_pj, _k_pi))) {				\
				(res) = 0;										\
				break;											\
			}													\
			_k_i = _k_j;										\
			_k_j = _k_j->next;									\
		}														\
	} while (0)

/* 单向环形链表排序性能比较
 * 随机数据：(最高) 插入排序 >> 选择排序 >> 冒泡排序 > 鸡尾酒排序 > 奇偶排序 (最低)
 * 顺序数据：(最高) 冒泡排序 > 奇偶排序 > 鸡尾酒排序 >> 插入排序 > 选择排序  (最低)
 * 逆序数据：(最高) 插入排序 >> 选择排序 >> 冒泡排序 > 鸡尾酒排序 > 奇偶排序  (最低)
 */

/**
 * @def sclnk_bubble_sort(bprev, _begin, _end, greater, type, member)
 * @brief 对表进行冒泡排序
 * @param[in,out] bprev 起始位置前驱
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] greater 比较函数或宏
 					升序:((*i)->x > (*j)->x)
 					降序:((*i)->x < (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see sclnk_sortable, sclnk_is_sorted
 */
#define sclnk_bubble_sort(bprev, _begin, _end, greater, type, member) do {\
		int _k_pc = 1;												\
		sclnk_iter _k_e = (_end);									\
		sclnk_iter _k_i = (_begin);									\
		sclnk_iter _k_ip = (bprev);									\
		for (; _k_pc && _k_i != _k_e; _k_i = _k_i->next) {			\
			sclnk_iter _k_jp = _k_i;								\
			sclnk_iter _k_j = _k_i->next;							\
			for (_k_pc = 0; _k_j != _k_e; _k_j = _k_j->next) {		\
				type * _k_pi = container_of(_k_i, type, member);	\
				type * _k_pj = container_of(_k_j, type, member);	\
				if (greater(_k_pi, _k_pj)) {						\
					sclnk_iter _k_t = _k_i;							\
					_k_i = _k_j;									\
					_k_j = _k_t;									\
					sclnk_iter_swap(_k_jp, _k_i, _k_ip, _k_j);		\
					_k_pc = 1;										\
				}													\
				_k_jp = _k_j;										\
			}														\
			_k_ip = _k_i;											\
		}															\
	} while (0)

/**
 * @def sclnk_oddeven_sort(bprev, _begin, _end, greater, type, member)
 * @brief 对表进行奇偶排序
 * @param[in,out] bprev 起始位置前驱
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] greater 比较函数或宏
 					升序:((*i)->x > (*j)->x)
 					降序:((*i)->x < (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see sclnk_sortable, sclnk_is_sorted
 */
#define sclnk_oddeven_sort(bprev, _begin, _end, greater, type, member) do {\
		int _k_pc = 1;														\
		sclnk_iter _k_e = (_end);											\
		sclnk_iter _k_f = (_begin);											\
		sclnk_iter _k_p = (bprev);											\
		while (_k_pc) {														\
			_k_pc = 0;														\
			_k_f = _k_p->next;												\
			__sclnk_oddeven_scan(greater, type, member, _k_p, _k_f);		\
			__sclnk_oddeven_scan(greater, type, member, _k_f, _k_f->next);	\
		}																	\
	} while (0)

#define __sclnk_oddeven_scan(greater, type, member, prev, _start) do {\
		sclnk_iter _k_ip = prev;								\
		sclnk_iter _k_i = _start;								\
		sclnk_iter _k_jp = _k_i;								\
		sclnk_iter _k_j = _k_i->next;							\
		while (_k_j != _k_e && _k_i != _k_e) {					\
			type * _k_pi = container_of(_k_i, type, member);	\
			type * _k_pj = container_of(_k_j, type, member);	\
			if (greater(_k_pi, _k_pj)) {						\
				sclnk_iter _k_t = _k_i;							\
				_k_i = _k_j;									\
				_k_j = _k_t;									\
				sclnk_iter_swap(_k_jp, _k_i, _k_ip, _k_j);		\
				_k_pc = 1;										\
			}													\
			_k_ip = _k_j;										\
			_k_i = _k_j->next;									\
			_k_jp = _k_i;										\
			_k_j = _k_i->next;									\
		}														\
	} while (0)

/**
 * @def sclnk_select_sort(bprev, _begin, _end, greater, type, member)
 * @brief 对表进行选择排序
 * @param[in,out] bprev 起始位置前驱
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] greater 比较函数或宏
 					升序:((*i)->x > (*j)->x)
 					降序:((*i)->x < (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see sclnk_sortable, sclnk_is_sorted
 */
#define sclnk_select_sort(bprev, _begin, _end, greater, type, member) do {\
		sclnk_iter _k_e = (_end);									\
		sclnk_iter _k_i = (_begin);									\
		sclnk_iter _k_ip = (bprev);									\
		sclnk_iter _k_jp = _k_i;									\
		sclnk_iter _k_j = _k_i->next;								\
		while (_k_j != _k_e) {										\
			sclnk_iter _k_mp = _k_ip;								\
			sclnk_iter _k_m = _k_i;									\
			while (_k_j != _k_e) {									\
				type * _k_pj = container_of(_k_j, type, member);	\
				type * _k_pm = container_of(_k_m, type, member);	\
				if (greater(_k_pm, _k_pj)) {						\
					_k_mp = _k_jp;									\
					_k_m = _k_j;									\
				}													\
				_k_jp = _k_j;										\
				_k_j = _k_j->next;									\
			}														\
			if (_k_i != _k_m) {										\
				sclnk_iter _k_t = _k_m;								\
				_k_m = _k_i;										\
				_k_i = _k_t;										\
				sclnk_iter_swap(_k_ip, _k_m, _k_mp, _k_i);			\
			}														\
			_k_ip = _k_i;											\
			_k_i = _k_i->next;										\
			_k_jp = _k_i;											\
			_k_j = _k_i->next;										\
		}															\
	} while (0)
/**
 * @def sclnk_insert_sort(bprev, _begin, _end, greater, type, member)
 * @brief 对表进行插入排序
 * @param[in,out] bprev 起始位置前驱
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] greater 比较函数或宏
 					升序:((*i)->x > (*j)->x)
 					降序:((*i)->x < (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see sclnk_sortable, sclnk_is_sorted
 */
#define sclnk_insert_sort(bprev, _begin, _end, greater_equal, type, member) \
	do {sclnk_iter _k_e = (_end);									\
		sclnk_iter _k_f = (_begin);									\
		sclnk_iter _k_p = (bprev);									\
		sclnk_iter _k_ip = _k_f;									\
		sclnk_iter _k_i = _k_f->next;								\
		for (; _k_i != _k_e; _k_i = _k_i->next) {					\
			type * _k_pi = container_of(_k_i, type, member);		\
			sclnk_iter _k_jp = _k_p;								\
			sclnk_iter _k_j = _k_p->next;							\
			while (_k_j != _k_i) {									\
				type * _k_pj = container_of(_k_j, type, member);	\
				if (greater_equal(_k_pj, _k_pi)) {					\
					sclnk_iter _k_t = _k_i;							\
					_k_i = _k_ip;									\
					sclnk_erase(_k_ip, _k_t);						\
					sclnk_insert(_k_jp, _k_j, _k_t);				\
					break;											\
				}													\
				_k_jp = _k_j;										\
				_k_j = _k_j->next;									\
			}														\
			_k_ip = _k_i;											\
		}															\
	} while (0)

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_SCLNK__ */
