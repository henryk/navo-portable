/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/linear/linked/dllnk.c
 * 
 * @brief Implements for doubly linear linked list.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#include "dllnk.h"

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

NV_IMPL int dllnk_check(dllnk_head* head, dllnk_iter* loop_entry)
{
	dllnk_iter sprv = (dllnk_iter)head;
	dllnk_iter fprv = (dllnk_iter)head;
	dllnk_iter slow = head->front;
	dllnk_iter fast = slow;
	
	while (fast && fast->next) {
		if (fprv != fast->prev || 
			sprv != slow->prev)
			return DLLNK_CHECK_LINK;
		sprv = slow;
		slow = slow->next;
		fprv = fast->next;
		fast = fprv->next;
		if (fast == slow && fast) {
			if (loop_entry) {
				/* 获取环路入口 */
				slow = head->front;
				while (fast != slow) {
					slow = slow->next;
					fast = fast->next;
				}
				*loop_entry = slow;
			}
			return DLLNK_CHECK_LOOP;
		}
	}
	
	return DLLNK_CHECK_PASS;
}

NV_IMPL void __dllnk_iter_swap(
	dllnk_iter aprev, dllnk_iter a, dllnk_iter anext, 
	dllnk_iter bprev, dllnk_iter b, dllnk_iter bnext)
{
	if (a == bprev) {
		/* (1) b节点是a节点的后继节点
		 *                               (a)
		 * -(ap)-(a)-(b)-(bn)- ===>                ===> -(ap)-(b)-(a)-(bn)-
		 *                          -(ap)-(b)-(bn)-
		 */
		aprev->next = b;
		b->prev = aprev;
		dllnk_insert_between(a, b, bnext);
	}
	else if (a == bnext) {
		/* (2) a节点是b节点的后继节点
		 *                               (b)
		 * -(bp)-(b)-(a)-(an)- ===>                ===> -(bp)-(a)-(b)-(an)-
		 *                          -(bp)-(a)-(an)-
		 */
		bprev->next = a;
		a->prev = bprev;
		dllnk_insert_between(b, a, anext);
	}
	else {
		/* (3) a节点、b节点不相邻 
		 * -(ap)-(a)-(an)-...-(bp)-(b)-(bn)- ===> 
		 *                                              (a)
		 *                                        -(ap)-(b)-(an)-...-(bp) (bn)-
		 *
		 *  --(ap)-(b)-(an)-...-(bp)-(a)-(bn)-- <===
		 */
		dllnk_insert_between(b, aprev, anext);
		dllnk_insert_between(a, bprev, bnext);
	}
}

NV_IMPL size_t dllnk_distance(dllnk_iter first, dllnk_iter last)
{
	size_t dist = 1;
	
	while (first != last) {
		dllnk_inc(first);
		++dist;
	}

	return dist;
}

NV_IMPL size_t dllnk_count(dllnk_head* head)
{
	size_t cnt = 0;
	dllnk_iter i = head->front;

	while (i) {
		dllnk_inc(i);
		++cnt;
	}

	return cnt;
}

NV_IMPL void dllnk_reverse(dllnk_head* head)
{
	dllnk_iter i = dllnk_vbegin(head);
	while (i)
		dllnk_vpush(head, i);
}

NV_IMPL dllnk_iter dllnk_advance(dllnk_head* head, dllnk_iter cur, int dist)
{
	if (dist > 0) {
		/* 增加 */
		while (cur && dist--)
			dllnk_inc(cur);
	} else {
		/* 减少 */
		while (cur != (dllnk_iter)(head) && dist++)
			dllnk_dec(cur);
	}
	
	return cur;
}

NV_IMPL size_t dllnk_index_of(dllnk_head* head, dllnk_iter itr)
{
	size_t cnt = 0;
	dllnk_iter i = head->front;
	
	while (i && i != itr) {
		dllnk_inc(i);
		++cnt;
	}
	
	return cnt;
}

NV_IMPL void __dllnk_serialize(dllnk_head* head, char** buf, 
								const size_t offset)
{
	dllnk_iter i = head->front;
	while (i) {
		*buf++ = ((char*)i) - offset;
		i = i->next;
	}
}

NV_IMPL void __dllnk_deserialize(dllnk_head* head, char** buf, 
								size_t cnt, const size_t offset)
{
	dllnk_iter t, p = (dllnk_iter)(head);

	if (!cnt)
		return;
	while (cnt--) {
		t = (dllnk_iter)(*buf++ + offset);
		p->next = t;
		t->prev = p;
		p = t;
	}
	t->next = NULL;
}

NV_IMPL void __dllnk_free(dllnk_iter begin, dllnk_iter end, 
						dllnk_pr1 erase_handler)
{
	while (begin != end)
		erase_handler(dllnk_inc_later(begin));
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

