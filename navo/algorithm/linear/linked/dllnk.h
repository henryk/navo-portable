/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/linear/linked/dllnk.h
 * 
 * @brief Doubly linear linked list.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_DLLNK__
#define __NV_DLLNK__

#include "../../../port/cdef.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup 双向线形链表
 * @ingroup 链式表
 * @{
 */

/**
 * @struct __dllnk_node
 * @brief 双向线形链表节点
 * @typedef dllnk_node
 * @brief 节点
 * @typedef dllnk_iter
 * @brief 迭代器
 */
typedef struct __dllnk_node {
	/** @brief 后继节点 */
	struct __dllnk_node* next;
	/** @brief 前驱节点 */
	struct __dllnk_node* prev;
}dllnk_node, *dllnk_iter;

/**
 * @struct dllnk_head
 * @brief 双向线形链表表头
 */
typedef struct {
	/** @brief 表首节点 */
	struct __dllnk_node* front;
}dllnk_head;

/**
 * @def DLLNK_HEAD(name)
 * @brief 定义一个名为name的表头，并对其初始化
 * @param name 表头名称
 * @def DLLNK_NODE(name)
 * @brief 定义一个名为name的节点，并对其初始化
 * @param name 节点名称
 */
#define DLLNK_HEAD(name) dllnk_head name = {NULL}
#define DLLNK_NODE(name) dllnk_node name = {NULL, NULL}

/**
 * @typedef dllnk_pr1
 * @brief 单参数回调函数指针
 * @typedef dllnk_pr2
 * @brief 双参数回调函数指针
 */
typedef int (*dllnk_pr1)(dllnk_node*);
typedef int (*dllnk_pr2)(dllnk_node*, dllnk_node*);

/**
 * @fn int dllnk_check(dllnk_head* head, dllnk_iter* loop_entry);
 * @brief 判断链表结构是否正常
 * @param[in] head 表头
 * @param[out] loop_entry 指向保存环路入口的迭代器，可以为NULL。
 			若为空，则不返回环路入口；
 			若不为空且仅有环路问题，则函数将环路入口保存到loop_entry
 * @return int 返回结果
 * @retval DLLNK_CHECK_PASS 0 正常
 * @retval DLLNK_CHECK_LOOP 1 有非法环路
 * @retval DLLNK_CHECK_LINK 2 有节点的前驱指针不正确
 */
NV_API int dllnk_check(dllnk_head* head, dllnk_iter* loop_entry);

#define DLLNK_CHECK_PASS 	0 /* 正常 */
#define DLLNK_CHECK_LOOP 	1 /* 有非法环路 */
#define DLLNK_CHECK_LINK 	2 /* 有节点的前驱指针不正确 */

/**
 * @fn static inline void dllnk_init(dllnk_head* head);
 * @brief 初始化表头
 * @param[out] head 目标表头
 * @par 示例:
 * @code
	dllnk_head head;
	// 初始化
	dllnk_init(&head);
 * @endcode
 */
static inline void dllnk_init(dllnk_head* head)
{
 	head->front = NULL;
}

/**
 * @fn static inline int dllnk_empty(dllnk_head* head);
 * @brief 判断表是否为空
 * @param[in] head 表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	if (!dllnk_empty(&head)) {
 		......
 	}
 * @endcode
 */
static inline int dllnk_empty(dllnk_head* head)
{
	return !head->front;
}

static inline int __dllnk_singular(dllnk_node* front)
{
	return (front && !front->next);
}

/**
 * @fn static inline int dllnk_singular(dllnk_head* head);
 * @brief 判断表是否只有单个节点
 * @param[in] head 表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	if (dllnk_singular(&head)) {
 		......
 	}
 * @endcode
 */
static inline int dllnk_singular(dllnk_head* head)
{
	return __dllnk_singular(head->front);
}

/**
 * @fn static inline int dllnk_sortable(dllnk_iter begin, dllnk_iter end);
 * @brief 判断表是否可以排序
 * @param[in] begin 起始位置
 * @param[in] end 结束位置
 * @note 即判断是否至少有两个节点
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	dllnk_iter begin = dllnk_begin(&head);
 	dllnk_iter end = dllnk_end(&head);
 	// 判断是否可以排序
 	if (dllnk_sortable(begin, end)) {
 		// 插入排序
 		dllnk_insert_sort(begin, end, DATA_GreaterEqual, DATA, node);
 	}
 * @endcode
 * @see dllnk_bubble_sort, dllnk_oddeven_sort, 
 		dllnk_select_sort, dllnk_insert_sort, dllnk_is_sorted
 */
static inline int dllnk_sortable(dllnk_iter begin, dllnk_iter end)
{
	return (begin != end && begin->next != end);
}

/**
 * @fn static inline int dllnk_serialable(dllnk_head* head);
 * @brief 判断表是否可以序列化
 * @param[in] head 表头
 * @note 即判断是否至少有一个节点
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	// 判断是否可以序列化
 	if (dllnk_serialable(&head)) {
 		slseq_head(DATA*) seq;
 		// 分配序列缓存
 		DATA** buf = (DATA**)malloc(sizeof(DATA*) * list_cnt);
 		// 序列化
 		dllnk_serialize(&head, buf, DATA, node);
 		SLSEQ_INIT_SERIAL(DATA*, &seq, buf, list_cnt);
 		// 希尔排序
 		slseq_shell_sort(DATA*, slseq_begin(&seq), 
 			slseq_end(&seq), DATA_Greater);
 		// 反序列化
 		dllnk_deserialize(&head, buf, list_cnt, DATA, node);
 		// 释放缓存
 		free(buf);
 	}
 * @endcode
 * @see dllnk_serialize, dllnk_deserialize
 */
static inline int dllnk_serialable(dllnk_head* head)
{
	return !!head->front;
}

/**
 * @fn static inline void dllnk_link(dllnk_iter prev, dllnk_iter next);
 * @brief 链接两个节点
 * @param[in,out] prev 前驱节点
 * @param[in,out] next 后继节点
 * @note 链接prev和next节点
 */
static inline void dllnk_link(dllnk_iter prev, dllnk_iter next)
{	/* 强制连接prev节点和next节点(next可以为空节点) 
	 * (1) next不为空 
	 * --(prev)--  (next)-- ===> --(prev)--(next)--
	 * (2) next为空 
	 * --(prev)--  (null) ===> --(prev)-(null)
	 */
	prev->next = next;
	if (next)
		next->prev = prev;
}

/**
 * @fn static inline void dllnk_insert_between(dllnk_node* newly, 
												dllnk_iter prev, 
												dllnk_iter next);
 * @brief 在两个节点之间插入一个新节点
 * @param[in,out] newly 新节点
 * @param[in,out] prev 前驱节点
 * @param[in,out] next 后继节点
 * @note 将newly节点插入到prev和next节点之间。如果prev和next不是相邻节点，
 *       则newly将替换掉prev和next之间的所有节点
 */
static inline void dllnk_insert_between(dllnk_node* newly, 
												dllnk_iter prev, 
												dllnk_iter next)
{	/* 强制连接prev节点和next节点，并在两者之间插入newly节点
	 * (1) prev节点和next节点原来未连接 
	 *       (newly)
	 *                   ===> --(prev)-(newly)-(next)--
	 * --(prev) (next)--
	 * (2) prev节点和next节点原来已连接 
	 *       (newly)
	 *                   ===> --(prev)-(newly)-(next)--
	 * --(prev)-(next)--
	 */
	newly->next = next;
	if (next)
		next->prev = newly;
	prev->next = newly;
	newly->prev = prev;
	/* 上面的是性能优化后的版本，实际过程同下：
	 * dllnk_link(prev, newly);
	 * dllnk_link(newly, next);
	 *
	 * (1)连接prev节点与newly节点  (2)再连接newly节点与next节点
	 *          (newly)                    (newly)
	 *          /                          /    \
	 *    --(prev)  (next)--         --(prev)  (next)--
	 */
}

/**
 * @fn static inline void dllnk_insert(dllnk_iter pos, dllnk_node* newly);
 * @brief 插入节点到指定位置
 * @param[in,out] pos 插入位置
 * @param[in,out] newly 新节点
 * @note 将newly节点插入到pos的位置上，pos是从表头开始的位置
 * @par 示例:
 * @code
 	dllnk_iter pos = dllnk_at(&head, 8);
 	DATA* p = (DATA*)malloc(sizeof(DATA));
 	// 将p节点插入到pos所在的位置
 	dllnk_insert(pos, &p->node);
 * @endcode
 */
static inline void dllnk_insert(dllnk_iter pos, dllnk_node* newly)
{
	dllnk_insert_between(newly, pos->prev, pos);
}

/**
 * @fn static inline void dllnk_erase(dllnk_iter entry);
 * @brief 擦除节点
 * @param[in,out] entry 目标节点
 * @par 示例:
 * @code
 	dllnk_iter e = dllnk_end(&head);
 	dllnk_iter i = dllnk_begin(&head);
 	while (i != e) {
 		if (is_bad_data(container_of(i, DATA, node)))
 			dllnk_erase(dllnk_inc_later(i));
 		else
 			dllnk_inc(i);
 	}
 * @endcode
 */
static inline void dllnk_erase(dllnk_iter entry)
{	/* 将entry节点从表中移除
	 *                        (entry)
	 *  -()-(entry)-()- ===> 
	 *                       --()--()--
	 */
	dllnk_link(entry->prev, entry->next);
}

NV_API void __dllnk_free(dllnk_iter begin, dllnk_iter end, 
						dllnk_pr1 erase_handler);

/**
 * @fn static inline void dllnk_erases(dllnk_iter begin, 
							dllnk_iter end, dllnk_pr1 erase_handler);
 * @brief 清空区域
 * @param[in,out] begin 区域起始位置
 * @param[in,out] end 区域终止位置
 * @param[in] erase_handler 移除响应函数，通常用于监视和释放节点，可以传递NULL,
 							表示不为删除的节点调用响应来处理
 * @note 函数会为每一个被移除的节点调用erase_handler来处理节点
 */
static inline void dllnk_erases(dllnk_iter begin, 
							dllnk_iter end, dllnk_pr1 erase_handler)
{
	dllnk_iter prev = begin->prev;
	dllnk_link(prev, end);
	if (erase_handler)
		__dllnk_free(begin, end, erase_handler);
}

/**
 * @fn static inline void dllnk_clear(dllnk_head* head, dllnk_pr1 erase_handler);
 * @brief 清空表
 * @param[in,out] head 表头
 * @param[in] erase_handler 移除响应函数，通常用于监视和释放节点，可以传递NULL,
 							表示不为删除的节点调用响应来处理
 * @note 函数会为每一个被移除的节点调用erase_handler来处理节点
 */
static inline void dllnk_clear(dllnk_head* head, dllnk_pr1 erase_handler)
{
	dllnk_erases(head->front, NULL, erase_handler);
}

/**
 * @fn static inline void dllnk_push_front(dllnk_head* head, dllnk_node* newly);
 * @brief 在表首添加节点
 * @param[in,out] head 表头
 * @param[in,out] newly 新节点
 */
static inline void dllnk_push_front(dllnk_head* head, dllnk_node* newly)
{
	dllnk_insert_between(newly, (dllnk_iter)head, head->front);
}

/**
 * @fn static inline void dllnk_pop_front(dllnk_head* head);
 * @brief 移除表首节点
 * @param[in,out] head 表头
 */
static inline void dllnk_pop_front(dllnk_head* head)
{
	dllnk_link((dllnk_iter)head, head->front->next);
}

/**
 * @fn static inline void dllnk_splice_between(
			dllnk_iter prev, dllnk_iter next, 
			dllnk_iter first_prev, dllnk_iter first, 
			dllnk_iter last, dllnk_iter last_next);
 * @brief 在两个节点之间插入多个连续节点
 * @param[in,out] prev 前驱节点
 * @param[in,out] next 后继节点
 * @param[in,out] first_prev 首节点的前驱节点
 * @param[in,out] first 首节点
 * @param[in,out] last 尾节点
 * @param[in,out] last_next 尾节点的后继节点
 * @note 将从first至last的节点嫁接到prev和next之间。如果prev和next之间有节点，
 * 		则它们之间的节点将会被替换
 */
static inline void dllnk_splice_between(
	dllnk_iter prev, dllnk_iter next, 
	dllnk_iter first_prev, dllnk_iter first, 
	dllnk_iter last, dllnk_iter last_next)
{	/* 将first节点至last节点的部分从旧表(B表)移接到新表(A表)中 
	 * (从first到last是按next方向)
	 * A:  -(prev)-(next)-      
	 * B: -(first_prev)-(first)-...-(last)-(last_next)-
	 * to:
	 * A: -(prev)-(first)-...-(last)-(next)-
	 * B: -(first_prev)-(last_next)-
	 */
	dllnk_link(last, next);
	prev->next = first;
	first->prev = prev;
	dllnk_link(first_prev, last_next);
}

/**
 * @fn static inline void dllnk_splice(dllnk_iter pos, 
 							dllnk_iter first, dllnk_iter last);
 * @brief 在两个节点之间插入多个连续节点
 * @param[in,out] pos 插入位置
 * @param[in,out] first 首节点
 * @param[in,out] last 尾节点
 * @note 将从first至last的节点嫁接到pos位置。可以跨链表移动
 * @par 示例:
 * @code
 	// 将a链表的节点全部嫁接到b链表
 	dllnk_splice(b, dllnk_front(a), dllnk_back(b));
 	// 将b到e的节点们移动到第4个节点的位置
 	dllnk_splice(dllnk_at(head, 3), b, e);
 * @endcode
 */
static inline void dllnk_splice(dllnk_iter pos, 
						dllnk_iter first, dllnk_iter last)
{
	dllnk_splice_between(pos->prev, pos, first->prev, first, last, last->next);
}

/**
 * @fn size_t dllnk_count(dllnk_head* head);
 * @brief 统计表的节点数量
 * @param[in] head 表头
 * @return size_t 返回表的节点总数
 */
NV_API size_t dllnk_count(dllnk_head* head);

/**
 * @fn size_t dllnk_distance(dllnk_iter first, dllnk_iter last);
 * @brief 计算两个节点的迭代距离
 * @param[in] first 首节点
 * @param[in] last 尾节点
 * @note 以索引为例，2到4的距离是4 (4-2+1=3)
 * @return size_t 返回距离
 */
NV_API size_t dllnk_distance(dllnk_iter first, dllnk_iter last);

/**
 * @fn dllnk_iter dllnk_advance(dllnk_head* head, dllnk_iter cur, int dist);
 * @brief 给迭代器增加一段距离或减小一段距离
 * @param[in] head 表头
 * @param[in] cur 迭代器
 * @param[in] dist 增加的距离。正数时，表示增加；负数时，表示减小
 * @return dllnk_iter 返回新迭代器
 */
NV_API dllnk_iter dllnk_advance(dllnk_head* head, dllnk_iter cur, int dist);

/**
 * @fn static inline dllnk_iter dllnk_at(dllnk_head* head, const size_t index);
 * @brief 根据正向索引得到迭代器
 * @param[in] head 表头
 * @param[in] index 索引，从0开始
 * @return dllnk_iter 返回对应的迭代器
 */
static inline dllnk_iter dllnk_at(dllnk_head* head, const size_t index)
{
	return dllnk_advance(head, head->front, index);
}

/**
 * @fn size_t dllnk_index_of(dllnk_head* head, dllnk_iter itr);
 * @brief 计算迭代器的正向索引
 * @param[in] head 表头
 * @param[in] itr 迭代器
 * @return size_t 返回索引
 */
NV_API size_t dllnk_index_of(dllnk_head* head, dllnk_iter itr);

/**
 * @fn static inline void dllnk_node_init(dllnk_node* entry);
 * @brief 初始化节点
 * @param[out] entry 节点
 */
static inline void dllnk_node_init(dllnk_node* entry)
{
	entry->next = NULL;
	entry->prev = NULL;
}

/**
 * @fn static inline void dllnk_iter_replace(dllnk_node* victim, 
 										dllnk_node* newly);
 * @brief 用新节点替换目标节点
 * @param[in,out] victim 受害目标节点
 * @param[in,out] newly 新节点
 */
static inline void dllnk_iter_replace(dllnk_node* victim, dllnk_node* newly)
{	/* 用newly节点替换表中的victim节点
	 *      (newly)                 (victim)
	 *                    ===>
	 * --()-(victim)-()--      --()-(newly)-()--
	 */
	dllnk_insert_between(newly, victim->prev, victim->next);
}

NV_API void __dllnk_iter_swap(
	dllnk_iter aprev, dllnk_iter a, dllnk_iter anext, 
	dllnk_iter bprev, dllnk_iter b, dllnk_iter bnext);

/**
 * @fn static inline void dllnk_iter_swap(dllnk_iter a, dllnk_iter b);
 * @brief 交换两个节点
 * @param[in,out] a 第一个节点
 * @param[in,out] b 第二个节点
 */
static inline void dllnk_iter_swap(dllnk_iter a, dllnk_iter b)
{
	__dllnk_iter_swap(a->prev, a, a->next, b->prev, b, b->next);
}

/**
 * @fn static inline void dllnk_replace(dllnk_head* victim, dllnk_head* newly);
 * @brief 用新表头替换目标表头
 * @param[in,out] victim 受害目标表头
 * @param[in,out] newly 新表头
 */
static inline void dllnk_replace(dllnk_head* victim, dllnk_head* newly)
{	/* 用newly头替换表中的victim头
	 *      [newly]                 [victim]
	 *                    ===>
	 * [victim]-(front)--      [newly]-(front)--
	 */
	dllnk_link((dllnk_iter)newly, victim->front);
}

static inline void __dllnk_swap(dllnk_iter a, dllnk_iter afront, 
								dllnk_iter b, dllnk_iter bfront)
{	/* 交换a、b两个头
	 * --(aback)-[a]-(afront)--       --(aback)-[b]-(afront)--
	 *                         ===>
	 * --(bback)-[b]-(bfront)--       --(bback)-[a]-(afront)--
	 */
	dllnk_link(b, afront);
	dllnk_link(a, bfront);
}

/**
 * @fn static inline void dllnk_swap(dllnk_head* a, dllnk_head* b);
 * @brief 交换两个表头
 * @param[in,out] a 第一个表头
 * @param[in,out] b 第二个表头
 */
static inline void dllnk_swap(dllnk_head* a, dllnk_head* b)
{
	__dllnk_swap((dllnk_iter)a, a->front, (dllnk_iter)b, b->front);
}

/**
 * @fn static inline int dllnk_exist(dllnk_head* head, dllnk_iter itr);
 * @brief 判断一个节点是否存在于表中
 * @param[in] head 表头
 * @param[in] itr 目标节点
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 */
static inline int dllnk_exist(dllnk_head* head, dllnk_iter itr)
{
	return dllnk_index_of(head, itr) != (size_t)(-1);
}

/**
 * @fn static inline dllnk_iter dllnk_front(dllnk_head* head);
 * @brief 得到表首节点
 * @param[in] head 表头
 * @return dllnk_iter 返回表首节点
 */
static inline dllnk_iter dllnk_front(dllnk_head* head)
{
	return head->front;
}

/**
 * @fn static inline dllnk_iter dllnk_begin(dllnk_head* head);
 * @brief 得到指向正向起始位置的迭代器
 * @param[in] head 表头
 * @return dllnk_iter 返回迭代器
 * @par 示例:
 * @code
 	// 遍历链表
 	int index = 0;
 	dllnk_iter e = dllnk_end();
 	dllnk_iter i = dllnk_begin(&head);
 	while (i != e) {
 		printf("[%d]=%d\n", index++, owner_of(i, DATA, node).data);
 		dllnk_inc(i);
 	}
 * @endcode
 * @see dllnk_end
 */
static inline dllnk_iter dllnk_begin(dllnk_head* head)
{
	return head->front;
}

/**
 * @fn static inline dllnk_iter dllnk_end();
 * @brief 得到指向正向终止位置的迭代器
 * @return dllnk_iter 返回迭代器
 * @note 此函数永远返回NULL
 * @see dllnk_begin
 */
static inline dllnk_iter dllnk_end()
{
	return NULL;
}

/**
 * @fn static inline dllnk_iter dllnk_end_of(dllnk_iter itr);
 * @brief 得到当前迭代器的正向终止位置
 * @param[in] itr 迭代器
 * @return dllnk_iter 返回迭代器
 * @see dllnk_begin, dllnk_end
 */
static inline dllnk_iter dllnk_end_of(dllnk_iter itr)
{
	return itr->next;
}

/**
 * @fn static inline dllnk_iter dllnk_next(dllnk_iter itr);
 * @brief 得到当前节点的后继节点
 * @param[in] itr 当前节点
 * @return dllnk_iter 返回后继节点
 */
static inline dllnk_iter dllnk_next(dllnk_iter itr)
{
	return itr->next;
}

/**
 * @fn static inline dllnk_iter dllnk_prev(dllnk_iter itr);
 * @brief 得到当前节点的前驱节点
 * @param[in] itr 当前节点
 * @return dllnk_iter 返回前驱节点
 */
static inline dllnk_iter dllnk_prev(dllnk_iter itr)
{
	return itr->prev;
}

static inline dllnk_iter __dllnk_inc_later(dllnk_iter cur, dllnk_iter* p)
{
	*p = cur->next;
	return cur;
}

static inline dllnk_iter __dllnk_dec_later(dllnk_iter cur, dllnk_iter* p)
{
	*p = cur->prev;
	return cur;
}

/**
 * @def dllnk_inc(itr)
 * @brief 迭代器递增
 * @def dllnk_dec(itr)
 * @brief 迭代器递减
 * @def dllnk_inc_later(itr)
 * @brief 迭代器后自增
 * @def dllnk_dec_later(itr)
 * @brief 迭代器后自减
 */
#define dllnk_inc(itr) ((itr) = (itr)->next)
#define dllnk_dec(itr) ((itr) = (itr)->prev)
#define dllnk_inc_later(itr) __dllnk_inc_later(itr, &(itr))
#define dllnk_dec_later(itr) __dllnk_dec_later(itr, &(itr))

/**
 * @def dllnk_foreach(_begin, _end, fn, type, member)
 * @brief 正向遍历_begin到_end的节点，并为每一个节点调用回调函数或宏
 * @param[in] _begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define dllnk_foreach(_begin, _end, fn, type, member) \
	do {dllnk_iter _q_e = (_end);							\
		dllnk_iter _q_f = (_begin);							\
		while (_q_f != _q_e) {								\
			type * _q_t = container_of(_q_f, type, member);	\
			_q_f = _q_f->next;								\
			fn(_q_t);										\
		}													\
	} while (0)

/**
 * @def dllnk_search(head, _begin, _end, equal, var, res, type, member)
 * @brief 顺序查找区域内的指定节点
 * @param[in,out] head 表头
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] equal 比较函数或宏。相等时返回1，不相等返回0
 * @param[in] var 搜索的键值，常量，变量均可
 * @param[out] res 保存搜索结果的变量，类型是节点指针。结果为NULL表示找不到节点
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define dllnk_search(head, _begin, _end, equal, var, res, type, member) \
	do {dllnk_iter _q_e = (_end);								\
		dllnk_iter _q_f = (_begin);								\
		dllnk_head* _q_h = (head);								\
		(res) = NULL;											\
		while (_q_f != _q_e) {									\
			type * _q_cb = container_of(_q_f, type, member);	\
			if (equal(_q_cb, (val))) {							\
				(res) = _q_cb;									\
				break;											\
			}													\
			_q_f = _q_f->next;									\
		}														\
	} while (0)

/* 反转链表的两种方式：
 * (1) 直接使用 dllnk_reverse(head); (推荐)
 * (2) 按照如下方式迭代
 *     dllnk_iter i = dllnk_vbegin(head);
 *     dllnk_iter e = dllnk_vend(head);
 *     while (i != e) {
 *			dllnk_vpush(head, i);
 *     }
 *     当迭代完成时，反转完成
 */

static inline dllnk_iter __dllnk_vbegin(dllnk_head* head, dllnk_iter front)
{
	head->front = NULL;
	return front;
}

/**
 * @fn static inline dllnk_iter dllnk_vbegin(dllnk_head* head);
 * @brief 得到用于反转的起始位置的迭代器
 * @param[in] head 表头
 * @return dllnk_iter 返回迭代器
 * @par 示例:
 * @code
 	// 反转表
 	dllnk_iter ve = dllnk_vend(&head);
 	dllnk_iter vi = dllnk_vbegin(&head);
 	while (vi != ve) {
 		dllnk_vpush(&head, vi);
 	}
 * @endcode
 * @see dllnk_vend, dllnk_vpush, dllnk_reverse
 */
static inline dllnk_iter dllnk_vbegin(dllnk_head* head)
{
	return __dllnk_vbegin(head, head->front);
}

/**
 * @fn static inline dllnk_iter dllnk_vend(dllnk_head* head);
 * @brief 得到用于反转的终止位置的迭代器
 * @param[in] head 表头
 * @return dllnk_iter 返回迭代器
 * @note 函数永远返回NULL
 * @par 示例:
 * @code
 	// 反转表
 	dllnk_iter ve = dllnk_vend(&head);
 	dllnk_iter vi = dllnk_vbegin(&head);
 	while (vi != ve) {
 		dllnk_vpush(&head, vi);
 	}
 * @endcode
 * @see dllnk_vbegin, dllnk_vpush, dllnk_reverse
 */
static inline dllnk_iter dllnk_vend(dllnk_head* head)
{
	return NULL;
}

/**
 * @def dllnk_vpush(head, itr)
 * @brief 入栈反转
 * @param[in,out] head 表头
 * @param[in,out] itr 迭代器
 * @par 示例:
 * @code
 	// 反转表
 	dllnk_iter ve = dllnk_vend(&head);
 	dllnk_iter vi = dllnk_vbegin(&head);
 	while (vi != ve) {
 		dllnk_vpush(&head, vi);
 	}
 * @endcode
 * @see dllnk_vbegin, dllnk_vend, dllnk_reverse
 */
#define dllnk_vpush(head, itr) dllnk_push_front(head, dllnk_inc_later(itr))

/**
 * @fn void dllnk_reverse(dllnk_head* head);
 * @brief 反转表
 * @param[in,out] head 表头
 * @see dllnk_vbegin, dllnk_vend, dllnk_vpush
 */
NV_API void dllnk_reverse(dllnk_head* head);

NV_API void __dllnk_serialize(dllnk_head* head, char** buf, 
							const size_t offset);

NV_API void __dllnk_deserialize(dllnk_head* head, char** buf, 
							size_t cnt, const size_t offset);

/**
 * @def dllnk_serialize(head, buf, type, member)
 * @brief 表的序列化
 * @param[in] head 表头
 * @param[out] buf 序列缓存
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @note 函数会遍历整个表，逐个将节点的拥有者地址存入缓存
 * @par 示例:
 * @code
 	// 判断是否可以序列化
 	if (dllnk_serialable(&head)) {
 		slseq_head(DATA*) seq;
 		// 分配序列缓存
 		DATA** buf = (DATA**)malloc(sizeof(DATA*) * list_cnt);
 		// 序列化
 		dllnk_serialize(&head, buf, DATA, node);
 		SLSEQ_INIT_SERIAL(DATA*, &seq, buf, list_cnt);
 		// 希尔排序
 		slseq_shell_sort(DATA*, &seq, slseq_begin(&seq), 
 			slseq_end(&seq), DATA_Greater);
 		// 反序列化
 		dllnk_deserialize(&head, buf, list_cnt, DATA, node);
 		// 释放缓存
 		free(buf);
 	}
 * @endcode
 * @see dllnk_deserialize, dllnk_serialable
 */
#define dllnk_serialize(head, buf, type, member) \
	__dllnk_serialize(head, (char**)(buf), offset_of(type, member))

/**
 * @def dllnk_deserialize(head, buf, cnt, type, member)
 * @brief 表的反序列化
 * @param[out] head 表头
 * @param[out] buf 序列缓存
 * @param[in] cnt 序列长度
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @note 函数会遍历整个序列来还原整个表
 * @par 示例:
 * @code
 	// 判断是否可以序列化
 	if (dllnk_serialable(&head)) {
 		slseq_head(DATA*) seq;
 		// 分配序列缓存
 		DATA** buf = (DATA**)malloc(sizeof(DATA*) * list_cnt);
 		// 序列化
 		dllnk_serialize(&head, buf, DATA, node);
 		SLSEQ_INIT_SERIAL(DATA*, &seq, buf, list_cnt);
 		// 希尔排序
 		slseq_shell_sort(DATA*, slseq_begin(&seq), 
 			slseq_end(&seq), DATA_Greater);
 		// 反序列化
 		dllnk_deserialize(&head, buf, list_cnt, DATA, node);
 		// 释放缓存
 		free(buf);
 	}
 * @endcode
 * @see dllnk_serialize, dllnk_serialable
 */
#define dllnk_deserialize(head, buf, cnt, type, member) \
	__dllnk_deserialize(head, (char**)(buf), cnt, offset_of(type, member))

/**
 * @def dllnk_sort_insert(head, newly, greater_equal, type, member)
 * @brief 按升序插入新节点
 * @param[in,out] head 表头
 * @param[in,out] newly 新节点
 * @param[in] greater_equal 比较函数或宏，
 							升序:((*i)->x >= (*j)->x) 
 							降序:((*i)->x <= (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define dllnk_sort_insert(head, newly, greater_equal, type, member) do {\
		dllnk_iter _k_n = (newly);								\
		dllnk_head* _k_h = (head);								\
		dllnk_iter _k_i = _k_h->next;							\
		while (_k_i) {											\
			type * _k_pi = container_of(_k_i, type, member);	\
			type * _k_pn = container_of(_k_n, type, member);	\
			if (greater_equal(_k_pi, _k_pn))					\
				break;											\
			_k_i = _k_i->next;									\
		}														\
		dllnk_insert(_k_i, _k_n);								\
	} while (0)

/**
 * @def dllnk_is_sorted(_begin, _end, greater_equal, res, type, member)
 * @brief 判断表是否按序
 * @param[in] _begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] greater_equal 比较函数或宏，
 							升序:((*i)->x >= (*j)->x) 
 							降序:((*i)->x <= (*j)->x)
 * @param[out] res 保存输出结果的变量
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see dllnk_bubble_sort, dllnk_oddeven_sort, 
 		dllnk_select_sort, dllnk_insert_sort, dllnk_sortable
 */
#define dllnk_is_sorted(_begin, _end, greater_equal, res, type, member) do {\
		dllnk_iter _k_e = (_end);								\
		dllnk_iter _k_i = (_begin);								\
		dllnk_iter _k_j = _k_i->next;							\
		(res) = 1;												\
		while (_k_j != _k_e) {									\
			type * _k_pi = container_of(_k_i, type, member);	\
			type * _k_pj = container_of(_k_j, type, member);	\
			if (!(greater_equal(_k_pj, _k_pi))) {				\
				(res) = 0;										\
				break;											\
			}													\
			_k_i = _k_j;										\
			_k_j = _k_j->next;									\
		}														\
	} while (0)

/* 双向线形链表排序性能比较
 * 随机数据：(最高) 插入排序 >> 选择排序 >> 冒泡排序 > 鸡尾酒排序 > 奇偶排序 (最低)
 * 顺序数据：(最高) 冒泡排序 > 奇偶排序 > 鸡尾酒排序 >> 插入排序 > 选择排序  (最低)
 * 逆序数据：(最高) 插入排序 >> 选择排序 >> 冒泡排序 > 鸡尾酒排序 > 奇偶排序  (最低)
 */

/**
 * @def dllnk_bubble_sort(_begin, _end, greater, type, member)
 * @brief 对表进行冒泡排序
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] greater 比较函数或宏
 					升序:((*i)->x > (*j)->x)
 					降序:((*i)->x < (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see dllnk_sortable, dllnk_is_sorted
 */
#define dllnk_bubble_sort(_begin, _end, greater, type, member) do {	\
		int _k_pc = 1;												\
		dllnk_iter _k_e = (_end);									\
		dllnk_iter _k_i = (_begin);									\
		for (; _k_pc && _k_i != _k_e; _k_i = _k_i->next) {			\
			dllnk_iter _k_j = _k_i->next;							\
			for (_k_pc = 0; _k_j != _k_e; _k_j = _k_j->next) {		\
				type * _k_pi = container_of(_k_i, type, member);	\
				type * _k_pj = container_of(_k_j, type, member);	\
				if (greater(_k_pi, _k_pj)) {						\
					dllnk_iter _k_t = _k_i;							\
					_k_i = _k_j;									\
					_k_j = _k_t;									\
					dllnk_iter_swap(_k_i, _k_j);					\
					_k_pc = 1;										\
				}													\
			}														\
		}															\
	} while (0)

/**
 * @def dllnk_oddeven_sort(_begin, _end, greater, type, member)
 * @brief 对表进行奇偶排序
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] greater 比较函数或宏
 					升序:((*i)->x > (*j)->x)
 					降序:((*i)->x < (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see dllnk_sortable, dllnk_is_sorted
 */
#define dllnk_oddeven_sort(_begin, _end, greater, type, member) do {	\
		int _k_pc = 1;													\
		dllnk_iter _k_e = (_end);										\
		dllnk_iter _k_f = (_begin);										\
		dllnk_iter _k_p = _k_f->prev;									\
		while (_k_pc) {													\
			_k_pc = 0;													\
			_k_f = _k_p->next;											\
			__dllnk_oddeven_scan(greater, type, member, _k_f);			\
			__dllnk_oddeven_scan(greater, type, member, _k_f->next);	\
		}																\
	} while (0)

#define __dllnk_oddeven_scan(greater, type, member, _start) do {	\
		dllnk_iter _k_i = _start;									\
		dllnk_iter _k_j = _k_i->next;								\
		while (_k_j != _k_e) {										\
			type * _k_pi = container_of(_k_i, type, member);		\
			type * _k_pj = container_of(_k_j, type, member);		\
			if (greater(_k_pi, _k_pj)) {							\
				dllnk_iter _k_t = _k_i;								\
				_k_i = _k_j;										\
				_k_j = _k_t;										\
				dllnk_iter_swap(_k_i, _k_j);						\
				_k_pc = 1;											\
			}														\
			if (!_k_j) break;										\
			_k_i = _k_j->next;										\
			if (!_k_i) break;										\
			_k_j = _k_i->next;										\
		}															\
	} while (0)

/**
 * @def dllnk_cocktail_sort(_begin, _end, greater, type, member)
 * @brief 对表进行鸡尾酒排序
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] greater 比较函数或宏
 					升序:((*i)->x > (*j)->x)
 					降序:((*i)->x < (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see dllnk_sortable, dllnk_is_sorted
 */
#define dllnk_cocktail_sort(_begin, _end, greater, type, member) do {	\
		int _k_pc = 1;													\
		dllnk_iter _k_e = (_end);										\
		dllnk_iter _k_f = (_begin);										\
		while (_k_pc) {													\
			dllnk_iter _k_i = _k_f;										\
			dllnk_iter _k_j = _k_f->next;								\
			_k_pc = 0;													\
			if (_k_j != _k_e) {											\
				type * _k_pj = container_of(_k_j, type, member);		\
				type * _k_pi = container_of(_k_i, type, member);		\
				if (greater(_k_pi, _k_pj)) {							\
					dllnk_iter _k_t = _k_i;								\
					_k_i = _k_j;										\
					_k_f = _k_j;										\
					_k_j = _k_t;										\
					dllnk_iter_swap(_k_i, _k_j);						\
					_k_pc = 1;											\
				}														\
				_k_i = _k_j;											\
				_k_j = _k_j->next;										\
			}															\
			while (_k_j != _k_e) {										\
				type * _k_pj = container_of(_k_j, type, member);		\
				type * _k_pi = container_of(_k_i, type, member);		\
				if (greater(_k_pi, _k_pj)) {							\
					dllnk_iter _k_t = _k_i;								\
					_k_i = _k_j;										\
					_k_j = _k_t;										\
					dllnk_iter_swap(_k_i, _k_j);						\
					_k_pc = 1;											\
				}														\
				_k_i = _k_j;											\
				_k_j = _k_j->next;										\
			}															\
			_k_i = _k_i->prev;											\
			_k_j = _k_i->prev;											\
			_k_f = _k_f->prev;											\
			while (_k_j != _k_f) {										\
				type * _k_pi = container_of(_k_i, type, member);		\
				type * _k_pj = container_of(_k_j, type, member);		\
				if (greater(_k_pj, _k_pi)) {							\
					dllnk_iter _k_t = _k_i;								\
					_k_i = _k_j;										\
					_k_j = _k_t;										\
					dllnk_iter_swap(_k_i, _k_j);						\
					_k_pc = 1;											\
				}														\
				_k_i = _k_j;											\
				_k_j = _k_j->prev;										\
			}															\
			_k_f = _k_f->next;											\
		}																\
	} while (0)

/**
 * @def dllnk_select_sort(_begin, _end, greater, type, member)
 * @brief 对表进行选择排序
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] greater 比较函数或宏
 					升序:((*i)->x > (*j)->x)
 					降序:((*i)->x < (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see dllnk_sortable, dllnk_is_sorted
 */
#define dllnk_select_sort(_begin, _end, greater, type, member) do {	\
		dllnk_iter _k_e = (_end);									\
		dllnk_iter _k_i = (_begin);									\
		dllnk_iter _k_j = _k_i->next;								\
		while (_k_j != _k_e) {										\
			dllnk_iter _k_m = _k_i;									\
			while (_k_j != _k_e) {									\
				type * _k_pj = container_of(_k_j, type, member);	\
				type * _k_pm = container_of(_k_m, type, member);	\
				if (greater(_k_pm, _k_pj))							\
					_k_m = _k_j;									\
				_k_j = _k_j->next;									\
			}														\
			if (_k_i != _k_m) {										\
				dllnk_iter _k_t = _k_m;								\
				_k_m = _k_i;										\
				_k_i = _k_t;										\
				dllnk_iter_swap(_k_m, _k_i);						\
			}														\
			_k_i = _k_i->next;										\
			_k_j = _k_i->next;										\
		}															\
	} while (0)

/**
 * @def dllnk_insert_sort(_begin, _end, greater, type, member)
 * @brief 对表进行插入排序
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] greater 比较函数或宏
 					升序:((*i)->x > (*j)->x)
 					降序:((*i)->x < (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see dllnk_sortable, dllnk_is_sorted
 */
#define dllnk_insert_sort(_begin, _end, greater_equal, type, member) do {\
		dllnk_iter _k_e = (_end);										\
		dllnk_iter _k_f = (_begin);										\
		dllnk_iter _k_i = _k_f->next;									\
		for (_k_f = _k_f->prev; _k_i != _k_e; _k_i = _k_i->next) {		\
			type * _k_pi = container_of(_k_i, type, member);			\
			dllnk_iter _k_j = _k_f->next;								\
			while (_k_j != _k_i) {										\
				type * _k_pj = container_of(_k_j, type, member);		\
				if (greater_equal(_k_pj, _k_pi)) {						\
					dllnk_iter _k_t = _k_i;								\
					_k_i = _k_i->prev;									\
					dllnk_erase(_k_t);									\
					dllnk_insert(_k_j, _k_t);							\
					break;												\
				}														\
				_k_j = _k_j->next;										\
			}															\
		}																\
	} while (0)

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_DLLNK__ */