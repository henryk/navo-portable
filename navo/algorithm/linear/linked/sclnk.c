/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/linear/linked/sclnk.c
 * 
 * @brief Implements for singly circular linked list.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#include "sclnk.h"

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

NV_IMPL int sclnk_check(sclnk_head* head, sclnk_iter* loop_entry)
{
	sclnk_iter slow = head->next;
	sclnk_iter fast = slow;
	
	while (fast != head && fast->next != head) {
		if (!fast || !fast->next)
			return SCLNK_CHECK_NULL;
		slow = slow->next;
		fast = fast->next->next;
		if (fast == slow) {
			if (loop_entry) {
				/* 获取环路入口 */
				slow = head->next;
				while (fast != slow) {
					slow = slow->next;
					fast = fast->next;
				}
				*loop_entry = slow;
			}
			return SCLNK_CHECK_LOOP;
		}
	}
	
	return SCLNK_CHECK_PASS;
}

NV_IMPL void __sclnk_iter_swap(
	sclnk_iter aprev, sclnk_iter a, sclnk_iter anext, 
	sclnk_iter bprev, sclnk_iter b, sclnk_iter bnext)
{
	if (a == bprev) {
		/* (1) b节点是a节点的后继节点
		 *                               (a)
		 * -(ap)-(a)-(b)-(bn)- ===>                ===> -(ap)-(b)-(a)-(bn)-
		 *                          -(ap)-(b)-(bn)-
		 */
		aprev->next = b;
		sclnk_insert_between(a, b, bnext);
	}
	else if (a == bnext) {
		/* (2) a节点是b节点的后继节点
		 *                               (b)
		 * -(bp)-(b)-(a)-(an)- ===>                ===> -(bp)-(a)-(b)-(an)-
		 *                          -(bp)-(a)-(an)-
		 */
		bprev->next = a;
		sclnk_insert_between(b, a, anext);
	}
	else {
		/* (3) a节点、b节点不相邻 
		 * -(ap)-(a)-(an)-...-(bp)-(b)-(bn)- ===> 
		 *                                              (a)
		 *                                        -(ap)-(b)-(an)-...-(bp) (bn)-
		 *
		 *  --(ap)-(b)-(an)-...-(bp)-(a)-(bn)-- <===
		 */
		sclnk_insert_between(b, aprev, anext);
		sclnk_insert_between(a, bprev, bnext);
	}
}

NV_IMPL size_t sclnk_distance(sclnk_iter first, sclnk_iter last)
{
	size_t dist = 1;
	
	while (first != last) {
		sclnk_inc(first);
		++dist;
	}
	
	return dist;
}

NV_IMPL size_t sclnk_count(sclnk_head* head)
{
	size_t cnt = 0;
	sclnk_iter i = head->next;

	while (i != head) {
		sclnk_inc(i);
		++cnt;
	}

	return cnt;
}

NV_IMPL void sclnk_reverse(sclnk_head* head)
{
	sclnk_iter i = sclnk_vbegin(head);
	while (i != sclnk_vend(head))
		sclnk_vpush(head, i);
}

NV_IMPL sclnk_iter sclnk_advance(sclnk_head* head, sclnk_iter cur, size_t dist)
{
	while (cur != head && dist--)
		sclnk_inc(cur);
	
	return cur;
}

NV_IMPL size_t sclnk_index_of(sclnk_head* head, sclnk_iter itr)
{
	size_t cnt = 0;
	sclnk_iter i = head->next;
	
	while (i != head && i != itr) {
		sclnk_inc(i);
		++cnt;
	}
	
	return cnt;
}

NV_IMPL void __sclnk_serialize(sclnk_head* head, char** buf, 
								const size_t offset)
{
	sclnk_iter i = head->next;
	while (i != head) {
		*buf++ = ((char*)i) - offset;
		i = i->next;
	}
}

NV_IMPL void __sclnk_deserialize(sclnk_head* head, char** buf, 
								size_t cnt, const size_t offset)
{
	sclnk_iter t, p = head;

	if (!cnt)
		return;
	while (cnt--) {
		t = (sclnk_iter)(*buf++ + offset);
		p->next = t;
		p = t;
	}
	t->next = head;
}

NV_IMPL void __sclnk_free(sclnk_iter begin, sclnk_iter end, 
						sclnk_pr1 erase_handler)
{
	while (begin != end)
		erase_handler(sclnk_inc_later(begin));
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

