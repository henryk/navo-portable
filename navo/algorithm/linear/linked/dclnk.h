/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/linear/linked/dclnk.h
 * 
 * @brief Doubly circular linked list.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_DCLNK__
#define __NV_DCLNK__

#include "../../../port/cdef.h"

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup 双向环形链表
 * @ingroup 链式表
 * @{
 */

/**
 * @struct __dclnk_node
 * @brief 双向环形链表节点
 * @typedef dclnk_head
 * @brief 表头
 * @typedef dclnk_node
 * @brief 节点
 * @typedef dclnk_iter
 * @brief 迭代器
 */
typedef struct __dclnk_node {
	/** @brief 后继节点 */
	struct __dclnk_node* next;
	/** @brief 前驱节点 */
	struct __dclnk_node* prev;
}dclnk_node, dclnk_head, *dclnk_iter;

/**
 * @def DCLNK_HEAD(name)
 * @brief 定义一个名为name的表头，并对其初始化
 * @param name 表头名称
 * @def DCLNK_NODE(name)
 * @brief 定义一个名为name的节点，并对其初始化
 * @param name 节点名称
 */
#define DCLNK_HEAD(name) dclnk_head name = {&(name), &(name)}
#define DCLNK_NODE(name) dclnk_node name = {&(name), &(name)}

/**
 * @typedef dclnk_pr1
 * @brief 单参数回调函数指针
 * @typedef dclnk_pr2
 * @brief 双参数回调函数指针
 */
typedef int (*dclnk_pr1)(dclnk_node*);
typedef int (*dclnk_pr2)(dclnk_node*, dclnk_node*);

/**
 * @fn int dclnk_check(dclnk_head* head, dclnk_iter* loop_entry);
 * @brief 判断链表结构是否正常
 * @param[in] head 表头
 * @param[out] loop_entry 指向保存环路入口的迭代器，可以为NULL。
 			若为空，则不返回环路入口；
 			若不为空且仅有环路问题，则函数将环路入口保存到loop_entry
 * @return int 返回结果
 * @retval DCLNK_CHECK_PASS 0 正常
 * @retval DCLNK_CHECK_LOOP 1 有非法环路
 * @retval DCLNK_CHECK_NULL 2 有节点指向NULL
 * @retval DCLNK_CHECK_LINK 3 有节点的前驱指针不正确
 */
NV_API int dclnk_check(dclnk_head* head, dclnk_iter* loop_entry);

#define DCLNK_CHECK_PASS 	0 /* 正常 */
#define DCLNK_CHECK_LOOP 	1 /* 有非法环路 */
#define DCLNK_CHECK_NULL 	2 /* 有节点指向NULL */
#define DCLNK_CHECK_LINK 	3 /* 有节点的前驱指针不正确 */

/**
 * @fn static inline void dclnk_init(dclnk_head* head);
 * @brief 初始化表头
 * @param[out] head 目标表头
 * @par 示例:
 * @code
	dclnk_head head;
	// 初始化
	dclnk_init(&head);
 * @endcode
 */
static inline void dclnk_init(dclnk_head* head)
{
	head->next = head;
	head->prev = head;
}

/**
 * @fn static inline int dclnk_empty(dclnk_head* head);
 * @brief 判断表是否为空
 * @param[in] head 表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	if (!dclnk_empty(&head)) {
 		......
 	}
 * @endcode
 */
static inline int dclnk_empty(dclnk_head* head)
{
	return head->next == head;
}

static inline int __dclnk_singular(dclnk_node* front, dclnk_node* end)
{
	return (front != end && front->next == end);
}

/**
 * @fn static inline int dclnk_singular(dclnk_head* head);
 * @brief 判断表是否只有单个节点
 * @param[in] head 表头
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	if (dclnk_singular(&head)) {
 		......
 	}
 * @endcode
 */
static inline int dclnk_singular(dclnk_head* head)
{
	return __dclnk_singular(head->next, head);
}

/**
 * @fn static inline int dclnk_sortable(dclnk_iter begin, dclnk_iter end);
 * @brief 判断表是否可以排序
 * @param[in] begin 起始位置
 * @param[in] end 结束位置
 * @note 即判断是否至少有两个节点
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	dclnk_iter begin = dclnk_begin(&head);
 	dclnk_iter end = dclnk_end(&head);
 	// 判断是否可以排序
 	if (dclnk_sortable(begin, end)) {
 		// 插入排序
 		dclnk_insert_sort(begin, end, DATA_GreaterEqual, DATA, node);
 	}
 * @endcode
 * @see dclnk_bubble_sort, dclnk_oddeven_sort, 
 		dclnk_select_sort, dclnk_insert_sort, dclnk_is_sorted
 */
static inline int dclnk_sortable(dclnk_iter begin, dclnk_iter end)
{
	return (begin != end && begin->next != end);
}

/**
 * @fn static inline int dclnk_serialable(dclnk_head* head);
 * @brief 判断表是否可以序列化
 * @param[in] head 表头
 * @note 即判断是否至少有一个节点
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 * @par 示例:
 * @code
 	// 判断是否可以序列化
 	if (dclnk_serialable(&head)) {
 		slseq_head(DATA*) seq;
 		// 分配序列缓存
 		DATA** buf = (DATA**)malloc(sizeof(DATA*) * list_cnt);
 		// 序列化
 		dclnk_serialize(&head, buf, DATA, node);
 		SLSEQ_INIT_SERIAL(&seq, buf, list_cnt);
 		// 希尔排序
 		slseq_shell_sort(DATA*, slseq_begin(&seq), 
 			slseq_end(&seq), DATA_Greater);
 		// 反序列化
 		dclnk_deserialize(&head, buf, list_cnt, DATA, node);
 		// 释放缓存
 		free(buf);
 	}
 * @endcode
 * @see dclnk_serialize, dclnk_deserialize
 */
static inline int dclnk_serialable(dclnk_head* head)
{
	return !!head->next;
}

/**
 * @fn static inline void dclnk_link(dclnk_iter prev, dclnk_iter next);
 * @brief 链接两个节点
 * @param[in,out] prev 前驱节点
 * @param[in,out] next 后继节点
 * @note 链接prev和next节点
 */
static inline void dclnk_link(dclnk_iter prev, dclnk_iter next)
{	/* 强制连接prev节点和next节点
	 * --(prev)-> <-(next)-- ==> --(prev)--(next)--
	 */
	next->prev = prev;
	prev->next = next;
}

/**
 * @fn static inline void dclnk_insert_between(dclnk_node* newly, 
												dclnk_iter prev, 
												dclnk_iter next);
 * @brief 在两个节点之间插入一个新节点
 * @param[in,out] newly 新节点
 * @param[in,out] prev 前驱节点
 * @param[in,out] next 后继节点
 * @note 将newly节点插入到prev和next节点之间。如果prev和next不是相邻节点，
 *       则newly将替换掉prev和next之间的所有节点
 */
static inline void dclnk_insert_between(dclnk_node* newly, 
												dclnk_iter prev, 
												dclnk_iter next)
{	/* 强制连接prev节点和next节点，并在两者之间插入newly节点
	 * (1) prev节点和next节点原来未连接 
	 *       (newly)
	 *                   ===> --(prev)-(newly)-(next)--
	 * --(prev) (next)--
	 * (2) prev节点和next节点原来已连接 
	 *       (newly)
	 *                   ===> --(prev)-(newly)-(next)--
	 * --(prev)-(next)--
	 */
	prev->next = newly;
	next->prev = newly;
	newly->prev = prev;
	newly->next = next;
	/* 上面的是性能优化后的版本，实际过程同下：
	 * dclnk_link(prev, newly);
	 * dclnk_link(newly, next);
	 *
	 * (1)连接prev节点与newly节点  (2)再连接newly节点与next节点
	 *          (newly)                    (newly)
	 *          /                          /    \
	 *    --(prev)  (next)--         --(prev)  (next)--
	 */
}

/**
 * @fn static inline void dclnk_insert(dclnk_iter pos, dclnk_node* newly);
 * @brief 插入节点到指定位置
 * @param[in,out] pos 插入位置
 * @param[in,out] newly 新节点
 * @note 将newly节点插入到pos的位置上，pos是从表头开始的位置
 * @par 示例:
 * @code
 	dclnk_iter pos = dclnk_at(&head, 8);
 	DATA* p = (DATA*)malloc(sizeof(DATA));
 	// 将p节点插入到pos所在的位置
 	dclnk_insert(pos, &p->node);
 * @endcode
 */
static inline void dclnk_insert(dclnk_iter pos, dclnk_node* newly)
{
	dclnk_insert_between(newly, pos->prev, pos);
}

/**
 * @fn static inline void dclnk_erase(dclnk_iter entry);
 * @brief 擦除节点
 * @param[in,out] entry 目标节点
 * @par 示例:
 * @code
 	dclnk_iter e = dclnk_end(&head);
 	dclnk_iter i = dclnk_begin(&head);
 	while (i != e) {
 		if (is_bad_data(container_of(i, DATA, node)))
 			dclnk_erase(dclnk_inc_later(i));
 		else
 			dclnk_inc(i);
 	}
 * @endcode
 */
static inline void dclnk_erase(dclnk_iter entry)
{	/* 将entry节点从表中移除
	 *                        (entry)
	 *  -()-(entry)-()- ===> 
	 *                       --()--()--
	 */
	dclnk_link(entry->prev, entry->next);
}

NV_API void __dclnk_free(dclnk_iter begin, dclnk_iter end, 
						dclnk_pr1 erase_handler);

/**
 * @fn static inline void dclnk_erases(dclnk_iter begin, 
							dclnk_iter end, dclnk_pr1 erase_handler);
 * @brief 清空区域
 * @param[in,out] begin 区域起始位置
 * @param[in,out] end 区域终止位置
 * @param[in] erase_handler 移除响应函数，通常用于监视和释放节点，可以传递NULL,
 							表示不为删除的节点调用响应来处理
 * @note 函数会为每一个被移除的节点调用erase_handler来处理节点
 */
static inline void dclnk_erases(dclnk_iter begin, 
							dclnk_iter end, dclnk_pr1 erase_handler)
{
	dclnk_iter prev = begin->prev;
	dclnk_link(prev, end);
	if (erase_handler)
		__dclnk_free(begin, end, erase_handler);
}

/**
 * @fn static inline void dclnk_clear(dclnk_head* head, dclnk_pr1 erase_handler);
 * @brief 清空表
 * @param[in,out] head 表头
 * @param[in] erase_handler 移除响应函数，通常用于监视和释放节点，可以传递NULL,
 							表示不为删除的节点调用响应来处理
 * @note 函数会为每一个被移除的节点调用erase_handler来处理节点
 */
static inline void dclnk_clear(dclnk_head* head, dclnk_pr1 erase_handler)
{
	dclnk_erases(head->next, head, erase_handler);
}

/**
 * @fn static inline void dclnk_push_front(dclnk_head* head, dclnk_node* newly);
 * @brief 在表首添加节点
 * @param[in,out] head 表头
 * @param[in,out] newly 新节点
 */
static inline void dclnk_push_front(dclnk_head* head, dclnk_node* newly)
{
	dclnk_insert_between(newly, head, head->next);
}

/**
 * @fn static inline void dclnk_push_back(dclnk_head* head, dclnk_node* newly);
 * @brief 在表尾添加节点
 * @param[in,out] head 表头
 * @param[in,out] newly 新节点
 */
static inline void dclnk_push_back(dclnk_head* head, dclnk_node* newly)
{
	dclnk_insert_between(newly, head->prev, head);
}

/**
 * @fn static inline void dclnk_pop_front(dclnk_head* head);
 * @brief 移除表首节点
 * @param[in,out] head 表头
 */
static inline void dclnk_pop_front(dclnk_head* head)
{
	dclnk_link(head, head->next->next);
}

/**
 * @fn static inline void dclnk_pop_back(dclnk_head* head);
 * @brief 移除表尾节点
 * @param[in,out] head 表头
 */
static inline void dclnk_pop_back(dclnk_head* head)
{
	dclnk_link(head->prev->prev, head);
}

/**
 * @fn static inline void dclnk_splice_between(
			dclnk_iter prev, dclnk_iter next, 
			dclnk_iter first_prev, dclnk_iter first, 
			dclnk_iter last, dclnk_iter last_next);
 * @brief 在两个节点之间插入多个连续节点
 * @param[in,out] prev 前驱节点
 * @param[in,out] next 后继节点
 * @param[in,out] first_prev 首节点的前驱节点
 * @param[in,out] first 首节点
 * @param[in,out] last 尾节点
 * @param[in,out] last_next 尾节点的后继节点
 * @note 将从first至last的节点嫁接到prev和next之间。如果prev和next之间有节点，
 * 		则它们之间的节点将会被替换
 */
static inline void dclnk_splice_between(
	dclnk_iter prev, dclnk_iter next, 
	dclnk_iter first_prev, dclnk_iter first, 
	dclnk_iter last, dclnk_iter last_next)
{	/* 将first节点至last节点的部分从旧表(B表)移接到新表(A表)中 
	 * (从first到last是按next方向)
	 * A:  -(prev)-(next)-      
	 * B: -(first_prev)-(first)-...-(last)-(last_next)-
	 * to:
	 * A: -(prev)-(first)-...-(last)-(next)-
	 * B: -(first_prev)-(last_next)-
	 */
	dclnk_link(last, next);
	dclnk_link(prev, first);
	dclnk_link(first_prev, last_next);
}

/**
 * @fn static inline void dclnk_splice(dclnk_iter pos, 
 							dclnk_iter first, dclnk_iter last);
 * @brief 在两个节点之间插入多个连续节点
 * @param[in,out] pos 插入位置
 * @param[in,out] first 首节点
 * @param[in,out] last 尾节点
 * @note 将从first至last的节点嫁接到pos位置。可以跨链表移动
 * @par 示例:
 * @code
 	// 将a链表的节点全部嫁接到b链表
 	dclnk_splice(b, dclnk_front(a), dclnk_back(b));
 	// 将b到e的节点们移动到第4个节点的位置
 	dclnk_splice(dclnk_at(head, 3), b, e);
 * @endcode
 */
static inline void dclnk_splice(dclnk_iter pos, 
						dclnk_iter first, dclnk_iter last)
{
	dclnk_splice_between(pos->prev, pos, first->prev, first, last, last->next);
}

/**
 * @fn size_t dclnk_count(dclnk_head* head);
 * @brief 统计表的节点数量
 * @param[in] head 表头
 * @return size_t 返回表的节点总数
 */
NV_API size_t dclnk_count(dclnk_head* head);

/**
 * @fn size_t dclnk_distance(dclnk_iter first, dclnk_iter last);
 * @brief 计算两个节点的迭代距离
 * @param[in] first 首节点
 * @param[in] last 尾节点
 * @note 以索引为例，2到4的距离是4 (4-2+1=3)
 * @return size_t 返回距离
 */
NV_API size_t dclnk_distance(dclnk_iter first, dclnk_iter last);

/**
 * @fn dclnk_iter dclnk_advance(dclnk_head* head, dclnk_iter cur, int dist);
 * @brief 给迭代器增加一段距离或减小一段距离
 * @param[in] head 表头
 * @param[in] cur 迭代器
 * @param[in] dist 增加的距离。正数时，表示增加；负数时，表示减小
 * @return dclnk_iter 返回新迭代器
 */
NV_API dclnk_iter dclnk_advance(dclnk_head* head, dclnk_iter cur, int dist);

/**
 * @fn static inline dclnk_iter dclnk_at(dclnk_head* head, const size_t index);
 * @brief 根据正向索引得到迭代器
 * @param[in] head 表头
 * @param[in] index 索引，从0开始
 * @return dclnk_iter 返回对应的迭代器
 */
static inline dclnk_iter dclnk_at(dclnk_head* head, const size_t index)
{
	return dclnk_advance(head, head->next, index);
}

/**
 * @fn static inline dclnk_iter dclnk_reverse_at(dclnk_head* head, 
 											const size_t index);
 * @brief 根据逆向索引得到迭代器
 * @param[in] head 表头
 * @param[in] index 索引，从0开始
 * @return dclnk_iter 返回对应的迭代器
 */
static inline dclnk_iter dclnk_reverse_at(dclnk_head* head, 
											const size_t index)
{
	return dclnk_advance(head, head->prev, -((int)index));
}

/**
 * @fn size_t dclnk_index_of(dclnk_head* head, dclnk_iter itr);
 * @brief 计算迭代器的正向索引
 * @param[in] head 表头
 * @param[in] itr 迭代器
 * @return size_t 返回索引
 */
NV_API size_t dclnk_index_of(dclnk_head* head, dclnk_iter itr);

/**
 * @fn size_t dclnk_reverse_index_of(dclnk_head* head, dclnk_iter itr);
 * @brief 计算迭代器的逆向索引
 * @param[in] head 表头
 * @param[in] itr 迭代器
 * @return size_t 返回索引
 */
NV_API size_t dclnk_reverse_index_of(dclnk_head* head, dclnk_iter itr);

/**
 * @fn static inline void dclnk_node_init(dclnk_node* entry);
 * @brief 初始化节点
 * @param[out] entry 节点
 */
static inline void dclnk_node_init(dclnk_node* entry)
{
	entry->next = entry;
	entry->prev = entry;
}

/**
 * @fn static inline void dclnk_iter_replace(dclnk_node* victim, 
 										dclnk_node* newly);
 * @brief 用新节点替换目标节点
 * @param[in,out] victim 受害目标节点
 * @param[in,out] newly 新节点
 */
static inline void dclnk_iter_replace(dclnk_node* victim, dclnk_node* newly)
{	/* 用newly节点替换表中的victim节点
	 *      (newly)                 (victim)
	 *                    ===>
	 * --()-(victim)-()--      --()-(newly)-()--
	 */
	dclnk_insert_between(newly, victim->prev, victim->next);
}

NV_API void __dclnk_iter_swap(
	dclnk_iter aprev, dclnk_iter a, dclnk_iter anext, 
	dclnk_iter bprev, dclnk_iter b, dclnk_iter bnext);

/**
 * @fn static inline void dclnk_iter_swap(dclnk_iter a, dclnk_iter b);
 * @brief 交换两个节点
 * @param[in,out] a 第一个节点
 * @param[in,out] b 第二个节点
 */
static inline void dclnk_iter_swap(dclnk_iter a, dclnk_iter b)
{
	__dclnk_iter_swap(a->prev, a, a->next, b->prev, b, b->next);
}

/**
 * @fn static inline void dclnk_replace(dclnk_head* victim, dclnk_head* newly);
 * @brief 用新表头替换目标表头
 * @param[in,out] victim 受害目标表头
 * @param[in,out] newly 新表头
 */
static inline void dclnk_replace(dclnk_head* victim, dclnk_head* newly)
{	/* 用newly头替换表中的victim头
	 *      [newly]                 [victim]
	 *                    ===>
	 * --()-[victim]-()--      --()-[newly]-()--
	 */
	dclnk_insert_between(newly, victim->prev, victim->next);
}

static inline void __dclnk_swap(
	dclnk_iter aback, dclnk_iter a, dclnk_iter afront, 
	dclnk_iter bback, dclnk_iter b, dclnk_iter bfront)
{	/* 交换a、b两个头
	 * --(aback)-[a]-(afront)--       --(aback)-[b]-(afront)--
	 *                         ===>
	 * --(bback)-[b]-(bfront)--       --(bback)-[a]-(afront)--
	 */
	dclnk_insert_between(b, aback, afront);
	dclnk_insert_between(a, bback, bfront);
}

/**
 * @fn static inline void dclnk_swap(dclnk_head* a, dclnk_head* b);
 * @brief 交换两个表头
 * @param[in,out] a 第一个表头
 * @param[in,out] b 第二个表头
 */
static inline void dclnk_swap(dclnk_head* a, dclnk_head* b)
{
	__dclnk_swap(a->prev, a, a->next, b->prev, b, b->next);
}

/**
 * @fn static inline int dclnk_exist(dclnk_head* head, dclnk_iter itr);
 * @brief 判断一个节点是否存在于表中
 * @param[in] head 表头
 * @param[in] itr 目标节点
 * @return int 返回结果
 * @retval 1 真
 * @retval 0 假
 */
static inline int dclnk_exist(dclnk_head* head, dclnk_iter itr)
{
	return dclnk_index_of(head, itr) != (size_t)(-1);
}

/**
 * @fn static inline dclnk_iter dclnk_front(dclnk_head* head);
 * @brief 得到表首节点
 * @param[in] head 表头
 * @return dclnk_iter 返回表首节点
 * @see dclnk_back
 */
static inline dclnk_iter dclnk_front(dclnk_head* head)
{
	return head->next;
}

/**
 * @fn static inline dclnk_iter dclnk_back(dclnk_head* head);
 * @brief 得到表尾节点
 * @param[in] head 表头
 * @return dclnk_iter 返回表尾节点
 * @see dclnk_front
 */
static inline dclnk_iter dclnk_back(dclnk_head* head)
{
	return head->prev;
}

/**
 * @fn static inline dclnk_iter dclnk_begin(dclnk_head* head);
 * @brief 得到指向正向起始位置的迭代器
 * @param[in] head 表头
 * @return dclnk_iter 返回迭代器
 * @par 示例:
 * @code
 	// 遍历链表
 	int index = 0;
 	dclnk_iter e = dclnk_end(&head);
 	dclnk_iter i = dclnk_begin(&head);
 	while (i != e) {
 		printf("[%d]=%d\n", index++, owner_of(i, DATA, node).data);
 		dclnk_inc(i);
 	}
 * @endcode
 * @see dclnk_end, dclnk_rbegin, dclnk_rend
 */
static inline dclnk_iter dclnk_begin(dclnk_head* head)
{
	return head->next;
}

/**
 * @fn static inline dclnk_iter dclnk_end(dclnk_head* head);
 * @brief 得到指向正向终止位置的迭代器
 * @param[in] head 表头
 * @return dclnk_iter 返回迭代器
 * @see dclnk_begin, dclnk_rbegin, dclnk_rend
 */
static inline dclnk_iter dclnk_end(dclnk_head* head)
{
	return head;
}

/**
 * @fn static inline dclnk_iter dclnk_end_of(dclnk_iter itr);
 * @brief 得到当前迭代器的正向终止位置
 * @param[in] itr 迭代器
 * @return dclnk_iter 返回迭代器
 * @see dclnk_begin, dclnk_end
 */
static inline dclnk_iter dclnk_end_of(dclnk_iter itr)
{
	return itr->next;
}

/**
 * @fn static inline dclnk_iter dclnk_rbegin(dclnk_head* head);
 * @brief 得到指向逆向起始位置的迭代器
 * @param[in] head 表头
 * @return dclnk_iter 返回迭代器
 * @par 示例:
 * @code
 	// 逆向遍历链表
 	int index = 0;
 	dclnk_iter e = dclnk_rend(&head);
 	dclnk_iter i = dclnk_rbegin(&head);
 	while (i != e) {
 		printf("[%d]=%d\n", index++, owner_of(i, DATA, node).data);
 		dclnk_dec(i);
 	}
 * @endcode
 * @see dclnk_end, dclnk_begin, dclnk_rend
 */
static inline dclnk_iter dclnk_rbegin(dclnk_head* head)
{
	return head->prev;
}

/**
 * @fn static inline dclnk_iter dclnk_rend(dclnk_head* head);
 * @brief 得到指向逆向终止位置的迭代器
 * @param[in] head 表头
 * @return dclnk_iter 返回迭代器
 * @see dclnk_end, dclnk_rbegin, dclnk_begin
 */
static inline dclnk_iter dclnk_rend(dclnk_head* head)
{
	return head;
}

/**
 * @fn static inline dclnk_iter dclnk_rend_of(dclnk_iter itr);
 * @brief 得到当前迭代器的逆向终止位置
 * @param[in] itr 当前迭代器
 * @return dclnk_iter 返回迭代器
 * @see dclnk_rbegin, dclnk_rend
 */
static inline dclnk_iter dclnk_rend_of(dclnk_iter itr)
{
	return itr->prev;
}

/**
 * @fn static inline dclnk_iter dclnk_next(dclnk_iter itr);
 * @brief 得到当前节点的后继节点
 * @param[in] itr 当前节点
 * @return dclnk_iter 返回后继节点
 * @see dclnk_prev
 */
static inline dclnk_iter dclnk_next(dclnk_iter itr)
{
	return itr->next;
}

/**
 * @fn static inline dclnk_iter dclnk_prev(dclnk_iter itr);
 * @brief 得到当前节点的前驱节点
 * @param[in] itr 当前节点
 * @return dclnk_iter 返回前驱节点
 * @see dclnk_next
 */
static inline dclnk_iter dclnk_prev(dclnk_iter itr)
{
	return itr->prev;
}

static inline dclnk_iter __dclnk_inc_later(dclnk_iter cur, dclnk_iter* p)
{
	*p = cur->next;
	return cur;
}

static inline dclnk_iter __dclnk_dec_later(dclnk_iter cur, dclnk_iter* p)
{
	*p = cur->prev;
	return cur;
}

/**
 * @def dclnk_inc(itr)
 * @brief 迭代器递增
 * @def dclnk_dec(itr)
 * @brief 迭代器递减
 * @def dclnk_inc_later(itr)
 * @brief 迭代器后自增
 * @def dclnk_dec_later(itr)
 * @brief 迭代器后自减
 */
#define dclnk_inc(itr) ((itr) = (itr)->next)
#define dclnk_dec(itr) ((itr) = (itr)->prev)
#define dclnk_inc_later(itr) __dclnk_inc_later(itr, &(itr))
#define dclnk_dec_later(itr) __dclnk_dec_later(itr, &(itr))

/**
 * @def dclnk_foreach(_begin, _end, fn, type, member)
 * @brief 正向遍历_begin到_end的节点，并为每一个节点调用回调函数或宏
 * @param[in] _begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define dclnk_foreach(_begin, _end, fn, type, member) do {		\
		dclnk_iter _q_e = (_end);								\
		dclnk_iter _q_f = (_begin);								\
		while (_q_f != _q_e) {									\
			type * _q_t = container_of(_q_f, type, member);		\
			_q_f = _q_f->next;									\
			fn(_q_t);											\
		}														\
	} while (0)

/**
 * @def dclnk_reverse_foreach(_rbegin, _rend, fn, type, member)
 * @brief 逆向遍历_begin到_end的节点，并为每一个节点调用回调函数或宏
 * @param[in] _rbegin 逆向起始位置
 * @param[in] _rend 逆向终止位置
 * @param[in] fn 回调函数或宏
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define dclnk_reverse_foreach(_rbegin, _rend, fn, type, member) \
	do {dclnk_iter _q_e = (_rend);								\
		dclnk_iter _q_f = (_rbegin);							\
		while (_q_f != _q_e) {									\
			type * _q_t = container_of(_q_f, type, member);		\
			_q_f = _q_f->prev;									\
			fn(_q_t);											\
		}														\
	} while (0)

/**
 * @def dclnk_search(head, _begin, _end, equal, var, res, type, member)
 * @brief 顺序查找区域内的指定节点
 * @param[in,out] head 表头
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] equal 比较函数或宏。相等时返回1，不相等返回0
 * @param[in] var 搜索的键值，常量，变量均可
 * @param[out] res 保存搜索结果的变量，类型是节点指针。结果为NULL表示找不到节点
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define dclnk_search(head, _begin, _end, equal, var, res, type, member) \
	do {dclnk_iter _q_e = (_end);								\
		dclnk_iter _q_f = (_begin);								\
		dclnk_head* _q_h = (head);								\
		(res) = NULL;											\
		while (_q_f != _q_e) {									\
			type * _q_cb = container_of(_q_f, type, member);	\
			if (unlikely(equal(_q_cb, (val)))) {				\
				(res) = _q_cb;									\
				break;											\
			}													\
			_q_f = _q_f->next;									\
		}														\
	} while (0)

/* 反转链表的两种方式：
 * (1) 直接使用 dclnk_reverse(head); (推荐)
 * (2) 按照如下方式迭代
 *     dclnk_iter i = dclnk_vbegin(head);
 *     dclnk_iter e = dclnk_vend(head);
 *     while (i != e) {
 *			dclnk_vpush(head, i);
 *     }
 *     当迭代完成时，反转完成
 */

static inline dclnk_iter __dclnk_vbegin(dclnk_head* head, dclnk_iter front)
{
	head->next = head;
	head->prev = head;
	return front;
}

/**
 * @fn static inline dclnk_iter dclnk_vbegin(dclnk_head* head);
 * @brief 得到用于反转的起始位置的迭代器
 * @param[in] head 表头
 * @return dclnk_iter 返回迭代器
 * @par 示例:
 * @code
 	// 反转表
 	dclnk_iter ve = dclnk_vend(&head);
 	dclnk_iter vi = dclnk_vbegin(&head);
 	while (vi != ve) {
 		dclnk_vpush(&head, vi);
 	}
 * @endcode
 * @see dclnk_vend, dclnk_vpush, dclnk_reverse
 */
static inline dclnk_iter dclnk_vbegin(dclnk_head* head)
{
	return __dclnk_vbegin(head, head->next);
}

/**
 * @fn static inline dclnk_iter dclnk_vend(dclnk_head* head);
 * @brief 得到用于反转的终止位置的迭代器
 * @param[in] head 表头
 * @return dclnk_iter 返回迭代器
 * @par 示例:
 * @code
 	// 反转表
 	dclnk_iter ve = dclnk_vend(&head);
 	dclnk_iter vi = dclnk_vbegin(&head);
 	while (vi != ve) {
 		dclnk_vpush(&head, vi);
 	}
 * @endcode
 * @see dclnk_vbegin, dclnk_vpush, dclnk_reverse
 */
static inline dclnk_iter dclnk_vend(dclnk_head* head)
{
	return head;
}

/**
 * @def dclnk_vpush(head, itr)
 * @brief 入栈反转
 * @param[in,out] head 表头
 * @param[in,out] itr 迭代器
 * @par 示例:
 * @code
 	// 反转表
 	dclnk_iter ve = dclnk_vend(&head);
 	dclnk_iter vi = dclnk_vbegin(&head);
 	while (vi != ve) {
 		dclnk_vpush(&head, vi);
 	}
 * @endcode
 * @see dclnk_vbegin, dclnk_vend, dclnk_reverse
 */
#define dclnk_vpush(head, itr) dclnk_push_front(head, dclnk_inc_later(itr))

/**
 * @fn void dclnk_reverse(dclnk_head* head);
 * @brief 反转表
 * @param[in,out] head 表头
 * @see dclnk_vbegin, dclnk_vend, dclnk_vpush
 */
NV_API void dclnk_reverse(dclnk_head* head);

NV_API void __dclnk_serialize(dclnk_head* head, char** buf, 
							const size_t offset);

NV_API void __dclnk_deserialize(dclnk_head* head, char** buf, 
							size_t cnt, const size_t offset);

/**
 * @def dclnk_serialize(head, buf, type, member)
 * @brief 表的序列化
 * @param[in] head 表头
 * @param[out] buf 序列缓存
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @note 函数会遍历整个表，逐个将节点的拥有者地址存入缓存
 * @par 示例:
 * @code
 	// 判断是否可以序列化
 	if (dclnk_serialable(&head)) {
 		slseq_head(DATA*) seq;
 		// 分配序列缓存
 		DATA** buf = (DATA**)malloc(sizeof(DATA*) * list_cnt);
 		// 序列化
 		dclnk_serialize(&head, buf, DATA, node);
 		SLSEQ_INIT_SERIAL(DATA*, &seq, buf, list_cnt);
 		// 希尔排序
 		slseq_shell_sort(DATA*, slseq_begin(&seq), 
 			slseq_end(&seq), DATA_Greater);
 		// 反序列化
 		dclnk_deserialize(&head, buf, list_cnt, DATA, node);
 		// 释放缓存
 		free(buf);
 	}
 * @endcode
 * @see dclnk_deserialize, dclnk_serialable
 */
#define dclnk_serialize(head, buf, type, member) \
	__dclnk_serialize(head, (char**)(buf), offset_of(type, member))

/**
 * @def dclnk_deserialize(head, buf, cnt, type, member)
 * @brief 表的反序列化
 * @param[out] head 表头
 * @param[out] buf 序列缓存
 * @param[in] cnt 序列长度
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @note 函数会遍历整个序列来还原整个表
 * @par 示例:
 * @code
 	// 判断是否可以序列化
 	if (dclnk_serialable(&head)) {
 		slseq_head(DATA*) seq;
 		// 分配序列缓存
 		DATA** buf = (DATA**)malloc(sizeof(DATA*) * list_cnt);
 		// 序列化
 		dclnk_serialize(&head, buf, DATA, node);
 		SLSEQ_INIT_SERIAL(DATA*, &seq, buf, list_cnt);
 		// 希尔排序
 		slseq_shell_sort(DATA*, slseq_begin(&seq), 
 			slseq_end(&seq), DATA_Greater);
 		// 反序列化
 		dclnk_deserialize(&head, buf, list_cnt, DATA, node);
 		// 释放缓存
 		free(buf);
 	}
 * @endcode
 * @see dclnk_serialize, dclnk_serialable
 */
#define dclnk_deserialize(head, buf, cnt, type, member) \
	__dclnk_deserialize(head, (char**)(buf), cnt, offset_of(type, member))

/**
 * @def dclnk_sort_insert(head, newly, greater_equal, type, member)
 * @brief 按升序插入新节点
 * @param[in,out] head 表头
 * @param[in,out] newly 新节点
 * @param[in] greater_equal 比较函数或宏，
 							升序:((*i)->x >= (*j)->x) 
 							降序:((*i)->x <= (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 */
#define dclnk_sort_insert(head, newly, greater_equal, type, member) \
	do {dclnk_iter _k_n = (newly);								\
		dclnk_head* _k_h = (head);								\
		dclnk_iter _k_i = _k_h->next;							\
		while (_k_i != _k_h) {									\
			type * _k_pi = container_of(_k_i, type, member);	\
			type * _k_pn = container_of(_k_n, type, member);	\
			if (greater_equal(_k_pi, _k_pn))					\
				break;											\
			_k_i = _k_i->next;									\
		}														\
		dclnk_insert(_k_i, _k_n);								\
	} while (0)

/**
 * @def dclnk_is_sorted(_begin, _end, greater_equal, res, type, member)
 * @brief 判断表是否按序
 * @param[in] _begin 起始位置
 * @param[in] _end 终止位置
 * @param[in] greater_equal 比较函数或宏，
 							升序:((*i)->x >= (*j)->x) 
 							降序:((*i)->x <= (*j)->x)
 * @param[out] res 保存输出结果的变量
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see dclnk_bubble_sort, dclnk_oddeven_sort, 
 		dclnk_select_sort, dclnk_insert_sort, dclnk_sortable
 */
#define dclnk_is_sorted(_begin, _end, greater_equal, res, type, member) \
	do {dclnk_iter _k_e = (_end);								\
		dclnk_iter _k_i = (_begin);								\
		dclnk_iter _k_j = _k_i->next;							\
		(res) = 1;												\
		while (_k_j != _k_e) {									\
			type * _k_pi = container_of(_k_i, type, member);	\
			type * _k_pj = container_of(_k_j, type, member);	\
			if (!(greater_equal(_k_pj, _k_pi))) {				\
				(res) = 0;										\
				break;											\
			}													\
			_k_i = _k_j;										\
			_k_j = _k_j->next;									\
		}														\
	} while (0)

/* 双向环形链表排序性能比较
 * 随机数据：(最高) 插入排序 >> 选择排序 >> 冒泡排序 > 鸡尾酒排序 > 奇偶排序 (最低)
 * 顺序数据：(最高) 冒泡排序 > 奇偶排序 > 鸡尾酒排序 >> 插入排序 > 选择排序  (最低)
 * 逆序数据：(最高) 插入排序 >> 选择排序 >> 冒泡排序 > 鸡尾酒排序 > 奇偶排序  (最低)
 */

/**
 * @def dclnk_bubble_sort(_begin, _end, greater, type, member)
 * @brief 对表进行冒泡排序
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] greater 比较函数或宏
 					升序:((*i)->x > (*j)->x)
 					降序:((*i)->x < (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see dclnk_sortable, dclnk_is_sorted
 */
#define dclnk_bubble_sort(_begin, _end, greater, type, member) do {	\
		int _k_pc = 1;												\
		dclnk_iter _k_e = (_end);									\
		dclnk_iter _k_i = (_begin);									\
		for (; _k_pc && _k_i != _k_e; _k_i = _k_i->next) {			\
			dclnk_iter _k_j = _k_i->next;							\
			for (_k_pc = 0; _k_j != _k_e; _k_j = _k_j->next) {		\
				type * _k_pi = container_of(_k_i, type, member);	\
				type * _k_pj = container_of(_k_j, type, member);	\
				if (greater(_k_pi, _k_pj)) {						\
					dclnk_iter _k_t = _k_i;							\
					_k_i = _k_j;									\
					_k_j = _k_t;									\
					dclnk_iter_swap(_k_i, _k_j);					\
					_k_pc = 1;										\
				}													\
			}														\
		}															\
	} while (0)

/**
 * @def dclnk_oddeven_sort(_begin, _end, greater, type, member)
 * @brief 对表进行奇偶排序
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] greater 比较函数或宏
 					升序:((*i)->x > (*j)->x)
 					降序:((*i)->x < (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see dclnk_sortable, dclnk_is_sorted
 */
#define dclnk_oddeven_sort(_begin, _end, greater, type, member) do {	\
		int _k_pc = 1;													\
		dclnk_iter _k_e = (_end);										\
		dclnk_iter _k_f = (_begin);										\
		dclnk_iter _k_p = _k_f->prev;									\
		while (_k_pc) {													\
			_k_pc = 0;													\
			_k_f = _k_p->next;											\
			__dclnk_oddeven_scan(greater, type, member, _k_f);			\
			__dclnk_oddeven_scan(greater, type, member, _k_f->next);	\
		}																\
	} while (0)

#define __dclnk_oddeven_scan(greater, type, member, _start) do {	\
		dclnk_iter _k_i = _start;									\
		dclnk_iter _k_j = _k_i->next;								\
		while (_k_j != _k_e && _k_i != _k_e) {						\
			type * _k_pi = container_of(_k_i, type, member);		\
			type * _k_pj = container_of(_k_j, type, member);		\
			if (greater(_k_pi, _k_pj)) {							\
				dclnk_iter _k_t = _k_i;								\
				_k_i = _k_j;										\
				_k_j = _k_t;										\
				dclnk_iter_swap(_k_i, _k_j);						\
				_k_pc = 1;											\
			}														\
			_k_i = _k_j->next;										\
			_k_j = _k_i->next;										\
		}															\
	} while (0)

/**
 * @def dclnk_cocktail_sort(_begin, _end, greater, type, member)
 * @brief 对表进行鸡尾酒排序
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] greater 比较函数或宏
 					升序:((*i)->x > (*j)->x)
 					降序:((*i)->x < (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see dclnk_sortable, dclnk_is_sorted
 */
#define dclnk_cocktail_sort(_begin, _end, greater, type, member) do {	\
		int _k_pc = 1;													\
		dclnk_iter _k_e = (_end);										\
		dclnk_iter _k_f = (_begin);										\
		while (_k_pc) {													\
			dclnk_iter _k_i = _k_f;										\
			dclnk_iter _k_j = _k_f->next;								\
			_k_pc = 0;													\
			if (_k_j != _k_e) {											\
				type * _k_pj = container_of(_k_j, type, member);		\
				type * _k_pi = container_of(_k_i, type, member);		\
				if (greater(_k_pi, _k_pj)) {							\
					dclnk_iter _k_t = _k_i;								\
					_k_i = _k_j;										\
					_k_f = _k_j;										\
					_k_j = _k_t;										\
					dclnk_iter_swap(_k_i, _k_j);						\
					_k_pc = 1;											\
				}														\
				_k_i = _k_j;											\
				_k_j = _k_j->next;										\
			}															\
			while (_k_j != _k_e) {										\
				type * _k_pj = container_of(_k_j, type, member);		\
				type * _k_pi = container_of(_k_i, type, member);		\
				if (greater(_k_pi, _k_pj)) {							\
					dclnk_iter _k_t = _k_i;								\
					_k_i = _k_j;										\
					_k_j = _k_t;										\
					dclnk_iter_swap(_k_i, _k_j);						\
					_k_pc = 1;											\
				}														\
				_k_i = _k_j;											\
				_k_j = _k_j->next;										\
			}															\
			_k_i = _k_i->prev;											\
			_k_j = _k_i->prev;											\
			_k_f = _k_f->prev;											\
			while (_k_j != _k_f) {										\
				type * _k_pi = container_of(_k_i, type, member);		\
				type * _k_pj = container_of(_k_j, type, member);		\
				if (greater(_k_pj, _k_pi)) {							\
					dclnk_iter _k_t = _k_i;								\
					_k_i = _k_j;										\
					_k_j = _k_t;										\
					dclnk_iter_swap(_k_i, _k_j);						\
					_k_pc = 1;											\
				}														\
				_k_i = _k_j;											\
				_k_j = _k_j->prev;										\
			}															\
			_k_f = _k_f->next;											\
		}																\
	} while (0)

/**
 * @def dclnk_select_sort(_begin, _end, greater, type, member)
 * @brief 对表进行选择排序
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] greater 比较函数或宏
 					升序:((*i)->x > (*j)->x)
 					降序:((*i)->x < (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see dclnk_sortable, dclnk_is_sorted
 */
#define dclnk_select_sort(_begin, _end, greater, type, member) do {	\
		dclnk_iter _k_e = (_end);									\
		dclnk_iter _k_i = (_begin);									\
		dclnk_iter _k_j = _k_i->next;								\
		while (_k_j != _k_e) {										\
			dclnk_iter _k_m = _k_i;									\
			while (_k_j != _k_e) {									\
				type * _k_pj = container_of(_k_j, type, member);	\
				type * _k_pm = container_of(_k_m, type, member);	\
				if (greater(_k_pm, _k_pj))							\
					_k_m = _k_j;									\
				_k_j = _k_j->next;									\
			}														\
			if (_k_i != _k_m) {										\
				dclnk_iter _k_t = _k_m;								\
				_k_m = _k_i;										\
				_k_i = _k_t;										\
				dclnk_iter_swap(_k_m, _k_i);						\
			}														\
			_k_i = _k_i->next;										\
			_k_j = _k_i->next;										\
		}															\
	} while (0)

/**
 * @def dclnk_insert_sort(_begin, _end, greater, type, member)
 * @brief 对表进行插入排序
 * @param[in,out] _begin 起始位置
 * @param[in,out] _end 终止位置
 * @param[in] greater 比较函数或宏
 					升序:((*i)->x > (*j)->x)
 					降序:((*i)->x < (*j)->x)
 * @param[in] type 拥有者类型
 * @param[in] member 所在成员名
 * @see dclnk_sortable, dclnk_is_sorted
 */
#define dclnk_insert_sort(_begin, _end, greater_equal, type, member) do {\
		dclnk_iter _k_e = (_end);										\
		dclnk_iter _k_f = (_begin);										\
		dclnk_iter _k_i = _k_f->next;									\
		for (_k_f = _k_f->prev; _k_i != _k_e; _k_i = _k_i->next) {		\
			type * _k_pi = container_of(_k_i, type, member);			\
			dclnk_iter _k_j = _k_f->next;								\
			while (_k_j != _k_i) {										\
				type * _k_pj = container_of(_k_j, type, member);		\
				if (greater_equal(_k_pj, _k_pi)) {						\
					dclnk_iter _k_t = _k_i;								\
					_k_i = _k_i->prev;									\
					dclnk_erase(_k_t);									\
					dclnk_insert(_k_j, _k_t);							\
					break;												\
				}														\
				_k_j = _k_j->next;										\
			}															\
		}																\
	} while (0)

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_DCLNK__ */

