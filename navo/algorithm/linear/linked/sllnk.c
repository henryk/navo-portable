/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/linear/linked/sllnk.c
 * 
 * @brief Implements for singly linear linked list.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#include "sllnk.h"

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

NV_IMPL int sllnk_check(sllnk_head* head, sllnk_iter* loop_entry)
{
	sllnk_iter slow = head->next;
	sllnk_iter fast = slow;
	
	while (fast && fast->next) {
		slow = slow->next;
		fast = fast->next->next;
		if (fast == slow && fast) {
			if (loop_entry) {
				/* 获取环路入口 */
				slow = head->next;
				while (fast != slow) {
					slow = slow->next;
					fast = fast->next;
				}
				*loop_entry = slow;
			}
			return SLLNK_CHECK_LOOP;
		}
	}
	
	return SLLNK_CHECK_PASS;
}

NV_IMPL void __sllnk_iter_swap(
	sllnk_iter aprev, sllnk_iter a, sllnk_iter anext, 
	sllnk_iter bprev, sllnk_iter b, sllnk_iter bnext)
{
	if (a == bprev) {
		/* (1) b节点是a节点的后继节点
		 *                               (a)
		 * -(ap)-(a)-(b)-(bn)- ===>                ===> -(ap)-(b)-(a)-(bn)-
		 *                          -(ap)-(b)-(bn)-
		 */
		aprev->next = b;
		sllnk_insert_between(a, b, bnext);
	}
	else if (a == bnext) {
		/* (2) a节点是b节点的后继节点
		 *                               (b)
		 * -(bp)-(b)-(a)-(an)- ===>                ===> -(bp)-(a)-(b)-(an)-
		 *                          -(bp)-(a)-(an)-
		 */
		bprev->next = a;
		sllnk_insert_between(b, a, anext);
	}
	else {
		/* (3) a节点、b节点不相邻 
		 * -(ap)-(a)-(an)-...-(bp)-(b)-(bn)- ===> 
		 *                                              (a)
		 *                                        -(ap)-(b)-(an)-...-(bp) (bn)-
		 *
		 *  --(ap)-(b)-(an)-...-(bp)-(a)-(bn)-- <===
		 */
		sllnk_insert_between(b, aprev, anext);
		sllnk_insert_between(a, bprev, bnext);
	}
}

NV_IMPL size_t sllnk_distance(sllnk_iter first, sllnk_iter last)
{
	size_t dist = 1;
	
	while (first != last) {
		sllnk_inc(first);
		++dist;
	}
	
	return dist;
}

NV_IMPL size_t sllnk_count(sllnk_head* head)
{
	size_t cnt = 0;
	sllnk_iter i = head->next;

	while (i) {
		sllnk_inc(i);
		++cnt;
	}

	return cnt;
}

NV_IMPL void sllnk_reverse(sllnk_head* head)
{
	sllnk_iter i = sllnk_vbegin(head);
	while (i)
		sllnk_vpush(head, i);
}

NV_IMPL sllnk_iter sllnk_advance(sllnk_iter cur, size_t dist)
{
	while (cur && dist--)
		sllnk_inc(cur);
	
	return cur;
}

NV_IMPL size_t sllnk_index_of(sllnk_head* head, sllnk_iter itr)
{
	size_t cnt = 0;
	sllnk_iter i = head->next;

	while (i && i != itr)	{
		sllnk_inc(i);
		++cnt;
	}

	return cnt;
}

NV_IMPL void __sllnk_serialize(sllnk_head* head, char** buf, 
								const size_t offset)
{
	sllnk_iter i = head->next;
	while (i != head) {
		*buf++ = ((char*)i) - offset;
		i = i->next;
	}
}

NV_IMPL void __sllnk_deserialize(sllnk_head* head, char** buf, 
								size_t cnt, const size_t offset)
{
	sllnk_iter t, p = head;

	if (!cnt)
		return;
	while (cnt--) {
		t = (sllnk_iter)(*buf++ + offset);
		p->next = t;
		p = t;
	}
	t->next = head;
}

NV_IMPL void __sllnk_free(sllnk_iter begin, sllnk_iter end, 
						sllnk_pr1 erase_handler)
{
	while (begin != end)
		erase_handler(sllnk_inc_later(begin));
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

