/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file algorithm/linear/linked/dclnk.c
 * 
 * @brief Implements for doubly circular linked list.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#include "dclnk.h"

#ifdef _MSC_VER
#pragma optimize("t", on)
#endif	/* #ifdef _MSC_VER */

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

NV_IMPL int dclnk_check(dclnk_head* head, dclnk_iter* loop_entry)
{
	dclnk_iter sprv = (dclnk_iter)head;
	dclnk_iter fprv = (dclnk_iter)head;
	dclnk_iter slow = head->next;
	dclnk_iter fast = slow;
	
	while (fast != head && fast->next != head) {
		if (!fast || !fast->next)
			return DCLNK_CHECK_NULL;
		if (fprv != fast->prev || 
			sprv != slow->prev)
			return DCLNK_CHECK_LINK;
		sprv = slow;
		slow = slow->next;
		fprv = fast->next;
		fast = fprv->next;
		if (fast == slow) {
			if (loop_entry) {
				/* 获取环路入口 */
				slow = head->next;
				while (fast != slow) {
					slow = slow->next;
					fast = fast->next;
				}
				*loop_entry = slow;
			}
			return DCLNK_CHECK_LOOP;
		}
	}
	
	return DCLNK_CHECK_PASS;
}

NV_IMPL void __dclnk_iter_swap(
	dclnk_iter aprev, dclnk_iter a, dclnk_iter anext, 
	dclnk_iter bprev, dclnk_iter b, dclnk_iter bnext)
{
	if (a == bprev) {
		/* (1) b节点是a节点的后继节点
		 *                               (a)
		 * -(ap)-(a)-(b)-(bn)- ===>                ===> -(ap)-(b)-(a)-(bn)-
		 *                          -(ap)-(b)-(bn)-
		 */
		dclnk_link(aprev, b);
		dclnk_insert_between(a, b, bnext);
	}
	else if (a == bnext) {
		/* (2) a节点是b节点的后继节点
		 *                               (b)
		 * -(bp)-(b)-(a)-(an)- ===>                ===> -(bp)-(a)-(b)-(an)-
		 *                          -(bp)-(a)-(an)-
		 */
		dclnk_link(bprev, a);
		dclnk_insert_between(b, a, anext);
	}
	else {
		/* (3) a节点、b节点不相邻 
		 * -(ap)-(a)-(an)-...-(bp)-(b)-(bn)- ===>      (a)
		 *                                        -(ap)-(b)-(an)-...-(bp) (bn)-
		 *  --(ap)-(b)-(an)-...-(bp)-(a)-(bn)-- <===
		 */
		dclnk_insert_between(b, aprev, anext);
		dclnk_insert_between(a, bprev, bnext);
	}
}

NV_IMPL size_t dclnk_distance(dclnk_iter first, dclnk_iter last)
{
	size_t dist = 1;

	while (first != last) {
		dclnk_inc(first);
		++dist;
	}

	return dist;
}

NV_IMPL size_t dclnk_count(dclnk_head* head)
{
	size_t cnt = 0;
	dclnk_iter i = head->next;
	
	while (i != head) {
		dclnk_inc(i);
		++cnt;
	}
	
	return cnt;
}

NV_IMPL void dclnk_reverse(dclnk_head* head)
{
	dclnk_iter i = dclnk_vbegin(head);	
	while (i != head)
		dclnk_vpush(head, i);
}

NV_IMPL dclnk_iter dclnk_advance(dclnk_head* head, dclnk_iter cur, int dist)
{
	if (dist > 0) {
		/* 增加 */
		while (cur != head && dist--)
			dclnk_inc(cur);
	} else {
		/* 减少 */
		while (cur != head && dist++)
			dclnk_dec(cur);
	}
	
	return cur;
}

NV_IMPL size_t dclnk_index_of(dclnk_head* head, dclnk_iter itr)
{
	size_t cnt = 0;
	dclnk_iter i = head->next;
	
	while (i != head && i != itr) {
		dclnk_inc(i);
		++cnt;
	}
	
	return cnt;
}

NV_IMPL size_t dclnk_reverse_index_of(dclnk_head* head, dclnk_iter itr)
{
	size_t cnt = 0;
	dclnk_iter i = head->prev;
	
	while (i != head && i != itr) {
		dclnk_dec(i);
		++cnt;
	}
	
	return cnt;
}

NV_IMPL void __dclnk_serialize(dclnk_head* head, char** buf, 
								const size_t offset)
{
	dclnk_iter i = head->next;
	while (i != head) {
		*buf++ = ((char*)i) - offset;
		i = i->next;
	}
}

NV_IMPL void __dclnk_deserialize(dclnk_head* head, char** buf, 
							size_t cnt, const size_t offset)
{
	dclnk_iter t, p = head;

	if (!cnt)
		return;
	while (cnt--) {
		t = (dclnk_iter)(*buf++ + offset);
		p->next = t;
		t->prev = p;
		p = t;
	}
	t->next = head;
	head->prev = t;
}

NV_IMPL void __dclnk_free(dclnk_iter begin, dclnk_iter end, 
							dclnk_pr1 erase_handler)
{
	while (begin != end)
		erase_handler(dclnk_inc_later(begin));
}

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

