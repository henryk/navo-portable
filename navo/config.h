/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file config.h
 * 
 * @brief Configurations.
 * 
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_CONFIG__
#define __NV_CONFIG__

#ifndef NV_IMPL
#define NV_IMPL 
#endif /* #ifndef NV_IMPL */

#ifndef NV_API
#define NV_API 
#endif /* #ifndef NV_API */

// #define NV_NO_OBJECTIVE

#endif /* #ifndef __NV_CONFIG__ */

