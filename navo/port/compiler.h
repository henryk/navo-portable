/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file port/compiler.h
 * 
 * @brief Compiler.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_COMPILER__
#define __NV_COMPILER__

/**
 * @defgroup 编译环境
 * @ingroup 平台判定
 * @{
 */

/* MinGW */
#if defined(__MINGW32__)
#define __MINGW__ __MINGW32__

/* Cygwin */
#elif defined(__CYGWIN__)

/* Microsoft Visual Studio */
#elif defined(_MSC_VER)
#define __MSC__ _MSC_VER

/* GNU GCC */
#elif defined(__GNUC__)

/* Apple gcc */
#elif defined(__APPLE_CC__)
#define __APPLEC__ __APPLE_CC__

/* Borland C/C++ */
#elif defined(__BORLANDC__)

/* ARM C/C++ */
#elif defined(__CC_ARM) || defined(__ARMCC_VERSION)
#define __ARMC__ __CC_ARM

/* IAR C/C++ */
#elif defined(__IAR_SYSTEMS_ICC__)
#define __IARC__ __IAR_SYSTEMS_ICC__

/* Intel C/C++ */
#elif defined(__INTEL_COMPILER) || defined(__ICC) || \
		defined(__ICL) || defined(__ECC)
#define __INTELC__ __INTEL_COMPILER

/* IBM XL C/C++ */
#elif defined(__xlc__) || defined(__xlC__) || \
		defined(__IBMC__) || defined(__IBMCPP__)
#ifndef __IBMC__
#define __IBMC__ __xlc__
#endif /* #ifndef __IBMC__ */

/* DMC++ */
#elif defined(__DMC__)

/* DJGPP */
#elif defined(__DJGPP__)

/* ImageCraft C */
#elif defined(__IMAGECRAFT__)

/* HP aC++ */
#elif defined(__HP_aCC)
#define __HPAC__ __HP_aCC

/* Turbo C/C++ */
#elif defined(__TURBOC__)

/* KAI C++ */
#elif defined(__KCC)
#define __KAIC__ __KCC

/* Metrowerks CodeWarrior */
#elif defined(__MWERKS__) || defined(__CWCC__)
#ifndef __CWCC__
#define __CWCC__ __MWERKS__
#endif /* #ifndef __CWCC__ */

/* Sun Studio */
#elif defined(__SUNPRO_C) || defined(__SUNPRO_CC)
#define __SUNC__ __SUNPRO_CC

/* Stratus VOS C */
#elif defined(__VOSC__)

/* SAS/C */
#elif defined(SASC) || defined(__SASC) || defined(__SASC__)
#ifndef __SASC__
#define __SASC__ __SASC
#endif /* #ifndef __SASC__ */

/* Unknown compiler */
#else

#endif /* #if defined(__MINGW32__) */

/** @} */

#endif /* #ifndef __NV_COMPILER__ */


