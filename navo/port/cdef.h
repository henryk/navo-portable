/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file port/cdef.h
 *
 * @brief Definitions and declarations for common constants, types, variables.
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_CDEF__
#define __NV_CDEF__

#if !defined(__GNUC__) && defined(_MSC_VER) && \
	_MSC_VER >= 1400 /* VS2005 */
#include <intrin.h>
#endif /* #if !defined(__GNUC__) && defined(_MSC_VER) ... */

#ifndef NV_NO_CONFIG
#include "../config.h"
#else
#define NV_API
#define NV_IMPL
#endif /* #ifndef NV_NO_CONFIG */

/* include stddef.h or cstddef */
#ifdef __cplusplus
#include <cstddef>
#else
#include <stddef.h>
#ifndef inline
#ifdef _MSC_VER
#define inline __forceinline
#else
#define inline __inline
#endif /* #ifdef _MSC_VER */
#endif /* #ifndef inline */
#endif /* #ifdef __cplusplus */

/* force_inline */
#ifndef force_inline
#ifdef __GNUC__
#define force_inline inline __attribute__((always_inline))
#elif defined(_MSC_VER)
#define force_inline __forceinline
#endif /* #ifdef __GNUC__ */
#endif /* #ifndef force_inline */

/* Alignment */
#ifndef intrin_align
#ifdef __GNUC__
#define intrin_align(N) __attribute__((aligned(N)))
#elif defined(_MSC_VER) && _MSC_VER >= 1300 /* VS2003 */
#define intrin_align(N) __declspec(align(N))
#else
#define intrin_align(N)
#endif /* #ifdef __GNUC__ */
#endif /* #ifndef intrin_align */

/* Dynamic link library interface declaration */
#ifdef __GNUC__
#define dll_import __attribute__((dllimport))
#define dll_export __attribute__((dllexport))
#elif defined(_MSC_VER)
#define dll_import __declspec(dllimport)
#define dll_export __declspec(dllexport)
#else
#define dll_import
#define dll_export
#error "Macro 'dll_import' and 'dll_export' not prepared."
#endif /* #ifdef __GNUC__ */

/* Branch prediction */
#ifndef likely
#if defined(__GNUC__) && (__GNUC__ > 2 || __GNUC_MINOR__ >= 96)
#define likely(EXP) __builtin_expect(!!(EXP), 1)
#define unlikely(EXP) __builtin_expect(!!(EXP), 0)
#else
#define likely(EXP) (EXP)
#define unlikely(EXP) (EXP)
#endif /* #if defined(__GNUC__) && (__GNUC__ > 2 || ... */
#endif /* #ifndef likely */

/* Memory prefetch */
#ifndef prefetch
#ifdef __GNUC__
#define prefetch(ADDRESS) __builtin_prefetch(ADDRESS)
#elif defined(_MSC_VER) && _MSC_VER >= 1500 /* VS2008 */
#pragma intrinsic(__lfetch)
#define prefetch(ADDRESS) __lfetch(0, ADDRESS)
#else
#define prefetch(ADDRESS)
#endif /* #ifdef __GNUC__ */
#endif /* #ifndef prefetch */

/* Get function return address */
#ifndef return_address
#ifdef __GNUC__
#define return_address() __builtin_return_address(0)
#elif defined(_MSC_VER) && _MSC_VER >= 1500
#pragma intrinsic(_ReturnAddress)
/* __asm mov eax, dword ptr[ebp + 4] */
#define return_address() _ReturnAddress()
#endif /* #ifdef __GNUC__ */
#endif /* #ifndef return_address */

/* Get the number of array elements */
#ifndef count_of
#define count_of(ARRAY) (sizeof(ARRAY) / sizeof(ARRAY[0]))
#endif /* #ifndef count_of */

/* Get array bound */
#ifndef bound_of
#define bound_of(ARRAY) ((ARRAY) + count_of(ARRAY))
#endif /* #ifndef bound_of */

/* Get the address offset of a structure member */
#ifndef offset_of
#ifdef __compiler_offsetof
#define offset_of(STRUCT, MEMBER) __compiler_offsetof(STRUCT, MEMBER)
#elif defined(__GNUC__)
#define offset_of(STRUCT, MEMBER) __builtin_offsetof(STRUCT, MEMBER)
#else
#define offset_of(STRUCT, MEMBER) ((size_t)(&(((STRUCT *)0)->MEMBER)))
#endif /* #ifdef __compiler_offsetof */
#endif /* #ifndef offset_of */

/* Get structure address by member address */
#ifndef container_of
#define container_of(PTR, STRUCT, MEMBER) \
	((STRUCT *)(((char *)PTR) - offset_of(STRUCT, MEMBER)))
#endif /* container_of */

/* Get structure by member address */
#ifndef owner_of
#define owner_of(PTR, STRUCT, MEMBER) \
	(* container_of(PTR, STRUCT, MEMBER))
#endif /* #ifndef owner_of */

/* Get certain member by the specified structure type */
#ifndef member_of
#define member_of(PTR, STRUCT, MEMBER) (((STRUCT *)PTR)->MEMBER)
#endif /* #ifndef member_of */

/*
 * 定义节点结构体的3种方式(前2种支持多重节点):
 *
 * (1)
 * struct ListNode
 * {
 *     int m_a;
 *     int m_b;
 *
 *     node_list(
 *         dclnk_node c;
 *         rblnk_node rb;
 *     );
 * };
 *
 * (2)
 * node_struct(ListNode, 
 *     dclnk_node c;
 *     rblnk_node rb;
 *     ,
 *     int m_a;
 *     int m_b;
 * );
 *
 * (3)
 * snode_struct(ListNode, dclnk_node, 
 *     int m_a;
 *     int m_b;
 * );
 *
 */

/* Intrusive node container */
#define NV_NODE_LIST_NAME __NV_node_list_body__
#define NV_UNIQUE_NODE_NAME __NV_unique_node__

#define node_list(BODY) struct { BODY } NV_NODE_LIST_NAME

#define node_struct(NAME, NODE_BODY, DATA_BODY) \
	struct NAME { node_list(NODE_BODY); DATA_BODY }
#define snode_struct(NAME, NODE_TYPE, DATA_BODY) \
	node_struct(NAME, NODE_TYPE NV_UNIQUE_NODE_NAME;, DATA_BODY)

#define node_struct_of(PTR, TYPE, NODE) \
	container_of(PTR, TYPE, NV_NODE_LIST_NAME. NODE)
#define snode_struct_of(PTR, TYPE) \
	container_of(PTR, TYPE, NV_NODE_LIST_NAME.NV_UNIQUE_NODE_NAME)

#define node_list_of(PTR) ((PTR)->NV_NODE_LIST_NAME)
#define snode_list_of(PTR) node_list_of(PTR)

#define node_of(PTR, NODE_NAME) (&(node_list_of(PTR). NODE_NAME))
#define snode_of(PTR) node_of(PTR, NV_UNIQUE_NODE_NAME)

/* Macros for ID combination */
#define NV_MAKE_STR(ID_STR) 				#ID_STR
#define NV_MAKE_ID2(ID1, ID2) 				ID1##ID2
#define NV_MAKE_ID3(ID1, ID2, ID3) 			ID1##ID2##ID3
#define NV_MAKE_ID4(ID1, ID2, ID3, ID4) 	ID1##ID2##ID3##ID4

/* Auto-block, execute initialize and finalize 
 * method at the beginning and end of the block automatically
 */
typedef struct {
#define NV_AUTO_BLOCK_MAX_LAYER (31)
#define NV_AUTO_BLOCK_TYPE unsigned int /* 32-bit = 31 layers */
	NV_AUTO_BLOCK_TYPE layer;
	NV_AUTO_BLOCK_TYPE buff;
}__NV_AUTO_BLOCK_BUFF;

#define auto_block_init() \
    __NV_AUTO_BLOCK_BUFF __NV_auto_block__ = {1, 0}

#define auto_block(INIT, FINAL) \
	INIT ; for (__NV_auto_block__.layer <<= 1, 						\
		__NV_auto_block__.buff &= (~__NV_auto_block__.layer);		\
		(!(__NV_auto_block__.buff & __NV_auto_block__.layer)) || 	\
		(__NV_auto_block__.layer >>= 1, (FINAL), 0); 				\
		__NV_auto_block__.buff |= __NV_auto_block__.layer)

#define break_block \
	__NV_auto_block__.buff |= __NV_auto_block__.layer; continue

#define AUTOBLOCK_IDLE 0

/* Allocation and release for user level memory */
#ifdef __cplusplus

#define nv_alloc(TYPE) new TYPE
#define nv_free(PTR) delete PTR
#define nv_alloc_array(TYPE, LENGTH) new TYPE[LENGTH]
#define nv_free_array(PTR) delete [] PTR

/* Support Objective */
#ifndef NV_NO_OBJECTIVE
#define nv_new(TYPE) new TYPE
#define nv_delete(TYPE, PTR) delete PTR
#endif /* #ifndef NV_NO_OBJECTIVE */

#else /* #ifdef __cplusplus */

#define nv_alloc(TYPE) ((TYPE *)malloc(sizeof(TYPE)))
#define nv_free(PTR) free(PTR)
#define nv_alloc_array(TYPE, LENGTH) \
	((TYPE *)malloc(sizeof(TYPE) * (LENGTH)))
#define nv_free_array(PTR) free(PTR)

/* Support Objective */
#ifndef NV_NO_OBJECTIVE
#define nv_new(TYPE) TYPE##_construct(nv_alloc(TYPE))
#define nv_delete(TYPE, PTR) nv_free(TYPE##_destruct(PTR))
#define NV_CONSTRUCT(TYPE, PARAM_NAME) \
	TYPE* TYPE##_construct(TYPE* PARAM_NAME)
#define NV_DESTRUCT(TYPE, PARAM_NAME) \
	TYPE* TYPE##_destruct(TYPE* PARAM_NAME)
#define NV_CONSTRUCT_PTR(TYPE, NAME) \
	TYPE* (* NAME)(TYPE*)
#define NV_DESTRUCT_PTR(TYPE, NAME) \
	TYPE* (* NAME)(TYPE*)
#endif /* #ifndef NV_NO_OBJECTIVE */
	
#endif /* #ifdef __cplusplus */

/* Pointer reference and auto-free */
#define break_using break_block
#define using_ptr(PTR, RIGHT_VALUE) \
	auto_block((PTR) = (RIGHT_VALUE), nv_free(PTR))
#define using_new(PTR, TYPE) \
	using_ptr(PTR, nv_alloc(TYPE))
#define using_array(PTR, RIGHT_VALUE) \
	auto_block((PTR) = (RIGHT_VALUE), nv_free_array(PTR))
#define using_new_array(PTR, TYPE, LENGTH) \
	using_ptr(PTR, nv_alloc_array(TYPE, LENGTH))

/* Support Objective */
#ifndef NV_NO_OBJECTIVE

#define using_obj(TYPE, PTR, RIGHT_VALUE) \
	auto_block((PTR) = (RIGHT_VALUE), nv_delete(TYPE, PTR))
#define using_new_obj(PTR, TYPE) \
	using_obj(TYPE, PTR, nv_new(TYPE))
#define using_static_obj(PTR, TYPE) \
	auto_block(TYPE##_construct(PTR), TYPE##_destruct(PTR))

#endif /* #ifndef NV_NO_OBJECTIVE */


#endif /* #ifndef __NV_CDEF__ */
