/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file port/little_endian.h
 * 
 * @brief Definitions for little endian operations.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_LITTLE_ENDIAN__
#define __NV_LITTLE_ENDIAN__

#include "swab.h"

/**
 * @defgroup 小端
 * @ingroup 字节顺序
 * @{
 */

/* 交换的常量 */
#define __cpu_to_le16_c(c) ((uint16_t)(c))
#define __le16_to_cpu_c(c) ((uint16_t)(c))
#define __cpu_to_le32_c(c) ((uint32_t)(c))
#define __le32_to_cpu_c(c) ((uint32_t)(c))
#define __cpu_to_le64_c(c) ((uint64_t)(c))
#define __le64_to_cpu_c(c) ((uint64_t)(c))
#define __cpu_to_be16_c(c) __bswap16_c(c)
#define __be16_to_cpu_c(c) __bswap16_c(c)
#define __cpu_to_be32_c(c) __bswap32_c(c)
#define __be32_to_cpu_c(c) __bswap32_c(c)
#define __cpu_to_be64_c(c) __bswap64_c(c)
#define __be64_to_cpu_c(c) __bswap64_c(c)

/* 传值交换，返回结果 */
#define __cpu_to_le16(v) ((uint16_t)(v))
#define __le16_to_cpu(v) ((uint16_t)(v))
#define __cpu_to_le32(v) ((uint32_t)(v))
#define __le32_to_cpu(v) ((uint32_t)(v))
#define __cpu_to_le64(v) ((uint64_t)(v))
#define __le64_to_cpu(v) ((uint64_t)(v))
#define __cpu_to_be16(v) __bswap16(v)
#define __be16_to_cpu(v) __bswap16(v)
#define __cpu_to_be32(v) __bswap32(v)
#define __be32_to_cpu(v) __bswap32(v)
#define __cpu_to_be64(v) __bswap64(v)
#define __be64_to_cpu(v) __bswap64(v)

/* 传址交换，返回结果，不存回内存 */
#define __cpu_to_le16_p(p) (*(uint16_t*)(p))
#define __le16_to_cpu_p(p) (*(uint16_t*)(p))
#define __cpu_to_le32_p(p) (*(uint32_t*)(p))
#define __le32_to_cpu_p(p) (*(uint32_t*)(p))
#define __cpu_to_le64_p(p) (*(uint64_t*)(p))
#define __le64_to_cpu_p(p) (*(uint64_t*)(p))
#define __cpu_to_be16_p(p) __bswap16_p(p)
#define __be16_to_cpu_p(p) __bswap16_p(p)
#define __cpu_to_be32_p(p) __bswap32_p(p)
#define __be32_to_cpu_p(p) __bswap32_p(p)
#define __cpu_to_be64_p(p) __bswap64_p(p)
#define __be64_to_cpu_p(p) __bswap64_p(p)

/* 传址交换，不返回结果，存回内存 */
#define __cpu_to_le16_s(p) do {} while (0)
#define __le16_to_cpu_s(p) do {} while (0)
#define __cpu_to_le32_s(p) do {} while (0)
#define __le32_to_cpu_s(p) do {} while (0)
#define __cpu_to_le64_s(p) do {} while (0)
#define __le64_to_cpu_s(p) do {} while (0)
#define __cpu_to_be16_s(p) __bswap16_s(p)
#define __be16_to_cpu_s(p) __bswap16_s(p)
#define __cpu_to_be32_s(p) __bswap32_s(p)
#define __be32_to_cpu_s(p) __bswap32_s(p)
#define __cpu_to_be64_s(p) __bswap64_s(p)
#define __be64_to_cpu_s(p) __bswap64_s(p)

/* 数组传址交换，存回内存 */
#define __cpu_to_le16_array(b, c) do {} while (0)
#define __le16_to_cpu_array(b, c) do {} while (0)
#define __cpu_to_le32_array(b, c) do {} while (0)
#define __le32_to_cpu_array(b, c) do {} while (0)
#define __cpu_to_le64_array(b, c) do {} while (0)
#define __le64_to_cpu_array(b, c) do {} while (0)
#define __cpu_to_be16_array(b, c) __bswap16_array(b, c)
#define __be16_to_cpu_array(b, c) __bswap16_array(b, c)
#define __cpu_to_be32_array(b, c) __bswap32_array(b, c)
#define __be32_to_cpu_array(b, c) __bswap32_array(b, c)
#define __cpu_to_be64_array(b, c) __bswap64_array(b, c)
#define __be64_to_cpu_array(b, c) __bswap64_array(b, c)

/**
 * @def __htonl(v)
 * @brief 32位主机字序转网络字序
 * @def __ntohl(v)
 * @brief 32位网络字序主机字序转
 * @def __htons(v)
 * @brief 16位主机字序转网络字序
 * @def __ntohs(v)
 * @brief 16位网络字序转主机字序
 */
#define __htonl(v) __cpu_to_be32(v)
#define __ntohl(v) __be32_to_cpu(v)
#define __htons(v) __cpu_to_be16(v)
#define __ntohs(v) __be16_to_cpu(v)

/** @} */

#endif /* #ifndef __NV_LITTLE_ENDIAN__ */

