/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file port/arch.h
 * 
 * @brief Architecture.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_ARCH__
#define __NV_ARCH__

#include "os.h"

/**
 * @defgroup CPU架构
 * @ingroup 平台判定
 * @{
 */

/* 整数模型 */
/* 整型大小 (in bits) */
#define NV_LP32_CHAR_BITS 		 8
#define NV_LP32_SHORT_BITS 		16
#define NV_LP32_INT_BITS 		16
#define NV_LP32_LONG_BITS 		32
#define NV_LP32_LONGLONG_BITS 	64
#define NV_LP32_PTR_BITS 		32

#define NV_ILP32_CHAR_BITS 		 8
#define NV_ILP32_SHORT_BITS 	16
#define NV_ILP32_INT_BITS 		32
#define NV_ILP32_LONG_BITS 		32
#define NV_ILP32_LONGLONG_BITS 	64
#define NV_ILP32_PTR_BITS 		32

#define NV_LP64_CHAR_BITS 		 8
#define NV_LP64_SHORT_BITS 		16
#define NV_LP64_INT_BITS 		32
#define NV_LP64_LONG_BITS 		64
#define NV_LP64_LONGLONG_BITS 	64
#define NV_LP64_PTR_BITS 		64

#define NV_ILP64_CHAR_BITS 		 8
#define NV_ILP64_SHORT_BITS 	16
#define NV_ILP64_INT_BITS 		64
#define NV_ILP64_LONG_BITS 		64
#define NV_ILP64_LONGLONG_BITS 	64
#define NV_ILP64_PTR_BITS 		64

#define NV_LLP64_CHAR_BITS 		 8
#define NV_LLP64_SHORT_BITS 	16
#define NV_LLP64_INT_BITS 		32
#define NV_LLP64_LONG_BITS 		32
#define NV_LLP64_LONGLONG_BITS 	64
#define NV_LLP64_PTR_BITS 		64

/**
 * @def NV_INT_MOD_BITS(model, type)
 * @brief module整数模型下type类型的比特数
 */
#define NV_INT_MOD_BITS(model, type) NV_##model##type##_BITS

#if defined(__GNUC__) && !defined(__MINGW32__)
#include <bits/wordsize.h>
#endif /* #if defined(__GNUC__) && !defined(__MINGW32__) */

#ifndef __WORDSIZE
#ifdef __SIZEOF_POINTER__
#if __SIZEOF_POINTER__ == 4
#define __WORDSIZE 32
#elif __SIZEOF_POINTER__ == 8
#define __WORDSIZE 64
#elif __SIZEOF_POINTER__ == 2
#define __WORDSIZE 16
#endif /* #if __SIZEOF_POINTER__ == 4 */
#endif /* #ifdef __SIZEOF_POINTER__ */
#endif /* #ifndef __WORDSIZE */

#ifdef __CHAR_BIT__
#define CHAR_BITS __CHAR_BIT__
#endif /* #ifdef __CHAR_BIT__ */

#ifdef __SHRT_MAX__
#if __SHRT_MAX__ == 32767
#define SHORT_BITS 16
#endif /* #if __SHRT_MAX__ == 32767 */
#endif /* #ifdef __SHRT_MAX__ */

#ifdef __SIZE_OF_INT__
#if __SIZE_OF_INT__ == 4
#define INT_BITS 32
#elif __SIZE_OF_INT__ == 8
#define INT_BITS 64
#elif __SIZE_OF_INT__ == 2
#define INT_BITS 16
#endif /* #if __SIZE_OF_INT__ == 4 */
#endif /* #ifdef __CHAR_BIT__ */

#ifdef __LONG_MAX__
#if __LONG_MAX__ == 2147483647L
#define LONG_BITS 32
#else
#define LONG_BITS 64
#endif /* #if __LONG_MAX__ == 2147483647L */
#endif /* #ifdef __LONG_MAX__ */

#ifdef __BYTE_ORDER__
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#ifndef __LITTLE_ENDIAN
#define __LITTLE_ENDIAN 1234
#endif /* #ifndef __LITTLE_ENDIAN */
#elif !defined(__BIG_ENDIAN)
#define __BIG_ENDIAN 4321
#endif /* #if BYTE_ORDER == _LITTLE_ENDIAN */
#endif /* #ifdef __BYTE_ORDER__ */

/* Intel x86 */
#if defined(_M_IX86) || defined(i386) || defined(__i386__) || \
	defined(__i386) || defined(__INTEL__) || defined(__X86__) || \
	defined(_X86_) || defined(__I86__)
#define NV_ARCH_X86
#include "arch/_x86.h"

/* Intel IA-64 */
#elif defined(_M_IA64) || defined(__ia64__) || defined(__IA64__) || \
	defined(_IA64) || defined(__ia64) || \
	defined(__itanium__) || defined(_IA64_) || \
	(defined(__CP_ICC) && (defined(__x86_64) || defined(__x86_64__)))
#define NV_ARCH_IA64
#include "arch/_ia64.h"

/* x64 / AMD-64 */
#elif defined(_M_X64) || defined(_M_IX64) || defined(__amd64) || \
		defined(__amd64__) || defined(_AMD64_) || defined(_M_AMD64) || \
		((!defined(__CP_ICC))&&(defined(__x86_64) || defined(__x86_64__)))
#define NV_ARCH_X64
#include "arch/_x64.h"

/* ARM */
#elif defined(__arm__) || defined(_M_ARM) || defined(_ARM)
#include "arch/_arm.h"

/* Alpha */
#elif defined(_M_ALPHA) || defined(__alpha) || \
    defined(__alpha__) || defined(_ALPHA_)
#define NV_ARCH_ALPHA
#include "arch/_alpha.h"

/* PowerPC-64 */
#elif defined(__powerpc64__) || defined(__ppc64__)
#define NV_ARCH_PPC64
#include "arch/_ppc64.h"

/* PowerPC */
#elif defined(_M_PPC) || defined(__powerpc) || defined(__powerpc__) || \
	defined(__ppc__) || defined(__POWERPC__) || defined(_ARCH_PPC)
#define NV_ARCH_PPC
#include "arch/_ppc.h"

/* MIPS */
#elif defined(_M_MRX000) || defined(__mips__) || defined(mips) || \
	defined(__mips) || defined(__MIPS__)
#define NV_ARCH_MIPS
#include "arch/_mips.h"

/* HP/PA RISC */
#elif defined(__hppa__) || defined(__hppa) || defined(__HPPA__)
#define NV_ARCH_HPPA
#include "arch/_hppa.h"

/* SPARC */
#elif defined(__sparc) || defined(__sparc__)
#define NV_ARCH_SPARC
#include "arch/_sparc.h"

/* Motorola 68000 */
#elif defined(__mc68000__) || defined(__MC68000__)
#define NV_ARCH_MC68000
#include "arch/_mc68000.h"

/* System/390 */
#elif defined(__s390__)
#define NV_ARCH_S390
#include "arch/_s390.h"

/* System/390x */
#elif defined(__s390x__)
#define NV_ARCH_S390X
#include "arch/_s390x.h"

#else

#error Please provide a arch/_xxx.h appropriate for your platform

#endif /* #if defined(_M_IX86) || defined(i386) ... */

/** @} */

#endif /* #ifndef __NV_ARCH__ */

