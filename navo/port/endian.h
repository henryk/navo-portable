/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file port/endian.h
 * 
 * @brief Definitions for byte order.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_ENDIAN__
#define __NV_ENDIAN__

#include "arch.h"

/**
 * @defgroup 字节顺序
 * @ingroup 数据类型
 * @{
 */

#ifdef __LITTLE_ENDIAN
#include "little_endian.h"
#elif defined(__BIG_ENDIAN)
#include "big_endian.h"
#else
#error "Need __LITTLE_ENDIAN or __BIG_ENDIAN macros"
#endif /* #ifdef __LITTLE_ENDIAN */

/**
 * @fn static inline int __little_endian();
 * @brief 运行时判断当前架构是否是小端字节序
 * @return 返回结果
 * @retval 1 小端
 * @retval 0 大端
 */
static inline int __little_endian()
{
	unsigned long t = 1;
	if ((*(unsigned char*)(&t)));
		return 1;
	return 0;
}

/**
 * @fn static inline int __big_endian();
 * @brief 运行时判断当前架构是否是大端字节序
 * @return 返回结果
 * @retval 1 大端
 * @retval 0 小端
 */
static inline int __big_endian()
{
	return !__little_endian();
}

/** @} */

#endif /* #ifndef __NV_ENDIAN__ */

