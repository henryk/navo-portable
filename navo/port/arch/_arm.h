/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file port/arch/_arm.h
 * 
 * @brief ARM Architecture.
 * 
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_ARCH_ARM__
#define __NV_ARCH_ARM__

/**
 * @defgroup ARM
 * @ingroup CPU�ܹ�
 * @{
 */

#ifdef __CC_ARM

#define NV_ARCH_ARM __TARGET_ARCH_ARM

#ifdef __BIG_ENDIAN
#define __ARMEB__
#else
#define __ARMEL__
#endif /* #ifdef __BIG_ENDIAN */

#else

#if defined(__ARMEB__) && !defined(__BIG_ENDIAN)
#define __BIG_ENDIAN 4321
#else
#define __LITTLE_ENDIAN 1234
#endif /* #if defined(__ARMEB__) && !defined(__BIG_ENDIAN) */

#if defined(__ARM_ARCH_8__) || defined(__ARM_ARCH_8A__)
#define NV_ARCH_ARM 8
#elif defined(__ARM_ARCH_7__) || defined(__ARM_ARCH_7A__) || \
	defined(__ARM_ARCH_7R__) || defined(__ARM_ARCH_7M__) || \
	defined(__ARM_ARCH_7EM__)
#define NV_ARCH_ARM 7
#elif defined(__ARM_ARCH_6__) || defined(__ARM_ARCH_6J__) || \
	defined(__ARM_ARCH_6K__) || defined(__ARM_ARCH_6M__) || \
	defined(__ARM_ARCH_6Z__) || defined(__ARM_ARCH_6ZK__) || \
	defined(__ARM_ARCH_6T2__)
#define NV_ARCH_ARM 6
#elif defined(__ARM_ARCH_5__) || defined(__ARM_ARCH_5T__) || \
	defined(__ARM_ARCH_5E__) || defined(__ARM_ARCH_5TE__)
#define NV_ARCH_ARM 5
#elif defined(__ARM_ARCH_4__) || defined(__ARM_ARCH_4T__)
#define NV_ARCH_ARM 4
#else
/* Unsupported ARM architecture */
#define NV_ARCH_ARM 0
#endif /* #if defined(__ARM_ARCH_8__) */

#endif /* #ifdef __CC_ARM */

#ifdef __NV_ARCH_INTEGER__

#if defined(NV_OS_LINUX)

#define INT64_SUPPORT

#define CHAR_BITS 		NV_INT_MOD_BITS(ILP32, CHAR)
#define SHORT_BITS 		NV_INT_MOD_BITS(ILP32, SHORT)
#define INT_BITS 		NV_INT_MOD_BITS(ILP32, INT)
#define LONG_BITS 		NV_INT_MOD_BITS(ILP32, LONG)
#define LONGLONG_BITS 	NV_INT_MOD_BITS(ILP32, LONGLONG)
#define PTR_BITS 		NV_INT_MOD_BITS(ILP32, PTR)
#define FLOAT_BITS 		32
#define DOUBLE_BITS 	64

#endif /* #if defined(__linux__) */

#ifndef __WORDSIZE
#define __WORDSIZE PTR_BITS
#endif /* #ifndef __WORDSIZE */

#endif /* #ifdef __NV_ARCH_INTEGER__ */

/** @} */

#endif /* #ifndef __NV_ARCH_ARM__ */

