/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file port/arch/_x64.h
 * 
 * @brief Intel/AMD X64 Architecture.
 * 
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_ARCH_X64__
#define __NV_ARCH_X64__

/**
 * @defgroup Intel_AMD-x64
 * @ingroup CPU�ܹ�
 * @{
 */

#ifndef __LITTLE_ENDIAN
#define __LITTLE_ENDIAN 1234
#endif /* #ifndef __LITTLE_ENDIAN */

#ifdef __NV_ARCH_INTEGER__

#if defined(__linux__)

#define INT64_SUPPORT

#define CHAR_BITS 		NV_INT_MOD_BITS(LP64, CHAR)
#define SHORT_BITS 		NV_INT_MOD_BITS(LP64, SHORT)
#define INT_BITS 		NV_INT_MOD_BITS(LP64, INT)
#define LONG_BITS 		NV_INT_MOD_BITS(LP64, LONG)
#define LONGLONG_BITS 	NV_INT_MOD_BITS(LP64, LONGLONG)
#define PTR_BITS 		NV_INT_MOD_BITS(LP64, PTR)
#define FLOAT_BITS 		32
#define DOUBLE_BITS 	64

#elif defined(__win64__)

#define INT64_SUPPORT

#define CHAR_BITS 		NV_INT_MOD_BITS(LLP64, CHAR)
#define SHORT_BITS 		NV_INT_MOD_BITS(LLP64, SHORT)
#define INT_BITS 		NV_INT_MOD_BITS(LLP64, INT)
#define LONG_BITS 		NV_INT_MOD_BITS(LLP64, LONG)
#define LONGLONG_BITS 	NV_INT_MOD_BITS(LLP64, LONGLONG)
#define PTR_BITS 		NV_INT_MOD_BITS(LLP64, PTR)
#define FLOAT_BITS 		32
#define DOUBLE_BITS 	64

#endif /* #if defined(__linux__) */

#ifndef __WORDSIZE
#define __WORDSIZE PTR_BITS
#endif /* #ifndef __WORDSIZE */

#endif /* #ifdef __NV_ARCH_INTEGER__ */

/** @} */

#endif /* #ifndef __NV_ARCH_X64__ */

