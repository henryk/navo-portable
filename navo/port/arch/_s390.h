/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file port/arch/_s390.h
 * 
 * @brief System/390 Architecture.
 * 
 * Copyright (C) 2012-2022, Henry Kwok ������. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_ARCH_S390__
#define __NV_ARCH_S390__

/**
 * @defgroup System-390
 * @ingroup CPU�ܹ�
 * @{
 */

#ifndef __BIG_ENDIAN
#define __BIG_ENDIAN 4321
#endif /* #ifndef __BIG_ENDIAN */

#ifdef __NV_ARCH_INTEGER__

#if defined(__linux__)

#define INT64_SUPPORT

#define CHAR_BITS 		NV_INT_MOD_BITS(ILP32, CHAR)
#define SHORT_BITS 		NV_INT_MOD_BITS(ILP32, SHORT)
#define INT_BITS 		NV_INT_MOD_BITS(ILP32, INT)
#define LONG_BITS 		NV_INT_MOD_BITS(ILP32, LONG)
#define LONGLONG_BITS 	NV_INT_MOD_BITS(ILP32, LONGLONG)
#define PTR_BITS 		NV_INT_MOD_BITS(ILP32, PTR)
#define FLOAT_BITS 		32
#define DOUBLE_BITS 	64

#endif /* #if defined(__linux__) */

#ifndef __WORDSIZE
#define __WORDSIZE PTR_BITS
#endif /* #ifndef __WORDSIZE */

#endif /* #ifdef __NV_ARCH_INTEGER__ */

/** @} */

#endif /* #ifndef __NV_ARCH_S390__ */

