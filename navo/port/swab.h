/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file port/swab.h
 * 
 * @brief Endian swap.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_SWAB__
#define __NV_SWAB__

#include "cdef.h"
#include "compiler.h"
#include "arch.h"
#include "integer.h"

/**
 * @defgroup 交换字序
 * @ingroup 字节顺序
 * @{
 */

/**
 * @def __bswap16_c(c)
 * @brief 16位常量字序反转
 * @param c 双字节常量
 */
#define __bswap16_c(c) ((uint16_t)(	\
	(((c) & 0x00FF) << 8) | 	\
	(((c) & 0xFF00) >> 8)))

/**
 * @def __bswap32_c(c)
 * @brief 32位常量字序反转
 * @param c 四字节常量
 */
#define __bswap32_c(c) ((uint32_t)(	\
	(((c) & 0x000000FFUL) << 24) | \
	(((c) & 0x0000FF00UL) <<  8) | \
	(((c) & 0x00FF0000UL) >>  8) | \
	(((c) & 0xFF000000UL) >> 24)))

/**
 * @fn static inline uint16_t __bswap16(uint16_t x);
 * @brief 反转16位整数
 * @param[in] x 原整数
 * @return uint16_t 返回反转后的16位整数
 */
static inline uint16_t __bswap16(uint16_t x)
{
	return __bswap16_c(x);
}

/**
 * @fn static inline uint32_t __bswap32(uint32_t x);
 * @brief 反转32位整数
 * @param[in] x 原整数
 * @return uint32_t 返回反转后的32位整数
 */
static inline uint32_t __bswap32(uint32_t x)
{
#if defined(NV_ARCH_X86) || defined(NV_ARCH_X64) || defined(NV_ARCH_IA64)
#ifdef __GNUC__
	__asm__("bswap %0" : "=r" (x) : "0" (x));
	return x;
#elif defined(_MSC_VER)
	__asm push eax
	__asm mov eax, x
	__asm bswap eax
	__asm mov x, eax
	__asm pop eax
	return x;
#else
	return __bswap32_c(x);
#endif /* #ifdef __GNUC__ */
#elif defined(__GNUC__)
	return __builtin_bswap32(x);
#else
	return __bswap32_c(x);
#endif /* #if defined(NV_ARCH_X86) || defined(NV_ARCH_X64) */
}

/**
 * @fn static inline uint16_t __bswap16_p(void* p);
 * @brief 反转16位整数
 * @param[in] p 16位整数指针
 * @return uint16_t 返回反转后的16位整数
 */
static inline uint16_t __bswap16_p(void* p)
{
	uint16_t x = *((uint16_t*)p);
	return __bswap16_c(x);
}

/**
 * @fn static inline uint32_t __bswap32_p(void* p);
 * @brief 反转32位整数
 * @param[in] p 32位整数指针
 * @return uint32_t 返回反转后的32位整数
 */
static inline uint32_t __bswap32_p(void* p)
{
	return __bswap32(*((uint32_t*)p));
}

/**
 * @fn static inline void __bswap16_s(void* p);
 * @brief 反转16位整数，并存回内存
 * @param[in,out] p 16位整数指针
 */
static inline void __bswap16_s(void* p)
{
	uint16_t x = *((uint16_t*)p);
	*((uint16_t*)p) = __bswap16_c(x);
}

/**
 * @fn static inline void __bswap32_s(void* p);
 * @brief 反转32位整数，并存回内存
 * @param[in,out] p 32位整数指针
 */
static inline void __bswap32_s(void* p)
{
	*((uint32_t*)p) = __bswap32(*((uint32_t*)p));
}

/**
 * @def __bswap16_array(b, c)
 * @brief 逐个反转16位整数数组的元素，并存回内存
 * @param[in,out] b 数组地址
 * @param[in] c 数组长度
 */
#define __bswap16_array(b, c) do {			\
		uint16_t* _sw_b = (uint16_t*)(b);	\
		size_t _sw_c = (c);					\
		while (_sw_c--) {					\
			__bswap16_s(_sw_b++);			\
		}									\
	} while (0)

/**
 * @def __bswap32_array(b, c)
 * @brief 逐个反转32位整数数组的元素，并存回内存
 * @param[in,out] b 数组地址
 * @param[in] c 数组长度
 */
#define __bswap32_array(b, c) do {			\
		uint32_t* _sw_b = (uint32_t*)(b);	\
		size_t _sw_c = (c);					\
		while (_sw_c--) {					\
			__bswap32_s(_sw_b++);			\
		}									\
	} while (0)

/**
 * @def __rol16_c(c, b)
 * @brief 左旋转16位整型常量
 * @param c 整型常量
 * @param b 旋转量
 * @def __ror16_c(c, b)
 * @brief 右旋转16位整型常量
 * @param c 整型常量
 * @param b 旋转量
 * @def __rol32_c(c, b)
 * @brief 左旋转32位整型常量
 * @param c 整型常量
 * @param b 旋转量
 * @def __ror32_c(c, b)
 * @brief 右旋转32位整型常量
 * @param c 整型常量
 * @param b 旋转量
 */
#define __rol16_c(c, b) (((uint16_t)(c) << (b)) | ((uint16_t)(c) >> (16 - (b))))
#define __ror16_c(c, b) (((uint16_t)(c) >> (b)) | ((uint16_t)(c) << (16 - (b))))
#define __rol32_c(c, b) (((uint32_t)(c) << (b)) | ((uint32_t)(c) >> (32 - (b))))
#define __ror32_c(c, b) (((uint32_t)(c) >> (b)) | ((uint32_t)(c) << (32 - (b))))

/**
 * @fn static uint16_t __rol16(uint16_t v, int b);
 * @brief 左旋转16位整数b位
 * @param[in] v 16位整数
 * @param[in] b 旋转量
 * @return uint16_t 返回旋转后的整数
 * @see __ror16
 */
static inline uint16_t __rol16(uint16_t v, int b)
{
	return __rol16_c(v, b);
}

/**
 * @fn static uint16_t __ror16(uint16_t v, int b);
 * @brief 右旋转16位整数b位
 * @param[in] v 16位整数
 * @param[in] b 旋转量
 * @return uint16_t 返回旋转后的整数
 * @see __rol16
 */
static inline uint16_t __ror16(uint16_t v, int b)
{
	return __ror16_c(v, b);
}

/**
 * @fn static uint32_t __rol32(uint32_t v, int b);
 * @brief 左旋转32位整数b位
 * @param[in] v 32位整数
 * @param[in] b 旋转量
 * @return uint32_t 返回旋转后的整数
 * @see __ror32
 */
static inline uint32_t __rol32(uint32_t v, int b)
{
	return __rol32_c(v, b);
}

/**
 * @fn static uint32_t __ror32(uint32_t v, int b);
 * @brief 右旋转32位整数b位
 * @param[in] v 32位整数
 * @param[in] b 旋转量
 * @return uint32_t 返回旋转后的整数
 * @see __rol32
 */
static inline uint32_t __ror32(uint32_t v, int b)
{
	return __ror32_c(v, b);
}

/**
 * @fn static uint16_t __rol16_p(void* p, int b);
 * @brief 左旋转指定内存地址的16位整数b位，但不存回p
 * @param[in] p 指向16位整数的指针
 * @param[in] b 旋转量
 * @return uint16_t 返回旋转后的整数
 * @see __ror16_p
 */
static inline uint16_t __rol16_p(void* p, int b)
{
	return __rol16(*((uint16_t*)p), b);
}

/**
 * @fn static uint16_t __ror16_p(void* p, int b);
 * @brief 右旋转指定内存地址的16位整数b位，但不存回p
 * @param[in] p 指向16位整数的指针
 * @param[in] b 旋转量
 * @return uint16_t 返回旋转后的整数
 * @see __rol16_p
 */
static inline uint16_t __ror16_p(void* p, int b)
{
	return __ror16(*((uint16_t*)p), b);
}

/**
 * @fn static uint32_t __rol32_p(void* p, int b);
 * @brief 左旋转指定内存地址的32位整数b位，但不存回p
 * @param[in] p 指向32位整数的指针
 * @param[in] b 旋转量
 * @return uint32_t 返回旋转后的整数
 * @see __ror32_p
 */
static inline uint32_t __rol32_p(void* p, int b)
{
	return __rol32(*((uint32_t*)p), b);
}

/**
 * @fn static uint32_t __ror32_p(void* p, int b);
 * @brief 右旋转指定内存地址的32位整数b位，但不存回p
 * @param[in] p 指向32位整数的指针
 * @param[in] b 旋转量
 * @return uint32_t 返回旋转后的整数
 * @see __rol32_p
 */
static inline uint32_t __ror32_p(void* p, int b)
{
	return __ror32(*((uint32_t*)p), b);
}

/**
 * @fn static void __rol16_s(void* p, int b);
 * @brief 左旋转指定内存地址的16位整数b位，并存回p
 * @param[in] p 指向16位整数的指针
 * @param[in] b 旋转量
 * @see __ror16_s
 */
static inline void __rol16_s(void* p, int b)
{
	*((uint16_t*)p) = __rol16(*((uint16_t*)p), b);
}

/**
 * @fn static void __ror16_s(void* p, int b);
 * @brief 右旋转指定内存地址的16位整数b位，并存回p
 * @param[in] p 指向16位整数的指针
 * @param[in] b 旋转量
 * @see __rol16_s
 */
static inline void __ror16_s(void* p, int b)
{
	*((uint16_t*)p) = __ror16(*((uint16_t*)p), b);
}

/**
 * @fn static void __rol32_s(void* p, int b);
 * @brief 左旋转指定内存地址的32位整数b位，并存回p
 * @param[in] p 指向32位整数的指针
 * @param[in] b 旋转量
 * @see __ror32_s
 */
static inline void __rol32_s(void* p, int b)
{
	*((uint32_t*)p) = __rol32(*((uint32_t*)p), b);
}

/**
 * @fn static void __ror32_s(void* p, int b);
 * @brief 右旋转指定内存地址的32位整数b位，并存回p
 * @param[in] p 指向32位整数的指针
 * @param[in] b 旋转量
 * @see __rol32_s
 */
static inline void __ror32_s(void* p, int b)
{
	*((uint32_t*)p) = __ror32(*((uint32_t*)p), b);
}

#ifdef INT64_SUPPORT

/**
 * @def __bswap64_c(c)
 * @brief 64位常量字序反转
 * @param c 八字节常量
 */
#define __bswap64_c(c) ((uint64_t)(				\
	(((c) & UINT64_C(0x00000000000000FF)) << 56) | \
	(((c) & UINT64_C(0x000000000000FF00)) << 40) | \
	(((c) & UINT64_C(0x0000000000FF0000)) << 24) | \
	(((c) & UINT64_C(0x00000000FF000000)) <<  8) | \
	(((c) & UINT64_C(0x000000FF00000000)) >>  8) | \
	(((c) & UINT64_C(0x0000FF0000000000)) >> 24) | \
	(((c) & UINT64_C(0x00FF000000000000)) >> 40) | \
	(((c) & UINT64_C(0xFF00000000000000)) >> 56)))

/**
 * @fn static inline uint64_t __bswap64(uint64_t x);
 * @brief 反转64位整数
 * @param[in] x 原整数
 * @return uint64_t 返回反转后的64位整数
 */
static inline uint64_t __bswap64(uint64_t x)
{
#ifdef __GNUC__
#if defined(NV_ARCH_X64) || defined(NV_ARCH_IA64)
	__asm__("bswapq %0" : "=r" (x) : "0" (x));
	return x;
#else
	return __builtin_bswap64(x);
#endif /* #if defined(NV_ARCH_X64) || defined(NV_ARCH_IA64) */
#else
	return __bswap64_c(x);
#endif /* #if defined(__GNUC__) && (...... */
}

/**
 * @fn static inline uint64_t __bswap64_p(void* p);
 * @brief 反转64位整数
 * @param[in] p 64位整数指针
 * @return uint64_t 返回反转后的64位整数
 */
static inline uint64_t __bswap64_p(void* p)
{
	return __bswap64(*((uint64_t*)p));
}

/**
 * @fn static inline void __bswap64_s(void* p);
 * @brief 反转64位整数，并存回内存
 * @param[in,out] p 64位整数指针
 */
static inline void __bswap64_s(void* p)
{
	*((uint64_t*)p) = __bswap64(*((uint64_t*)p));
}

/**
 * @def __bswap64_array(b, c)
 * @brief 逐个反转64位整数数组的元素，并存回内存
 * @param[in,out] b 数组地址
 * @param[in] c 数组长度
 */
#define __bswap64_array(b, c) do {			\
		uint64_t* _sw_b = (uint64_t*)(b);	\
		size_t _sw_c = (c);					\
		while (_sw_c--) {					\
			__bswap64_s(_sw_b++);			\
		}									\
	} while (0)

/**
 * @def __rol64_c(c, b)
 * @brief 左旋转64位整型常量
 * @param c 整型常量
 * @param b 旋转量
 * @def __ror64_c(c, b)
 * @brief 右旋转64位整型常量
 * @param c 整型常量
 * @param b 旋转量
 */
#define __rol64_c(c, b) (((uint64_t)(c) << (b)) | ((uint64_t)(c) >> (64 - (b))))
#define __ror64_c(c, b) (((uint64_t)(c) >> (b)) | ((uint64_t)(c) << (64 - (b))))

/**
 * @fn static inline uint64_t __rol64(uint64_t v, int b);
 * @brief 左旋转64位整数b位
 * @param[in] v 64位整数
 * @param[in] b 旋转量
 * @see __ror64
 */
static inline uint64_t __rol64(uint64_t v, int b)
{
	return __rol64_c(v, b);
}

/**
 * @fn static inline uint64_t __ror64(uint64_t v, int b);
 * @brief 右旋转64位整数b位
 * @param[in] v 64位整数
 * @param[in] b 旋转量
 * @see __rol64
 */
static inline uint64_t __ror64(uint64_t v, int b)
{
	return __rol64_c(v, b);
}


/**
 * @fn static uint64_t __rol64_p(void* p, int b);
 * @brief 左旋转指定内存地址的64位整数b位，但不存回p
 * @param[in] p 指向64位整数的指针
 * @param[in] b 旋转量
 * @return uint64_t 返回旋转后的整数
 * @see __ror64_p
 */
static inline uint64_t __rol64_p(void* p, int b)
{
	return __rol64(*((uint64_t*)p), b);
}

/**
 * @fn static uint64_t __ror64_p(void* p, int b);
 * @brief 右旋转指定内存地址的64位整数b位，但不存回p
 * @param[in] p 指向64位整数的指针
 * @param[in] b 旋转量
 * @return uint64_t 返回旋转后的整数
 * @see __rol64_p
 */
static inline uint64_t __ror64_p(void* p, int b)
{
	return __ror64(*((uint64_t*)p), b);
}

/**
 * @fn static void __rol64_s(void* p, int b);
 * @brief 左旋转指定内存地址的64位整数b位，并存回p
 * @param[in] p 指向64位整数的指针
 * @param[in] b 旋转量
 * @see __ror64_s
 */
static inline void __rol64_s(void* p, int b)
{
	*((uint64_t*)p) = __rol64(*((uint64_t*)p), b);
}

/**
 * @fn static void __ror64_s(void* p, int b);
 * @brief 右旋转指定内存地址的64位整数b位，并存回p
 * @param[in] p 指向64位整数的指针
 * @param[in] b 旋转量
 * @see __rol64_s
 */
static inline void __ror64_s(void* p, int b)
{
	*((uint64_t*)p) = __ror64(*((uint64_t*)p), b);
}

#endif /* #ifdef INT64_SUPPORT */

/** @} */

#endif /* #ifndef __NV_SWAB__ */


