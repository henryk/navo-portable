/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file port/bits.h
 *
 * @brief Definitions for bit operations.
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_BITS__
#define __NV_BITS__

#include "cdef.h"
#include "integer.h"

/* Microsoft intrinsic functions */
#if !defined(__GNUC__) && defined(_MSC_VER) && _MSC_VER >= 1400
#ifdef INT64_SUPPORT
#pragma intrinsic(_BitScanForward, _BitScanForward64)
#pragma intrinsic(_BitScanReverse, _BitScanReverse64)
#else
#pragma intrinsic(_BitScanForward, _BitScanReverse)
#endif /* #ifdef INT64_SUPPORT */
#if _MSC_VER >= 1500
#ifdef INT64_SUPPORT
#pragma intrinsic(__lzcnt, __lzcnt64)
#else
#pragma intrinsic(__lzcnt)
#endif /* #ifdef INT64_SUPPORT */
#endif /* #if _MSC_VER >= 1500 */
#endif /* #if !defined(__GNUC__) && defined(_MSC_VER) && _MSC_VER >= 1400 */

#ifdef __cplusplus
extern "C" {
#endif /* #ifdef __cplusplus */

/**
 * @defgroup 位操作
 * @ingroup 数据类型
 * @{
 */

#define __BIT8C_L0 	0x01
#define __BIT8C_L1 	0x02
#define __BIT8C_L2 	0x04
#define __BIT8C_L3 	0x08
#define __BIT8C_L4 	0x10
#define __BIT8C_L5 	0x20
#define __BIT8C_L6 	0x40
#define __BIT8C_L7 	0x80

#define __BIT16C_L0 	0x0001
#define __BIT16C_L1 	0x0002
#define __BIT16C_L2 	0x0004
#define __BIT16C_L3 	0x0008
#define __BIT16C_L4 	0x0010
#define __BIT16C_L5 	0x0020
#define __BIT16C_L6 	0x0040
#define __BIT16C_L7 	0x0080
#define __BIT16C_L8 	0x0100
#define __BIT16C_L9 	0x0200
#define __BIT16C_L10 	0x0400
#define __BIT16C_L11 	0x0800
#define __BIT16C_L12 	0x1000
#define __BIT16C_L13 	0x2000
#define __BIT16C_L14 	0x4000
#define __BIT16C_L15 	0x8000

#define __BIT32C_L0 	0x00000001UL
#define __BIT32C_L1 	0x00000002UL
#define __BIT32C_L2 	0x00000004UL
#define __BIT32C_L3 	0x00000008UL
#define __BIT32C_L4 	0x00000010UL
#define __BIT32C_L5 	0x00000020UL
#define __BIT32C_L6 	0x00000040UL
#define __BIT32C_L7 	0x00000080UL
#define __BIT32C_L8 	0x00000100UL
#define __BIT32C_L9 	0x00000200UL
#define __BIT32C_L10 	0x00000400UL
#define __BIT32C_L11 	0x00000800UL
#define __BIT32C_L12 	0x00001000UL
#define __BIT32C_L13 	0x00002000UL
#define __BIT32C_L14 	0x00004000UL
#define __BIT32C_L15 	0x00008000UL
#define __BIT32C_L16 	0x00010000UL
#define __BIT32C_L17 	0x00020000UL
#define __BIT32C_L18 	0x00040000UL
#define __BIT32C_L19 	0x00080000UL
#define __BIT32C_L20 	0x00100000UL
#define __BIT32C_L21 	0x00200000UL
#define __BIT32C_L22 	0x00400000UL
#define __BIT32C_L23 	0x00800000UL
#define __BIT32C_L24 	0x01000000UL
#define __BIT32C_L25 	0x02000000UL
#define __BIT32C_L26 	0x04000000UL
#define __BIT32C_L27 	0x08000000UL
#define __BIT32C_L28 	0x10000000UL
#define __BIT32C_L29 	0x20000000UL
#define __BIT32C_L30 	0x40000000UL
#define __BIT32C_L31 	0x80000000UL

/**
 * @def __bitmask_c(b, i)
 * @brief b位整数的第i位掩码
 * @param[in] b 整数位数，支持8、16、32、64四种
 * @param[in] i 指定低位起第几位，从0开始
 * @return 返回对应的掩码
 */
#define __bitmask_c(b, i) make_id4(__BIT, b, C_L, i)

/**
 * @def __getb_c(b, x, i)
 * @brief 获取b位整数x的低位起第i位
 * @param[in] b 整数的位数，支持8、16、32、64四种
 * @param[in] x 该整数
 * @param[in] i 指定低位起第几位，从0开始
 * @return 返回结果
 * @retval 1 比特1
 * @retval 0 比特0
 * @par 示例:
 * @code
 	uint32_t bs;
 	......
 	// 判断32位整数bs的低位第6位是否为1
 	if (__getb_c(32, bs, 6)) {
 		doSomething();
 	}
 * @endcode
 * @see __setb_c, __clrb_c
 */
#define __getb_c(b, x, i) \
	(!!(((UINT_TYPE(b))(x)) & __bitmask_c(b, i)))

/**
 * @def __setb_c(b, x, i)
 * @brief 修改b位整数x的低位起第i位为1，但不存回x
 * @param[in] b 整数的位数，支持8、16、32、64四种
 * @param[in] x 该整数
 * @param[in] i 指定低位起第几位，从0开始
 * @return 返回修改后的32位整数
 * @par 示例:
 * @code
 	uint32_t bs;
 	......
 	// 判断32位整数bs的低位第8位是否为1
 	if (__getb_c(32, bs, 8)) {
 		// 设置bs的第11位为1
 		bs = __setb_c(32, bs, 11);
 	}
 * @endcode
 * @see __getb_c, __clrb_c
 */
#define __setb_c(b, x, i) \
	(((UINT_TYPE(b))(x)) | __bitmask_c(b, i)))

/**
 * @def __clrb_c(b, x, i)
 * @brief 修改b位整数x的低位起第i位为1，但不存回x
 * @param[in] b 整数的位数，支持8、16、32、64四种
 * @param[in] x 该整数
 * @param[in] i 指定低位起第几位，从0开始
 * @return 返回修改后的32位整数
 * @par 示例:
 * @code
 	uint32_t bs;
 	......
 	// 判断32位整数bs的低位第4位是否为1
 	if (__getb_c(32, bs, 4)) {
 		// 设置bs的第9位为0
 		bs = __clrb_c(32, bs, 9);
 	}
 * @endcode
 * @see __getb_c, __setb_c
 */
#define __clrb_c(b, x, i) \
	(((UINT_TYPE(b))(x)) & (~ __bitmask_c(b, i))))

/**
 * @def __setb_cr(b, x, i)
 * @brief 修改b位整数x的低位起第i位为1，并存回x
 * @param[in] b 整数的位数，支持8、16、32、64四种
 * @param[in] x 该整数
 * @param[in] i 指定低位起第几位，从0开始
 * @return 返回修改后的32位整数
 * @par 示例:
 * @code
 	uint32_t bs;
 	......
 	// 判断32位整数bs的低位第8位是否为1
 	if (__getb_c(32, bs, 8)) {
 		// 设置bs的第11位为1
 		__setb_cr(32, bs, 11);
 	}
 * @endcode
 * @see __getb_c, __clrb_cr
 */
#define __setb_cr(b, x, i) \
	(((UINT_TYPE(b))(x)) |= __bitmask_c(b, i)))

/**
 * @def __clrb_cr(b, x, i)
 * @brief 修改b位整数x的低位起第i位为1，并存回x
 * @param[in] b 整数的位数，支持8、16、32、64四种
 * @param[in] x 该整数
 * @param[in] i 指定低位起第几位，从0开始
 * @return 返回修改后的32位整数
 * @par 示例:
 * @code
 	uint32_t bs;
 	......
 	// 判断32位整数bs的低位第4位是否为1
 	if (__getb_c(32, bs, 4)) {
 		// 设置bs的第9位为0
 		__clrb_cr(32, bs, 9);
 	}
 * @endcode
 * @see __getb_c, __setb_cr
 */
#define __clrb_cr(b, x, i) \
	(((UINT_TYPE(b))(x)) &= (~ __bitmask_c(b, i))))

/**
 * @def __getb_cp(b, p, i)
 * @brief 获取指定内存地址为p的b位整数的低位起第i位
 * @param[in] b 整数的位数，支持8、16、32、64四种
 * @param[in] p 该整数的内存地址
 * @param[in] i 指定低位起第几位，从0开始
 * @return 返回结果
 * @retval 1 比特1
 * @retval 0 比特0
 * @par 示例:
 * @code
 	uint32_t* pb = ....;
 	......
 	// 判断32位整数*pb的低位第6位是否为1
 	if (__getb_cp(32, pb, 6)) {
 		doSomething();
 	}
 * @endcode
 * @see __setb_cp, __clrb_cp
 */
#define __getb_cp(b, p, i) \
	(!!((*(UINT_TYPE(b) *)(p)) & __bitmask_c(b, i)))

/**
 * @def __setb_cp(b, p, i)
 * @brief 修改指定内存地址为p的b位整数的低位起第i位为1，但不存回p
 * @param[in] b 整数的位数，支持8、16、32、64四种
 * @param[in] p 该整数的内存地址
 * @param[in] i 指定低位起第几位，从0开始
 * @return 返回修改后的32位整数
 * @par 示例:
 * @code
 	uint32_t* pb = ....;
 	......
 	// 判断32位整数*pb的低位第8位是否为1
 	if (__getb_cp(32, pb, 8)) {
 		// 设置bs的第11位为1
 		bs = __setb_cp(32, pb, 11);
 	}
 * @endcode
 * @see __getb_cp, __clrb_cp
 */
#define __setb_cp(b, p, i) \
	((*(UINT_TYPE(b) *)(p)) | __bitmask_c(b, i)))

/**
 * @def __clrb_cp(b, p, i)
 * @brief 修改指定内存地址为p的b位整数的低位起第i位为1，但不存回p
 * @param[in] b 整数的位数，支持8、16、32、64四种
 * @param[in] p 该整数的内存地址
 * @param[in] i 指定低位起第几位，从0开始
 * @return 返回修改后的32位整数
 * @par 示例:
 * @code
 	uint32_t* pb = ....;
 	......
 	// 判断32位整数*pb的低位第4位是否为1
 	if (__getb_cp(32, pb, 4)) {
 		// 设置bs的第9位为0
 		bs = __clrb_cp(32, pb, 9);
 	}
 * @endcode
 * @see __getb_cp, __setb_cp
 */
#define __clrb_cp(b, p, i) \
	((*(UINT_TYPE(b) *)(p)) & (~ __bitmask_c(b, i))))

/**
 * @def __setb_cs(b, p, i)
 * @brief 修改指定内存地址为p的b位整数的低位起第i位为1，并存回p
 * @param[in] b 整数的位数，支持8、16、32、64四种
 * @param[in] p 该整数的内存地址
 * @param[in] i 指定低位起第几位，从0开始
 * @return 返回修改后的32位整数
 * @par 示例:
 * @code
 	uint32_t* pb = ....;
 	......
 	// 判断32位整数*pb的低位第8位是否为1
 	if (__getb_cp(32, pb, 8)) {
 		// 设置bs的第11位为1
 		__setb_cs(32, pb, 11);
 	}
 * @endcode
 * @see __getb_cp, __clrb_cs
 */
#define __setb_cs(b, p, i) \
	(*((UINT_TYPE(b) *)(p)) |= __bitmask_c(b, i)))

/**
 * @def __clrb_cs(b, p, i)
 * @brief 修改指定内存地址为p的b位整数的低位起第i位为1，并存回p
 * @param[in] b 整数的位数，支持8、16、32、64四种
 * @param[in] p 该整数的内存地址
 * @param[in] i 指定低位起第几位，从0开始
 * @return 返回修改后的32位整数
 * @par 示例:
 * @code
 	uint32_t* pb = ....;
 	......
 	// 判断32位整数*pb的低位第4位是否为1
 	if (__getb_cp(32, pb, 4)) {
 		// 设置bs的第9位为0
 		__clrb_cs(32, pb, 9);
 	}
 * @endcode
 * @see __getb_cp, __setb_cs
 */
#define __clrb_cs(b, p, i) \
	(*((UINT_TYPE(b) *)(p)) &= (~ __bitmask_c(b, i))))

/**
 * @fn static inline int __getb8(uint8_t x, int i);
 * @brief 获取8位整数低位起第p位的值
 * @param[in] x 8位整数
 * @param[in] i 位置，从低位开始的位置
 * @return int 返回低位起第p位的值
 * @retval 1 比特1
 * @retval 0 比特0
 */
static inline int __getb8(uint8_t x, int i)
{
	return (int)((x >> i) & 1);
}

/**
 * @fn static inline int __getb16(uint16_t x, int i);
 * @brief 获取16位整数低位起第p位的值
 * @param[in] x 16位整数
 * @param[in] i 位置，从低位开始的位置
 * @return int 返回低位起第p位的值
 * @retval 1 比特1
 * @retval 0 比特0
 */
static inline int __getb16(uint16_t x, int i)
{
	return (int)((x >> i) & 1);
}

/**
 * @fn static inline int __getb32(uint32_t x, int i);
 * @brief 获取32位整数低位起第p位的值
 * @param[in] x 32位整数
 * @param[in] i 位置，从低位开始的位置
 * @return int 返回低位起第p位的值
 * @retval 1 比特1
 * @retval 0 比特0
 */
static inline int __getb32(uint32_t x, int i)
{
	return (int)((x >> i) & 1UL);
}

/**
 * @fn static inline int __getb8_p(void* p, int i);
 * @brief 获取8位整数低位起第p位的值
 * @param[in] p 8位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 * @return int 返回低位起第p位的值
 * @retval 1 比特1
 * @retval 0 比特0
 */
static inline int __getb8_p(void* p, int i)
{
	return (int)((*((uint8_t*)p) >> i) & 1);
}

/**
 * @fn static inline int __getb16_p(void* p, int i);
 * @brief 获取16位整数低位起第p位的值
 * @param[in] p 16位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 * @return int 返回低位起第p位的值
 * @retval 1 比特1
 * @retval 0 比特0
 */
static inline int __getb16_p(void* p, int i)
{
	return (int)((*((uint16_t*)p) >> i) & 1);
}

/**
 * @fn static inline int __getb32_p(void* p, int i);
 * @brief 获取32位整数低位起第p位的值
 * @param[in] p 32位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 * @return int 返回低位起第p位的值
 * @retval 1 比特1
 * @retval 0 比特0
 */
static inline int __getb32_p(void* p, int i)
{
	return (int)((*((uint32_t*)p) >> i) & 1UL);
}

/**
 * @fn static inline uint8_t __setb8(uint8_t x, int i);
 * @brief 修改8位整数低位起第p位为1
 * @param[in] x 8位整数
 * @param[in] i 位置，从低位开始的位置
 * @return uint8_t 返回修改后的8位整数
 */
static inline uint8_t __setb8(uint8_t x, int i)
{
	return x | (1 << i);
}

/**
 * @fn static inline uint16_t __setb16(uint16_t x, int i);
 * @brief 修改16位整数低位起第p位为1
 * @param[in] x 16位整数
 * @param[in] i 位置，从低位开始的位置
 * @return uint16_t 返回修改后的16位整数
 */
static inline uint16_t __setb16(uint16_t x, int i)
{
	return x | (1 << i);
}

/**
 * @fn static inline uint32_t __setb32(uint32_t x, int i);
 * @brief 修改32位整数低位起第p位为1
 * @param[in] x 32位整数
 * @param[in] i 位置，从低位开始的位置
 * @return uint32_t 返回修改后的32位整数
 */
static inline uint32_t __setb32(uint32_t x, int i)
{
	return x | (1UL << i);
}

/**
 * @fn static inline uint8_t __setb8_p(void* p, int i);
 * @brief 修改指定内存地址的8位整数低位起第p位为1，结果不存回原址
 * @param[in] p 8位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 * @return uint8_t 返回修改后的8位整数
 */
static inline uint8_t __setb8_p(void* p, int i)
{
	return *((uint8_t*)p) | (1 << i);
}

/**
 * @fn static inline uint16_t __setb16_p(void* p, int i);
 * @brief 修改指定内存地址的16位整数低位起第p位为1，结果不存回原址
 * @param[in] p 16位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 * @return uint16_t 返回修改后的16位整数
 */
static inline uint16_t __setb16_p(void* p, int i)
{
	return *((uint16_t*)p) | (1 << i);
}

/**
 * @fn static inline uint32_t __setb32_p(void* p, int i);
 * @brief 修改指定内存地址的32位整数低位起第p位为1，结果不存回原址
 * @param[in] p 32位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 * @return uint32_t 返回修改后的32位整数
 */
static inline uint32_t __setb32_p(void* p, int i)
{
	return *((uint32_t*)p) | (1UL << i);
}

/**
 * @fn static inline void __setb8_s(void* p, int i);
 * @brief 修改指定内存地址的8位整数低位起第p位为1，并将结果不存回原址
 * @param[in] p 8位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 */
static inline void __setb8_s(void* p, int i)
{
	*((uint8_t*)p) |= (1 << i);
}

/**
 * @fn static inline uint16_t __setb16_s(void* p, int i);
 * @brief 修改指定内存地址的16位整数低位起第p位为1，并将结果不存回原址
 * @param[in] p 16位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 */
static inline void __setb16_s(void* p, int i)
{
	*((uint16_t*)p) |= (1 << i);
}

/**
 * @fn static inline uint32_t __setb32_s(void* p, int i);
 * @brief 修改指定内存地址的32位整数低位起第p位为1，并将结果存回原址
 * @param[in] p 32位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 */
static inline void __setb32_s(void* p, int i)
{
	*((uint32_t*)p) |= (1UL << i);
}

/**
 * @fn static inline uint8_t __clrb8(uint8_t x, int i);
 * @brief 修改8位整数低位起第p位为0
 * @param[in] x 8位整数
 * @param[in] i 位置，从低位开始的位置
 * @return uint8_t 返回修改后的8位整数
 */
static inline uint8_t __clrb8(uint8_t x, int i)
{
	return x & (~(1 << i));
}

/**
 * @fn static inline uint16_t __clrb16(uint16_t x, int i);
 * @brief 修改16位整数低位起第p位为0
 * @param[in] x 16位整数
 * @param[in] i 位置，从低位开始的位置
 * @return uint16_t 返回修改后的16位整数
 */
static inline uint16_t __clrb16(uint16_t x, int i)
{
	return x & (~(1 << i));
}

/**
 * @fn static inline uint32_t __clrb32(uint32_t x, int i);
 * @brief 修改32位整数低位起第p位为0
 * @param[in] x 32位整数
 * @param[in] i 位置，从低位开始的位置
 * @return uint32_t 返回修改后的32位整数
 */
static inline uint32_t __clrb32(uint32_t x, int i)
{
	return x & (~(1UL << i));
}

/**
 * @fn static inline uint8_t __clrb8_p(void* p, int i);
 * @brief 修改指定内存地址的8位整数低位起第p位为0，结果不存回原址
 * @param[in] p 8位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 * @return uint8_t 返回修改后的8位整数
 */
static inline uint8_t __clrb8_p(void* p, int i)
{
	return *((uint8_t*)p) & (~(1 << i));
}

/**
 * @fn static inline uint16_t __clrb16_p(void* p, int i);
 * @brief 修改指定内存地址的16位整数低位起第p位为0，结果不存回原址
 * @param[in] p 16位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 * @return uint16_t 返回修改后的16位整数
 */
static inline uint16_t __clrb16_p(void* p, int i)
{
	return *((uint16_t*)p) & (~(1 << i));
}

/**
 * @fn static inline uint32_t __clrb32_p(void* p, int i);
 * @brief 修改指定内存地址的32位整数低位起第p位为0，结果不存回原址
 * @param[in] p 32位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 * @return uint32_t 返回修改后的32位整数
 */
static inline uint32_t __clrb32_p(void* p, int i)
{
	return *((uint32_t*)p) & (~(1UL << i));
}

/**
 * @fn static inline void __clrb8_s(void* p, int i);
 * @brief 修改指定内存地址的8位整数低位起第p位为0，并将结果存回原址
 * @param[in] p 8位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 */
static inline void __clrb8_s(void* p, int i)
{
	*((uint8_t*)p) &= (~(1 << i));
}

/**
 * @fn static inline void __clrb16_s(void* p, int i);
 * @brief 修改指定内存地址的16位整数低位起第p位为0，并将结果存回原址
 * @param[in] p 16位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 */
static inline void __clrb16_s(void* p, int i)
{
	*((uint16_t*)p) &= (~(1 << i));
}

/**
 * @fn static inline void __clrb32_s(void* p, int i);
 * @brief 修改指定内存地址的32位整数低位起第p位为0，并将结果存回原址
 * @param[in] p 32位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 */
static inline void __clrb32_s(void* p, int i)
{
	*((uint32_t*)p) &= (~(1UL << i));
}

/**
 * @def __to_gray_c(x)
 * @brief 自然码常量转格雷码常量
 * @param[in] x 自然码
 */
#define __to_gray_c(x) ((x) ^ ((x) >> 1))

/**
 * @fn static inline uint8_t __to_gray8(uint8_t x);
 * @brief 8位自然码转格雷码
 * @param[in] x 自然码
 * @return uint8_t 返回8位格雷码
 */
static inline uint8_t __to_gray8(uint8_t x)
{
	return __to_gray_c(x);
}

/**
 * @fn static inline uint8_t __to_gray8_p(void* p);
 * @brief 将指定内存地址的8位自然码转格雷码，但不存回p
 * @param[in] p 8位自然码内存地址
 * @return uint8_t 返回8位格雷码
 */
static inline uint8_t __to_gray8_p(void* p)
{
	return __to_gray8(*((uint8_t*)p));
}

/**
 * @fn static inline void __to_gray8_s(void* p);
 * @brief 将指定内存地址的8位自然码转格雷码，并存回p
 * @param[in] p 8位自然码内存地址
 */
static inline void __to_gray8_s(void* p)
{
	*((uint8_t*)p) = __to_gray8(*((uint8_t*)p));
}

/**
 * @fn static inline uint8_t __to_natural8(uint8_t x);
 * @brief 8位格雷码转自然码
 * @param[in] x 格雷码
 * @return uint8_t 返回8位自然码
 */
static inline uint8_t __to_natural8(uint8_t x)
{
	x = x ^ (x >> 1);
	x = x ^ (x >> 2);
	x = x ^ (x >> 4);
	return x;
}

/**
 * @fn static inline uint8_t __to_natural8_p(void* p);
 * @brief 将指定内存地址的8位格雷码转自然码，但不存回p
 * @param[in] p 8位格雷码内存地址
 * @return uint8_t 返回8位自然码
 */
static inline uint8_t __to_natural8_p(void* p)
{
	return __to_natural8(*((uint8_t*)p));
}

/**
 * @fn static inline void __to_natural8_s(void* p);
 * @brief 将指定内存地址的8位格雷码转自然码，并存回p
 * @param[in] p 8位格雷码内存地址
 */
static inline void __to_natural8_s(void* p)
{
	*((uint8_t*)p) = __to_natural8(*((uint8_t*)p));
}

/**
 * @fn static inline uint16_t __to_gray16(uint16_t x);
 * @brief 16位自然码转格雷码
 * @param[in] x 自然码
 * @return uint16_t 返回16位格雷码
 */
static inline uint16_t __to_gray16(uint16_t x)
{
	return __to_gray_c(x);
}

/**
 * @fn static inline uint16_t __to_gray16_p(void* p);
 * @brief 将指定内存地址的16位自然码转格雷码，但不存回p
 * @param[in] p 16位自然码内存地址
 * @return uint16_t 返回16位格雷码
 */
static inline uint16_t __to_gray16_p(void* p)
{
	return __to_gray16(*((uint16_t*)p));
}

/**
 * @fn static inline void __to_gray16_s(void* p);
 * @brief 将指定内存地址的16位自然码转格雷码，并存回p
 * @param[in] p 16位自然码内存地址
 */
static inline void __to_gray16_s(void* p)
{
	*((uint16_t*)p) = __to_gray16(*((uint16_t*)p));
}

/**
 * @fn static inline uint16_t __to_natural16(uint16_t x);
 * @brief 16位格雷码转自然码
 * @param[in] x 格雷码
 * @return uint16_t 返回16位自然码
 */
static inline uint16_t __to_natural16(uint16_t x)
{
	x = x ^ (x >> 1);
	x = x ^ (x >> 2);
	x = x ^ (x >> 4);
	x = x ^ (x >> 8);
	return x;
}

/**
 * @fn static inline uint16_t __to_natural16_p(void* p);
 * @brief 将指定内存地址的16位格雷码转自然码，但不存回p
 * @param[in] p 16位格雷码内存地址
 * @return uint16_t 返回16位自然码
 */
static inline uint16_t __to_natural16_p(void* p)
{
	return __to_natural16(*((uint16_t*)p));
}

/**
 * @fn static inline void __to_natural16_s(void* p);
 * @brief 将指定内存地址的16位格雷码转自然码，并存回p
 * @param[in] p 16位格雷码内存地址
 */
static inline void __to_natural16_s(void* p)
{
	*((uint16_t*)p) = __to_natural16(*((uint16_t*)p));
}

/**
 * @fn static inline uint32_t __to_gray32(uint32_t x);
 * @brief 32位自然码转格雷码
 * @param[in] x 自然码
 * @return uint32_t 返回32位格雷码
 */
static inline uint32_t __to_gray32(uint32_t x)
{
	return __to_gray_c(x);
}

/**
 * @fn static inline uint32_t __to_gray32_p(void* p);
 * @brief 将指定内存地址的32位自然码转格雷码，但不存回p
 * @param[in] p 32位自然码内存地址
 * @return uint32_t 返回32位格雷码
 */
static inline uint32_t __to_gray32_p(void* p)
{
	return __to_gray32(*((uint32_t*)p));
}

/**
 * @fn static inline void __to_gray32_s(void* p);
 * @brief 将指定内存地址的32位自然码转格雷码，并存回p
 * @param[in] p 32位自然码内存地址
 */
static inline void __to_gray32_s(void* p)
{
	*((uint32_t*)p) = __to_gray32(*((uint32_t*)p));
}

/**
 * @fn static inline uint32_t __to_natural32(uint32_t x);
 * @brief 32位格雷码转自然码
 * @param[in] x 格雷码
 * @return uint32_t 返回32位自然码
 */
static inline uint32_t __to_natural32(uint32_t x)
{
	x = x ^ (x >>  1);
	x = x ^ (x >>  2);
	x = x ^ (x >>  4);
	x = x ^ (x >>  8);
	x = x ^ (x >> 16);
	return x;
}

/**
 * @fn static inline uint32_t __to_natural32_p(void* p);
 * @brief 将指定内存地址的32位格雷码转自然码，但不存回p
 * @param[in] p 32位格雷码内存地址
 * @return uint32_t 返回32位自然码
 */
static inline uint32_t __to_natural32_p(void* p)
{
	return __to_natural32(*((uint32_t*)p));
}

/**
 * @fn static inline void __to_natural32_s(void* p);
 * @brief 将指定内存地址的32位格雷码转自然码，并存回p
 * @param[in] p 32位格雷码内存地址
 */
static inline void __to_natural32_s(void* p)
{
	*((uint32_t*)p) = __to_natural32(*((uint32_t*)p));
}

/**
 * @fn static inline uint8_t __flip8(uint8_t x);
 * @brief 按位反转8位整数
 * @param[in] x 8位整数
 * @return uint8_t 返回反转结果
 */
static inline uint8_t __flip8(uint8_t x)
{
	x = ((x >> 1) & 0x55) | ((x & 0x55) << 1);
	x = ((x >> 2) & 0x33) | ((x & 0x33) << 2);
	x = ((x >> 4) & 0x0F) | ((x & 0x0F) << 4);
	return x;
}

/**
 * @fn static inline uint16_t __flip16(uint16_t x);
 * @brief 按位反转16位整数
 * @param[in] x 16位整数
 * @return uint16_t 返回反转结果
 */
static inline uint16_t __flip16(uint16_t x)
{
	x = ((x >> 1) & 0x5555) | ((x & 0x5555) << 1);
	x = ((x >> 2) & 0x3333) | ((x & 0x3333) << 2);
	x = ((x >> 4) & 0x0F0F) | ((x & 0x0F0F) << 4);
	x = ((x >> 8) & 0x00FF) | ((x & 0x00FF) << 8);
	return x;
}

/**
 * @fn static inline uint32_t __flip32(uint32_t x);
 * @brief 按位反转32位整数
 * @param[in] x 32位整数
 * @return uint32_t 返回反转结果
 */
static inline uint32_t __flip32(uint32_t x)
{
	x = ((x >>  1) & 0x55555555UL) | ((x & 0x55555555UL) <<  1);
	x = ((x >>  2) & 0x33333333UL) | ((x & 0x33333333UL) <<  2);
	x = ((x >>  4) & 0x0F0F0F0FUL) | ((x & 0x0F0F0F0FUL) <<  4);
	x = ((x >>  8) & 0x00FF00FFUL) | ((x & 0x00FF00FFUL) <<  8);
	x = ((x >> 16)               ) | ((x               ) << 16);
	return x;
}

/**
 * @fn static inline uint8_t __flip8_p(void* p);
 * @brief 指定地址的8位整数按位反转，不存回原址
 * @param[in] p 8位整数内存地址
 * @return uint8_t 返回反转结果
 */
static inline uint8_t __flip8_p(void* p)
{
	return __flip8(*((uint8_t*)p));
}

/**
 * @fn static inline uint16_t __flip16_p(void* p);
 * @brief 指定地址的16位整数按位反转，不存回原址
 * @param[in] p 16位整数内存地址
 * @return uint16_t 返回反转结果
 */
static inline uint16_t __flip16_p(void* p)
{
	return __flip16(*((uint16_t*)p));
}

/**
 * @fn static inline uint32_t __flip32_p(void* p);
 * @brief 指定地址的32位整数按位反转，不存回原址
 * @param[in] p 32位整数内存地址
 * @return uint32_t 返回反转结果
 */
static inline uint32_t __flip32_p(void* p)
{
	return __flip32(*((uint32_t*)p));
}

/**
 * @fn static inline void __flip8_s(void* p);
 * @brief 指定地址的8位整数按位反转，并将结果存回原址
 * @param[in] p 8位整数内存地址
 */
static inline void __flip8_s(void* p)
{
	*((uint8_t*)p) = __flip8(*((uint8_t*)p));
}

/**
 * @fn static inline void __flip16_s(void* p);
 * @brief 指定地址的16位整数按位反转，并将结果存回原址
 * @param[in] p 16位整数内存地址
 */
static inline void __flip16_s(void* p)
{
	*((uint16_t*)p) = __flip16(*((uint16_t*)p));
}

/**
 * @fn static inline void __flip32_s(void* p);
 * @brief 指定地址的32位整数按位反转，并将结果存回原址
 * @param[in] p 32位整数内存地址
 */
static inline void __flip32_s(void* p)
{
	*((uint32_t*)p) = __flip32(*((uint32_t*)p));
}

/**
 * @fn static inline int __clz8(uint8_t x);
 * @brief 计算8位整型前导0的个数
 * @param[in] x 待测整数
 * @return int 返回前导0的个数
 */
static inline int __clz8(uint8_t x)
{
#ifdef __GNUC__
	return __builtin_clz((uint32_t)x) - 24;
#elif defined(_MSC_VER) && _MSC_VER >= 1500 /* VS2008以上 */
	return (int)__lzcnt16((uint16_t)x);
#else
	int c = 8;
    if (x) { --c;
        if (x & 0xF0) { x &= 0xF0; c -= 4; }
        if (x & 0xCC) { x &= 0xCC; c -= 2; }
        if (x & 0xAA) { x &= 0xAA; c -= 1; }
    }
    return c;
#endif /* #ifdef __GNUC__ */
}

/**
 * @fn static inline int __clz8_p(void* p);
 * @brief 计算指定内存地址的8位整型前导0的个数
 * @param[in] p 待测整数的内存地址
 * @return int 返回前导0的个数
 */
static inline int __clz8_p(void* p)
{
	return __clz8(*((uint8_t*)p));
}

/**
 * @fn static inline int __clz16(uint16_t x);
 * @brief 计算16位整型前导0的个数
 * @param[in] x 待测整数
 * @return int 返回前导0的个数
 */
static inline int __clz16(uint16_t x)
{
#ifdef __GNUC__
	return __builtin_clz((uint32_t)x) - 16;
#elif defined(_MSC_VER) && _MSC_VER >= 1500 /* VS2008以上 */
	return (int)__lzcnt16(x);
#else
	int c = 16;
    if (x) { --c;
        if (x & 0xFF00) { x &= 0xFF00; c -= 8; }
        if (x & 0xF0F0) { x &= 0xF0F0; c -= 4; }
        if (x & 0xCCCC) { x &= 0xCCCC; c -= 2; }
        if (x & 0xAAAA) { x &= 0xAAAA; c -= 1; }
    }
    return c;
#endif /* #ifdef __GNUC__ */
}

/**
 * @fn static inline int __clz16_p(void* p);
 * @brief 计算指定内存地址的16位整型前导0的个数
 * @param[in] p 待测整数的内存地址
 * @return int 返回前导0的个数
 */
static inline int __clz16_p(void* p)
{
	return __clz16(*((uint16_t*)p));
}

/**
 * @fn static inline int __clz32(uint32_t x);
 * @brief 计算32位整型前导0的个数
 * @param[in] x 待测整数
 * @return int 返回前导0的个数
 */
static inline int __clz32(uint32_t x)
{
#ifdef __GNUC__
	return __builtin_clz(x);
#elif defined(_MSC_VER) && _MSC_VER >= 1500 /* VS2008以上 */
	return (int)__lzcnt(x);
#else
	int c = 32;
    if (x) { --c;
        if (x & 0xFFFF0000UL) { x &= 0xFFFF0000UL; c -= 16; }
        if (x & 0xFF00FF00UL) { x &= 0xFF00FF00UL; c -=  8; }
        if (x & 0xF0F0F0F0UL) { x &= 0xF0F0F0F0UL; c -=  4; }
        if (x & 0xCCCCCCCCUL) { x &= 0xCCCCCCCCUL; c -=  2; }
        if (x & 0xAAAAAAAAUL) { x &= 0xAAAAAAAAUL; c -=  1; }
    }
    return c;
#endif /* #ifdef __GNUC__ */
}

/**
 * @fn static inline int __clz32_p(void* p);
 * @brief 计算指定内存地址的32位整型前导0的个数
 * @param[in] p 待测整数的内存地址
 * @return int 返回前导0的个数
 */
static inline int __clz32_p(void* p)
{
	return __clz32(*((uint32_t*)p));
}

/**
 * @fn static inline int __ctz8(uint8_t x);
 * @brief 计算8位整型尾随0的个数
 * @param[in] x 待测整数
 * @return int 返回尾随0的个数
 */
static inline int __ctz8(uint8_t x)
{
#ifdef __GNUC__
	return __builtin_ctz((uint32_t)x);
#elif defined(_MSC_VER) && _MSC_VER >= 1400 /* VS2005以上 */
	unsigned long _i = 0;
	_BitScanForward(&_i, (uint32_t)x);
	return (int)(_i - 24);
#else
	int c = 8;
    if (x &= -((uint8_t)x)) { --c;
	    if (x & 0x0F) c -=  4;
	    if (x & 0x33) c -=  2;
	    if (x & 0x55) c -=  1;
    }
    return c;
#endif /* #ifdef __GNUC__ */
}

/**
 * @fn static inline int __ctz8_p(void* p);
 * @brief 计算指定内存地址的8位整型尾随0的个数
 * @param[in] p 待测整数的内存地址
 * @return int 返回尾随0的个数
 */
static inline int __ctz8_p(void* p)
{
	return __ctz8(*((uint8_t*)p));
}

/**
 * @fn static inline int __ctz16(uint16_t x);
 * @brief 计算16位整型尾随0的个数
 * @param[in] x 待测整数
 * @return int 返回尾随0的个数
 */
static inline int __ctz16(uint16_t x)
{
#ifdef __GNUC__
	return __builtin_ctz((uint32_t)x);
#elif defined(_MSC_VER) && _MSC_VER >= 1400 /* VS2005以上 */
	unsigned long _i = 0;
	_BitScanForward(&_i, (uint32_t)x);
	return (int)(_i - 16);
#else
	int c = 16;
    if (x &= -((int16_t)x)) { --c;
	    if (x & 0x00FF) c -=  8;
	    if (x & 0x0F0F) c -=  4;
	    if (x & 0x3333) c -=  2;
	    if (x & 0x5555) c -=  1;
    }
    return c;
#endif /* #ifdef __GNUC__ */
}

/**
 * @fn static inline int __ctz16_p(void* p);
 * @brief 计算指定内存地址的16位整型尾随0的个数
 * @param[in] p 待测整数的内存地址
 * @return int 返回尾随0的个数
 */
static inline int __ctz16_p(void* p)
{
	return __ctz16(*((uint16_t*)p));
}

/**
 * @fn static inline int __ctz32(uint32_t x);
 * @brief 计算32位整型尾随0的个数
 * @param[in] x 待测整数
 * @return int 返回尾随0的个数
 */
static inline int __ctz32(uint32_t x)
{
#ifdef __GNUC__
	return __builtin_ctz(x);
#elif defined(_MSC_VER) && _MSC_VER >= 1400 /* VS2005以上 */
	unsigned long _i = 0;
	_BitScanForward(&_i, x);
	return (int)_i;
#else
	int c = 32;
    if (x &= -((int32_t)x)) { --c;
	    if (x & 0x0000FFFFUL) c -= 16;
	    if (x & 0x00FF00FFUL) c -=  8;
	    if (x & 0x0F0F0F0FUL) c -=  4;
	    if (x & 0x33333333UL) c -=  2;
	    if (x & 0x55555555UL) c -=  1;
    }
    return c;
#endif /* #ifdef __GNUC__ */
}

/**
 * @fn static inline int __ctz32_p(void* p);
 * @brief 计算指定内存地址的32位整型尾随0的个数
 * @param[in] p 待测整数的内存地址
 * @return int 返回尾随0的个数
 */
static inline int __ctz32_p(void* p)
{
	return __ctz32(*((uint32_t*)p));
}

/**
 * @fn static inline int __popcount8(uint8_t x);
 * @brief 计算8位整型二进制形式含有1的个数
 * @param[in] x 待测整数
 * @return int 返回1的个数
 */
static inline int __popcount8(uint8_t x)
{
	x = (x & 0x55) + ((x >> 1) & 0x55);
	x = (x & 0x33) + ((x >> 2) & 0x33);
	x = (x & 0x0F) + ((x >> 4) & 0x0F);
	return (int)x;
}

/**
 * @fn static inline int __popcount8_p(void* p);
 * @brief 计算指定内存地址的8位整型二进制形式含有1的个数
 * @param[in] p 待测整数的内存地址
 * @return int 返回1的个数
 */
static inline int __popcount8_p(void* p)
{
	return __popcount8(*((uint8_t*)p));
}

/**
 * @fn static inline int __popcount16(uint16_t x);
 * @brief 计算16位整型二进制形式含有1的个数
 * @param[in] x 待测整数
 * @return int 返回1的个数
 */
static inline int __popcount16(uint16_t x)
{
#ifdef __GNUC__
	return (int)__builtin_popcount((uint32_t)x);
#elif defined(_MSC_VER) && _MSC_VER >= 1500
	return (int)__popcnt16(x);
#else
	x = (x & 0x5555) + ((x >> 1) & 0x5555);
	x = (x & 0x3333) + ((x >> 2) & 0x3333);
	x = (x & 0x0F0F) + ((x >> 4) & 0x0F0F);
	x = (x & 0x00FF) + ((x >> 8) & 0x00FF);
	return (int)x;
#endif /* #ifdef __GNUC__ */
}

/**
 * @fn static inline int __popcount16_p(void* p);
 * @brief 计算指定内存地址的16位整型二进制形式含有1的个数
 * @param[in] p 待测整数的内存地址
 * @return int 返回1的个数
 */
static inline int __popcount16_p(void* p)
{
	return __popcount16(*((uint16_t*)p));
}

/**
 * @fn static inline int __popcount32(uint32_t x);
 * @brief 计算32位整型二进制形式含有1的个数
 * @param[in] x 待测整数
 * @return int 返回1的个数
 */
static inline int __popcount32(uint32_t x)
{
#ifdef __GNUC__
	return (int)__builtin_popcount(x);
#elif defined(_MSC_VER) && _MSC_VER >= 1500
	return (int)__popcnt(x);
#else
	/* 首先把32位分成16份，每份两个数，我们先求出每相邻两个数一共有多少个1，
     * 如此对于每一份，左边1个数是n&010101...01,右边是(n>>1) & 0101..01
     * 如此一次后，这16份每份存的都是原数中1的个数,
     * 之后16份变8份，8份变4份，直到变成1份
	 */
	x = (x & 0x55555555UL) + ((x >>  1) & 0x55555555UL);
	x = (x & 0x33333333UL) + ((x >>  2) & 0x33333333UL);
	x = (x & 0x0F0F0F0FUL) + ((x >>  4) & 0x0F0F0F0FUL);
	x = (x & 0x00FF00FFUL) + ((x >>  8) & 0x00FF00FFUL);
	x = (x & 0x0000FFFFUL) + ((x >> 16) & 0x0000FFFFUL);
	return (int)x;
#endif /* #ifdef __GNUC__ */
}

/**
 * @fn static inline int __popcount32_p(void* p);
 * @brief 计算指定内存地址的32位整型二进制形式含有1的个数
 * @param[in] p 待测整数的内存地址
 * @return int 返回1的个数
 */
static inline int __popcount32_p(void* p)
{
	return __popcount32(*((uint32_t*)p));
}

/**
 * @fn static inline int __poprange8(uint8_t x);
 * @brief 计算8位整型二进制形式有效位数目
 * @param[in] x 待测整数
 * @return int 返回有效位的个数
 */
static inline int __poprange8(uint8_t x)
{
#if defined(_MSC_VER) && _MSC_VER >= 1400
	unsigned long _c = 0;
	_BitScanReverse(&_c, (uint32_t)x);
	return (int)_c;
#else
	return (x)? 8 - (__clz8(x) + __ctz8(x)): 0;
#endif /* #if defined(_MSC_VER) && _MSC_VER >= 1400 */
}

/**
 * @fn static inline int __poprange8_p(void* p);
 * @brief 计算指定内存地址的8位整型二进制形式有效位的个数
 * @param[in] p 待测整数的内存地址
 * @return int 返回有效位的个数
 */
static inline int __poprange8_p(void* p)
{
	return __poprange8(*((uint8_t*)p));
}

/**
 * @fn static inline int __poprange16(uint16_t x);
 * @brief 计算16位整型二进制形式有效位数目
 * @param[in] x 待测整数
 * @return int 返回有效位的个数
 */
static inline int __poprange16(uint16_t x)
{
#if defined(_MSC_VER) && _MSC_VER >= 1400
	unsigned long _c = 0;
	_BitScanReverse(&_c, (uint32_t)x);
	return (int)_c;
#else
	return (x)? 16 - (__clz16(x) + __ctz16(x)): 0;
#endif /* #if defined(_MSC_VER) && _MSC_VER >= 1400 */
}

/**
 * @fn static inline int __poprange16_p(void* p);
 * @brief 计算指定内存地址的16位整型二进制形式有效位的个数
 * @param[in] p 待测整数的内存地址
 * @return int 返回有效位的个数
 */
static inline int __poprange16_p(void* p)
{
	return __poprange16(*((uint16_t*)p));
}

/**
 * @fn static inline int __poprange32(uint32_t x);
 * @brief 计算32位整型二进制形式有效位数目
 * @param[in] x 待测整数
 * @return int 返回有效位的个数
 */
static inline int __poprange32(uint32_t x)
{
#if defined(_MSC_VER) && _MSC_VER >= 1400
	unsigned long _c = 0;
	_BitScanReverse(&_c, x);
	return (int)_c;
#else
	return (x)? 32 - (__clz32(x) + __ctz32(x)): 0;
#endif /* #if defined(_MSC_VER) && _MSC_VER >= 1400 */
}

/**
 * @fn static inline int __poprange32_p(void* p);
 * @brief 计算指定内存地址的32位整型二进制形式有效位的个数
 * @param[in] p 待测整数的内存地址
 * @return int 返回有效位的个数
 */
static inline int __poprange32_p(void* p)
{
	return __poprange32(*((uint32_t*)p));
}

/**
 * @fn static inline int __parity8(uint8_t x);
 * @brief 计算8位整型的奇偶校验码，即二进制形式中1的个数的模2
 * @param[in] x 待测整数
 * @return int 返回校验结果
 */
static inline int __parity8(uint8_t x)
{
#ifdef __GNUC__
	return __builtin_parity((uint32_t)x);
#else
	return __popcount8(x) & 1;
#endif /* #ifdef __GNUC__ */
}

/**
 * @fn static inline int __parity8_p(void* p);
 * @brief 计算指定内存地址的8位整型的奇偶校验码，即二进制形式中1的个数的模2
 * @param[in] p 待测整数的内存地址
 * @return int 返回校验结果
 */
static inline int __parity8_p(void* p)
{
	return __parity8(*((uint8_t*)p));
}

/**
 * @fn static inline int __parity16(uint16_t x);
 * @brief 计算16位整型的奇偶校验码，即二进制形式中1的个数的模2
 * @param[in] x 待测整数
 * @return int 返回校验结果
 */
static inline int __parity16(uint16_t x)
{
#ifdef __GNUC__
	return __builtin_parity((uint32_t)x);
#else
	return __popcount16(x) & 1;
#endif /* #ifdef __GNUC__ */
}

/**
 * @fn static inline int __parity16_p(void* p);
 * @brief 计算指定内存地址的16位整型的奇偶校验码，即二进制形式中1的个数的模2
 * @param[in] p 待测整数的内存地址
 * @return int 返回校验结果
 */
static inline int __parity16_p(void* p)
{
	return __parity16(*((uint16_t*)p));
}

/**
 * @fn static inline int __parity32(uint32_t x);
 * @brief 计算32位整型的奇偶校验码，即二进制形式中1的个数的模2
 * @param[in] x 待测整数
 * @return int 返回校验结果
 */
static inline int __parity32(uint32_t x)
{
#ifdef __GNUC__
	return __builtin_parity(x);
#else
	return __popcount32(x) & 1;
#endif /* #ifdef __GNUC__ */
}

/**
 * @fn static inline int __parity32_p(void* p);
 * @brief 计算指定内存地址的32位整型的奇偶校验码，即二进制形式中1的个数的模2
 * @param[in] p 待测整数的内存地址
 * @return int 返回校验结果
 */
static inline int __parity32_p(void* p)
{
	return __parity32(*((uint32_t*)p));
}


/* Need 64-bit integer support */
#ifdef INT64_SUPPORT

#define __BIT64C_L0 	UINT64_C(0x0000000000000001)
#define __BIT64C_L1 	UINT64_C(0x0000000000000002)
#define __BIT64C_L2 	UINT64_C(0x0000000000000004)
#define __BIT64C_L3 	UINT64_C(0x0000000000000008)
#define __BIT64C_L4 	UINT64_C(0x0000000000000010)
#define __BIT64C_L5 	UINT64_C(0x0000000000000020)
#define __BIT64C_L6 	UINT64_C(0x0000000000000040)
#define __BIT64C_L7 	UINT64_C(0x0000000000000080)
#define __BIT64C_L8 	UINT64_C(0x0000000000000100)
#define __BIT64C_L9 	UINT64_C(0x0000000000000200)
#define __BIT64C_L10 	UINT64_C(0x0000000000000400)
#define __BIT64C_L11 	UINT64_C(0x0000000000000800)
#define __BIT64C_L12 	UINT64_C(0x0000000000001000)
#define __BIT64C_L13 	UINT64_C(0x0000000000002000)
#define __BIT64C_L14 	UINT64_C(0x0000000000004000)
#define __BIT64C_L15 	UINT64_C(0x0000000000008000)
#define __BIT64C_L16 	UINT64_C(0x0000000000010000)
#define __BIT64C_L17 	UINT64_C(0x0000000000020000)
#define __BIT64C_L18 	UINT64_C(0x0000000000040000)
#define __BIT64C_L19 	UINT64_C(0x0000000000080000)
#define __BIT64C_L20 	UINT64_C(0x0000000000100000)
#define __BIT64C_L21 	UINT64_C(0x0000000000200000)
#define __BIT64C_L22 	UINT64_C(0x0000000000400000)
#define __BIT64C_L23 	UINT64_C(0x0000000000800000)
#define __BIT64C_L24 	UINT64_C(0x0000000001000000)
#define __BIT64C_L25 	UINT64_C(0x0000000002000000)
#define __BIT64C_L26 	UINT64_C(0x0000000004000000)
#define __BIT64C_L27 	UINT64_C(0x0000000008000000)
#define __BIT64C_L28 	UINT64_C(0x0000000010000000)
#define __BIT64C_L29 	UINT64_C(0x0000000020000000)
#define __BIT64C_L30 	UINT64_C(0x0000000040000000)
#define __BIT64C_L31 	UINT64_C(0x0000000080000000)
#define __BIT64C_L32 	UINT64_C(0x0000000100000000)
#define __BIT64C_L33 	UINT64_C(0x0000000200000000)
#define __BIT64C_L34 	UINT64_C(0x0000000400000000)
#define __BIT64C_L35 	UINT64_C(0x0000000800000000)
#define __BIT64C_L36 	UINT64_C(0x0000001000000000)
#define __BIT64C_L37 	UINT64_C(0x0000002000000000)
#define __BIT64C_L38 	UINT64_C(0x0000004000000000)
#define __BIT64C_L39 	UINT64_C(0x0000008000000000)
#define __BIT64C_L40 	UINT64_C(0x0000010000000000)
#define __BIT64C_L41 	UINT64_C(0x0000020000000000)
#define __BIT64C_L42 	UINT64_C(0x0000040000000000)
#define __BIT64C_L43 	UINT64_C(0x0000080000000000)
#define __BIT64C_L44 	UINT64_C(0x0000100000000000)
#define __BIT64C_L45 	UINT64_C(0x0000200000000000)
#define __BIT64C_L46 	UINT64_C(0x0000400000000000)
#define __BIT64C_L47 	UINT64_C(0x0000800000000000)
#define __BIT64C_L48 	UINT64_C(0x0001000000000000)
#define __BIT64C_L49 	UINT64_C(0x0002000000000000)
#define __BIT64C_L50 	UINT64_C(0x0004000000000000)
#define __BIT64C_L51 	UINT64_C(0x0008000000000000)
#define __BIT64C_L52 	UINT64_C(0x0010000000000000)
#define __BIT64C_L53 	UINT64_C(0x0020000000000000)
#define __BIT64C_L54 	UINT64_C(0x0040000000000000)
#define __BIT64C_L55 	UINT64_C(0x0080000000000000)
#define __BIT64C_L56 	UINT64_C(0x0100000000000000)
#define __BIT64C_L57 	UINT64_C(0x0200000000000000)
#define __BIT64C_L58 	UINT64_C(0x0400000000000000)
#define __BIT64C_L59 	UINT64_C(0x0800000000000000)
#define __BIT64C_L60 	UINT64_C(0x1000000000000000)
#define __BIT64C_L61 	UINT64_C(0x2000000000000000)
#define __BIT64C_L62 	UINT64_C(0x4000000000000000)
#define __BIT64C_L63 	UINT64_C(0x8000000000000000)

/**
 * @fn static inline int __getb64(uint64_t x, int i);
 * @brief 获取64位整数低位起第p位的值
 * @param[in] x 64位整数
 * @param[in] i 位置，从低位开始的位置
 * @return int 返回低位起第p位的值
 * @retval 1 比特1
 * @retval 0 比特0
 */
static inline int __getb64(uint64_t x, int i)
{
	return (int)((x >> i) & UINT64_C(1));
}

/**
 * @fn static inline int __getb64_p(void* p, int i);
 * @brief 获取64位整数低位起第p位的值
 * @param[in] p 64位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 * @return int 返回低位起第p位的值
 * @retval 1 比特1
 * @retval 0 比特0
 */
static inline int __getb64_p(void* p, int i)
{
	return (int)((*((uint64_t*)p) >> i) & UINT64_C(1));
}

/**
 * @fn static inline uint64_t __setb64(uint64_t x, int i);
 * @brief 修改64位整数低位起第p位为1
 * @param[in] x 64位整数
 * @param[in] i 位置，从低位开始的位置
 * @return uint64_t 返回修改后的64位整数
 */
static inline uint64_t __setb64(uint64_t x, int i)
{
	return x | (UINT64_C(1) << i);
}

/**
 * @fn static inline uint64_t __setb64_p(void* p, int i);
 * @brief 修改指定内存地址的64位整数低位起第p位为1，结果不存回原址
 * @param[in] p 64位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 * @return uint64_t 返回修改后的64位整数
 */
static inline uint64_t __setb64_p(void* p, int i)
{
	return *((uint64_t*)p) | (UINT64_C(1) << i);
}

/**
 * @fn static inline uint64_t __setb64_s(void* p, int i);
 * @brief 修改指定内存地址的64位整数低位起第p位为1，并将结果存回原址
 * @param[in] p 64位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 */
static inline void __setb64_s(void* p, int i)
{
	*((uint64_t*)p) |= (UINT64_C(1) << i);
}

/**
 * @fn static inline uint64_t __clrb64(uint64_t x, int i);
 * @brief 修改64位整数低位起第p位为0
 * @param[in] x 64位整数
 * @param[in] i 位置，从低位开始的位置
 * @return uint64_t 返回修改后的64位整数
 */
static inline uint64_t __clrb64(uint64_t x, int i)
{
	return x & (~(UINT64_C(1) << i));
}

/**
 * @fn static inline uint64_t __clrb64_p(void* p, int i);
 * @brief 修改指定内存地址的64位整数低位起第p位为0，结果不存回原址
 * @param[in] p 64位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 * @return uint64_t 返回修改后的64位整数
 */
static inline uint64_t __clrb64_p(void* p, int i)
{
	return *((uint64_t*)p) & (~(UINT64_C(1) << i));
}

/**
 * @fn static inline void __clrb64_s(void* p, int i);
 * @brief 修改指定内存地址的64位整数低位起第p位为0，并将结果存回原址
 * @param[in] p 64位整数内存地址
 * @param[in] i 位置，从低位开始的位置
 */
static inline void __clrb64_s(void* p, int i)
{
	*((uint64_t*)p) &= (~(UINT64_C(1) << i));
}

/**
 * @fn static inline uint64_t __to_gray64(uint64_t x);
 * @brief 64位自然码转格雷码
 * @param[in] x 自然码
 * @return uint64_t 返回64位格雷码
 */
static inline uint64_t __to_gray64(uint64_t x)
{
	return __to_gray_c(x);
}

/**
 * @fn static inline uint64_t __to_gray64_p(void* p);
 * @brief 将指定内存地址的64位自然码转格雷码，但不存回p
 * @param[in] p 64位自然码内存地址
 * @return uint64_t 返回64位格雷码
 */
static inline uint64_t __to_gray64_p(void* p)
{
	return __to_gray64(*((uint64_t*)p));
}

/**
 * @fn static inline void __to_gray64_s(void* p);
 * @brief 将指定内存地址的64位自然码转格雷码，并存回p
 * @param[in] p 64位自然码内存地址
 */
static inline void __to_gray64_s(void* p)
{
	*((uint64_t*)p) = __to_gray64(*((uint64_t*)p));
}

/**
 * @fn static inline uint64_t __to_natural64(uint64_t x);
 * @brief 64位格雷码转自然码
 * @param[in] x 格雷码
 * @return uint64_t 返回64位自然码
 */
static inline uint64_t __to_natural64(uint64_t x)
{
	x = x ^ (x >>  1);
	x = x ^ (x >>  2);
	x = x ^ (x >>  4);
	x = x ^ (x >>  8);
	x = x ^ (x >> 16);
	x = x ^ (x >> 32);
	return x;
}

/**
 * @fn static inline uint64_t __to_natural64_p(void* p);
 * @brief 将指定内存地址的64位格雷码转自然码，但不存回p
 * @param[in] p 64位格雷码内存地址
 * @return uint64_t 返回64位自然码
 */
static inline uint64_t __to_natural64_p(void* p)
{
	return __to_natural64(*((uint64_t*)p));
}

/**
 * @fn static inline void __to_natural64_s(void* p);
 * @brief 将指定内存地址的64位格雷码转自然码，并存回p
 * @param[in] p 64位格雷码内存地址
 */
static inline void __to_natural64_s(void* p)
{
	*((uint64_t*)p) = __to_natural64(*((uint64_t*)p));
}

/**
 * @fn static inline uint64_t __flip64(uint64_t x);
 * @brief 按位反转64位整数
 * @param[in] x 64位整数
 * @return uint64_t 返回反转结果
 */
static inline uint64_t __flip64(uint64_t x)
{
	x = ((x >>  1) & UINT64_C(0x5555555555555555)) |
		((x        & UINT64_C(0x5555555555555555)) <<  1);
	x = ((x >>  2) & UINT64_C(0x3333333333333333)) |
		((x        & UINT64_C(0x3333333333333333)) <<  2);
	x = ((x >>  4) & UINT64_C(0x0F0F0F0F0F0F0F0F)) |
		((x        & UINT64_C(0x0F0F0F0F0F0F0F0F)) <<  4);
	x = ((x >>  8) & UINT64_C(0x00FF00FF00FF00FF)) |
		((x        & UINT64_C(0x00FF00FF00FF00FF)) <<  8);
	x = ((x >> 16) & UINT64_C(0x0000FFFF0000FFFF)) |
		((x        & UINT64_C(0x0000FFFF0000FFFF)) << 16);
	x = (x >> 32) | (x << 32);
	return x;
}

/**
 * @fn static inline uint64_t __flip64_p(void* p);
 * @brief 指定地址的64位整数按位反转，不存回原址
 * @param[in] p 64位整数内存地址
 * @return uint64_t 返回反转结果
 */
static inline uint64_t __flip64_p(void* p)
{
	return __flip64(*((uint64_t*)p));
}

/**
 * @fn static inline void __flip64_p(void* p);
 * @brief 指定地址的64位整数按位反转，并将结果存回原址
 * @param[in] p 64位整数内存地址
 */
static inline void __flip64_s(void* p)
{
	*((uint64_t*)p) = __flip64(*((uint64_t*)p));
}

/**
 * @fn static inline int __clz64(uint64_t x);
 * @brief 计算64位整型前导0的个数
 * @param[in] x 待测整数
 * @return int 返回前导0的个数
 */
static inline int __clz64(uint64_t x)
{
#ifdef __GNUC__
	return __builtin_clzll(x);
#elif defined(_MSC_VER) && _MSC_VER >= 1500 /* VS2008以上 */
	return (int)__lzcnt64(x);
#else
	int c = 64;
    if (x) { --c;
		if (x &  UINT64_C(0xFFFFFFFF00000000)) {
			x &= UINT64_C(0xFFFFFFFF00000000); c -= 32; }
        if (x &  UINT64_C(0xFFFF0000FFFF0000)) {
			x &= UINT64_C(0xFFFF0000FFFF0000); c -= 16; }
        if (x &  UINT64_C(0xFF00FF00FF00FF00)) {
			x &= UINT64_C(0xFF00FF00FF00FF00); c -=  8; }
        if (x &  UINT64_C(0xF0F0F0F0F0F0F0F0)) {
			x &= UINT64_C(0xF0F0F0F0F0F0F0F0); c -=  4; }
        if (x &  UINT64_C(0xCCCCCCCCCCCCCCCC)) {
			x &= UINT64_C(0xCCCCCCCCCCCCCCCC); c -=  2; }
        if (x &  UINT64_C(0xAAAAAAAAAAAAAAAA)) {
			x &= UINT64_C(0xAAAAAAAAAAAAAAAA); c -=  1; }
    }
    return c;
#endif /* #ifdef __GNUC__ */
}

/**
 * @fn static inline int __clz64_p(void* p);
 * @brief 计算指定内存地址的64位整型前导0的个数
 * @param[in] p 待测整数的内存地址
 * @return int 返回前导0的个数
 */
static inline int __clz64_p(void* p)
{
	return __clz64(*((uint64_t*)p));
}

/**
 * @fn static inline int __ctz64(uint64_t x);
 * @brief 计算64位整型尾随0的个数
 * @param[in] x 待测整数
 * @return int 返回尾随0的个数
 */
static inline int __ctz64(uint64_t x)
{
#ifdef __GNUC__
	return __builtin_ctzll(x);
#elif defined(_MSC_VER) && _MSC_VER >= 1400 /* VS2005以上 */
	unsigned long _i = 0;
	_BitScanForward64(&_i, x);
	return (int)_i;
#else
	int c = 64;
    if (x &= -((int64_t)x)) { --c;
		if (x & UINT64_C(0x00000000FFFFFFFF)) c -= 32;
	    if (x & UINT64_C(0x0000FFFF0000FFFF)) c -= 16;
	    if (x & UINT64_C(0x00FF00FF00FF00FF)) c -=  8;
	    if (x & UINT64_C(0x0F0F0F0F0F0F0F0F)) c -=  4;
	    if (x & UINT64_C(0x3333333333333333)) c -=  2;
	    if (x & UINT64_C(0x5555555555555555)) c -=  1;
    }
    return c;
#endif /* #ifdef __GNUC__ */
}

/**
 * @fn static inline int __ctz64_p(void* p);
 * @brief 计算指定内存地址的64位整型尾随0的个数
 * @param[in] p 待测整数的内存地址
 * @return int 返回尾随0的个数
 */
static inline int __ctz64_p(void* p)
{
	return __ctz64(*((uint64_t*)p));
}

/**
 * @fn static inline int __popcount64(uint64_t x);
 * @brief 计算64位整型二进制形式含有1的个数
 * @param[in] x 待测整数
 * @return int 返回1的个数
 */
static inline int __popcount64(uint64_t x)
{
#ifdef __GNUC__
	return (int)__builtin_popcountll(x);
#elif defined(_MSC_VER) && _MSC_VER >= 1500
	return (int)__popcnt64(x);
#else
	x = (        x & UINT64_C(0x5555555555555555)) +
		((x >>  1) & UINT64_C(0x5555555555555555));
	x = (        x & UINT64_C(0x3333333333333333)) +
		((x >>  2) & UINT64_C(0x3333333333333333));
	x = (        x & UINT64_C(0x0F0F0F0F0F0F0F0F)) +
		((x >>  4) & UINT64_C(0x0F0F0F0F0F0F0F0F));
	x = (        x & UINT64_C(0x00FF00FF00FF00FF)) +
		((x >>  8) & UINT64_C(0x00FF00FF00FF00FF));
	x = (        x & UINT64_C(0x0000FFFF0000FFFF)) +
		((x >> 16) & UINT64_C(0x0000FFFF0000FFFF));
	x = (        x & UINT64_C(0x00000000FFFFFFFF)) +
		((x >> 32) & UINT64_C(0x00000000FFFFFFFF));
	return (int)x;
#endif /* #ifdef __GNUC__ */
}

/**
 * @fn static inline int __popcount64_p(void* p);
 * @brief 计算指定内存地址的64位整型二进制形式含有1的个数
 * @param[in] p 待测整数的内存地址
 * @return int 返回1的个数
 */
static inline int __popcount64_p(void* p)
{
	return __popcount64(*((uint64_t*)p));
}

/**
 * @fn static inline int __poprange64(uint64_t x);
 * @brief 计算64位整型二进制形式有效位数目
 * @param[in] x 待测整数
 * @return int 返回有效位的个数
 */
static inline int __poprange64(uint64_t x)
{
#if defined(_MSC_VER) && _MSC_VER >= 1400
	unsigned long _c = 0;
	_BitScanReverse64(&_c, x);
	return (int)_c;
#else
	return (x)? 64 - (__clz64(x) + __ctz64(x)): 0;
#endif /* #if defined(_MSC_VER) && _MSC_VER >= 1400 */
}

/**
 * @fn static inline int __poprange64_p(void* p);
 * @brief 计算指定内存地址的64位整型二进制形式有效位的个数
 * @param[in] p 待测整数的内存地址
 * @return int 返回有效位的个数
 */
static inline int __poprange64_p(void* p)
{
	return __poprange64(*((uint64_t*)p));
}

/**
 * @fn static inline int __popcount64(uint64_t x);
 * @brief 计算64位整型的奇偶校验码，即二进制形式中1的个数的模2
 * @param[in] x 待测整数
 * @return int 返回校验结果
 */
static inline int __parity64(uint64_t x)
{
#ifdef __GNUC__
	return __builtin_parityll(x);
#else
	return __popcount64(x) & 1;
#endif /* #ifdef __GNUC__ */
}

/**
 * @fn static inline int __parity64_p(void* p);
 * @brief 计算指定内存地址的64位整型的奇偶校验码，即二进制形式中1的个数的模2
 * @param[in] p 待测整数的内存地址
 * @return int 返回校验结果
 */
static inline int __parity64_p(void* p)
{
	return __parity64(*((uint64_t*)p));
}

#endif /* #ifdef INT64_SUPPORT */

/** @} */

#ifdef __cplusplus
}
#endif /* #ifdef __cplusplus */

#endif /* #ifndef __NV_BITS__ */
