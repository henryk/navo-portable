/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file port/os.h
 * 
 * @brief Operating system.
 * 
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 * 
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_OS__
#define __NV_OS__

/**
 * @defgroup 操作系统
 * @ingroup 平台判定
 * @{
 */

/* Win64 */
#if defined(_WIN64) || defined(WIN64) || defined(__WIN64)
#ifndef __win64__
#define __win64__
#endif /* #ifndef __win64__ */

/* Win32 */
#elif defined(_WIN32) || defined(WIN32) || defined(__WIN32__) || \
		defined(__WIN32) || defined(_MSC_VER) || defined(__TOS_WIN__)
#ifndef __win32__
#define __win32__
#endif /* #ifndef __win32__ */

/* Mac OS */
#elif defined(__APPLE__) || defined(__MaxOSX__) || defined(__APPLE_CC__ ) || \
		defined(__MACH__) || defined(macintosh) || defined(Macintosh)
#ifndef __macosx__
#define __macosx__
#endif /* #ifndef __macosx__ */

/* Android */
#elif defined(ANDROID)
#ifndef __android__
#define __android__
#endif /* #ifndef __macosx__ */

/* NetBSD */
#elif defined(__NetBSD__)
#ifndef __netbsd__
#define __netbsd__
#endif /* #ifndef __netbsd__ */

/* OpenBSD */
#elif defined(__OpenBSD__)
#ifndef __openbsd__
#define __openbsd__
#endif /* #ifndef __openbsd__ */

/* FreeBSD */
#elif defined(__FreeBSD__)
#ifndef __freebsd__
#define __freebsd__
#endif /* #ifndef __freebsd__ */

/* Linux */
#elif defined(__linux__) || defined(__linux) || \
	defined(linux) || defined(_LINUX)
#ifndef __linux__
#define __linux__
#endif /* #ifndef __linux__ */

/* WinCE */
#elif defined(_WIN32_WCE)
#ifndef __wince__
#define __wince__
#endif /* #ifndef __wince__ */

/* Unix */
#elif defined(__unix__) || defined(__unix)
#ifndef __unix__
#define __unix__
#endif /* #ifndef __unix__ */

/* Symbian OS */
#elif defined(__SYMBIAN32__)
#ifndef __symbian__
#define __symbian__
#endif /* #ifndef __symbian__ */

/* HP-UX */
#elif defined(_hpux) || defined(hpux) || defined(__hpux)
#ifndef __hpux__
#define __hpux__
#endif /* #ifndef __hpux__ */

/* AIX */
#elif defined(_AIX) || defined(__TOS_AIX__)
#ifndef __aix__
#define __aix__
#endif /* #ifndef __aix__ */

/* eCos */
#elif defined(__ECOS)
#ifndef __ecos__
#define __ecos__
#endif /* #ifndef __ecos__ */

/* Minix */
#elif defined(__minix)
#ifndef __minix__
#define __minix__
#endif /* #ifndef __minix__ */

/* LynxOS */
#elif defined(__Lynx__)
#ifndef __lynx__
#define __lynx__
#endif /* #ifndef __lynx__ */

/* Solaris */
#elif defined(__sun) || defined(sun) || \
	defined(__SVR4) || defined(__svr4__)
#ifndef __sun__
#define __sun__
#endif /* #ifndef __sun__ */

/* OS/2 */
#elif defined(OS2) || defined(_OS2) || \
	defined(__OS2__) || defined(__TOS_OS2__)
#ifndef __os2__
#define __os2__
#endif /* #ifndef __os2__ */

/* MS-DOS */
#elif defined(MSDOS) || defined(__MSDOS__) || \
	defined(_MSDOS) || defined(__DOS__)
#ifndef __dos__
#define __dos__
#endif /* #ifndef __dos__ */

/* Unknown system */
#else

#endif	/* #if defined(_WIN64) || defined(WIN64) */

/** @} */

#endif /* #ifndef __NV_OS__ */

