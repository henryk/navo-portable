/* -*- Mode: C; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*- */
/**
 * @file port/integer.h
 *
 * @brief Provides standard integer types.
 *
 * Copyright (C) 2012-2022, Henry Kwok 郭弘扬. All rights reserved
 *
 */

#ifdef _MSC_VER
#pragma once
#endif /* #ifdef _MSC_VER */

#ifndef __NV_INTEGER__
#define __NV_INTEGER__

/**
 * @defgroup 整型定义
 * @ingroup 数据类型
 * @{
 */

/**
 * @typedef int8_t
 * @brief 8比特整型
 * @typedef uint8_t
 * @brief 8比特无符号整型
 * @typedef int16_t
 * @brief 16比特整型
 * @typedef uint16_t
 * @brief 16比特无符号整型
 * @typedef int32_t
 * @brief 32比特整型
 * @typedef uint32_t
 * @brief 32比特无符号整型
 * @typedef int64_t
 * @brief 64比特整型
 * @typedef uint64_t
 * @brief 64比特无符号整型
 * @typedef int_least8_t
 * @brief 至少8比特的整型
 * @typedef uint_least8_t
 * @brief 至少8比特无符号的整型
 * @typedef int_least16_t
 * @brief 至少16比特的整型
 * @typedef uint_least16_t
 * @brief 至少16比特无符号的整型
 * @typedef int_least32_t
 * @brief 至少32比特的整型
 * @typedef uint_least32_t
 * @brief 至少32比特无符号的整型
 * @typedef int_least64_t
 * @brief 至少64比特的整型
 * @typedef uint_least64_t
 * @brief 至少64比特无符号的整型
 * @typedef int_fast8_t
 * @brief 至少8比特的快速整型
 * @typedef uint_fast8_t
 * @brief 至少8比特无符号的快速整型
 * @typedef int_fast16_t
 * @brief 至少16比特的快速整型
 * @typedef uint_fast16_t
 * @brief 至少16比特无符号的快速整型
 * @typedef int_fast32_t
 * @brief 至少32比特的快速整型
 * @typedef uint_fast32_t
 * @brief 至少32比特无符号的快速整型
 * @typedef int_fast64_t
 * @brief 至少64比特的快速整型
 * @typedef uint_fast64_t
 * @brief 至少64比特无符号的快速整型
 */
/* __USE_STDINT_H__ 是否支持stdint.h */
#undef __USE_STDINT_H__
#ifdef __GNUC__
#define __USE_STDINT_H__
#elif defined(_MSC_VER) && _MSC_VER >= 1600
#define __USE_STDINT_H__
#elif defined(__BORLANDC__) && __BORLANDC__ >= 0x0560
#define __USE_STDINT_H__
#endif /* #ifdef __GNUC__ */

#ifdef __USE_STDINT_H__

#ifdef __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#endif /* #ifdef __cplusplus */

#if defined(_MSC_VER) || defined(__UINT64_TYPE__) || \
    defined(_WIN64) || defined(__BORLANDC__)
#define INT64_SUPPORT
#endif /* #if defined(_MSC_VER) || defined(__UINT64 ...... */

#elif defined(_MSC_VER)

#ifndef _W64
#if !defined(__midl) && (defined(_X86_) || defined(_M_IX86))
#define _W64 __w64
#else
#define _W64
#endif /* #if !defined(__midl) && (defined(_X86_) || defined(_M_IX86)) */
#endif /* #ifndef _W64 */

/* Exact-width Integer types */
#if _MSC_VER >= 1300

typedef __int8 int8_t;
typedef __int16 int16_t;
typedef __int32 int32_t;
typedef unsigned __int8 uint8_t;
typedef unsigned __int16 uint16_t;
typedef unsigned __int32 uint32_t;

#else

typedef signed char int8_t;
typedef signed short int16_t;
typedef signed int int32_t;
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;

#endif /* #if _MSC_VER >= 1300 */

typedef __int64 int64_t;
typedef unsigned __int64 uint64_t;

/* Minimum-width Integer types */
typedef signed char int_least8_t;
typedef unsigned char uint_least8_t;
typedef signed short int_least16_t;
typedef unsigned short uint_least16_t;
typedef signed int int_least32_t;
typedef unsigned int uint_least32_t;
typedef int64_t int_least64_t;
typedef uint64_t uint_least64_t;

/* Fastest minimum-width Integer types */
typedef signed char int_fast8_t;
typedef unsigned char uint_fast8_t;
typedef signed short int_fast16_t;
typedef unsigned short uint_fast16_t;
typedef signed int int_fast32_t;
typedef unsigned int uint_fast32_t;
typedef int64_t int_fast64_t;
typedef uint64_t uint_fast64_t;

#define INT64_SUPPORT

/**
 * @typedef intptr_t
 * @brief 指针整型
 * @typedef uintptr_t
 * @brief 无符号指针整型
 */
#if defined(_WIN64) || defined(WIN64)
typedef int64_t intptr_t;
typedef uint64_t uintptr_t;
#else
typedef _W64 int32_t intptr_t;
typedef _W64 uint32_t uintptr_t;
#endif /* #if defined(_WIN64) || defined(WIN64) */

typedef int64_t intmax_t;
typedef uint64_t uintmax_t;

#define INT8_C(val) 	val
#define UINT8_C(val) 	val
#define INT16_C(val) 	val
#define UINT16_C(val) 	val
#define INT32_C(val) 	val##L
#define UINT32_C(val) 	val##UL
#define INT64_C(val) 	val##I64
#define UINT64_C(val) 	val##UI64

#define INTMAX_C 	INT64_C
#define UINTMAX_C 	UINT64_C

#else

#define __NV_ARCH_INTEGER__
#include "arch.h"
#undef __NV_ARCH_INTEGER__

/* 8-bit integer type */
typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef signed char int_least8_t;
typedef unsigned char uint_least8_t;
typedef signed char int_fast8_t;
typedef unsigned char uint_fast8_t;

/* 16-bit integer type */
#if SHORT_BITS == 16
typedef signed short int16_t;
typedef unsigned short uint16_t;
typedef signed short int_least16_t;
typedef unsigned short uint_least16_t;
typedef signed short int_fast16_t;
typedef unsigned short uint_fast16_t;
#elif INT_BITS == 16
typedef signed int int16_t;
typedef unsigned int uint16_t;
typedef signed int int_least16_t;
typedef unsigned int uint_least16_t;
typedef signed int int_fast16_t;
typedef unsigned int uint_fast16_t;
#endif /* #if SHORT_BITS == 16 */

/* 32-bit integer type */
#if LONG_BITS == 32
typedef signed long int32_t;
typedef unsigned long uint32_t;
typedef signed long int_least32_t;
typedef unsigned long uint_least32_t;
typedef signed long int_fast32_t;
typedef unsigned long uint_fast32_t;
#elif INT_BITS == 32
typedef signed int int32_t;
typedef unsigned int uint32_t;
typedef signed int int_least32_t;
typedef unsigned int uint_least32_t;
typedef signed int int_fast32_t;
typedef unsigned int uint_fast32_t;
#endif /* #if LONG_BITS == 32 */

/* 64-bit integer type */
#ifdef INT64_SUPPORT
#if LONG_BITS == 64
typedef signed long int64_t;
typedef unsigned long uint64_t;
typedef signed long int_least64_t;
typedef unsigned long uint_least64_t;
typedef signed long int_fast64_t;
typedef unsigned long uint_fast64_t;
#elif LONGLONG_BITS == 64
typedef signed long long int64_t;
typedef unsigned long long uint64_t;
typedef signed long long int_least64_t;
typedef unsigned long long uint_least64_t;
typedef signed long long int_fast64_t;
typedef unsigned long long uint_fast64_t;
#endif /* #if LONG_BITS == 64 */
#endif /* #ifdef INT64_SUPPORT */

#ifdef __WORDSIZE
#if __WORDSIZE == 32
typedef int32_t intptr_t;
typedef uint32_t uintptr_t;
#elif __WORDSIZE == 64
typedef int64_t intptr_t;
typedef uint64_t uintptr_t;
#else
typedef int16_t intptr_t;
typedef uint16_t uintptr_t;
#endif /* #if __WORDSIZE == 32 */
#else
#error "Need __WORDSIZE marco"
#endif /* #ifdef __WORDSIZE */

#define INT8_C(val) 	val
#define UINT8_C(val) 	val
#define INT16_C(val) 	val
#define UINT16_C(val) 	val
#define INT32_C(val) 	val##L
#define UINT32_C(val) 	val##UL

#ifdef INT64_SUPPORT
#define INT64_C(val) 	val##LL
#define UINT64_C(val) 	val##ULL
#define INTMAX_C 	INT64_C
#define UINTMAX_C 	UINT64_C
#else
#define INTMAX_C 	INT32_C
#define UINTMAX_C 	UINT32_C
#endif /* #ifdef INT64_SUPPORT */

#endif /* #ifdef __USE_STDINT_H__ */

#define INT_TYPE(bits) int##bits##_t
#define UINT_TYPE(bits) uint##bits##_t

/** @} */

#endif /* #ifndef __NV_INTEGER__ */

