/**
 * @defgroup 平台适配
 * @{
 * 		@defgroup 平台判定
 * 		@{
 * 		@}
 * 		@defgroup 数据类型
 * 		@{
 * 		@}
 * 		@defgroup 平台操作
 * 		@{
 * 		@}
 * 		@defgroup 网络适配
 * 		@{
 * 		@}
 * @}
 * @defgroup 结构与算法
 * @{
 * 		@defgroup 线性结构
 * 		@{
 * 			@defgroup 链式表
 * 			@{
 * 			@}
 * 			@defgroup 顺序表
 * 			@{
 * 			@}
 * 			@defgroup 字符串
 * 			@{
 * 			@}
 * 		@}
 * 		@defgroup 非线性结构
 * 		@{
 * 			@defgroup 二叉树
 * 			@{
 * 			@}
 * 			@defgroup 树
 * 			@{
 * 			@}
 * 			@defgroup 图
 * 			@{
 * 			@}
 * 		@}
 * 		@defgroup 哈希
 * 		@{
 * 			@defgroup 数值哈希
 * 			@{
 * 			@}
 * 			@defgroup 签名哈希
 * 			@{
 * 			@}
 * 			@defgroup 字符串哈希
 * 			@{
 * 			@}
 * 		@}
 * 		@defgroup 加密
 * 		@{
 * 			@defgroup 对称加密
 * 			@{
 * 			@}
 * 			@defgroup 非对称加密
 * 			@{
 * 			@}
 * 		@}
 * 		@defgroup 随机数
 * 		@{
 * 		@}
 * @}
 */